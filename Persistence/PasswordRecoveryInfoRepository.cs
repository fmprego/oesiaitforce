using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using ee.Utils;

namespace ee.Persistence
{
  public class PasswordRecoveryInfoRepository : IPasswordRecoveryInfoRepository
  {
    private readonly EEDbContext context;

    private readonly UserManager<Usuario> userManager;

    public PasswordRecoveryInfoRepository(EEDbContext context, UserManager<Usuario> userManager)
    {
      this.context = context;
      this.userManager = userManager;
    }
    
    public void Add(PasswordRecoveryInfo passwordRecoveryInfo){

       if (!passwordRecoveryInfo.Equals(null)){
            context.PasswordsRecoveryInfo.Add(passwordRecoveryInfo);
            context.SaveChanges();
        }
    }

    public void Delete(int id){
        var userAspInfo = context.PasswordsRecoveryInfo
           .Single(p => p.Id == id);
        if (userAspInfo!= null)
            context.Remove(userAspInfo);
    }

    public void UpdateRecoveryInfoById(int id, PasswordRecoveryInfo newInfo){
       
        var userAspInfo = context.PasswordsRecoveryInfo
           .Single(p => p.Id == id);

       if (userAspInfo != null){
            userAspInfo.codeUsed = newInfo.codeUsed;
            context.PasswordsRecoveryInfo.Update(userAspInfo);
            context.SaveChanges();
        }
    }

    public PasswordRecoveryInfo getPasswordInfoById(int id){
       
        return context.PasswordsRecoveryInfo
            .SingleOrDefault(p=>p.Id == id);
    }

    public async Task<Usuario> getAspNetUserByEmail(string email){
        
        var user = await context.Users.SingleOrDefaultAsync(p => p.Email.Equals(email)); 
        return user;
    }

    public PasswordRecoveryInfo getPwdRecoveryByIdAspNetUser(string IdAspNetUser){

        PasswordRecoveryInfo pri = context.PasswordsRecoveryInfo
            .OrderByDescending(p => p.Id)
            .Where(p => p.idAspNetUser.Equals(IdAspNetUser))
            .FirstOrDefault();
        return pri;
    }

     public async Task<PasswordRecoveryInfo> getPwdRecoveryByIdAspNetUserAndCode(string IdAspNetUser, string requestCode){
            
        PasswordRecoveryInfo pri = await context.PasswordsRecoveryInfo
            .SingleOrDefaultAsync(p => p.idAspNetUser == IdAspNetUser && p.requestCode.Trim().Equals(requestCode.ToString().Trim()));
        return pri;
    }

    public async void UpdateAspNetUser(string id, string newPassword){
       
        var user = context.Users
           .SingleOrDefault(p => p.Id.Equals(id));

       if (user != null){

          user.PasswordHash = userManager.PasswordHasher.HashPassword(user, newPassword);
          context.Users.Update(user);
          await context.SaveChangesAsync();

        }
    }

  }
}