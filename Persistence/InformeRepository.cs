using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class InformeRepository : IInformeRepository
  {
    private readonly EEDbContext context;

    public InformeRepository(EEDbContext context)
    {
      this.context = context;
    }

    public async Task<Informe> GetOne(int informeId)
    {
        return await context.Informes
        .Include(i => i.ResultadoCategorias).ThenInclude(rc => rc.Categoria)
        .Include(i => i.ResultadoPreguntas).ThenInclude(rp => rp.Pregunta)
        .Include(i => i.ResultadoPreguntas)
          .ThenInclude(rp => rp.ResultadoOpciones)
          .ThenInclude(ro => ro.Opcion)
        .SingleOrDefaultAsync(i => i.Id == informeId);
    }

    public async Task<Informe> GetByEncuestaAndNivel(int encuestaId, Nivel nivel)
    {
      return await context.Informes
        .Where(i => i.EncuestaId == encuestaId)
        .Where(i => i.Nivel1 == nivel.Nivel1)
        .Where(i => i.Nivel2 == nivel.Nivel2)
        .Where(i => i.Nivel3 == nivel.Nivel3)
        .Where(i => i.Nivel4 == nivel.Nivel4)
        .Include(i => i.ResultadoCategorias).ThenInclude(rc => rc.Categoria)
        .Include(i => i.ResultadoPreguntas).ThenInclude(rp => rp.Pregunta)
        .SingleOrDefaultAsync();
    }

    public async Task<Informe> InitInforme(int encuestaId, Nivel nivel)
    {
      var informe = new Informe {
        EncuestaId = encuestaId,
        Nivel1 = nivel.Nivel1,
        Nivel2 = nivel.Nivel2,
        Nivel3 = nivel.Nivel3,
        Nivel4 = nivel.Nivel4
      };
      await context.Informes.AddAsync(informe);
      await context.SaveChangesAsync();
      return informe;
    }

    public async Task<Informe> Save(Informe informe)
    {
      context.Informes.Update(informe);
      await context.SaveChangesAsync();
      return await this.GetOne(informe.Id);
    }
  }
}