using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Core.Exceptions;
using ee.Models;
using ee.Models.Query;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class IdiomaRepository : IIdiomaRepository
  {
    private readonly EEDbContext context;
    public IdiomaRepository(EEDbContext context)
    {
      this.context = context;
    }
    public async Task<IEnumerable<Idioma>> GetIdiomas(string localeIdioma)
    {
        IQueryable<Idioma> query;

        query = context.Idiomas.Include(i => i.Traducciones)
              .Select(i => new Idioma { Id = i.Id, Nombre = i.Traducciones.Where(t => t.Locale == localeIdioma).FirstOrDefault().Nombre, Locale = i.Locale})
              .AsQueryable();
        
        return await query.ToListAsync();
    }
  }
}