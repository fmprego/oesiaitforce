using ee.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
    public class EEDbContext : IdentityDbContext<Usuario>
    {
        public DbSet<Geografia> Geografias { get; set; }
        public DbSet<Unidad> Unidades { get; set; }
        public DbSet<Encuesta> Encuestas { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<EncuestaCategoria> EncuestasCategorias { get; set; }
        public DbSet<Pregunta> Preguntas { get; set; }
        public DbSet<EncuestaPregunta> EncuestasPreguntas { get; set; }
        public DbSet<Opcion> Opciones { get; set; }
        public DbSet<GrupoOpciones> GruposOpciones { get; set; }
        public DbSet<Estructura> Estructuras { get; set; }
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Lider> Lideres { get; set; }
        public DbSet<Receptor> Receptores { get; set; }
        public DbSet<Cuestionario> Cuestionarios { get; set; }
        public DbSet<Respuesta> Respuestas { get; set; }
        public DbSet<Informe> Informes { get; set; }
        public DbSet<ResultadoPregunta> ResultadoPreguntas { get; set; }
        public DbSet<ResultadoCategoria> ResultadoCategorias { get; set; }
        public DbSet<ResultadoOpcion> ResultadoOpciones { get; set; }        
        public DbSet<Idioma> Idiomas { get; set; }
        public DbSet<TipoEncuesta> TipoEncuesta { get; set; }
        public DbSet<PasswordRecoveryInfo> PasswordsRecoveryInfo { get; set; }
        //public DbSet<AspNetUser> AspNetUser { get; set; }
        public EEDbContext(DbContextOptions<EEDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<EncuestaPregunta>().HasKey(ep => new { ep.EncuestaId, ep.PreguntaId});
            builder.Entity<EncuestaPregunta>()
                .HasOne(ep => ep.Encuesta)
                .WithMany(e => e.Preguntas)
                .HasForeignKey(ep => ep.EncuestaId);
            builder.Entity<EncuestaPregunta>()
                .HasOne(ep => ep.Pregunta)
                .WithMany(p => p.Encuestas)
                .HasForeignKey(ep => ep.PreguntaId);

            builder.Entity<EncuestaCategoria>().HasKey(ec => new { ec.EncuestaId, ec.CategoriaId});
            builder.Entity<EncuestaCategoria>()
                .HasOne(ec => ec.Encuesta)
                .WithMany(e => e.Categorias)
                .HasForeignKey(ec => ec.EncuestaId);
            builder.Entity<EncuestaCategoria>()
                .HasOne(ec => ec.Categoria)
                .WithMany(c => c.Encuestas)
                .HasForeignKey(ec => ec.CategoriaId);
        }
    }
}