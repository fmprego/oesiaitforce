using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class PersonaRepository : IPersonaRepository
  {
    private readonly EEDbContext context;
    public PersonaRepository(EEDbContext context)
    {
      this.context = context;
    }
    
    public void Add(IEnumerable<Persona> personas)
    {
      // para los líderes, se comprueba si tienen usuario asignado ya.
      var lideres = personas.Where(p => p.EsLider);
      foreach (Lider lider in lideres)
      {
        var usuario = context.Users.SingleOrDefault(u => u.Email == lider.Email);
        if (usuario != null) lider.UsuarioId = usuario.Id;
      }
      context.AddRange(personas);
    }
    
    public void Delete(IEnumerable<Persona> personas)
    {
      context.RemoveRange(personas);
    }

    public void DeleteByEstructura(int estructuraId)
    {
      var _estructuraId = new SqlParameter("@EstructuraId", estructuraId);
      var sql = "DELETE FROM dbo.Personas WHERE EstructuraId = @EstructuraId";
      context.Database.ExecuteSqlCommand(sql, _estructuraId);
    }

    public async Task<IEnumerable<string>> GetNivel1(int estructuraId)
    {
      return await context.Personas
        .Where(p => p.EstructuraId == estructuraId)
        .Select(p => p.Nivel1)
        .Distinct()
        .ToListAsync();
    }

    public async Task<IEnumerable<Nivel>> GetNivel(int estructuraId, string nivel1 = null, string nivel2 = null, string nivel3 = null)
    {
      var query = context.Personas
        .Where(p => p.EstructuraId == estructuraId);
      
      if (nivel1 != null)
        query = query.Where(p => p.Nivel1.Equals(nivel1));

      if (nivel2 != null)
        query = query.Where(p => p.Nivel2.Equals(nivel2));

      if (nivel3 != null)
        query = query.Where(p => p.Nivel3.Equals(nivel3));

      return await query.Select(p => new Nivel { 
        Nivel1 = p.Nivel1, 
        Nivel2 = p.Nivel2, 
        Nivel3 = p.Nivel3, 
        Nivel4 = p.Nivel4})
        .Distinct()
        .ToListAsync();

    }

    public async Task UpdateUsuarioIdByEmail(string email, string id)
    {
      var lideres = await context.Lideres.Where(p => p.Email == email).ToListAsync();
      foreach (Lider lider in lideres)
      {
        lider.UsuarioId = id;
      }
      await context.SaveChangesAsync();
    }

  }
}