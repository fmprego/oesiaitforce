using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class OpcionRepository : IOpcionRepository
  {
    private readonly EEDbContext context;

    public OpcionRepository(EEDbContext context)
    {
      this.context = context;
    }

    public async Task<Opcion> Create(Opcion opcion, int preguntaId)
    {
      opcion.PreguntaId = preguntaId;
      context.Opciones.Add(opcion);
      await context.SaveChangesAsync();
      return opcion;
    }

    public void Delete(Opcion opcion)
    {
      context.Opciones.Remove(opcion);
    }

    public async Task DeleteGrupo(GrupoOpciones grupo)
    {
      var opciones = this.context.Opciones
        .Where(o => grupo.Opciones.Contains(o))
        .ToList();
      this.context.Opciones.RemoveRange(opciones);
      await this.context.SaveChangesAsync();
    }

    public async Task<IEnumerable<Opcion>> FindByPregunta(int preguntaId)
    {
      return await context.Opciones
        .Where(o => o.PreguntaId == preguntaId)
        .ToListAsync();
    }
  }
}