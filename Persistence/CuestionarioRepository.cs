using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using ee.Models.DTO;
using System.Collections.Generic;

namespace ee.Persistence
{
  public class CuestionarioRepository : ICuestionarioRepository
  {
    private readonly EEDbContext context;
    public CuestionarioRepository(EEDbContext context)
    {
      this.context = context;
    }

    public async Task<Cuestionario> Save(Cuestionario cuestionario)
    {
      context.Cuestionarios.Add(cuestionario);
      await context.SaveChangesAsync();
      return await context.Cuestionarios.SingleOrDefaultAsync(c => c.Id == cuestionario.Id);
    }

    public async Task<QueryResult<Cuestionario>> GetByEncuestaAndNivel(int encuestaId, Nivel nivel)
    {

      var result = new QueryResult<Cuestionario>();
      IQueryable<Cuestionario> query;
      query = context.Cuestionarios.Where(c => c.EncuestaId == encuestaId);
      if(nivel.Nivel1!=null){
        query.Where(c => c.Nivel1 == nivel.Nivel1);
      }
        if(nivel.Nivel2!=null){
        query.Where(c => c.Nivel2 == nivel.Nivel2);
      }
        if(nivel.Nivel3!=null){
        query.Where(c => c.Nivel3 == nivel.Nivel3);
      }
        if(nivel.Nivel4!=null){
        query.Where(c => c.Nivel4 == nivel.Nivel4);
      }
      query.Include(c => c.Respuestas).ThenInclude(r => r.Opcion);
      

      result.Items = await query.ToListAsync();
      result.TotalItems = result.Items.Count();
      return result;
     /* return await context.Cuestionarios
        .Where(c => c.EncuestaId == encuestaId).
        .Where(c => c.Nivel1 == nivel.Nivel1)
        .Where(c => c.Nivel2 == nivel.Nivel2)
        .Where(c => c.Nivel3 == nivel.Nivel3)
        .Where(c => c.Nivel4 == nivel.Nivel4)
        .Include(c => c.Respuestas).ThenInclude(r => r.Opcion)
        .ToListAsync();*/
    }    
 
   public async Task<IEnumerable<Cuestionario>> GetByEncuestaAndNivel2(int encuestaId, Nivel nivel, bool IncludeOpcion = false)
    {
      
      if (IncludeOpcion)
      {

        var consulta = context.Cuestionarios
          .Where(c => c.EncuestaId == encuestaId)
          .Where(c => nivel.Nivel1 == null || c.Nivel1 == nivel.Nivel1)
          .Where(c => nivel.Nivel2 == null || c.Nivel2 == nivel.Nivel2)
          .Where(c => nivel.Nivel3 == null || c.Nivel3 == nivel.Nivel3)
          .Where(c => nivel.Nivel4 == null || c.Nivel4 == nivel.Nivel4)
          .Include(cu => cu.Respuestas).ThenInclude(r => r.Opcion)
          .Select(c => new Cuestionario { 
              Id = c.Id, 
              EncuestaId = c.EncuestaId,
              Nivel1 = c.Nivel1,
              Nivel2 = c.Nivel2,
              Nivel3 = c.Nivel3,
              Nivel4 = c.Nivel4,
              Hora = c.Hora,
              Respuestas = c.Respuestas
                .Select(r => new Respuesta 
                {
                  Id = r.Id,
                  PreguntaId = r.PreguntaId,
                  CuestionarioId = r.CuestionarioId,
                  TipoPregunta = r.TipoPregunta,
                  Escala = r.Escala,
                  Texto = r.TipoPregunta == Models.Enum.TipoPregunta.Seleccion?r.Opcion.Texto:r.Texto,
                  OpcionId = r.OpcionId
                })               
                .ToList()
          });

        return await consulta.ToListAsync();
      }
      else
      {      
        return await context.Cuestionarios
          .Where(c => c.EncuestaId == encuestaId)
          .Where(c => nivel.Nivel1 == null || c.Nivel1 == nivel.Nivel1)
          .Where(c => nivel.Nivel2 == null || c.Nivel2 == nivel.Nivel2)
          .Where(c => nivel.Nivel3 == null || c.Nivel3 == nivel.Nivel3)
          .Where(c => nivel.Nivel4 == null || c.Nivel4 == nivel.Nivel4)
          .Include(cu => cu.Respuestas).ToListAsync();
      }
    }


  }
}