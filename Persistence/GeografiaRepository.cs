using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Core.Exceptions;
using ee.Models;
using ee.Models.Query;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class GeografiaRepository : IGeografiaRepository
  {
    private readonly EEDbContext context;
    public GeografiaRepository(EEDbContext context)
    {
      this.context = context;
    }
    public async Task<Geografia> GetGeografia(int id)
    {
      var result = await context.Geografias.Include(g => g.Usuarios).SingleOrDefaultAsync(g => g.Id == id);
      if (result == null) throw new GeografiaNotFoundException();
      return result;
    }

    public async Task<IEnumerable<Geografia>> GetGeografias(GeografiaQuery queryObj)
    {
      var query = context.Geografias.Include(g => g.Usuarios).AsQueryable();
      return await query.ToListAsync();
    }

    public async Task<Geografia> CreateGeografia(Geografia geografia)
    {
      context.Geografias.Add(geografia); 
      await context.SaveChangesAsync();
      return await this.GetGeografia(geografia.Id);
    }

    public void DeleteGeografia(Geografia geografia)
    {
      context.Geografias.Remove(geografia);
    }

    public async Task<Geografia> UpdateGeografia(int id, Geografia changed)
    {
      var geografia = await this.GetGeografia(id);
      geografia.Nombre = changed.Nombre;
      context.Geografias.Update(geografia);
      await context.SaveChangesAsync();
      return geografia;
    }

  }
}