using System.Threading.Tasks;
using ee.Core;

namespace ee.Persistence
{
  public class UnitOfWork : IUnitOfWork
  {
    private readonly EEDbContext context;
    public UnitOfWork(EEDbContext context)
    {
      this.context = context;
    }

    public async Task CompleteAsync()
    {
      await context.SaveChangesAsync();
    }
  }
}