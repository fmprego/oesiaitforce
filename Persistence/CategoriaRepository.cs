using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Core.Exceptions;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
    public class CategoriaRepository : ICategoriaRepository
    {
        private readonly EEDbContext context;
        public CategoriaRepository(EEDbContext context)
        {
            this.context = context;
        }

        public async Task<Categoria> GetCategoria(int id, bool includeRelated = true)
        {
            Categoria result = null;
            if (!includeRelated)
            {
                result = await context.Categorias.SingleOrDefaultAsync(g => g.Id == id);
            }
            else
            {
                result = await context.Categorias
                  .Include(c => c.Preguntas).ThenInclude(p => p.Usuario)
                  .SingleOrDefaultAsync(g => g.Id == id);
            }
            return result;
        }

        public async Task<QueryResult<Categoria>> GetCategorias()
        {
            var result = new QueryResult<Categoria>();
            var query = context.Categorias
              .Include(c => c.Preguntas).ThenInclude(p => p.Usuario)
              .AsQueryable();
            result.Items = await query.ToListAsync();
            result.TotalItems = await query.CountAsync();
            return result;
        }

        public async Task<Categoria> CreateCategoria(Categoria categoria)
        {
            context.Categorias.Add(categoria);
            await context.SaveChangesAsync();
            return await this.GetCategoria(categoria.Id);
        }

        public void DeleteCategoria(Categoria categoria)
        {
            context.Categorias.Remove(categoria);
        }

        public async Task<Categoria> UpdateCategoria(int id, Categoria categoria)
        {
            var cat = await this.GetCategoria(id);
            cat.Nombre = categoria.Nombre;
            context.Categorias.Update(cat);
            await context.SaveChangesAsync();
            return cat;
        }
    }
}