using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class RespuestaRepository : IRespuestaRepository
  {
    private readonly EEDbContext context;
    public RespuestaRepository(EEDbContext context)
    {
      this.context = context;
    }

    public async Task<IEnumerable<Respuesta>> GetByPreguntaIdAndCuestionarioIdIn(int preguntaId, IEnumerable<int> cuestionarioIds)
    {
      return await context.Respuestas
        .Where(r => r.PreguntaId == preguntaId)
        .Where(r => cuestionarioIds.Contains(r.CuestionarioId))
        .OrderBy(r => r.CuestionarioId)
        .ToListAsync();
    }

        public async Task<IEnumerable<Respuesta>> GetRespuestas(IEnumerable<int> cuestionarioIds)
        {
           return await context.Respuestas
          .Where(r => cuestionarioIds.Contains(r.CuestionarioId))
          .OrderBy(r => r.CuestionarioId)
          .ToListAsync();
        }
    }
}