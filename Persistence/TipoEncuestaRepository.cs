using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Core.Exceptions;
using ee.Models;
using ee.Models.Query;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class TipoEncuestaRepository : ITipoEncuestaRepository
  {
    private readonly EEDbContext context;
    public TipoEncuestaRepository(EEDbContext context)
    {
      this.context = context;
    }
    public async Task<TipoEncuesta> GetTipoEncuesta(int id)
    {
      var result = await context.TipoEncuesta.SingleOrDefaultAsync(g => g.Id == id);
      if (result == null) throw new GeografiaNotFoundException();
      return result;
    }

    public async Task<IEnumerable<TipoEncuesta>> GetTiposEncuesta(TipoEncuestaQuery queryObj)
    {
      var query = context.TipoEncuesta.AsQueryable();
      return await query.ToListAsync();
    }

    public async Task<IEnumerable<TipoEncuesta>> GetTiposEncuestaAll(TipoEncuestaQuery queryObj)
    {
      var query = context.TipoEncuesta.Include(t=>t.Encuestas).AsQueryable();
      return await query.ToListAsync();
    }

    public async Task<TipoEncuesta> CreateTipoEncuesta(TipoEncuesta tipo)
    {
      context.TipoEncuesta.Add(tipo); 
      await context.SaveChangesAsync();
      return await this.GetTipoEncuesta(tipo.Id);
    }

    public void DeleteTipoEncuesta(TipoEncuesta tipo)
    {
      context.TipoEncuesta.Remove(tipo);
    }

    public async Task<TipoEncuesta> UpdateTipoEncuesta(int id, TipoEncuesta changed)
    {
      var tipo = await this.GetTipoEncuesta(id);
      tipo.Nombre = changed.Nombre;
      context.TipoEncuesta.Update(tipo);
      await context.SaveChangesAsync();
      return tipo;
    }

  }
}