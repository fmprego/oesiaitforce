using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class EstructuraRepository : IEstructuraRepository
  {
    private readonly EEDbContext context;
    public EstructuraRepository(EEDbContext context)
    {
      this.context = context;
    }

    public async Task<Estructura> GetOne(int id)
    {
      return await context.Estructuras
        .Include(e => e.Encuesta)
        .Include(e => e.Personas)
        .SingleOrDefaultAsync(e => e.Id == id);
    }

    public async Task<Estructura> GetByEncuesta(Encuesta encuesta)
    {
      return await context.Estructuras
        .SingleOrDefaultAsync(e => e.EncuestaId == encuesta.Id);
    }

    public async Task<Estructura> Create(Estructura estructura)
    {
      context.Estructuras.Add(estructura);
      await context.SaveChangesAsync();
      return await this.GetOne(estructura.Id);
    }

    public async Task<QueryResult<Lider>> GetLideres(int id)
    {
      var query = context.Lideres
        .Include(l => l.Usuario)
        .Where(l => l.EstructuraId == id);
      return new QueryResult<Lider> {
        Items = await query.ToListAsync(),
        TotalItems = await query.CountAsync(),
      };
    }

    public async Task<int> GetTotalLideres(int id)
    {
      var query = context.Lideres
        .Include(l => l.Usuario)
        .Where(l => l.EstructuraId == id);
      return await query.CountAsync();
    }

    public async Task<QueryResult<Receptor>> GetReceptores(int id)
    {
      var query = context.Receptores
        .Where(r => r.EstructuraId == id);
      return new QueryResult<Receptor> {
        Items = await query.ToListAsync(),
        TotalItems = await query.CountAsync(),
      };
    }
    public async Task<int> GetTotalReceptores(int id)
    {
      var query = context.Receptores
        .Where(r => r.EstructuraId == id);
      return await query.CountAsync();
    }
    public async Task<IEnumerable<Lider>> GetLideresSinUsuario(int id)
    {
      return await context.Lideres
        .Where(l => l.EstructuraId == id)
        .Where(l => l.UsuarioId == null)
        .ToListAsync();
    }

    public async Task<Persona> GetLider(Estructura estructura, string liderEmail)
    {
      return await context.Personas
        .Where(p => p.EstructuraId == estructura.Id)
        .Where(p => p.Email == liderEmail)
        .Where(p => p.EsLider)
        .SingleOrDefaultAsync();
    }
  }
}