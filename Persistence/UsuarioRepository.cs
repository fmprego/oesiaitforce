using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Core.Exceptions;
using ee.dbo.Core.Exceptions;
using ee.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ee.Persistence
{
  public class UsuarioRepository : IUsuarioRepository
  {
    private readonly UserManager<Usuario> userManager;
    private readonly IMapper mapper;
    private readonly EEDbContext context;
    private readonly ILogger logger;

    public UsuarioRepository(UserManager<Usuario> userManager, 
        EEDbContext context, IMapper mapper, ILogger<UsuarioRepository> logger)
    {
      this.logger = logger;
      this.context = context;
      this.mapper = mapper;
      this.userManager = userManager;
    }

    public async Task<IEnumerable<Usuario>> GetAdmins(int? geografiaId)
    {
      var adminClaim = new Claim("Role", "Admin");
      var users = await userManager.GetUsersForClaimAsync(adminClaim);
      for (int i = users.Count -1; i >= 0; i--)
      {
        if (geografiaId != null && users[i].GeografiaId != geografiaId)
        {
          users.RemoveAt(i);
          continue;
        }
        var geo = await context.Geografias.FindAsync(users[i].GeografiaId);
        if (geo != null) users[i].Geografia = geo;
        var claims = await userManager.GetClaimsAsync(users[i]);
        foreach (Claim claim in claims)
        {
          if (claim.Value == "Master")
            users.RemoveAt(i);
        }
      }
      return users;
    }

    public async Task<Usuario> GetAdmin(string id)
    {
      var claim = new Claim("Role", "Admin");
      var users = await userManager.GetUsersForClaimAsync(claim);
      foreach (Usuario user in users)
      {
        if (user.Id.Equals(id))
        {
          var geo = await context.Geografias.FindAsync(user.GeografiaId);
          if (geo != null) user.Geografia = geo;
          return user;
        }
      }
      throw new UsuarioNotFoundException();
    }

    public async Task<Usuario> CreateAdmin(Usuario usuario, string password)
    {
      var result = await userManager.FindByEmailAsync(usuario.Email);
      if (result != null)
      {
          var claims = await userManager.GetClaimsAsync(result);
          if (!claims.Any(c => c.Value == "Admin"))
          { 
            await userManager.AddClaimAsync(result, new Claim("Role", "Admin"));
          }
          if (!claims.Any(c => c.Value == "Lider"))
          {          
            await userManager.AddClaimAsync(result, new Claim("Role", "Lider"));
          }
          if (!claims.Any(c => c.Value == result.Id))
          {           
            await userManager.AddClaimAsync(result, new Claim(ClaimTypes.NameIdentifier, result.Id));
          }
          return result;
      }
      else
      {
        result = await userManager.FindByNameAsync(usuario.UserName);
        if (result != null)
        {
          var claims = await userManager.GetClaimsAsync(result);
          if (!claims.Any(c => c.Value == "Admin"))
          { 
            await userManager.AddClaimAsync(result, new Claim("Role", "Admin"));
          }
          if (!claims.Any(c => c.Value == "Lider"))
          {          
            await userManager.AddClaimAsync(result, new Claim("Role", "Lider"));
          }
          if (!claims.Any(c => c.Value == result.Id))
          {           
            await userManager.AddClaimAsync(result, new Claim(ClaimTypes.NameIdentifier, result.Id));
          }
          return result;
        }
        else
        {
            usuario.LastUpdate = DateTime.Now;
            usuario.UserName = usuario.Email;
            var createdAdmin = await userManager.CreateAsync(usuario, password);

            if (createdAdmin.Succeeded)
            {
            await userManager.AddClaimAsync(usuario, new Claim("Role", "Admin"));
            await userManager.AddClaimAsync(usuario, new Claim("Role", "Lider"));
            await userManager.AddClaimAsync(usuario, new Claim(ClaimTypes.NameIdentifier, usuario.Id));
            return usuario;
            }
            throw new ArgumentException("Los datos no son válidos.");
        }
      }
    }

    public async Task DeleteAdmin(Usuario admin)
    {
      var claims = await userManager.GetClaimsAsync(admin);
      var claimAdmin = claims.Where(c => c.Value == "Admin").FirstOrDefault();

      if (claimAdmin != null)
      { 
        await userManager.RemoveClaimAsync(admin,claimAdmin);
      }   

      if (!context.Personas.Any(p => p.Email.ToUpper() == admin.Email.ToUpper()))   
      {
        if (context.UserClaims.Any(c => c.ClaimValue == "Admin" && c.UserId != admin.Id))
        {
          await userManager.DeleteAsync(admin);
        }
        else
        {
          throw new OneAdminNotFoundException();
        }
      }
    }

    public async Task<Usuario> UpdateAdmin(Usuario admin, Usuario changed)
    {
        admin.Nombre = changed.Nombre;
        admin.Apellidos = changed.Apellidos;
        admin.GeografiaId = changed.GeografiaId;
        admin.Email = changed.Email;
        admin.UserName = changed.Email;
        admin.LastUpdate = DateTime.Now;
        var updatedAdmin = await userManager.UpdateAsync(admin);
        return admin;
    }

    public async Task<Usuario> GetLider(string id)
    {
      var claim = new Claim("Role", "Lider");
      var users = await userManager.GetUsersForClaimAsync(claim);
      return users.SingleOrDefault(u => u.Id == id);
    }

    public async Task<IEnumerable<Usuario>> CreateLideres(IEnumerable<Usuario> lideres, string pass, int geografiaId)
    {
      foreach (Usuario lider in lideres)
      {
        var result = await userManager.FindByEmailAsync(lider.Email);
        if (result != null)
          throw new ArgumentException("El usuario " + lider.Email + " ya existe.");

        lider.LastUpdate = DateTime.Now;
        lider.GeografiaId = geografiaId;
        //lider.UserName = lider.Email.Split("@")[0];
        lider.UserName = lider.Email;
        var createdLider = await userManager.CreateAsync(lider, pass);
        if (createdLider.Succeeded)
        {
          await userManager.AddClaimAsync(lider, new Claim("Role", "Lider"));
          await userManager.AddClaimAsync(lider, new Claim(ClaimTypes.NameIdentifier, lider.Id));
        }
        else
        {
          string message = "Los datos no son válidos. Lider:";
          throw new ArgumentException(String.Join(message, lider.ToString()));
        }
      }
      return lideres;
    }
  }
}