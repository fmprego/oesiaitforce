using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;
using System;
using ee.Models.Enum;

namespace ee.Persistence
{
  public class PreguntaRepository : IPreguntaRepository
  {
    private readonly EEDbContext context;

    public PreguntaRepository(EEDbContext context)
    {
      this.context = context;
    }
    public async Task<QueryResult<Pregunta>> GetAll(int[] categorias)
    {
      if (categorias != null)
      {
        var query = context.Preguntas
          .Include(p => p.Categoria)
          .Include(p => p.Usuario)
          .Where(p => categorias.Contains(p.CategoriaId))
          .AsQueryable();
        return new QueryResult<Pregunta> {
          Items = await query.ToListAsync(),
          TotalItems = await query.CountAsync()
        };
      }
      else
      {
        var query = context.Preguntas
          .Include(p => p.Categoria)
          .Include(p => p.Usuario)
          .AsQueryable();
        
        return new QueryResult<Pregunta> {
          Items = await query.ToListAsync(),
          TotalItems = await query.CountAsync()
        };
      }
    }

    public async Task<QueryResult<Pregunta>> GetByEncuesta(int encuestaId)
    {
      var query = context.EncuestasPreguntas
        .Include(ep => ep.Pregunta).ThenInclude(p => p.Opciones)
        .Where(p => p.EncuestaId == encuestaId)
        .Select(ep => new Pregunta {
          Id = ep.Pregunta.Id,
          Texto = ep.Pregunta.Texto,
          Descripcion = ep.Pregunta.Descripcion,
          Categoria = ep.Pregunta.Categoria,
          Usuario = ep.Pregunta.Usuario,
          Escala = ep.Pregunta.Escala,
          TipoPregunta = ep.Pregunta.TipoPregunta,
          Opciones = ep.Pregunta.Opciones
        })
        .AsQueryable();
    
      return new QueryResult<Pregunta> {
        Items = await query.ToListAsync(),
        TotalItems = await query.CountAsync()
      };
    }

    public async Task<Pregunta> GetOne(int id)
    {
      return await context.Preguntas
        .Include(p => p.Categoria)
        .Include(p => p.Usuario)
        .Include(p => p.Opciones)
        .SingleOrDefaultAsync(p => p.Id == id);
    }

    public async Task<IEnumerable<Pregunta>> FindByCategoria(int categoriaId)
    {
      return await context.Preguntas
        .Include(p => p.Categoria)
        .Where(p => p.CategoriaId == categoriaId)
        .ToListAsync();
    }

    public async Task<Pregunta> Create(Pregunta pregunta, string userId)
    {
      pregunta.UsuarioId = userId;
      context.Preguntas.Add(pregunta);
      await context.SaveChangesAsync();
      return await this.GetOne(pregunta.Id);
    }

    public async Task<Pregunta> Update(int id, Pregunta changed, string userId)
    {
      var pregunta = await this.GetOne(id);
      pregunta.Texto = changed.Texto;
      pregunta.Descripcion = changed.Descripcion;
      pregunta.CategoriaId = changed.CategoriaId;
      pregunta.TipoPregunta = changed.TipoPregunta;
      pregunta.Escala = changed.Escala;
      context.Preguntas.Update(pregunta);
      await context.SaveChangesAsync();
      return pregunta;
    }

    public async Task SetEditableByEncuesta(bool editable, int encuestaId)
    {

      if(editable){
      var preguntas = await context.EncuestasPreguntas
        .Where(ep => ep.EncuestaId == encuestaId && 
          !context.EncuestasPreguntas.Any(ep2=>
              ep2.PreguntaId == ep.PreguntaId && ep2.Encuesta.Estado >= EstadoEncuesta.Activa  && ep2.EncuestaId != encuestaId))
        .Select(ep => ep.Pregunta)
        .ToListAsync(); 
        preguntas.ForEach(p => p.Editable = editable);
      }else {
         var preguntas = await context.EncuestasPreguntas
        .Where(ep => ep.EncuestaId == encuestaId)
        .Select(ep => ep.Pregunta)
        .ToListAsync(); 
        preguntas.ForEach(p => p.Editable = editable);
      }

      await context.SaveChangesAsync();
    }    

    public void Delete(Pregunta pregunta)
    {
      context.Preguntas.Remove(pregunta);
    }

    public async Task<IEnumerable<Encuesta>> getEncuestasByPregunta(int id)
    {
      return await context.Encuestas        
        .Where(p => p.Preguntas.Any(pe => pe.PreguntaId  == id))
        .ToListAsync();
    }
    }
}