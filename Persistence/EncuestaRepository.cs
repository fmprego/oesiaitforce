using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using ee.Models.Enum;

namespace ee.Persistence
{
    public class EncuestaRepository : IEncuestaRepository
    {
        private readonly EEDbContext context;
        public EncuestaRepository(EEDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Idioma>> GetIdiomas(string localeIdioma)
        {
            IQueryable<Idioma> query;

            query = context.Idiomas.Include(i => i.Traducciones)
                  .Select(i => new Idioma { Id = i.Id, Nombre = i.Traducciones.Where(t => t.Locale == localeIdioma).FirstOrDefault().Nombre, Locale = i.Locale})
                  .AsQueryable();
            
            return await query.ToListAsync();
        }

        public async Task<QueryResult<Encuesta>> GetAll(ClaimsPrincipal usuario)
        {
            var result = new QueryResult<Encuesta>();
            IQueryable<Encuesta> query;

            query = context.Encuestas
                  .Include(e => e.Idioma)
                  .Include(e => e.TipoEncuesta)
                  .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
                  .OrderByDescending(e => e.FechaCreacion)
                  .AsQueryable();
            

            result.Items = await query.ToListAsync();
            result.TotalItems = result.Items.Count();

            return result;
        }

        public async Task<QueryResult<Encuesta>> GetCerradas(ClaimsPrincipal usuario)
        {
            string name = usuario.Identity.Name;
            var result = new QueryResult<Encuesta>();
            IQueryable<Encuesta> query;
             query = context.Encuestas
            .Where(e => e.Estado.Equals(EstadoEncuesta.Cerrada))
            .Include(e => e.Idioma)
            .Include(e => e.TipoEncuesta)
            .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
          //  .Where(e => e.Estructura == context.Estructuras.Where(es => context.Personas.Any(p => p.Email.Equals(name))))
            .AsQueryable();
            
            result.Items = await query.ToListAsync();
            result.TotalItems = result.Items.Count();

            return result;
        }

       public async Task<QueryResult<Encuesta>> getEncuestasParaInformes(ClaimsPrincipal usuario,Usuario user){

            var result = new QueryResult<Encuesta>();
            IQueryable<Encuesta> query;

            if (usuario.HasClaim("Role", "Master"))
            {
                query = context.Encuestas
                  //.Where(e => (e.Estado != EstadoEncuesta.Cerrada))
                  .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
                  .Include(e => e.TipoEncuesta)
                  .OrderByDescending(e => e.FechaCreacion)
                  .AsQueryable();
            }
            else if(usuario.HasClaim("Role","Admin"))
            {
                query = context.Encuestas
                  //.Where(e => (e.Estado != EstadoEncuesta.Cerrada))
                  .Where(e => e.GeografiaId == user.GeografiaId)
                  .Include(e => e.Idioma)
                  .Include(e => e.TipoEncuesta)
                  .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
                  
                 // .Include(e => e.Estructura).ThenInclude(es => es.Personas).Where
                  .AsQueryable();
            }else {
                 query = context.Encuestas
                  .Include(e => e.Idioma)
                  .Include(e => e.TipoEncuesta)
                  .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
                  //.Where(e => e.Estado.Equals(EstadoEncuesta.Cerrada))
                  .Include(e => e.Estructura).ThenInclude(es => es.Personas)
                  .Where(e => e.Estructura.Personas.Any(p => p.Email == user.Email))
                  //.Where(e => e.Estructura.Any()context.Estructuras.Where(es => context.Lideres.Any(p => p.UsuarioId == user.Id)).Select(es => es.Id));
                  .AsQueryable();
            }

            result.Items = await query.ToListAsync();
            result.TotalItems = result.Items.Count();

            return result;

       }
        public async Task<QueryResult<Encuesta>> GetPending()
        {
            var result = new QueryResult<Encuesta>();
            IQueryable<Encuesta> query;

            query = context.Encuestas
              .Include(e => e.Idioma)
              .Include(e => e.TipoEncuesta)
              .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
              .Where(e => e.Estado == EstadoEncuesta.Pendiente)
              .AsQueryable();

            result.Items = await query.ToListAsync();
            result.TotalItems = result.Items.Count();

            return result;
        }

        public async Task<Encuesta> GetOne(int id, bool includeRelated = false)
        {
            if (includeRelated)
            {
                return await context.Encuestas
                  .Include(e => e.Idioma)
                  .Include(e => e.TipoEncuesta)
                  .Include(e => e.Informes)
                  .Include(e => e.Preguntas).ThenInclude(ep => ep.Pregunta).ThenInclude(p => p.Opciones)
                  .Include(e => e.Categorias).ThenInclude(ec => ec.Categoria)
                  .Include(e => e.Estructura)
                  .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
                  .SingleOrDefaultAsync(e => e.Id == id);
            }
            return await context.Encuestas
              .Include(e => e.Idioma)
              .Include(e => e.TipoEncuesta)
              .Include(e => e.Categorias).ThenInclude(ec => ec.Categoria)
              .Include(e => e.Estructura)
              .Include(e => e.Usuario).ThenInclude(u => u.Geografia)
              .SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Encuesta> Create(Encuesta encuesta, string userId)
        {
            encuesta.UsuarioId = userId;
            context.Encuestas.Add(encuesta);
            await context.SaveChangesAsync();
            return await this.GetOne(encuesta.Id);
        }

        public async Task<Encuesta> Update(int id, Encuesta changed)
        {
            var encuesta = await this.GetOne(id);
            encuesta.Titulo = changed.Titulo;
            encuesta.IdiomaId = changed.IdiomaId;
            encuesta.TipoEncuestaId = changed.TipoEncuestaId;
            encuesta.Descripcion = changed.Descripcion;
            encuesta.FechaInicio = changed.FechaInicio;
            encuesta.FechaCierre = changed.FechaCierre;
            encuesta.Estado = changed.Estado;
            context.Encuestas.Update(encuesta);
            await context.SaveChangesAsync();
            return encuesta;
        }

        public void Delete(Encuesta encuesta)
        {
            context.Encuestas.Remove(encuesta);
        }

        public async Task<Encuesta> UpdateCategorias(Encuesta encuesta, IEnumerable<Categoria> categorias)
        {
            context.EncuestasCategorias
              .RemoveRange(context.EncuestasCategorias
                .Where(ec => ec.EncuestaId == encuesta.Id));
            await context.SaveChangesAsync();

            foreach (Categoria categoria in categorias)
            {
                context.Add(new EncuestaCategoria { EncuestaId = encuesta.Id, CategoriaId = categoria.Id });
            }
            await context.SaveChangesAsync();
            return await this.GetOne(encuesta.Id);
        }
        
        public async Task<Encuesta> UpdatePreguntas(Encuesta encuesta, IEnumerable<Pregunta> preguntas)
        {
            context.EncuestasPreguntas
              .RemoveRange(context.EncuestasPreguntas
                .Where(ec => ec.EncuestaId == encuesta.Id));
            await context.SaveChangesAsync();

            foreach (Pregunta pregunta in preguntas)
            {
                context.Add(new EncuestaPregunta { EncuestaId = encuesta.Id, PreguntaId = pregunta.Id });
            }
            await context.SaveChangesAsync();
            return await this.GetOne(encuesta.Id);
        }

        public async Task<IEnumerable<EncuestaPregunta>> GetEncuestaPreguntas(Encuesta encuesta)
        {
            return await context.EncuestasPreguntas
              .Where(ep => ep.EncuestaId == encuesta.Id)
              .ToListAsync();
        }

        public async Task<IEnumerable<EncuestaCategoria>> GetEncuestaCategorias(Encuesta encuesta)
        {
            return await context.EncuestasCategorias
              .Where(ep => ep.EncuestaId == encuesta.Id)
              .ToListAsync();
        }

        public async Task UpdateEncuestaPreguntas(Encuesta encuesta, IEnumerable<EncuestaPregunta> eps)
        {
            context.EncuestasPreguntas
              .RemoveRange(context.EncuestasPreguntas
                .Where(ec => ec.EncuestaId == encuesta.Id));
            await context.SaveChangesAsync();

            context.AddRange(eps);
            await context.SaveChangesAsync();
        }

        public async Task UpdateEncuestaCategorias(Encuesta encuesta, IEnumerable<EncuestaCategoria> eps)
        {
            context.EncuestasCategorias
              .RemoveRange(context.EncuestasCategorias
                .Where(ec => ec.EncuestaId == encuesta.Id));
            await context.SaveChangesAsync();

            context.AddRange(eps);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Encuesta>> getEncuestasRelacionadas(int encuestaId)
        {
            return await context.Encuestas.Where(e => e.OrigenId == encuestaId).Where(e => e.Estado != EstadoEncuesta.Historico).ToListAsync();
        }

        public async Task<int> GetNumeroEncuestasPorTipo(int tipoEncuestaId)
        {
            return await context.Encuestas.Where(e => e.TipoEncuestaId == (int?)tipoEncuestaId).CountAsync();
        }

    }
}