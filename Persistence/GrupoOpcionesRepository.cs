using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
    public class GrupoOpcionesRepository : IGrupoOpcionesRepository
    {
        private readonly EEDbContext context;

        public GrupoOpcionesRepository(EEDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<GrupoOpciones>> GetAll()
        {
            return await context.GruposOpciones
              .Include(g => g.Opciones)
              .ToListAsync();
        }

        public async Task<GrupoOpciones> GetOne(int id)
        {
            return await this.context.GruposOpciones
              .Include(go => go.Opciones)
              .SingleOrDefaultAsync(go => go.Id == id);
        }

        public async Task<GrupoOpciones> Save(GrupoOpciones grupo)
        {
            // borramos ids de opciones en caso de que tengan
            var opciones = grupo.Opciones.Select(o => new Opcion
            {
                Texto = o.Texto,
                Peso = o.Peso,
                Pregunta = o.Pregunta,
                ImagenCab = o.ImagenCab,
                ImagenContain = o.ImagenContain
            }).ToList();
            grupo.Opciones = opciones;

            await context.GruposOpciones.AddAsync(grupo);
            await context.SaveChangesAsync();
            grupo = await context.GruposOpciones.SingleOrDefaultAsync(g => g.Id == grupo.Id);
            return grupo;
        }

        public async Task<GrupoOpciones> Delete(GrupoOpciones grupo)
        {
            this.context.GruposOpciones.Remove(grupo);
            await this.context.SaveChangesAsync();
            return await this.GetOne(grupo.Id);
        }
    }
}