using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Extensions;
using ee.Models;
using ee.Models.Query;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public class UnidadRepository : IUnidadRepository
  {
    private readonly EEDbContext context;
    public UnidadRepository(EEDbContext context)
    {
      this.context = context;
    }
    public async Task<Unidad> GetUnidad(int id)
    {
      return await context.Unidades.FindAsync(id);
    }

    public async Task<QueryResult<Unidad>> GetUnidades(UnidadQuery queryObj)
    {
      var result = new QueryResult<Unidad>();
      var query = context.Unidades.AsQueryable();
      // query = query.ApplyFiltering(queryObj);
      // query = query.ApplyPagging(queryObj);
      result.Items = await query.ToListAsync();
      result.TotalItems = await context.Unidades.CountAsync();
      return result;
    }
  }
}