using System.Text;  
using System.Security.Cryptography;  

namespace ee.Utils  
{  
  public static class Encryptor  
  {  
    public static string MD5Utils(string text)  
    {  
      MD5 md5 = new MD5CryptoServiceProvider();  

      //compute hash from the bytes of text  
      md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));  
  
      //get hash result after compute it  
      byte[] result = md5.Hash;  

      StringBuilder strBuilder = new StringBuilder();  
      for (int i = 0; i < result.Length; i++)  
      {  
        strBuilder.Append(result[i].ToString("x2"));  
      }  

      return strBuilder.ToString();  
    }  
  }  
}