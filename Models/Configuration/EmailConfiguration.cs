namespace ee.Models.Configuration
{
    public class EmailConfiguration :IEmailConfiguration
    {
        public string SmtpServer { get; set; }
	    public int SmtpPort  { get; set; }
        public string DefaultName {get; set; }
	    public string DefaultMail { get; set; }
        public string Domain { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int StartTls { get; set; }
    }
}