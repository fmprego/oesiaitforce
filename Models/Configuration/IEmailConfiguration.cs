namespace ee.Models.Configuration
{
    public interface IEmailConfiguration
    {
	    string SmtpServer { get; }
	    int SmtpPort { get; }
        string DefaultName { get; }
	    string DefaultMail { get;}
        string Domain { get; }
        string User { get; }
        string Password { get; }
        int StartTls { get; }
    }
}