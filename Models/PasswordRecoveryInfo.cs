using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;

namespace ee.Models
{
    [Table("PasswordRecoveryInfo")]
    public class PasswordRecoveryInfo
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string idAspNetUser { get; set; }
        [Required]
        public DateTime requestDate  { get; set; }
        [Required]
        public int requestExpirationTime { get; set; }
        [Required]
        public string requestCode { get; set; }
        [Required]
        public bool codeUsed { get; set; }
    }
}