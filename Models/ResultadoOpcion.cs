using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("ResultadoOpciones")]
    public class ResultadoOpcion
    {
      public int Id { get; set; }

      public int ResultadoPreguntaId { get; set; }

      [Required]
      public ResultadoPregunta ResultadoPregunta { get; set; }

      public int OpcionId { get; set; }

      [Required]
      public Opcion Opcion { get; set; }

      public int Recuento { get; set; }

      public float Porcentaje { get; set; }

    }
}