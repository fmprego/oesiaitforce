namespace ee.Models.Enum
{
    // Creada : 0, Pendiente : 1, Preactiva : 2, Activa : 3, Cerrada : 4, Historico : 5
    public enum EstadoEncuesta { Creada, Pendiente, Preactiva, Activa, Cerrada, Historico}
}