using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ee.Models.Enum;

namespace ee.Models
{
    [Table("Respuestas")]
    public class Respuesta
    {
        public int Id { get; set; }

        [Required]
        public int? PreguntaId { get; set; }
        public Pregunta Pregunta { get; set; }

        [Required]
        public int CuestionarioId { get; set; }
        public Cuestionario Cuestionario { get; set; }

        [Required]
        public TipoPregunta TipoPregunta { get; set; }

        [Range(0.01, 1)]
        public float? Escala { get; set; }

        [StringLength(511)]
        public string Texto { get; set; }

        public int? OpcionId { get; set; }
        public Opcion Opcion { get; set; }
    }
}