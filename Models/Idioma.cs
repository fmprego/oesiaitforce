using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("Idiomas")]
    public class Idioma
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(10)]
        public string Locale { get; set; }
        public ICollection<Encuesta> Encuestas { get; set; }
        public ICollection<IdiomasIdioma> Traducciones { get; set; }

        public Idioma() 
        {
            this.Encuestas = new Collection<Encuesta>();
            this.Traducciones = new Collection<IdiomasIdioma>();
        }

        public Idioma(string nombre, string locale)
        {
            this.Nombre = nombre;
            this.Locale = locale;
            this.Encuestas = new Collection<Encuesta>();
            this.Traducciones = new Collection<IdiomasIdioma>();
        }
    }
}