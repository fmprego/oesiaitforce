using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("ResultadoPreguntas")]
    public class ResultadoPregunta
    {
        public int Id { get; set; }  
        public int InformeId { get; set; }

        [Required]
        public Informe Informe { get; set; }

        public int PreguntaId { get; set; }

        [Required]
        public Pregunta Pregunta { get; set; }

        [Required]
        [Range(0.01, 1)]
        //public float Valor { get; set; }
        public float? Valor { get; set; }

        [Range(0.01, 1)]
        public float Peso { get; set; }

        public ICollection<ResultadoOpcion> ResultadoOpciones { get; set; }

    }
}