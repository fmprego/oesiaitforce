using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("EncuestaCategorias")]
    public class EncuestaCategoria
    {
       public int EncuestaId { get; set; }
       public Encuesta Encuesta { get; set; }
       public int CategoriaId { get; set; }
       public Categoria Categoria { get; set; }
    }
}