using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ee.Models.Enum;

namespace ee.Models
{
    [Table("Encuestas")]
    public class Encuesta
    {
        public Encuesta(int id, string titulo, string descripcion, Usuario usuario, string usuarioId, int origenId, 
            int geografiaId, Estructura estructura, EstadoEncuesta estado,
            int idiomaId, Idioma idioma, int tipoEncuestaId, TipoEncuesta tipoEncuesta) 
        {
            this.Id = id;
            this.Titulo = titulo;
            this.Descripcion = descripcion;
            this.Usuario = usuario;
            this.UsuarioId = usuarioId;
            this.OrigenId = origenId;
            this.GeografiaId = geografiaId;
            this.Estructura = estructura;
            this.Estado = estado;
               
            this.Idioma = idioma;
            this.IdiomaId = idiomaId;     

            this.TipoEncuesta = tipoEncuesta;
            this.TipoEncuestaId = tipoEncuestaId;          
        }
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Titulo { get; set; }

        [StringLength(2047)]
        public string Descripcion { get; set; }

        [Required]
        public Usuario Usuario { get; set; }

        public string UsuarioId { get; set; }

        public int OrigenId { get;set; }

        public int GeografiaId{ get;set; }

        public Estructura Estructura { get; set; }

        public DateTimeOffset? FechaCreacion{get;set;}

        public DateTimeOffset? FechaInicio { get; set; }

        public DateTimeOffset? FechaCierre { get; set; }

        public EstadoEncuesta Estado { get; set; }

        [Required]
        public Idioma Idioma { get; set; }

        public int IdiomaId { get; set; }        

        public TipoEncuesta TipoEncuesta { get; set; }

        public int TipoEncuestaId { get; set; }        

        public ICollection<EncuestaPregunta> Preguntas { get; set; }
        public ICollection<EncuestaCategoria> Categorias { get; set; }
        public ICollection<Informe> Informes { get; set; }

        public Encuesta()
        {
            this.Preguntas = new Collection<EncuestaPregunta>();
            this.Categorias = new Collection<EncuestaCategoria>();
            this.Informes = new Collection<Informe>();
        }

        public Encuesta(string titulo, string descripcion, string usuarioId, int idiomaId, int tipoEncuestaId)
        {
            this.Titulo = titulo;
            this.Descripcion = descripcion;
            this.UsuarioId = usuarioId;
            this.IdiomaId = idiomaId;
            this.TipoEncuestaId = tipoEncuestaId;
        }
    }
}