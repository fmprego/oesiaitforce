using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("EncuestaPreguntas")]
    public class EncuestaPregunta
    {
       public int EncuestaId { get; set; }
       public Encuesta Encuesta { get; set; }
       public int PreguntaId { get; set; }
       public Pregunta Pregunta { get; set; }

       [Required]
       [Range(0.01, 1)]
       public float Peso { get; set; }
    }
}