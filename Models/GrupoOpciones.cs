using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("GruposOpciones")]
    public class GrupoOpciones
    {
        public int Id { get; set; }

        [Required]
        [StringLength(127)]
        public string Nombre { get; set; }
        public ICollection<Opcion> Opciones { get; set; }

        public GrupoOpciones() {}
    }
}