using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("Estructuras")]
    public class Estructura
    {
      public int Id { get; set; }

      [Required]
      public int EncuestaId { get; set; }
      public Encuesta Encuesta { get; set; }
      public ICollection<Persona> Personas { get; set; }

    }
}