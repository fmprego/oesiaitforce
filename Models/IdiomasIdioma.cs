using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("IdiomasIdioma")]
    public class IdiomasIdioma
    {
        public int Id { get; set; }

        public int IdiomaId { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(10)]
        public string Locale { get; set; }

        public IdiomasIdioma() 
        {
        }
    }
}