using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("ResultadoCategorias")]
    public class ResultadoCategoria
    {
        public int Id { get; set; }  
        public int InformeId { get; set; }

        [Required]
        public Informe Informe { get; set; }

        public int CategoriaId { get; set; }

        [Required]
        public Categoria Categoria { get; set; }

        [Required]
        [Range(0.01, 1)]
        //public float Valor { get; set; }
        public float? Valor { get; set; }
    }
}