using CsvHelper.Configuration;

namespace ee.Models.Csv
{
    public class PersonaMapping : ClassMap<PersonaRecord>
    {
        public PersonaMapping()
        {
            AutoMap();
            Map(pr => pr.Numero).Name("Numero");
            Map(pr => pr.Nivel1).Name("Nivel1");
            Map(pr => pr.Nivel2).Name("Nivel2");
            Map(pr => pr.Nivel3).Name("Nivel3");
            Map(pr => pr.Nivel4).Name("Nivel4");
            Map(pr => pr.Nombre).Name("NumeroPersonal");
            Map(pr => pr.EsLider).Name("LiderCentro");
            Map(pr => pr.NumCentro).Name("CentroAdscrito");
            Map(pr => pr.NomCentro).Name("NombreCentroAdscrito");
            Map(pr => pr.Email).Name("Mail");
        }
    }
}