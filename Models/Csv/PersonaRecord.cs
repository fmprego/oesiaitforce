namespace ee.Models.Csv
{
    public class PersonaRecord
    {
        public string Numero { get; set; }
        public string Nivel1 { get; set; }
        public string Nivel2 { get; set; }
        public string Nivel3 { get; set; }
        public string Nivel4 { get; set; }
        public string Nombre { get; set; }
        public string EsLider { get; set; }
        public string NumCentro { get; set; }
        public string NomCentro { get; set; }
        public string Email { get; set; }        
    }
}