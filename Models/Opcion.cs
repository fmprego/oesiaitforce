using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("Opciones")]
    public class Opcion
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Texto { get; set; }

        [Required]
        public float Peso { get; set; }

        public Pregunta Pregunta { get; set; }

        public int? PreguntaId { get; set; }
        public int? GrupoOpcionesId { get; set; }

        public byte[] ImagenContain { get; set; }

        public string ImagenCab { get; set; }

        public string Imagen { get; set; }

        public Opcion() { }
    }
}