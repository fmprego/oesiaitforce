using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("Categorias")]
    public class Categoria
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        public ICollection<Pregunta> Preguntas { get; set; }
        public ICollection<EncuestaCategoria> Encuestas { get; set; }

        public Categoria() {}

        public Categoria(string nombre)
        {
            this.Nombre = nombre;
            this.Preguntas = new Collection<Pregunta>();
        }
        
    }
}