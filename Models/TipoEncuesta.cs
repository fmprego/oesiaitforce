using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("TipoEncuesta")]
    public class TipoEncuesta
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        public ICollection<Encuesta> Encuestas { get; set; }

        public TipoEncuesta() 
        {
            this.Encuestas = new Collection<Encuesta>();            
        }

        public TipoEncuesta(string nombre)
        {
            this.Nombre = nombre;
            this.Encuestas = new Collection<Encuesta>();
        }
    }
}