using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ee.Models
{
    public class ResultadoNivel
    {
        //public ICollection<float> valores {get; set; }
        public ICollection<float?> valores { get; set; }
        public HashSet<string> categorias {get; set; }
         public string nombreEncuesta {get; set;}

         public ResultadoNivel(){
            //valores = new Collection<float>();
            valores = new Collection<float?>();
            categorias = new HashSet<string>();
        }    
    }
   
    
}