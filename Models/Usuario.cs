using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace ee.Models
{
    public class Usuario : IdentityUser
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public Geografia Geografia { get; set; }
        public int GeografiaId { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime LastLogin { get; set; }

    }
}