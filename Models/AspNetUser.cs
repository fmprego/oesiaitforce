using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace ee.Models
{
    [Table("AspNetUsers")]
    public class AspNetUser
    {
    public int Id{set; get;}
    public int AccessFailedCount{set; get;}
    public string Apellidos{set; get;}
    public int ConcurrencyStamp{set; get;}
    public string Email{set; get;}
    public bool EmailConfirmed{set; get;}
    public int GeografiaId{set; get;}
    public DateTimeOffset LastLogin{set; get;}
    public DateTimeOffset LastUpdate{set; get;}
    public bool LockoutEnabled{set; get;}
    public DateTimeOffset LockoutEnd{set; get;}
    public string Nombre{set; get;}
    public string NormalizedEmail{set; get;}
    public string NormalizedUserName{set; get;}
    public string PasswordHash{set; get;}
    public string PhoneNumber{set; get;}
    public string PhoneNumberConfirmed{set; get;}
    public string SecurityStamp{set; get;}
    public bool TwoFactorEnabled{set; get;}
    public string UserName{set; get;}
    }
}