using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("Geografias")]
    public class Geografia
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }

        public Geografia() 
        {
            this.Usuarios = new Collection<Usuario>();
        }

        public Geografia(string nombre)
        {
            this.Nombre = nombre;
            this.Usuarios = new Collection<Usuario>();
        }
    }
}