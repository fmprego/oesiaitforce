using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ee.Models
{
    [Table("Informes")]
    public class Informe
    {
        public int Id { get; set; }  
        public int EncuestaId { get; set; }

        [Required]
        public Encuesta Encuesta { get; set; }

        public string Nivel1 { get; set; }

        public string Nivel2 { get; set; }

        public string Nivel3 { get; set; }
        
        public string Nivel4 { get; set; }

        public int Respuestas { get; set; }

        public ICollection<ResultadoCategoria> ResultadoCategorias { get; set; }

        public ICollection<ResultadoPregunta> ResultadoPreguntas { get; set; }
        
        [NotMapped]
        public ICollection<ResultadoNivel> ResultadoNiveles {get; set; }
        public Informe()
        {
            this.ResultadoCategorias = new Collection<ResultadoCategoria>();
            this.ResultadoPreguntas = new Collection<ResultadoPregunta>();
            this.ResultadoNiveles = new Collection<ResultadoNivel>();
        }
    }
}