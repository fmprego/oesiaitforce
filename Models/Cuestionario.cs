using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    [Table("Cuestionarios")]
    public class Cuestionario
    {
        public int Id { get; set; } 

        public int EncuestaId { get; set; }

        [Required]
        public Encuesta Encuesta { get; set; }

        public string Nivel1 { get; set; }

        public string Nivel2 { get; set; }

        public string Nivel3 { get; set; }
        
        public string Nivel4 { get; set; }

        public DateTimeOffset Hora { get; set; }

        public ICollection<Respuesta> Respuestas { get; set; }
    }
}