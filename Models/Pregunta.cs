using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ee.Models.Enum;

namespace ee.Models
{
    [Table("Preguntas")]
    public class Pregunta
    {
      public int Id { get; set; }

      [Required]
      public string Texto { get; set; }
   
      public string Descripcion { get; set; }

      [Required]
      public Categoria Categoria { get; set; }

      public int CategoriaId { get; set; }

      public Usuario Usuario { get; set; }

      public string UsuarioId { get; set; }

      [Required]
      public TipoPregunta TipoPregunta { get; set; }

      public ICollection<EncuestaPregunta> Encuestas { get; set; }

      public ICollection<Opcion> Opciones { get; set; }

      public Boolean Editable { get; set; }

      public int? Escala { get; set;}

      public Pregunta()
      {
        this.Opciones = new Collection<Opcion>();
      }

      public Pregunta(string Texto, int categoriaId)
      {
        this.Texto = Texto;
        this.CategoriaId = categoriaId;
        this.Editable = true;
      }
    }
}