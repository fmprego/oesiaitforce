using ee.Extensions;

namespace ee.Models.Query
{
    public class UnidadQuery : IQueryObject
    {
        public string Nombre { get; set; }
        public int? GeografiaId { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
    }
}