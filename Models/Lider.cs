using System.ComponentModel.DataAnnotations.Schema;

namespace ee.Models
{
    public class Lider : Persona
    {
        public string UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

    }
}