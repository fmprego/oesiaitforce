using System;
using System.Collections.ObjectModel;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Controllers.Resources.Query;
using ee.Models;
using ee.Models.Csv;
using ee.Models.Enum;
using ee.Models.Query;

namespace ee.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to domain
            CreateMap<Persona, Lider>();
            CreateMap<Lider, Usuario>()
                .ForMember(u => u.Id, opt => opt.Ignore());

            // Domain to API Resource
            CreateMap(typeof(QueryResult<>), typeof(QueryResultResource<>)); 
            CreateMap<Geografia, GeografiaResource>()
                .ForMember(gr => gr.AdminCount, opt => opt.MapFrom(g => g.Usuarios.Count));
            CreateMap<TipoEncuesta, TipoEncuestaResource>()
                .ForMember(gr => gr.EncuestaCount, opt => opt.MapFrom(g => g.Encuestas.Count));                
            CreateMap<Unidad, UnidadResource>();
            CreateMap<Usuario, UsuarioResource>()
                .ForMember(ur => ur.Geografia, opt => opt.MapFrom(u => u.Geografia.Nombre));
            CreateMap<Encuesta, EncuestaResource>()
                .ForMember(er => er.EstructuraId, opt => opt.MapFrom(e => e.Estructura.Id));
            CreateMap<Categoria, CategoriaResource>()
                .ForMember(cr => cr.PreguntasCount, opt => opt.MapFrom(c => c.Preguntas.Count));
            CreateMap<Categoria, SimpleCategoriaResource>()
                .ForMember(cr => cr.PreguntasCount, opt => opt.MapFrom(c => c.Preguntas.Count));                
            CreateMap<Pregunta, PreguntaResource>()
                .ForMember(pr => pr.Creator, opt => opt.MapFrom(p => p.Usuario.UserName))
                .ForMember(pr => pr.NombreCategoria, opt => opt.MapFrom(p => p.Categoria.Nombre));
            CreateMap<TipoPregunta, TipoPreguntaResource>()
                .ForMember(tpr => tpr.Id, opt => opt.MapFrom(tp => (int)tp))
                .ForMember(tpr => tpr.Descripcion, opt => opt.MapFrom(tp => tp.ToString()));
            CreateMap<Opcion, OpcionResource>();
            CreateMap<GrupoOpciones, GrupoOpcionesResource>();
            CreateMap<EncuestaCategoria, CategoriaResource>()
                .ForMember(cr => cr.Id, opt => opt.MapFrom(ec => ec.Categoria.Id))
                .ForMember(cr => cr.Nombre, opt => opt.MapFrom(ec => ec.Categoria.Nombre))
                .ForMember(cr => cr.Preguntas, opt => opt.Ignore())
                .ForMember(cr => cr.PreguntasCount, opt => opt.Ignore());
            CreateMap<EncuestaPregunta, PreguntaResource>()
                .ForMember(pr => pr.Id, opt => opt.MapFrom(ep => ep.Pregunta.Id))
                .ForMember(pr => pr.Texto, opt => opt.MapFrom(ep => ep.Pregunta.Texto))
                .ForMember(pr => pr.CategoriaId, opt => opt.MapFrom(ep => ep.Pregunta.CategoriaId))
                .ForMember(pr => pr.NombreCategoria, opt => opt.MapFrom(ep => ep.Pregunta.Categoria.Nombre))
                .ForMember(pr => pr.TipoPregunta, opt => opt.MapFrom(ep => ep.Pregunta.TipoPregunta))
                .ForMember(pr => pr.Creator, opt => opt.Ignore())
                .ForMember(pr => pr.Opciones, opt => opt.Ignore());
            CreateMap<Estructura, SimpleEstructuraResource>()
                .ForMember(ser => ser.PersonasCount, opt => opt.MapFrom(e => e.Personas.Count));
            CreateMap<EstadoEncuesta, EstadoEncuestaResource>()
                .ForMember(eer => eer.Id, opt => opt.MapFrom(ee => (int)ee))
                .ForMember(eer => eer.Descripcion, opt => opt.MapFrom(ee => ee.ToString()));
            CreateMap<Idioma, IdiomaResource>();                
            CreateMap<ResultadoCategoria, ResultadoCategoriaResource>()
                .ForMember(rcr => rcr.NombreCategoria, opt => opt.MapFrom(rc => rc.Categoria.Nombre));
            CreateMap<ResultadoPregunta, ResultadoPreguntaResource>()
                .ForMember(rpr => rpr.CategoriaId, opt => opt.MapFrom(rp => rp.Pregunta.CategoriaId))
                .ForMember(rpr => rpr.Texto, opt => opt.MapFrom(rp => rp.Pregunta.Texto))
                .ForMember(rpr => rpr.TipoPregunta, opt => opt.MapFrom(rp => rp.Pregunta.TipoPregunta))
                .ForMember(rpr => rpr.ResultadoOpciones, opt => opt.NullSubstitute(new Collection<ResultadoOpcion>()));
            CreateMap<ResultadoNivel, ResultadoNivelResources>()
                .ForMember(rn => rn.valores, opt => opt.MapFrom(rc => rc.valores))
                .ForMember(rn => rn.nombreEncuesta, opt => opt.MapFrom(rc => rc.nombreEncuesta))
                .ForMember(rn => rn.categorias, opt => opt.MapFrom(rc => rc.categorias));
            CreateMap<ResultadoOpcion, ResultadoOpcionResource>()
                .ForMember(ror => ror.Texto, opt => opt.MapFrom(ro => ro.Opcion.Texto));
            CreateMap<PasswordRecoveryInfo, PasswordRecoveryInfoResource>();
            CreateMap<AspNetUser, AspNetUserResource>();
            
            // API Resource to Domain
            CreateMap<GeografiaQueryResource, GeografiaQuery>();
            CreateMap<TipoEncuestaQueryResource, TipoEncuestaQuery>();
            CreateMap<UnidadQueryResource, UnidadQuery>();
            CreateMap<SaveAdminResource, Usuario>();
            CreateMap<SaveLiderResource, Usuario>();
            CreateMap<IdiomaResource, Idioma>();
            CreateMap<TipoEncuestaResource, TipoEncuesta>();
            CreateMap<GeografiaResource, Geografia>();
            CreateMap<CategoriaResource, Categoria>()
                .ForMember(c => c.Preguntas, opt => opt.Ignore());
            CreateMap<SaveEncuestaResource, Encuesta>();
            CreateMap<EncuestaResource, Encuesta>()
                .ForMember(e => e.Estado, opt => opt.Ignore())
                .ForMember(e => e.Idioma, opt => opt.Ignore())
                .ForMember(e => e.TipoEncuesta, opt => opt.Ignore())
                .ForMember(e => e.Preguntas, opt => opt.Ignore())
                .ForMember(e => e.Usuario, opt => opt.Ignore())
                .ForMember(e => e.Categorias, opt => opt.Ignore());
            CreateMap<SavePreguntaResource, Pregunta>()
                .ForMember(p => p.TipoPregunta, 
                    opt => opt.MapFrom(spr => (TipoPregunta)spr.TipoPregunta)
                );
            CreateMap<OpcionResource, Opcion>();
            CreateMap<SaveOpcionResource, Opcion>();
            CreateMap<PreguntaResource, Pregunta>()
                .ForMember(p => p.Categoria, opt => opt.Ignore())
                .ForMember(p => p.Usuario, opt => opt.Ignore())
                .ForMember(p => p.Encuestas, opt => opt.Ignore())
                .ForMember(p => p.TipoPregunta, opt => opt.Ignore());
            CreateMap<SaveCuestionarioResource, Cuestionario>()
                .ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<SaveRespuestaResource, Respuesta>()
                .ForMember(c => c.Id, opt => opt.Ignore())
                .ForMember(c => c.Pregunta, opt => opt.Ignore())
                .ForMember(c => c.CuestionarioId, opt => opt.Ignore())
                .ForMember(c => c.Cuestionario, opt => opt.Ignore())
                .ForMember(c => c.Opcion, opt => opt.Ignore())
                .ForMember(c => c.TipoPregunta, opt => opt.MapFrom(srr => (TipoPregunta)srr.TipoPregunta.Id));
            CreateMap<EncuestaPreguntaResource, EncuestaPregunta>()
                .ForMember(ep => ep.Encuesta, opt => opt.Ignore())
                .ForMember(ep => ep.Pregunta, opt => opt.Ignore());
            CreateMap<NivelResource, Nivel>();
            
            CreateMap<PasswordRecoveryInfoResource, PasswordRecoveryInfo>();
            CreateMap<AspNetUserResource, AspNetUser>();

            // CSV Record to Object
            CreateMap<PersonaRecord, Persona>()
                .ForMember(p => p.Id, opt => opt.Ignore())
                .ForMember(p => p.EsLider, opt => opt.MapFrom(pr => 
                    pr.EsLider.Equals("Lider") || pr.EsLider.Equals("Líder") ? true : false
                ))
                .ForMember(p => p.Nivel1, opt => opt.MapFrom(pr =>
                    pr.Nivel1.Equals("--") ? null : pr.Nivel1
                ))
                .ForMember(p => p.Nivel2, opt => opt.MapFrom(pr =>
                    pr.Nivel2.Equals("--") ? null : pr.Nivel2
                ))
                .ForMember(p => p.Nivel3, opt => opt.MapFrom(pr =>
                    pr.Nivel3.Equals("--") ? null : pr.Nivel3
                ))
                .ForMember(p => p.Nivel4, opt => opt.MapFrom(pr =>
                    pr.Nivel4.Equals("--") ? null : pr.Nivel4
                ))
                .ForMember(p => p.NumCentro, opt => opt.ResolveUsing(StringToIntConverter));
        }

        private static object StringToIntConverter(PersonaRecord pr)
        {
            int intValue = 0;
            if (pr.NumCentro != null)
            {
                Int32.TryParse(pr.NumCentro, out intValue);
            }
            return intValue;
        }
    }
}