﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ee.Migrations
{
    public partial class AddEscalaImagenDescripcionAumentaString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Texto",
                table: "Preguntas",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 511);

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "Preguntas",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Escala",
                table: "Preguntas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Imagen",
                table: "Opciones",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Descripcion",
                table: "Encuestas",
                maxLength: 2047,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "Preguntas");

            migrationBuilder.DropColumn(
                name: "Escala",
                table: "Preguntas");

            migrationBuilder.DropColumn(
                name: "Imagen",
                table: "Opciones");

            migrationBuilder.AlterColumn<string>(
                name: "Texto",
                table: "Preguntas",
                maxLength: 511,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Descripcion",
                table: "Encuestas",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2047,
                oldNullable: true);
        }
    }
}
