﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ee.Migrations
{
    public partial class AddResultadoOpciones : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ResultadoOpciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OpcionId = table.Column<int>(nullable: false),
                    Recuento = table.Column<int>(nullable: false),
                    ResultadoPreguntaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResultadoOpciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResultadoOpciones_Opciones_OpcionId",
                        column: x => x.OpcionId,
                        principalTable: "Opciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResultadoOpciones_ResultadoPreguntas_ResultadoPreguntaId",
                        column: x => x.ResultadoPreguntaId,
                        principalTable: "ResultadoPreguntas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoOpciones_OpcionId",
                table: "ResultadoOpciones",
                column: "OpcionId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoOpciones_ResultadoPreguntaId",
                table: "ResultadoOpciones",
                column: "ResultadoPreguntaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ResultadoOpciones");
        }
    }
}
