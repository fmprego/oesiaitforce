﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ee.Migrations
{
    public partial class AddEncuestasOrigenIdGeografiaFechaCreacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FechaCreacion",
                table: "Encuestas",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GeografiaId",
                table: "Encuestas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OrigenId",
                table: "Encuestas",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FechaCreacion",
                table: "Encuestas");

            migrationBuilder.DropColumn(
                name: "GeografiaId",
                table: "Encuestas");

            migrationBuilder.DropColumn(
                name: "OrigenId",
                table: "Encuestas");
        }
    }
}
