﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ee.Migrations
{
    public partial class ModificarCampoImagen : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Imagen",
                table: "Opciones");

            migrationBuilder.AddColumn<byte[]>(
                name: "ImagenContain",
                table: "Opciones",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagenContain",
                table: "Opciones");

            migrationBuilder.AddColumn<string>(
                name: "Imagen",
                table: "Opciones",
                nullable: true);
        }
    }
}
