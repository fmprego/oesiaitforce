import { Respuesta } from "./respuesta";

export interface Cuestionario {
  encuestaId: number;
  hora: Date;
  nivel1: string;
  nivel2: string | null;
  nivel3: string | null;
  nivel4: string | null;
  respuestas: Respuesta[];
}

export class Cuestionario {
  constructor(
    encuestaId: number,
    respuestas: Respuesta[],
    nivel1: string,
    nivel2?: string,
    nivel3?: string,
    nivel4?: string,
    hora?: Date,
  ) {
    this.encuestaId = encuestaId;
    this.respuestas = respuestas;
    this.nivel1 = nivel1;
    this.nivel2 = nivel2 || null,
    this.nivel3 = nivel3 || null,
    this.nivel4 = nivel4 || null,
    this.hora = hora || new Date();
  }
}
