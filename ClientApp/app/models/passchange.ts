export class PassChange {
  constructor(
    public email: string,
    public oldPass: string,
    public newPass: string,
    public newPass2: string,
    public validationCode: string
  ) {};
}