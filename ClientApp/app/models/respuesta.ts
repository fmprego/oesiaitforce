import { TipoPregunta } from "./pregunta";

export interface Respuesta {
  id: number;
  preguntaId: number;
  cuestionarioId: number;
  tipoPregunta: TipoPregunta | null;
  escala: number;
  texto: string;
  opcionId: number | null;
}

export class Respuesta {
  constructor(tipoPregunta?: TipoPregunta, preguntaId?: number) {
    this.tipoPregunta = tipoPregunta || null;
    this.preguntaId = preguntaId || 0;
  }
}
