import { Usuario } from "./usuario";

export interface Admin extends Usuario {
  geografia: string,
  geografiaId: number,
  lastUpdate: Date,
  lastLogin: Date,
}

export class Admin {
  constructor() {};
}