export interface Informe {
  id: number,
  encuestaId: number,
  nivel1: string,
  nivel2: string,
  nivel3: string,
  nivel4: string,
  respuestas: number,
  resultadoCategorias: ResultadoCategoria[],
  resultadoPreguntas: ResultadoPregunta[],
  resultadoNiveles : ResultadoNivel[];
}

export class Informe {
  constructor() {};
}

export interface InformeInfo {
  encuestaId: number,
  global: boolean,
  nivel1: string,
  nivel2: string,
  nivel3: string,
  nivel4: string
}

export class InformeInfo {
  constructor() {};
}

export interface ResultadoNivel{
  valores : number [],
  categorias : string [],
  nombreEncuesta : string,
}

export interface ResultadoPregunta {
  id: number,
  preguntaId: number,
  texto: string,
  tipoPregunta: string,
  categoriaId: number,
  valor: number,
  peso: number,
  porcentaje: number,
  resultadoOpciones: ResultadoOpcion[],
}

export class ResultadoPregunta {
  constructor() {};
}

export interface ResultadoOpcion {
  id: number,
  opcionId: number,
  texto: string,
  recuento: number,
  porcentaje: number,
}

export interface ResultadoCategoria {
  id: number,
  categoriaId: number,
  nombreCategoria: string,
  valor: number,
}
