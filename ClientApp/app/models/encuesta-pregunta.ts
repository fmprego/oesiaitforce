
export interface EncuestaPregunta {
  encuestaId: number;
  preguntaId: number;
  peso: number;
}

export class EncuestaPregunta {
  constructor(encuestaId: number, preguntaId: number, peso: number) {
    this.encuestaId = encuestaId;
    this.preguntaId = preguntaId;
    this.peso = peso;
  }
}
