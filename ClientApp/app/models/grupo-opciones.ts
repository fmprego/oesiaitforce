import { Opcion } from "./opcion";

export interface GrupoOpciones {
  id: number,
  nombre: string,
  opciones: Opcion[],
}

export class GrupoOpciones {
  constructor() {};
}