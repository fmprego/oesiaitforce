import { Categoria } from "./categoria";
import { Opcion } from "./opcion";
import { Respuesta } from "./respuesta";

export interface Pregunta {
  id: number;
  texto: string;
  descripcion: string;
  categoriaId: number;
  nombreCategoria: string;
  creator: string;
  tipoPregunta: TipoPregunta;
  opciones: Opcion[];
  respuesta: Respuesta;
  peso: number;
  escala: number;
    valor: number;
    orden: number;
}

export interface TipoPregunta {
  id: number;
  descripcion: string;
}

export class Pregunta {
  constructor() {
    this.tipoPregunta = new TipoPregunta();
  }
}

// tslint:disable-next-line:max-classes-per-file
export class TipoPregunta {
  // tslint:disable-next-line:no-empty
  constructor() {
  }
}
