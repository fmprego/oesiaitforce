export interface Persona {
  id: number,
  numero: number,
  nivel1: string,
  nivel2: string,
  nivel3: string,
  nivel4: string,
  nombre: string,
  numCentro: number,
  nomCentro: string,
  email: string,
}

export class Persona {
  constructor() {};
}