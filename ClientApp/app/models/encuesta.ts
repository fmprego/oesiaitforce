import { Admin } from "./admin";
import { Categoria } from "./categoria";
import { Pregunta } from "./pregunta";
import { Informe } from "./informe";
import { TipoEncuesta } from "./tipoencuesta";

export interface Encuesta {
  id: number;
  origeId: number;
  titulo: string;
  descripcion: string;
  categorias: Categoria[];
  preguntas: Pregunta[];
  informes: Informe[];
  usuario: Admin;
  geografiaId: number;
  fechaCreacion: Date;
  fechaInicio: Date;
  fechaCierre: Date;
  estado: EstadoEncuesta;
  estructuraId: number;  
  idiomaId: number;
  tipoEncuestaId: number;
  idioma: IdiomaEncuesta;
  tipoEncuesta: TipoEncuesta;   
  global:boolean;
  sendMails: boolean;
  url:string;
}

export interface EstadoEncuesta {
  id: number;
  descripcion: string;
}

export interface IdiomaEncuesta {
  id: number;
  nombre: string;
  locale: string;
}

export class Encuesta {
  constructor() {
    this.usuario = new Admin();
    this.tipoEncuesta = new TipoEncuesta();    
  }
}
