import { Pregunta } from "./pregunta";

export interface Categoria {
  id: number,
  nombre: string,
  preguntasCount: number,
  preguntas: Pregunta[],
}

export class Categoria {
  constructor() {};
}