import {BrowserModule} from '@angular/platform-browser';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { InformesListComponent } from './components/informes/informes-list.component';
import { InformeService } from "./services/informe.service";
import { GenericDialog } from "./components/app/dialogs/generic.dialog";
import { AprobacionDialog } from "./components/revisiones/dialogs/aprobacion.dialog";
import { ResumenEncuestaComponent } from "./components/encuestas/resumen/resumen-encuesta.component";
import { LiderService } from "./services/lider.service";
import { AltaLideresDialog } from "./components/encuestas/estructura/dialogs/alta-lideres.dialog";
import { GenericErrorDialog } from "./components/common/dialog/generic-error.dialog";
import { EstructuraService } from "./services/estructura.service";
import { SelectPreguntasComponent } from "./components/encuestas/preguntas/select-preguntas.component";
import { GrupoOpcionesService } from "./services/grupo-opciones.service";
import { UnsavedChangesDialog } from "./components/common/unsaved-changes-dialog.component";
import { OpcionesEditorComponent } from "./components/preguntas/opciones-editor.component";
import { LoadingComponent } from "./components/common/loading-component";
import { PreguntaDeleteDialog } from "./components/categorias/dialogs/pregunta-delete-dialog";
import { PreguntaService } from "./services/preguntas.service";
import { CategoriaEditorComponent } from "./components/categorias/categoria-editor.component";
import { CategoriasListComponent } from "./components/categorias/categorias-list.component";
import { PreguntasComponent } from "./components/preguntas/preguntas.component";
import { CategoriaService } from "./services/categoria.service";
import { SelectCategoriasComponent } from "./components/encuestas/categorias/select-categorias.component";
import { NewEncuestaComponent } from "./components/encuestas/new/new-encuesta.component";
import { GeografiaDeleteDialogForbidden } from "./components/geografias/dialogs/geografia-delete-dialog-forbidden";
import { TipoEncuestaDeleteDialogForbidden } from "./components/tipoencuesta/dialogs/tipoencuesta-delete-dialog-forbidden";
import { EncuestaService } from "./services/encuesta.service";
import { GeografiaItemComponent } from "./components/geografias/geografia-item.component";
import { TipoEncuestaItemComponent } from "./components/tipoencuesta/tipoencuesta-item.component";
import { AdminService } from "./services/admin.service";
import { NgModule, LOCALE_ID } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "./app.material.module";
import { AuthModule } from "./app.auth.module";
import { MomentModule } from "angular2-moment";
import { FlexLayoutModule } from "@angular/flex-layout";
import { HAMMER_GESTURE_CONFIG } from "@angular/platform-browser";
import { GestureConfig } from "@angular/material";
import { ChartsModule } from "ng2-charts";
import { ContextMenuModule} from "ngx-contextmenu";

import { BarRatingModule } from "ngx-bar-rating";

import { AppComponent } from "./components/app/app.component";
import { AdminPanelComponent } from "./components/admin/admin-panel.component";
import { AdminItemComponent } from "./components/admin/admin-item.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { FetchDataComponent } from "./components/fetchdata/fetchdata.component";
import { CounterComponent } from "./components/counter/counter.component";
import { GeografiasListComponent } from "./components/geografias/geografias-list.component";
import { EncuestasListComponent } from "./components/encuestas/encuestas-list.component";
import { AdminFormComponent } from "./components/admin/admin-form.component";
import { GeografiaFormComponent } from "./components/geografias/geografia-form.component";
import { TipoEncuestaListComponent } from "./components/tipoencuesta/tipoencuesta-list.component";
import { TipoEncuestaFormComponent } from "./components/tipoencuesta/tipoencuesta-form.component";

import { UnidadService } from "./services/unidad.service";
import { GeografiaService } from "./services/geografia.service";
import { TipoEncuestaService } from "./services/tipoencuesta.service";
import { AuthService } from "./services/auth.service";
import { AuthGuard } from "./services/auth-guard.service";
import { MasterGuard } from "./services/master-guard.service";
import { AdminGuard } from "./services/admin-guard.service";
import { ChangePassComponent } from "./components/login/change-pass.component";
import { GeografiaDeleteDialog } from "./components/geografias/dialogs/geografia-delete-dialog";
import { TipoEncuestaDeleteDialog } from "./components/tipoencuesta/dialogs/tipoencuesta-delete-dialog";
import { PreguntaEditorComponent } from "./components/preguntas/pregunta-editor.component";
import { CategoriaFormComponent } from "./components/categorias/categoria-form.component";
import { CategoriaDeleteDialogForbidden } from "./components/categorias/dialogs/categoria-delete-dialog-forbidden";
import { LoadEstructuraComponent } from "./components/encuestas/estructura/load-estructura.component";
import { KeysPipe } from "./pipes/keys.pipe";
import { LoadEstructuraNoFileDialog } from "./components/encuestas/estructura/dialogs/load-estructura-no-file.dialog";
import { FechasEncuestaComponent } from "./components/encuestas/fechas/fechas-encuesta.component";
import { RevisionesComponent } from "./components/revisiones/revisiones.component";
import { InformeComponent } from "./components/informes/informe.component";
import { InformeComparativaComponent } from "./components/informes/informe-comparativa.component";

import { registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import { ActivacionDialog } from "./components/encuestas/dialogs/activacion.dialog";
import { SendMailDialog } from "./components/encuestas/dialogs/send-mail.dialog";
import { ModificarEncuestaActivaDialog } from './components/encuestas/dialogs/modificar-encuesta-activa.dialog';
import { LinkEncuestaDialog } from "./components/encuestas/dialogs/link-encuesta.dialog";
import { CuestionarioComponent } from "./components/cuestionarios/cuestionario.component";
import { CuestionarioService } from "./services/cuestionario.service";
import { EncuestaDeleteDialog } from "./components/encuestas/dialogs/encuesta-delete.dialog";
import { AdminDeleteDialog } from "./components/admin/dialog/admin-delete.dialog";
import { LiderGuard } from "./services/lider-guard.service";
import { InformeSelectorComponent } from "./components/informes/informe-selector.component";
import { InformeCategoriaComponent } from "./components/informes/informe-categoria.component";
import { InformeParticipacionComponent } from "./components/informes/informe-participacion.component";
import { InformePreguntaComponent } from "./components/informes/informe-pregunta.component";
import { RespuestaService } from "./services/respuestas.service";
import { RespuestasTextoDialog } from "./components/informes/dialogs/respuestas-texto.dialog";
import { InformesTiposComponent } from './components/informes/informes-tipos.component';
import { InformeTiposGraficoComponent } from './components/informes/informe-tipos-grafico.component';
import { RecoveryPassComponent } from "./components/login/recovery-pass.component";
import { MailRecoveryPassComponent } from "./components/login/mail-recovery-pass.component";
import { RecoveryPasswordService} from "./services/recovery-password.service";

registerLocaleData(localeEs, "es");

@NgModule({
    declarations: [
        AppComponent,
        LoadingComponent,
        AdminPanelComponent,
        AdminItemComponent,
        AdminFormComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        LoginComponent,
        ChangePassComponent,
        GeografiasListComponent,
        GeografiaItemComponent,
        GeografiaFormComponent,
        GeografiaDeleteDialog,
        GeografiaDeleteDialogForbidden,
        TipoEncuestaListComponent,
        TipoEncuestaItemComponent,
        TipoEncuestaFormComponent,
        TipoEncuestaDeleteDialog,
        TipoEncuestaDeleteDialogForbidden,        
        LoadEstructuraNoFileDialog,
        EncuestasListComponent,
        NewEncuestaComponent,
        SelectCategoriasComponent,
        CategoriasListComponent,
        CategoriaEditorComponent,
        CategoriaFormComponent,
        CategoriaDeleteDialogForbidden,
        PreguntasComponent,
        PreguntaEditorComponent,
        PreguntaDeleteDialog,
        OpcionesEditorComponent,
        UnsavedChangesDialog,
        SelectPreguntasComponent,
        LoadEstructuraComponent,
        KeysPipe,
        GenericErrorDialog,
        AltaLideresDialog,
        FechasEncuestaComponent,
        ResumenEncuestaComponent,
        RevisionesComponent,
        AprobacionDialog,
        ActivacionDialog,
        SendMailDialog,
        ModificarEncuestaActivaDialog,
        LinkEncuestaDialog,
        CuestionarioComponent,
        EncuestaDeleteDialog,
        AdminDeleteDialog,
        GenericDialog,
        InformeComponent,
        InformeComparativaComponent,
        InformesListComponent,
        InformeSelectorComponent,
        InformeCategoriaComponent,
        InformeParticipacionComponent,
        InformePreguntaComponent,
        RespuestasTextoDialog,
        InformesTiposComponent,
        InformeTiposGraficoComponent,
        RecoveryPassComponent,
        MailRecoveryPassComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        FormsModule,
        HttpClientModule,
        MaterialModule,
        AuthModule,
        MomentModule,
        ChartsModule,
        FlexLayoutModule,
        BarRatingModule,
        ContextMenuModule.forRoot(),
        RouterModule.forRoot([
            { path: "", redirectTo: "home", pathMatch: "full" },
            { path: "login", component: LoginComponent },
            { path: "change-pass", component: ChangePassComponent },
            { path: "home", component: HomeComponent },
            { path: "admin-panel", component: AdminPanelComponent, canActivate: [MasterGuard] },
            { path: "admin-form", component: AdminFormComponent, canActivate: [MasterGuard] },
            { path: "admin-form/:id", component: AdminFormComponent, canActivate: [MasterGuard] },
            { path: "counter", component: CounterComponent },
            { path: "fetch-data", component: FetchDataComponent },
            { path: "geografias", component: GeografiasListComponent },
            { path: "geografia-form", component: GeografiaFormComponent, canActivate: [MasterGuard] },
            { path: "geografia-form/:id", component: GeografiaFormComponent, canActivate: [MasterGuard] },
            { path: "tiposencuesta", component: TipoEncuestaListComponent },
            { path: "tipoencuesta-form", component: TipoEncuestaFormComponent, canActivate: [MasterGuard] },
            { path: "tipoencuesta-form/:id", component: TipoEncuestaFormComponent, canActivate: [MasterGuard] },            
            { path: "encuestas", component: EncuestasListComponent, canActivate: [AdminGuard] },
            { path: "informes", component: InformesListComponent, canActivate: [LiderGuard] },
            { path: "informes/:id", component: InformeComponent, canActivate: [LiderGuard] },
            { path: "informestipo", component: InformesTiposComponent, canActivate: [LiderGuard] },
            { path: "informestiposgrafico", component: InformeTiposGraficoComponent, canActivate: [LiderGuard] },
            { path: "informescomparativa/:id", component: InformeComparativaComponent, canActivate: [LiderGuard] },
            { path: "encuestas/cuestionario/:id", component: CuestionarioComponent },
            { path: "encuesta", component: NewEncuestaComponent, canActivate: [AdminGuard] },
            { path: "encuesta/:id", component: NewEncuestaComponent, canActivate: [AdminGuard] },
            { path: "encuesta/categorias/:id", component: SelectCategoriasComponent, canActivate: [AdminGuard] },
            { path: "encuesta/preguntas/:id", component: SelectPreguntasComponent, canActivate: [AdminGuard] },
            { path: "encuesta/estructura/:id", component: LoadEstructuraComponent, canActivate: [AdminGuard] },
            { path: "encuesta/fechas/:id", component: FechasEncuestaComponent, canActivate: [AdminGuard] },
            { path: "encuesta/resumen/:id", component: ResumenEncuestaComponent, canActivate: [AdminGuard] },
            { path: "categorias", component: CategoriasListComponent, canActivate: [AdminGuard] },
            { path: "categoria/:id", component: CategoriaEditorComponent, canActivate: [AdminGuard] },
            { path: "categoria/:categoriaId/pregunta-editor",
                    component: PreguntaEditorComponent, canActivate: [AdminGuard] },
            { path: "categoria/:categoriaId/pregunta-editor/:preguntaId",
                     component: PreguntaEditorComponent, canActivate: [AdminGuard] },
            { path: "categoria/:categoriaId/pregunta-editor/:preguntaId/:action",
                     component: PreguntaEditorComponent, canActivate: [AdminGuard] },
            { path: "encuesta/:encuestaId/categoria/:categoriaId/pregunta-editor/:preguntaId/:action",
            component: PreguntaEditorComponent, canActivate: [AdminGuard] },                     
            { path: "categoria-form", component: CategoriaFormComponent, canActivate: [AdminGuard] },
            { path: "categoria-form/:id", component: CategoriaFormComponent, canActivate: [AdminGuard] },
            { path: "preguntas", component: PreguntasComponent, canActivate: [AdminGuard] },
            { path: "pregunta-editor", component: PreguntaEditorComponent, canActivate: [AdminGuard] },
            { path: "pregunta-editor/:preguntaId", component: PreguntaEditorComponent, canActivate: [AdminGuard] },
            { path: "revisiones", component: RevisionesComponent, canActivate: [MasterGuard] },
            { path: "recovery-pass/:validationcode/:email", component: RecoveryPassComponent},
            { path: "mail-recovery-pass", component: MailRecoveryPassComponent},            
            { path: "**", redirectTo: "home" },
        ]),
    ],
    providers: [
        { provide: LOCALE_ID, useValue: "es" },
        { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
        AuthGuard,
        MasterGuard,
        AdminGuard,
        LiderGuard,
        AuthService,
        UnidadService,
        AdminService,
        GeografiaService,
        TipoEncuestaService,
        EncuestaService,
        CategoriaService,
        PreguntaService,
        RespuestaService,
        GrupoOpcionesService,
        EstructuraService,
        LiderService,
        CuestionarioService,
        InformeService,
        RecoveryPasswordService
    ],
    exports: [
        KeysPipe,
    ],
})
export class AppModuleShared {
}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
