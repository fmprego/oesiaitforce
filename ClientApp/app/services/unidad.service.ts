import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Unidad } from '../models/unidad';

import 'rxjs/add/operator/map'
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class UnidadService {
  
  private readonly endpoint: string = '/api/unidades';
  
  constructor(private authHttp: AuthHttp) { }

  getUnidades(): Observable<UnidadesResult> {
    // const queryString = this.toQueryString(page, pageSize);
    // const url = `${this.endpoint}?${queryString}`;
    return this.authHttp.get(this.endpoint).map(res => {
      const result: UnidadesResult = {totalItems: 0, items: []};
      result.items = res.json().items.map((it: any) => {
        const unidad: Unidad = { nombre: it.nombre, geografia: it.geografia };
        return unidad;
      })
      result.totalItems = res.json().totalItems;
      return result;
    })
  }

  // toQueryString(page: number, pageSize: number) {
  //   let parts = [];
  //   if (page != null && typeof(page) !== 'undefined') {
  //     parts.push(`page=${page}`);
  //   }
  //   if (pageSize != null && typeof(pageSize) !== 'undefined') {
  //     parts.push(`pageSize=${pageSize}`);
  //   }
  //   return parts.join('&');
  // }

}

export interface UnidadesResult {
  totalItems: number,
  items: Unidad[],
}