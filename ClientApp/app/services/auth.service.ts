import { Usuario } from './../models/usuario';
import { PassChange } from './../models/passchange';
import { Injectable } from '@angular/core';
import { Login } from '../models/login';
import { Http } from '@angular/http';
import { JwtHelper, AuthHttp } from 'angular2-jwt';

@Injectable()
export class AuthService {

  private readonly authEndpoint: string = '/api/auth';
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(
    private http: Http,
    private authHttp: AuthHttp
  ) {}

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    if (!token) return false;
    return !this.jwtHelper.isTokenExpired(token);
  }

  public isMaster(): boolean {
    if (!this.isAuthenticated()) return false;
    const roles: string = localStorage.getItem('ee_role') || '';
    return roles.split(',').indexOf('Master') > -1;
  }

  public isAdmin(): boolean {
    if (this.isMaster()) return true;
    const roles: string = localStorage.getItem('ee_role') || '';
    return roles.split(',').indexOf('Admin') > -1;
  }

  public isLider(): boolean {
    if (this.isMaster()) return true;
    if(this.isAdmin()) return true;
    const roles: string = localStorage.getItem('ee_role') || '';
    return roles.split(',').indexOf('Lider') > -1;
  }

  public getCurrentEmail(): string {
    const email: string = localStorage.getItem('ee_email') || '';
    return email;
  }

  public getCurrentLocale(): string {
    const locale: string = localStorage.getItem('ee_locale') || '';
    return locale;
  }

  public getLoggedUser(): Usuario | void {
    const email = localStorage.getItem('ee_email');
    const nombre = localStorage.getItem('ee_name');    
    if (email && nombre) {
      const user: Usuario = new Usuario();
      user.nombre = nombre;
      user.email = email;
      return user;
    }
  }

  public changePassword(data: PassChange) {
    return this.authHttp.put(`${this.authEndpoint}/pass`, data)
      .map(res => res.json());
  }

  public login(data: Login) {
    return this.http.post(`${this.authEndpoint}`, data)
      .map(res => res.json());
  }

  public logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('ee_role');
    localStorage.removeItem('ee_email');
    localStorage.removeItem('ee_name');
    localStorage.removeItem('ee_expires_at');
    localStorage.removeItem('ee_locale');
  }

  setSession(tokenObj: any, locale: string) {
    if (tokenObj.token) {
      const token = tokenObj.token;
      localStorage.setItem('token', token);
      const decoded = this.jwtHelper.decodeToken(token);
      localStorage.setItem('ee_role', decoded.Role);
      localStorage.setItem('ee_email', decoded.sub);
      localStorage.setItem('ee_name', decoded.unique_name);
      localStorage.setItem('ee_expires_at', decoded.exp);
      localStorage.setItem('ee_locale', locale);
    }
  }
  
}