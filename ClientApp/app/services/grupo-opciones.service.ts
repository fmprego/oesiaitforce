import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

import { Opcion } from './../models/opcion';
import { GrupoOpciones } from './../models/grupo-opciones';

@Injectable()
export class GrupoOpcionesService {

  private readonly endpoint: string = '/api/gruposopciones';

  constructor(private authHttp: AuthHttp) { }
  
  getAll(): Observable<GrupoOpciones[]> {
    return this.authHttp.get(this.endpoint)
      .map(res => res.json());
  }

  saveGrupo(grupo: GrupoOpciones): Observable<GrupoOpciones> {
    return this.authHttp.post(this.endpoint, grupo)
      .map(res => res.json());
  }

  deleteGrupo(grupoOpcionesId: number): Observable<GrupoOpciones> {
    return this.authHttp.delete(`${this.endpoint}/${grupoOpcionesId}`)
      .map(res => res.json());
  }

}