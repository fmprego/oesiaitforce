import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate } from '@angular/router/src/interfaces';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class MasterGuard implements CanActivate {

  constructor(
    protected auth: AuthService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {}

  canActivate(): boolean {
    if (this.auth.isMaster()) {
      return true;
    }
    this.router.navigate(['/login']);
    this.snackBar.open('Necesitas permisos de master para acceder.','Ruta incorrecta',{duration:5000,panelClass:['red-snackbar']})
    return false;
  }
}