import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Categoria } from './../models/categoria';

@Injectable()
export class CategoriaService {

  private readonly endpoint: string = '/api/categorias';

  constructor(private authHttp: AuthHttp) { }

  getCategorias(): Observable<CategoriasResult> {
    return this.authHttp.get(this.endpoint).map(res => {
      const result: CategoriasResult = {totalItems: 0, items: []};
      result.items = res.json().items.map((it: any) => {
        const categoria: Categoria = {
          id: it.id,
          nombre: it.nombre,
          preguntasCount: it.preguntasCount,
          preguntas: it.preguntas,
        };
        return categoria;
      })
      result.totalItems = res.json().totalItems;
      return result;
    })
  }

  getCategoria(id: number): Observable<Categoria> {
    return this.authHttp.get(`${this.endpoint}/${id}?related=true`)
      .map(res => res.json());
  }

  create(categoria: Categoria): Observable<Categoria> {
    return this.authHttp.post(this.endpoint, categoria)
      .map(res => res.json());
  }

  update(categoria: Categoria): Observable<Categoria> {
    return this.authHttp.put(`${this.endpoint}/${categoria.id}`, categoria)
      .map(res => res.json());
  }

  delete(id: number) {
    return this.authHttp.delete(`${this.endpoint}/${id}`)
      .map(res => res.json);
  }  

}

export interface CategoriasResult {
  totalItems: number,
  items: Categoria[],
}