import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';

import { Admin } from '../models/admin';

@Injectable()
export class AdminService {

  private readonly endpoint: string = '/api/admins';

  constructor(private authHttp: AuthHttp) { }

  getAdmins(geografiaId?: number): Observable<Admin[]> {
    const url = geografiaId ? `${this.endpoint}?geo=${geografiaId}` : this.endpoint;
    return this.authHttp.get(url).map(res => res.json());
  }

  getAdminsCount(geografiaId?: number): Observable<number> {
    const url = geografiaId ? `${this.endpoint}?geo=${geografiaId}` : this.endpoint;
    return this.authHttp.get(url).map(res => res.json().length);
  }

  getAdmin(id: string): Observable<Admin> {
    return this.authHttp.get(`${this.endpoint}/${id}`).map(res => res.json());
  }

  deleteAdmin(id: string) {
    return this.authHttp.delete(`${this.endpoint}/${id}`)
      .map(res => res.json);
  }

  createAdmin(admin: Admin): Observable<Admin> {
    return this.authHttp.post(this.endpoint, admin)
      .map(res => res.json());
  }

  updateAdmin(admin: Admin): Observable<Admin> {
    return this.authHttp.put(`${this.endpoint}/${admin.id}`, admin)
      .map(res => res.json());
  }
}