import { Lider } from './../models/lider';
import { Encuesta } from './../models/encuesta';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LiderService {

  private readonly endpoint: string = '/api/lideres';

  constructor(private authHttp: AuthHttp) { }

  signupAll(estructuraId: number): Observable<Lider[]> {
    const url = `${this.endpoint}/${estructuraId}/signupall`;
    return this.authHttp.post(url, null)
      .map(res => res.json());
  }

}