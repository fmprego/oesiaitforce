import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { Login } from '../models/login';
import { PassChange } from './../models/passchange';

@Injectable()
export class RecoveryPasswordService {

  private readonly endpoint: string = '/api/pwdrecovery';

  constructor(
    private http: Http
  ) {}
 
  public mailRecoverPass(data: Login): Observable<any>  {
      return this.http.post(`${this.endpoint}/mailRecoverPass`, data).map(res => res.json());
  }

  public setNewPassword(data: PassChange): Observable<any> {
      return this.http.put(`${this.endpoint}/setnewpassword`, data).map(res => res.json());
  }
  
}