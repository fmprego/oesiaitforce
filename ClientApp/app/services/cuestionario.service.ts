import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Cuestionario } from '../models/cuestionario';
import { Http, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Nivel } from '../models/nivel';
import { Encuesta } from '../models/encuesta';

@Injectable()
export class CuestionarioService {

  private readonly endpoint: string = '/api/cuestionarios';

  constructor(private authHttp: AuthHttp, private http: Http) { }

  postCuestionario(cuestionario: Cuestionario): Observable<Cuestionario> {
    return this.http.post(this.endpoint, cuestionario)
      .map((res) => res.json());
  }

  getCuestionarios(encuestaId: number): Observable<CuestionariosResult> {

    const url = `${this.endpoint}/${encuestaId}`;
    return this.authHttp.get(url).map((res) => res.json());
  }

  getCuestionariosCSV(encuestaId: number): Observable<string[][]> {

    const url = `${this.endpoint}/${encuestaId}/getCSV`;
    return this.authHttp.get(url).map((res) => res.json());
  }
}

export interface CuestionariosResult {
  totalItems: number;
  items: Cuestionario[];
}
