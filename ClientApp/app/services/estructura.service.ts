import { LideresResult } from "./estructura.service";
import { Injectable } from "@angular/core";

import "rxjs/add/operator/map";
import { AuthHttp } from "angular2-jwt";
import { Observable } from "rxjs/Observable";
import { Lider } from "../models/lider";
import { Receptor } from "../models/receptor";
import { Http } from "@angular/http";


@Injectable()
export class EstructuraService {

  private readonly endpoint: string = "/api/estructuras";

  constructor(
    private authHttp: AuthHttp,
    private http: Http,
  ) { }

  public postFile(file: File, encuestaId: number): Observable<any> {
    const url = `${this.endpoint}/${encuestaId}`;
    const formData: FormData = new FormData();
    formData.append("file", file, file.name);
    return this.authHttp.post(url, formData)
      .map((res) => res.json());
  }
  public getLideres(id: number): Observable<LideresResult> {
    let url = `${this.endpoint}/${id}/lideres`;
    return this.authHttp.get(url)
      .map((res) => {            
        return res.json();
      });
  }
  public getTotalLideres(id: number): Observable<LideresResult> {
    const url = `${this.endpoint}/${id}/totallideres`;
    return this.authHttp.get(url)
      .map((res) => res.json());
  }

  public getReceptores(id: number): Observable<ReceptoresResult> {
    const url = `${this.endpoint}/${id}/receptores`;
    return this.authHttp.get(url)
      .map((res) => res.json());
  }

  public getTotalReceptores(id: number): Observable<ReceptoresResult> {
    const url = `${this.endpoint}/${id}/totalreceptores`;
    return this.authHttp.get(url)
      .map((res) => res.json());
  }

  public getNivel1(id: number): Observable<string[]> {
    let url = `${this.endpoint}/${id}/nivel1`;
    return this.http.get(url)
      .map((res) => {            
        return res.json();
      });
  }

  public getNivel(id: number, nivel1: string,
                  nivel2: string | null = null,
                  nivel3: string | null = null,
                  nivel4: string | null = null): Observable<any[]> {
    let url = `${this.endpoint}/${id}/nivel?nivel1=${encodeURIComponent(nivel1)}`;
    if (!!nivel2) {url += ("&nivel2=" + encodeURIComponent(nivel2)); }
    if (!!nivel3) {url += ("&nivel3=" + encodeURIComponent(nivel3)); }
    if (!!nivel4) { url += ("&nivel4=" + encodeURIComponent(nivel4)); }
    return this.http.get(url)
      .map((res) => res.json());
  }

}

export interface LideresResult {
  totalItems: number;
  items: Lider[];
}

export interface ReceptoresResult {
  totalItems: number;
  items: Receptor[];
}
