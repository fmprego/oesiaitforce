import { Observable } from "rxjs/Observable";
import { Encuesta } from "./../models/encuesta";
import { Injectable } from "@angular/core";
import { AuthHttp } from "angular2-jwt";
import { Informe } from "../models/informe";
import { Usuario } from "../models/usuario";
import { RequestOptions } from "@angular/http";

@Injectable()
export class InformeService {

  private readonly endpoint: string = "/api/informes";

  constructor(private authHttp: AuthHttp) {}

  public getInforme(id: number,global:boolean, nivel1: string | null = null, nivel2: string | null = null, nivel3: string | null = null, nivel4: string | null = null): Observable<Informe> {
    let url = `${this.endpoint}/${id}/${global}/nivel?`;
    if (!!nivel1) {url += ("&nivel1=" + encodeURIComponent(nivel1)); }
    if (!!nivel2) {url += ("&nivel2=" + encodeURIComponent(nivel2)); }
    if (!!nivel3) {url += ("&nivel3=" + encodeURIComponent(nivel3)); }
    if (!!nivel4) {url += ("&nivel4=" + encodeURIComponent(nivel4)); }

    return this.authHttp.get(url).map((res) => res.json());
  }

  public getInformeComparativa(id: number,global:boolean, nivel1: string | null = null, nivel2: string | null = null, nivel3: string | null = null, nivel4: string | null = null): Observable<Informe> {
    let url = `${this.endpoint}/${id}/${global}/comparativa/nivel?`;
    if (!!nivel1) {url += ("&nivel1=" + encodeURIComponent(nivel1)); }
    if (!!nivel2) {url += ("&nivel2=" + encodeURIComponent(nivel2)); }
    if (!!nivel3) {url += ("&nivel3=" + encodeURIComponent(nivel3)); }
    if (!!nivel4) {url += ("&nivel4=" + encodeURIComponent(nivel4)); }

    return this.authHttp.get(url).map((res) => res.json());
  }

  public generarInformes(encuestaId: number): Observable<Informe[]> {
    return this.authHttp.post(`${this.endpoint}/${encuestaId}`, undefined)
      .map((res) => res.json());
  }

  public getInformeTipos(ids: number[]): Observable<Informe> {
    var queryparams = { idsEncuestas: ids};
    let url = `${this.endpoint}/tipos?ids=${ids}`;
    return this.authHttp.get(url).map((res) => res.json());
  }
}
