import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Geografia } from '../models/geografia';

@Injectable()
export class GeografiaService {

  private readonly endpoint: string = '/api/geografias';

  constructor(private authHttp: AuthHttp) {}

  getGeografias(): Observable<Geografia[]> {
    return this.authHttp.get(this.endpoint).map(res => res.json());
  }

  getGeografiasSimple(): Observable<Geografia[]> {
    return this.authHttp.get(`${this.endpoint}?simple=true`).map(res => res.json());
  }

  getGeografia(id: number): Observable<Geografia> {
    return this.authHttp.get(`${this.endpoint}/${id}`)
      .map(res => res.json());
  }

  deleteGeografia(id: number) {
    return this.authHttp.delete(`${this.endpoint}/${id}`)
      .map(res => res.json());
  }

  createGeografia(geografia: Geografia): Observable<Geografia> {
    return this.authHttp.post(this.endpoint, geografia)
      .map(res => res.json());
  }

  updateGeografia(geografia: Geografia): Observable<Geografia> {
    return this.authHttp.put(`${this.endpoint}/${geografia.id}`, geografia)
      .map(res => res.json());
  }
}