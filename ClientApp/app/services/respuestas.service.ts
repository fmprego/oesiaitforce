import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Respuesta } from '../models/respuesta';

@Injectable()
export class RespuestaService {

  private readonly endpoint: string = '/api/respuestas';

  constructor(private authHttp: AuthHttp) { }

  getRespuestasTexto(encuestaId: number, preguntaId: number,nivel1:string,nivel2:string,nivel3:string,nivel4:string): Observable<Respuesta[]> {
    let url = `${this.endpoint}?encuesta=${encuestaId}&pregunta=${preguntaId}`;
    if (!!nivel1) {url += ("&nivel1=" + encodeURIComponent(nivel1)); }
    if (!!nivel2) {url += ("&nivel2=" + encodeURIComponent(nivel2)); }
    if (!!nivel3) {url += ("&nivel3=" + encodeURIComponent(nivel3)); }
    if (!!nivel4) {url += ("&nivel4=" + encodeURIComponent(nivel4)); }
    return this.authHttp.get(url)
      .map(res => res.json());
  }
}