import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate } from '@angular/router/src/interfaces';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    protected auth: AuthService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {}

  canActivate(): boolean {
    if (this.auth.isAuthenticated()) {
      return true;
    }
    this.router.navigate(['/login']);
    this.snackBar.open('Necesitas estar identificado en el sistema para acceder.','Ruta incorrecta',{duration:5000,panelClass:['red-snackbar']});

    return false;
  }
}