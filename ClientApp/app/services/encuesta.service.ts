import { EncuestasResult } from "./encuesta.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { AuthHttp } from "angular2-jwt";

import { Encuesta, IdiomaEncuesta } from "../models/encuesta";
import { Categoria } from "../models/categoria";
import { Pregunta } from "../models/pregunta";
import { EncuestaPregunta } from "../models/encuesta-pregunta";
import { Http } from "@angular/http";

import { TranslateService } from '@ngx-translate/core'; 

@Injectable()
export class EncuestaService {

  private readonly endpoint: string = "/api/encuestas";

  constructor(
    private authHttp: AuthHttp,
    private http: Http,
    private translate: TranslateService,
  ) { }

  public getIdiomas(): Observable<IdiomaEncuesta[]> {
    return this.http.get(`${this.endpoint}/idiomas/${this.translate.currentLang}`)
      .map((res) => res.json());
  }  

  public getEncuestas(): Observable<EncuestasResult> {
    return this.authHttp.get(this.endpoint)
      .map((res) => res.json());
  }

  public getEncuestasParaInforme(): Observable<EncuestasResult> {
    return this.authHttp.get(`${this.endpoint}/paraInforme`)
      .map((res) => res.json());
  }

  public getEncuestasCerradas(): Observable<EncuestasResult> {
    return this.authHttp.get(`${this.endpoint}/cerradas`)
      .map((res) => res.json());
  }

  public getPendientes(): Observable<EncuestasResult> {
    return this.authHttp.get(`${this.endpoint}/pendientes`)
      .map((res) => res.json());
  }

  public getEncuesta(id: number, includeRelated: boolean = false): Observable<Encuesta> {
    let url = `${this.endpoint}/${id}`;
    if (includeRelated) { url += "?includeRelated=true"; }
    return this.http.get(url)
      .map((res) => {            
        return res.json();
      });
  }

  public createEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.post(this.endpoint, encuesta)
      .map((res) => res.json());
  }

  public updateEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.put(`${this.endpoint}/${encuesta.id}`, encuesta)
      .map((res) => res.json());
  }

  public encolarEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.post(`${this.endpoint}/${encuesta.id}/encolar`, encuesta)
      .map((res) => res.json());
  }

  public aprobarEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.post(`${this.endpoint}/${encuesta.id}/aprobar`, encuesta)
      .map((res) => res.json());
  }

  public activarEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.post(`${this.endpoint}/${encuesta.id}/activar`, encuesta)
      .map((res) => res.json());
  }

  public delete(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.delete(`${this.endpoint}/${encuesta.id}`)
      .map((res) => res.json());
  }

  public updateCategorias(encuesta: Encuesta, categorias: Categoria[]) {
    const url = `${this.endpoint}/${encuesta.id}/addcategorias`;
    return this.authHttp.post(url, categorias).map((res) => res.json());
  }

  public updatePreguntas(encuesta: Encuesta, preguntas: Pregunta[]): Observable<Encuesta> {
    return this.authHttp.post(`${this.endpoint}/${encuesta.id}/preguntas`, preguntas)
      .map((res) => res.json());
  }

  public getEncuestaPreguntas(encuestaId: number): Observable<EncuestaPregunta[]> {
    return this.authHttp.get(`${this.endpoint}/${encuestaId}/ep`)
      .map((res) => res.json());
  }

  public updateEncuestaPreguntas(encuestaId: number, encuestaPreguntas: EncuestaPregunta[]): Observable<boolean> {
    return this.authHttp.post(`${this.endpoint}/${encuestaId}/ep`, encuestaPreguntas)
      .map((res) => res.json());
  }

  public cerrarEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.post(`${this.endpoint}/${encuesta.id}/cerrar`, encuesta)
      .map((res) => res.json());
  }

  public pasoHistoricoEncuesta(encuesta: Encuesta): Observable<Encuesta> {
    return this.authHttp.post(`${this.endpoint}/${encuesta.id}/pasohistorico`, encuesta)
       .map((res) => res.json());
    }

    public copiarEncuesta(encuesta: Encuesta): Observable<Encuesta> {
        return this.authHttp.post(`${this.endpoint}/${encuesta.id}/copiaEncuesta`, encuesta)
            .map((res) => res.json());
    }
}



export interface EncuestasResult {
  totalItems: number;
  items: Encuesta[];
}
