import { Http } from "@angular/http";
import { PreguntasResult } from "./preguntas.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { AuthHttp } from "angular2-jwt";

import { Pregunta, TipoPregunta } from "./../models/pregunta";
import { Encuesta } from "./../models/encuesta";
import { Opcion } from "./../models/opcion";
import { EncuestaPregunta } from "../models/encuesta-pregunta";

@Injectable()
export class PreguntaService {

  private readonly endpoint: string = "/api/preguntas";

  constructor(
    private authHttp: AuthHttp,
    private http: Http,
  ) { }

  public getPreguntas(): Observable<PreguntasResult> {
    return this.authHttp.get(this.endpoint)
      .map((res) => res.json());
  }

  public getPregunta(id: number): Observable<Pregunta> {
    const url = `${this.endpoint}/${id}`;
    return this.authHttp.get(url)
      .map((res) => res.json());
  }

  public create(pregunta: Pregunta): Observable<Pregunta> {
    const { tipoPregunta } = pregunta;
    const data = {
      ...pregunta,
      tipoPregunta: tipoPregunta.id,
    };
    return this.authHttp.post(this.endpoint, data)
      .map((res) => res.json());
  }

  public update(pregunta: Pregunta): Observable<Pregunta> {
    const { tipoPregunta } = pregunta;
    const data = {
      ...pregunta,
      tipoPregunta: tipoPregunta.id,
    };
    return this.authHttp.put(`${this.endpoint}/${pregunta.id}`, data)
      .map((res) => res.json());
  }

  public delete(pregunta: Pregunta): Observable<Pregunta> {
    return this.authHttp.delete(`${this.endpoint}/${pregunta.id}`)
      .map((res) => res.json());
  }

  public getTipos(): Observable<TipoPregunta> {
    return this.authHttp.get(`${this.endpoint}/tipos`)
      .map((res) => res.json());
  }

  public getByEncuestaId(encuestaId: number): Observable<PreguntasResult> {
    return this.http.get(`${this.endpoint}/byencuesta/${encuestaId}`)
      .map((res) => res.json());
  }

  public getByCategorias(categorias: number[]): Observable<PreguntasResult> {
    let queryStr = "categorias=";
    categorias.forEach((it) => queryStr += `${it}&categorias=`);
    return this.authHttp.get(`${this.endpoint}?${queryStr}`)
      .map((res) => res.json());
  }

  public copyPregunta(pregunta: Pregunta): Observable<Pregunta> {
    return this.authHttp.post(`${this.endpoint}/${pregunta.id}/copiaPregunta`, pregunta)
        .map((res) => res.json());
  }

  public getEncuestasByPregunta(preguntaId: number): Observable<Encuesta> {
    return this.http.get(`${this.endpoint}/getencuestasbypregunta/${preguntaId}`)
      .map((res) => res.json());
  }
}

export interface PreguntasResult {
  totalItems: number;
  items: Pregunta[];
}
