import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Geografia } from '../models/geografia';
import { TipoEncuesta } from '../models/tipoencuesta';

@Injectable()
export class TipoEncuestaService {

  private readonly endpoint: string = '/api/tipoencuesta';

  constructor(private authHttp: AuthHttp) {}

  getTiposEncuestaAll(): Observable<TipoEncuesta[]> {
    return this.authHttp.get(`${this.endpoint}/all`).map(res => res.json());
  }

  getTiposEncuesta(): Observable<TipoEncuesta[]> {
    return this.authHttp.get(this.endpoint).map(res => res.json());
  }

  getTipoEncuesta(id: number): Observable<TipoEncuesta> {
    return this.authHttp.get(`${this.endpoint}/${id}`)
      .map(res => res.json());
  }

  deleteTipoEncuesta(id: number) {
    return this.authHttp.delete(`${this.endpoint}/${id}`)
      .map(res => res.json());
  }

  createTipoEncuesta(tipo: TipoEncuesta): Observable<TipoEncuesta> {
    return this.authHttp.post(this.endpoint, tipo)
      .map(res => res.json());
  }

  updateTipoEncuesta(tipo: TipoEncuesta): Observable<TipoEncuesta> {
    return this.authHttp.put(`${this.endpoint}/${tipo.id}`, tipo)
      .map(res => res.json());
  }
}