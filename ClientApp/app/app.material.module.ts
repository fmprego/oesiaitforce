import { AltaLideresDialog } from "./components/encuestas/estructura/dialogs/alta-lideres.dialog";
import { AprobacionDialog } from "./components/revisiones/dialogs/aprobacion.dialog";
import { GenericDialog } from "./components/app/dialogs/generic.dialog";
import { NgModule } from "@angular/core";

import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatOptionModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSnackBarModule,
  MatSidenavModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from "@angular/material";

import { PaginatorIntl } from "./components/common/paginator-intl";
import { GeografiaDeleteDialog } from "./components/geografias/dialogs/geografia-delete-dialog";
import { GeografiaDeleteDialogForbidden } from "./components/geografias/dialogs/geografia-delete-dialog-forbidden";
import { TipoEncuestaDeleteDialog } from "./components/tipoencuesta/dialogs/tipoencuesta-delete-dialog";
import { TipoEncuestaDeleteDialogForbidden } from "./components/tipoencuesta/dialogs/tipoencuesta-delete-dialog-forbidden";
import { PreguntaDeleteDialog } from "./components/categorias/dialogs/pregunta-delete-dialog";
import { CategoriaDeleteDialogForbidden } from "./components/categorias/dialogs/categoria-delete-dialog-forbidden";
import { UnsavedChangesDialog } from "./components/common/unsaved-changes-dialog.component";
import { LoadEstructuraNoFileDialog } from "./components/encuestas/estructura/dialogs/load-estructura-no-file.dialog";
import { GenericErrorDialog } from "./components/common/dialog/generic-error.dialog";
import { ActivacionDialog } from "./components/encuestas/dialogs/activacion.dialog";
import { SendMailDialog } from "./components/encuestas/dialogs/send-mail.dialog";
import { ModificarEncuestaActivaDialog } from './components/encuestas/dialogs/modificar-encuesta-activa.dialog';
import { LinkEncuestaDialog } from "./components/encuestas/dialogs/link-encuesta.dialog";
import { EncuestaDeleteDialog } from "./components/encuestas/dialogs/encuesta-delete.dialog";
import { AdminDeleteDialog } from "./components/admin/dialog/admin-delete.dialog";
import { RespuestasTextoDialog } from "./components/informes/dialogs/respuestas-texto.dialog";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatGridListModule,
    MatListModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatStepperModule,
    MatSliderModule,
    MatOptionModule,
    MatRadioModule,
    MatTabsModule,
    MatChipsModule,
    MatSnackBarModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatTooltipModule,
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: PaginatorIntl },
  ],

  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatGridListModule,
    MatListModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatStepperModule,
    MatSliderModule,
    MatOptionModule,
    MatRadioModule,
    MatTabsModule,
    MatChipsModule,
    MatSnackBarModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatTooltipModule,
  ],
  entryComponents: [
    GeografiaDeleteDialog,
    GeografiaDeleteDialogForbidden,
    TipoEncuestaDeleteDialog,
    TipoEncuestaDeleteDialogForbidden,    
    PreguntaDeleteDialog,
    CategoriaDeleteDialogForbidden,
    UnsavedChangesDialog,
    LoadEstructuraNoFileDialog,
    GenericErrorDialog,
    AltaLideresDialog,
    AprobacionDialog,
    ActivacionDialog,
    SendMailDialog,
    ModificarEncuestaActivaDialog,
    LinkEncuestaDialog,
    EncuestaDeleteDialog,
    AdminDeleteDialog,
    GenericDialog,
    RespuestasTextoDialog,
  ],
})
export class MaterialModule {}
