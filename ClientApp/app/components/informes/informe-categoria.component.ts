import { ResultadoPregunta } from './../../models/informe';
import { Categoria } from './../../models/categoria';
import { Component, Input, OnChanges } from '@angular/core';
import { Informe, ResultadoCategoria } from '../../models/informe';
import { Pregunta, TipoPregunta } from '../../models/pregunta';
import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-informe-categoria',
  templateUrl: 'informe-categoria.component.html',
  styleUrls: ['informe-categoria.component.css'],
})

export class InformeCategoriaComponent implements OnChanges {

  @Input() informe: Informe;
  @Input() categoria: Categoria;
  resultadoPreguntas: ResultadoPregunta[] = [];
  resultadoCategoria: number;
  mostrarBarra:boolean = true;
  informeresultado: string;

  categoriaChartData: any = {
    type: 'horizontalBar',
    categorias: [],
    datasets: [],
    legend: false,
    colors: [],
    options: {
      scaleShowVerticalLines: false,
      legend: {
        position: 'right',
      },

      scales: {
        xAxes: [{
          ticks: {
            min: 0,
            max: 100,
          }
        }],
      },
    },
  };

  constructor(public authService: AuthService,
    private translate: TranslateService) { }

  public ngOnInit() {
    this.setIdioma();

    this.translate.get('informes.resultado').subscribe(translation => {
      this.informeresultado = translation;      
    });      

  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  ngOnChanges() {
    if (this.informe != undefined)
    {
      this.setResultadoCategoria();
      var resultadoAux = this.informe.resultadoPreguntas
        .filter(rp => rp.categoriaId == this.categoria.id);
      this.resultadoPreguntas = resultadoAux.filter(rp => rp.tipoPregunta == 'Seleccion');
      this.resultadoPreguntas = this.resultadoPreguntas.concat(resultadoAux.filter(rp => rp.tipoPregunta == 'Escala'));
      if(this.resultadoPreguntas.length==0){
        this.mostrarBarra=false;
      }else {this.mostrarBarra=true;}
      this.resultadoPreguntas = this.resultadoPreguntas.concat(resultadoAux.filter(rp => rp.tipoPregunta == 'Texto'));
      this.resultadoPreguntas = this.resultadoPreguntas.concat(resultadoAux.filter(rp => rp.tipoPregunta == 'Comentario'));
    }
}

  setResultadoCategoria() {

    const rc = this.informe.resultadoCategorias
      .filter(rc => rc.categoriaId === this.categoria.id)[0]
    const result: number = (!rc) ? 0 : parseFloat((rc.valor * 100).toFixed(2));
    this.resultadoCategoria = result;
    this.drawCategoriaChart();
    
  }

  drawCategoriaChart() {
    const categorias: string[] = [this.categoria.nombre]
    const valores: number[] = [this.resultadoCategoria];
    const backgroundColor = ['#00a68b'];
    this.categoriaChartData.colors.push({ backgroundColor });
    this.categoriaChartData.categorias = categorias;
    this.categoriaChartData.datasets = [{
      label: this.informeresultado,
      data: valores,
    }];
  }

}