import { Nivel } from './../../models/nivel';
import { Receptor } from './../../models/receptor';
import { EstructuraService } from './../../services/estructura.service';
import { Usuario } from './../../models/usuario';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { InformeService } from "../../services/informe.service";
import { Encuesta } from "../../models/encuesta";
import { Informe, InformeInfo } from "../../models/informe";
import { EncuestaService } from "../../services/encuesta.service";
import { MatSnackBar } from "@angular/material";
import { CuestionarioService, CuestionariosResult } from "../../services/cuestionario.service";
import { Cuestionario } from "../../models/cuestionario";
import { Observable } from "rxjs/Observable";
import { forkJoin } from "rxjs/observable/forkJoin";
import { Lider } from '../../models/lider';
import { Persona } from '../../models/persona';
import { relativeTimeThreshold } from '../../../../node_modules/moment';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: "app-informe-selector",
  templateUrl: "informe-selector.component.html",
  styleUrls: ["informe-selector.component.css"],
})

export class InformeSelectorComponent implements OnInit {

  @Input() public encuesta: Encuesta;
  @Output() public onSelect = new EventEmitter<InformeInfo>();
  public nivel1: string[] = [];
  public nivel2: string[] = [];
  public nivel3: string[] = [];
  public nivel4: string[] = [];
  public selectedNivel1: string;
  public selectedNivel2: string;
  public selectedNivel3: string;
  public selectedNivel4: string;
  public loading: boolean = false;
  public lideres: Lider[];
  public lider: Lider;
  public esLider: Boolean;

  constructor(
    private informeService: InformeService,
    private encuestaService: EncuestaService,
    private estructuraService: EstructuraService,
    public snackBar: MatSnackBar,
    public authService: AuthService,
    private translate: TranslateService
  ) { }

  public ngOnInit() {
    this.setIdioma();
    this.loading = true;
    
    this.estructuraService.getLideres(this.encuesta.estructuraId).subscribe((lideres) => {    
      this.lideres = lideres.items;
      this.estructuraService.getNivel1(this.encuesta.estructuraId).subscribe((nivel) => { 
        this.nivel1 = nivel;
        this.esLider = !this.authService.isMaster() && !this.authService.isAdmin() && this.authService.isLider();
        if (this.esLider) {
          const email = this.authService.getCurrentEmail().toUpperCase();
          this.lider = this.lideres.filter(l => l.email.toUpperCase() == email)[0];
          if (this.lider.nivel1 != null && this.lider.nivel1 != "") {
            this.nivel1 = [this.lider.nivel1];
            this.selectedNivel1 = this.lider.nivel1;
            this.onNivel1Change();
          }
          
          this.populateSelector();
          this.loading = false;
        } else {
          this.populateSelector();
          this.loading = false;
        }
      });
    });
    
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  public populateSelector() {

  }

  public onNivel1Change() {
    if(this.selectedNivel1==null){      
      this.nivel2 = [];
      this.nivel3 = [];
      this.nivel4 = [];
    }else{
    if (this.esLider && (this.lider.nivel2 != null && this.lider.nivel2 != "")) {
      this.nivel2 = [this.lider.nivel2];
      this.selectedNivel2 = this.lider.nivel2;
      this.onNivel2Change();
    } else {      
    this.estructuraService.getNivel(this.encuesta.estructuraId, this.selectedNivel1)
      .subscribe(niveles => {
          this.nivel2 = niveles.map(x => x.nivel2).filter(it => it != null).filter(it => it != "")
            .filter((val, idx, self) => self.indexOf(val) === idx)
        this.selectedNivel2 = null;
        this.selectedNivel3 = null;
        this.selectedNivel4 = null;
        this.nivel3 = [];
        this.nivel4 = [];
      });
    }
  }
  }

  public onNivel2Change() {
    if(this.selectedNivel2==null){
      this.nivel3 = [];
      this.nivel4 = [];
    }else{
    if (this.esLider && (this.lider.nivel3 != null && this.lider.nivel3 != "")) {
      this.nivel3 = [this.lider.nivel3];
      this.selectedNivel3 = this.lider.nivel3;
      this.onNivel3Change();
    } else {      
    this.estructuraService.getNivel(this.encuesta.estructuraId, this.selectedNivel1, this.selectedNivel2)
      .subscribe(niveles => {
          this.nivel3 = niveles.map(x => x.nivel3)
            .filter(it => it != null).filter(it => it != "")
            .filter((val, idx, self) => self.indexOf(val) === idx);
       
        this.selectedNivel3 = null;
        this.selectedNivel4 = null;
        this.nivel4 = [];
      });
    }
  }
  }

  public onNivel3Change() {
    if(this.selectedNivel3==null){
      this.nivel4 = [];
    }else{
    if (this.esLider && (this.lider.nivel4 != null && this.lider.nivel4 != "")) {
      this.nivel4 = [this.lider.nivel4];
      this.selectedNivel4 = this.lider.nivel4;
    } else {

    this.estructuraService.getNivel(this.encuesta.estructuraId, this.selectedNivel1, this.selectedNivel2, this.selectedNivel3)
      .subscribe(niveles => {
          this.nivel4 = niveles.map(x => x.nivel4).filter(it => it != null).filter(it => it != "")
            .filter((val, idx, self) => self.indexOf(val) === idx);
            this.selectedNivel4 = null;
      });
    }
    }
  }

  public loadInforme() {
    var informeInfo = new InformeInfo();

    informeInfo.encuestaId = this.encuesta.id;
    informeInfo.global = this.encuesta.global;
    informeInfo.nivel1 = this.selectedNivel1;
    informeInfo.nivel2 = this.selectedNivel2;
    informeInfo.nivel3 = this.selectedNivel3;
    informeInfo.nivel4 = this.selectedNivel4;

    this.onSelect.emit(informeInfo);

    /*
    this.informeService.getInforme(this.encuesta.id, this.encuesta.global, this.selectedNivel1, this.selectedNivel2, this.selectedNivel3, this.selectedNivel4)
      .subscribe((informe2) => {
        informe2.nivel1 = this.selectedNivel1;
        informe2.nivel2 = this.selectedNivel2;
        informe2.nivel3 = this.selectedNivel3;
        informe2.nivel4 = this.selectedNivel4;
        this.onSelect.emit(informe2);
      },
        (err) => 
        {
          var texto = "";
          this.translate.get('informes.confidencialidad').subscribe(translation => {
            texto = translation;        
          });    

          this.snackBar.open(texto, 'Error', { duration: 5000, panelClass: ['red-snackbar'] });
        } 
      );*/
  }

  public noInformeSnack() {
    var texto = "";
    var titulo = "";
    this.translate.get('informes.nivelvalido').subscribe(translation => {
      texto = translation;        
    });      
    this.translate.get('informes.ok').subscribe(translation => {
      titulo = translation;        
    });            
    this.snackBar.open(texto, titulo, { duration: 2000 });
  }

  public getInformeByNivel(): Informe {
    return this.encuesta.informes
      .filter((i) => i.nivel1 === this.selectedNivel1 || i.nivel1 === null)
      .filter((i) => i.nivel2 === this.selectedNivel2 || i.nivel2 === null)
      .filter((i) => i.nivel3 === this.selectedNivel3 || i.nivel3 === null)
      .filter((i) => i.nivel4 === this.selectedNivel4 || i.nivel4 === null)[0];
  }
}
