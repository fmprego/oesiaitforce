import { Categoria } from './../../models/categoria';
import { Lider } from './../../models/lider';
import { Usuario } from './../../models/usuario';
import { Participante } from './../../models/participantes';
import { Nivel } from './../../models/nivel';
import { ReceptoresResult,LideresResult } from './../../services/estructura.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { merge } from 'rxjs/observable/merge';
import { async } from '@angular/core/testing';
import { CuestionarioService, CuestionariosResult } from './../../services/cuestionario.service';
import { InformeService } from './../../services/informe.service';
import { ResultadoCategoria, ResultadoNivel } from './../../models/informe';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Encuesta } from '../../models/encuesta';
import { Informe, InformeInfo } from '../../models/informe';
import { EncuestaService } from '../../services/encuesta.service';
import { AuthService } from '../../services/auth.service';
import { EstructuraService } from '../../services/estructura.service';
import { Observable } from 'rxjs/Observable';
import { Persona } from '../../models/persona';
import {TranslateService} from '@ngx-translate/core';
import { MatSnackBar } from "@angular/material";




@Component({
  selector: 'app-informetiposgrafico-component',
  templateUrl: 'informe-tipos-grafico.component.html',
  styleUrls: ['informe-tipos-grafico.component.css'],
})

export class InformeTiposGraficoComponent implements OnInit {

  model: number [];
  informe: Informe;
  loading: boolean = false;
  loadingDiv: boolean = false;
  cuestionarios: CuestionariosResult;
  informeresultado: string;
  categoriasChartData : any;


  constructor(
    private encuestaService: EncuestaService,
    private estructuraService: EstructuraService,
    private cuestionarioService: CuestionarioService,
    private informeService: InformeService,
    private authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
  ) {
    route.queryParams.subscribe((p) => {
      this.model = p.encuestas ? p.encuestas : null;
    });
    this.crearCategoriasChartData();
  }

  ngOnInit() {    
    this.setIdioma();
    this.translate.get('informes.activacion').subscribe(translation => {
      this.categoriasChartData.options.title.text = translation;      
    });     

    this.translate.get('informes.resultado').subscribe(translation => {
      this.informeresultado = translation;      
    });   
    
    this.loading = true;
    
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  public ngAfterViewInit() {    
      this.informeSelected(null);    
    // this.encuestaService.getEncuesta(this.model.id, true).subscribe((encuesta) => {      
    //   encuesta.global = this.model.global;
    //   this.model = encuesta;
    //   var categoriaPreguntas = this.model.preguntas.map(p=> p.categoriaId);
    //   this.model.categorias = this.model.categorias.filter(x=> categoriaPreguntas.indexOf(x.id)!=-1)   
      this.loading = false;   
      this.loadingDiv = true;
    // });
  }

  drawCategoriasChart(informe: Informe) {
    this.loading = true;
    const rcs: ResultadoCategoria[] = informe.resultadoCategorias;
    const rn : ResultadoNivel [] = informe.resultadoNiveles;
    const categorias: string[] = rn[rn.length-1].categorias;
    // const backgroundColor = rcs.map((it) => '#00a68b');
    // const backgroundColor1 = rcs.map((it) => '#a0036e');
    // const backgroundColor2 = rcs.map((it) => '#023189');
    // const backgroundColor3 = rcs.map((it) => '#774601');
    // const backgroundColor4 = rcs.map((it) => '#015b15');
    // this.categoriasChartData.colors.push(backgroundColor);
    // this.categoriasChartData.colors.push(backgroundColor1);
    // this.categoriasChartData.colors.push(backgroundColor2);
    // this.categoriasChartData.colors.push(backgroundColor3);
    // this.categoriasChartData.colors.push(backgroundColor4);
    this.categoriasChartData.categorias = categorias;
    this.cargarDataSet (rn, informe); 
    this.loading = false;
  }

  borrarDataSet(){
    this.loading = true;
    this.categoriasChartData.datasets=[];
    this.loading = false;
  }

  cargarDataSet (results : ResultadoNivel[], informe: Informe){
    let data : any = [];
    results.forEach(function (value) {
      let valores: number[] = value.valores; 
      data.push({
              label: value.nombreEncuesta,   
              data: valores,});
      console.log(data);
  
    });
    this.categoriasChartData.datasets = data;
          
  }

  informeSelected(informeInfo: InformeInfo) {
    this.loading = true;
    this.informeService.getInformeTipos(this.model)
      .subscribe((informe2) => {
        
        
        this.informe = informe2;

        this.drawCategoriasChart(this.informe);

        this.loading = false;          
      },
        (err) => 
        {
          var texto = "";
          this.translate.get('informes.confidencialidad').subscribe(translation => {
            texto = translation;        
          });    

          this.snackBar.open(texto, 'Error', { duration: 5000, panelClass: ['red-snackbar'] });
          this.borrarDataSet();
          this.loading = false;
        } 
      );      
  }

  crearCategoriasChartData (){
    this.categoriasChartData= {
      type: 'horizontalBar',
      categorias: [],
      datasets: [],
      legend: true,
      colors: [],
      
      options: {
        scaleShowVerticalLines: false,
        legend: {
          display: true,
          position: 'right',
          onClick: function(){},
        },
        title: {
          display: true,
          text: 'Aceptación (%)',
        },
        scales: {
          xAxes: [{
            ticks: {
              min: 0,
              max: 100,
            },
          }],
        },
        animation: {
          onComplete: function () {
            var chartInstance = this.chart,
            ctx = chartInstance.ctx;
            ctx.textAlign = 'center';
            ctx.font="15px Arial";
            ctx.textBaseline = 'top';
            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                var data = dataset.data[index]== 0 ? "N/A" : dataset.data[index]+"%";
                ctx.fillText(data, (dataset.data[index]>80)?bar._model.x-300:bar._model.x+35, bar._model.y-8);
              });
            });
          }
        },
      },
    };
  }

  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  public back() {
    this.router.navigate(["/informestipo"]);
  }
}
