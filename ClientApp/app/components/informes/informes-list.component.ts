import { Cuestionario } from './../../models/cuestionario';
import { InformeService } from "./../../services/informe.service";
import { GenericDialog } from "./../app/dialogs/generic.dialog";
import { Component, ViewChild, OnInit } from "@angular/core";
import { MatTableDataSource, MatSort, MatPaginator, MatPaginatorIntl, MatDialog, MatSnackBar } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";

import { merge } from "rxjs/observable/merge";
import "rxjs/add/operator/map";
import { startWith } from "rxjs/operators/startWith";
import { switchMap } from "rxjs/operators/switchMap";
import { Router, ActivatedRoute } from "@angular/router";

import { EncuestaService, EncuestasResult } from "./../../services/encuesta.service";
import { Encuesta, EstadoEncuesta } from "../../models/encuesta";
import { Observable } from "rxjs/Observable";
import { CuestionariosResult, CuestionarioService } from "../../services/cuestionario.service";
import {saveAs} from "file-saver";

import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: "app-informes-list",
  templateUrl: "informes-list.component.html",
  styleUrls: ["./informes-list.component.css"],
})

export class InformesListComponent implements OnInit {

  public displayedColumns = ["titulo", "estado", "descripcion", "administrador", "geografia", "actions"];
  public dataSource = new MatTableDataSource<Encuesta>([]);
  public totalItems: number = 0;
  public loading: boolean = false;
  public cerradas: boolean = false;
  informefichero: string = "";

  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  constructor(
    private encuestaService: EncuestaService,
    private informeService: InformeService,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private  cuestionarioService: CuestionarioService,
    private authService: AuthService,
    private translate: TranslateService
  ) {
  }

  public ngOnInit() {
    this.setIdioma();    

    this.translate.get('informeslista.informefichero').subscribe(translation => {
      this.informefichero = translation
    });

    this.setIdiomaPaginador();

    this.loading = true;
  }

  public ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.populateList();
  }

  public populateList() {
    this.loading = true;
    let result$: Observable<EncuestasResult>;
    result$ = this.encuestaService.getEncuestasParaInforme();
    result$.subscribe((data) => {
    data.items.map(e => 
      { if(e.titulo.startsWith("GLOBAL")) {
          e.global=true;
        };

        this.traducirestado(e.estado);
      })
    this.dataSource.data = data.items;
    this.totalItems = this.dataSource.data.length;
    this.loading = false;
    });
  }

  traducirestado(estado: EstadoEncuesta)
  {

    // Creada : 0, Pendiente : 1, Preactiva : 2, Activa : 3, Cerrada : 4, Historico : 5
    switch(estado.id) { 
      case 0: { 
        this.translate.get('estadoencuesta.creada').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 1: { 
        this.translate.get('estadoencuesta.pendiente').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 2: { 
        this.translate.get('estadoencuesta.preactiva').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 3: { 
        this.translate.get('estadoencuesta.activa').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      }  
      case 4: { 
        this.translate.get('estadoencuesta.cerrada').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 5: { 
        this.translate.get('estadoencuesta.historico').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      }                 
      default: { 
         //statements; 
         break; 
      } 
   }     
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  public gotoinforme(encuesta: Encuesta) {
    this.router.navigate([`/informes/${encuesta.id}`]);
  }

  public gotoinformecomparativa(encuesta: Encuesta) {
    this.router.navigate([`/informescomparativa/${encuesta.id}`]);
  }

  public getInformeCsv(informeCsv: Encuesta) {
    this.loading = true;
    let result$: Observable<string[][]>;
    result$ = this.cuestionarioService.getCuestionariosCSV(informeCsv.id);
    result$.subscribe((data) => {
      let itemsCsv = data;
      if(itemsCsv.length != 0) {
        this.csvExport(itemsCsv);
      }else if (itemsCsv.length === 0) { 
        var texto = "";
        var titulo = "";
        this.translate.get('informeslista.nohaydatos').subscribe(translation => {
          texto = translation;        
        });  
        this.translate.get('informeslista.aviso').subscribe(translation => {
          titulo = translation;        
        });          

        this.snackBar.open(texto, titulo,{duration:5000});
        this.loading = false; 
      }
    });
    this.loading = false;
  }

  public csvExport(data: string[][]) {
    let items = data;
    const replacer = (key:string , value:string) => value === null ? '' : value
    let csv: string[] = items.map(row => row.join(";"));
    csv = [csv.join('\r\n')];
    let filename = this.informefichero + "_" + Date.now() + ".csv";
    let blob = new Blob(csv, {type: "text/csv; charset=utf-8"});
    let filesaver = saveAs(blob, filename);
  }

  public showError(err: Error) {
    // TODO console.error(err);
    this.loading = false;
    var texto = "";
    var titulo = "";

    this.translate.get('informeslista.error').subscribe(translation => {
      texto = translation;        
    }); 

    this.translate.get('informeslista.ok').subscribe(translation => {
      titulo = translation;        
    });  

    this.snackBar.open(texto,titulo,
      { duration: 2000 },
    );
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
}
