import { Respuesta } from './../../../models/respuesta';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AuthService } from './../../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'respuestas-texto-dialog',
  templateUrl: 'respuestas-texto.dialog.html'
})

export class RespuestasTextoDialog {
  
  constructor(
    public authService: AuthService,
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dialogRef: MatDialogRef<RespuestasTextoDialog>    
  ) { }

  public ngOnInit() {
    this.setIdioma();
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
} 