import { Participante } from './../../models/participantes';
import { ResultadoPregunta } from './../../models/informe';
import { Categoria } from './../../models/categoria';
import { Component, Input, OnInit } from '@angular/core';
import { Informe, ResultadoCategoria } from '../../models/informe';
import { Pregunta, TipoPregunta } from '../../models/pregunta';
import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-informe-participacion',
  templateUrl: 'informe-participacion.component.html',
  styleUrls: ['informe-participacion.component.css'],
})

export class InformeParticipacionComponent implements OnInit {

  @Input() participante: Participante;
  cuestionariossinrealizarLabel:string;
  cuestionariosrealizadosLabel:string;

  participantesChartData: any = {
    type: 'pie',
    unidades: [], 
    participantes: [],
    options: {
      legend: {
        display: true,
      },
    },
    onAnimationComplete: function () {

        var ctx = this.chart.ctx;
        ctx.font = this.scale.font;
        ctx.fillStyle = this.scale.textColor
        ctx.textAlign = "center";
        ctx.textBaseline = "bottom";

        this.datasets.forEach(function (dataset) {
            dataset.bars.forEach(function (bar) {
                ctx.fillText(bar.value, bar.x, bar.y - 5);
            });
        })
  }
}

  
  constructor(public authService: AuthService,
    private translate: TranslateService) { }

  ngOnInit() {
    this.setIdioma();
     this.setResultadoParticipacion();
    
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);

      this.translate.get('informes.cuestionariossinrealizar').subscribe(translation => {
        this.cuestionariossinrealizarLabel = translation
      });
      this.translate.get('informes.cuestionariosrealizados').subscribe(translation => {
        this.cuestionariosrealizadosLabel = translation
      });      

      this.participantesChartData.unidades.push(this.cuestionariossinrealizarLabel);
      this.participantesChartData.unidades.push(this.cuestionariosrealizadosLabel);      
  }

  setResultadoParticipacion() {
    this.drawParticipantesChart();
  }

  drawParticipantesChart() {    
    this.participantesChartData.participantes.push([this.participante.participantes, this.participante.cuestionariosRealizados]);
  }
}