import { Categoria } from './../../models/categoria';
import { Lider } from './../../models/lider';
import { Usuario } from './../../models/usuario';
import { Participante } from './../../models/participantes';
import { Nivel } from './../../models/nivel';
import { ReceptoresResult,LideresResult } from './../../services/estructura.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { merge } from 'rxjs/observable/merge';
import { async } from '@angular/core/testing';
import { CuestionarioService, CuestionariosResult } from './../../services/cuestionario.service';
import { InformeService } from './../../services/informe.service';
import { ResultadoCategoria, ResultadoNivel } from './../../models/informe';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Encuesta } from '../../models/encuesta';
import { Informe, InformeInfo } from '../../models/informe';
import { EncuestaService } from '../../services/encuesta.service';
import { AuthService } from '../../services/auth.service';
import { EstructuraService } from '../../services/estructura.service';
import { Observable } from 'rxjs/Observable';
import { Persona } from '../../models/persona';
import {TranslateService} from '@ngx-translate/core';
import { MatSnackBar } from "@angular/material";




@Component({
  selector: 'app-informecomparativa-component',
  templateUrl: 'informe-comparativa.component.html',
  styleUrls: ['informe-comparativa.component.css'],
})

export class InformeComparativaComponent implements OnInit {

  model: Encuesta = new Encuesta();
  informe: Informe;
  loading: boolean = false;
  loadingDiv: boolean = false;
  cuestionarios: CuestionariosResult;
  informeresultado: string;
  categoriasChartData : any;


  constructor(
    private encuestaService: EncuestaService,
    private estructuraService: EstructuraService,
    private cuestionarioService: CuestionarioService,
    private informeService: InformeService,
    private authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
  ) {
    route.params.subscribe((p) => {
      this.model.id = p.id ? p.id : 0;
    });
    this.crearCategoriasChartData();
  }

  ngOnInit() {    
    this.setIdioma();

    this.translate.get('informes.activacion').subscribe(translation => {
      this.categoriasChartData.options.title.text = translation;      
    });     

    this.translate.get('informes.resultado').subscribe(translation => {
      this.informeresultado = translation;      
    });   

    this.loading = true;
    
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  public ngAfterViewInit() {        
    this.encuestaService.getEncuesta(this.model.id, true).subscribe((encuesta) => {      
      encuesta.global = this.model.global;
      this.model = encuesta;
      var categoriaPreguntas = this.model.preguntas.map(p=> p.categoriaId);
      this.model.categorias = this.model.categorias.filter(x=> categoriaPreguntas.indexOf(x.id)!=-1)   
      this.loading = false;   
      this.loadingDiv = true;
    });
  }

  drawCategoriasChart(informe: Informe) {
    this.loading = true;
    const rcs: ResultadoCategoria[] = informe.resultadoCategorias;
    const rn : ResultadoNivel [] = informe.resultadoNiveles;
    //const categorias: string[] = rcs.filter(rc => rc.valor != 0).map((rc) => rc.nombreCategoria);
    const categorias: string[] = rcs.map((rc) => rc.nombreCategoria);
    const backgroundColor = rcs.map((it) => '#00a68b');
    const backgroundColor1 = rcs.map((it) => '#a0036e');
    const backgroundColor2 = rcs.map((it) => '#023189');
    const backgroundColor3 = rcs.map((it) => '#774601');
    const backgroundColor4 = rcs.map((it) => '#015b15');
    this.categoriasChartData.colors.push(backgroundColor);
    this.categoriasChartData.colors.push(backgroundColor1);
    this.categoriasChartData.colors.push(backgroundColor2);
    this.categoriasChartData.colors.push(backgroundColor3);
    this.categoriasChartData.colors.push(backgroundColor4);
    this.categoriasChartData.categorias = categorias;
    this.cargarDataSet (rn, informe); 
    this.loading = false;
  }

  borrarDataSet(){
    this.loading = true;
    this.categoriasChartData.datasets=[];
    this.loading = false;
  }

  cargarDataSet (results : ResultadoNivel[], informe: Informe){
    var global;
    this.translate.get('informes.global').subscribe(translation => {
      global = translation;      
    });  
    if(this.informe.nivel1 == null){
      const valores: number[] = results[0].valores;     
      this.categoriasChartData.datasets=[{
        label: global,
        data: valores,
        backgroundColor : this.categoriasChartData.colors[0],
      }];
    }
    else{
      if(this.informe.nivel2 == null){
        const valores: number[] = results[0].valores;
        const valores1 :number[] = results[1].valores;
        this.categoriasChartData.datasets=[{
          label: global,   
          data: valores,
          backgroundColor : this.categoriasChartData.colors[0],
        },{
          label: informe.nivel1,
          data: valores1,
          backgroundColor : this.categoriasChartData.colors[1],
        }];
      }else{
        if(this.informe.nivel3 == null){
          const valores: number[] = results[0].valores;
          const valores1: number[] = results[1].valores;
          const valores2: number[] = results[2].valores;
          this.categoriasChartData.datasets=[{
            label: global,
            data: valores,
            backgroundColor : this.categoriasChartData.colors[0],
          },{
            label: informe.nivel1,
            data: valores1,
            backgroundColor : this.categoriasChartData.colors[1],
          },{
            label: informe.nivel2,
            data: valores2,
            backgroundColor : this.categoriasChartData.colors[2],
          }];
        }else{
          if(this.informe.nivel4 == null){
            const valores: number[] = results[0].valores;
            const valores1: number[] = results[1].valores;
            const valores2: number[] = results[2].valores;
            const valores3: number[] = results[3].valores;
            this.categoriasChartData.datasets=[{
              label: global,
              data: valores,
              backgroundColor : this.categoriasChartData.colors[0],
            },{
              label: informe.nivel1,
              data: valores1,
              backgroundColor : this.categoriasChartData.colors[1],
            },{
              label: informe.nivel2,
              data: valores2,
              backgroundColor : this.categoriasChartData.colors[2],
            },{
              label: informe.nivel3,
              data: valores3,
              backgroundColor : this.categoriasChartData.colors[3],
            }];
          }else{
            const valores: number[] = results[0].valores;
            const valores1: number[] = results[1].valores;
            const valores2: number[] = results[2].valores;
            const valores3: number[] = results[3].valores;
            const valores4: number[] = results[4].valores;
            this.categoriasChartData.datasets=[{
              label: global,
              data: valores,
              backgroundColor : this.categoriasChartData.colors[0],
            },{
              label: informe.nivel1,
              data: valores1,
              backgroundColor : this.categoriasChartData.colors[1],
            },{
              label: informe.nivel2,
              data: valores2,
              backgroundColor : this.categoriasChartData.colors[2],
            },{
              label: informe.nivel3,
              data: valores3,
              backgroundColor : this.categoriasChartData.colors[3],
            }],{
              label: informe.nivel4,
              data: valores4,
              backgroundColor : this.categoriasChartData.colors[4],
            };
          }
        }
      }
    }
      
  }

  informeSelected(informeInfo: InformeInfo) {
    this.loading = true;
    this.informeService.getInformeComparativa(informeInfo.encuestaId, informeInfo.global, informeInfo.nivel1, informeInfo.nivel2, informeInfo.nivel3, informeInfo.nivel4)
      .subscribe((informe2) => {
        informe2.nivel1 = informeInfo.nivel1;
        informe2.nivel2 = informeInfo.nivel2;
        informe2.nivel3 = informeInfo.nivel3;
        informe2.nivel4 = informeInfo.nivel4;
        
        this.informe = informe2;

        this.drawCategoriasChart(this.informe);
        var nivelSelected: Nivel = new Nivel();
        nivelSelected.nivel1 = this.informe.nivel1;
        nivelSelected.nivel2 = this.informe.nivel2;
        nivelSelected.nivel3 = this.informe.nivel3;
        nivelSelected.nivel4 = this.informe.nivel4;
        this.loading = false;          
      },
        (err) => 
        {
          var texto = "";
          this.translate.get('informes.confidencialidad').subscribe(translation => {
            texto = translation;        
          });    

          this.snackBar.open(texto, 'Error', { duration: 5000, panelClass: ['red-snackbar'] });
          this.borrarDataSet();
          this.loading = false;
        } 
      );      
  }

  crearCategoriasChartData (){
    this.categoriasChartData= {
      type: 'horizontalBar',
      categorias: [],
      datasets: [],
      legend: false,
      colors: [],
      
      options: {
        scaleShowVerticalLines: false,
        legend: {
          position: 'right',
        },
        title: {
          display: true,
          text: 'Aceptación (%)',
        },
        scales: {
          xAxes: [{
            ticks: {
              min: 0,
              max: 100,
            },
          }],
        },
        animation: {
          onComplete: function () {
            var chartInstance = this.chart,
            ctx = chartInstance.ctx;
            ctx.textAlign = 'center';
            ctx.font="20px Arial";
            ctx.textBaseline = 'top';
            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                var data = dataset.data[index]+"%";
                ctx.fillText(data, (dataset.data[index]>80)?bar._model.x-300:bar._model.x+35, bar._model.y-12);
              });
            });
          }
        },
      },
    };
  }

  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }
}
