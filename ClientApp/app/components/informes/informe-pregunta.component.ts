import { Nivel } from './../../models/nivel';
import { Component, OnInit, Input } from '@angular/core';
import { ResultadoPregunta, Informe } from '../../models/informe';
import { RespuestaService } from '../../services/respuestas.service';
import { Respuesta } from '../../models/respuesta';
import { MatDialog } from '@angular/material';
import { RespuestasTextoDialog } from './dialogs/respuestas-texto.dialog';
import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-informe-pregunta',
  templateUrl: 'informe-pregunta.component.html',
  styleUrls: ['informe-pregunta.component.css'],
})

export class InformePreguntaComponent implements OnInit {

  @Input() resultadoPregunta: ResultadoPregunta;
  @Input() informe: Informe;
  loading: boolean = false;

  seleccionChartData: any = {
    type: 'doughnut',
    selecciones: [],
    opciones: [],
    options: {
      legend: {
        display: false,
      },
    },
  };  

  constructor(
    private respuestaService: RespuestaService,
    public authService: AuthService,
    private translate: TranslateService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.setIdioma();
    this.resultadoPregunta.porcentaje = this.resultadoPregunta.valor * 100;
    this.drawSeleccionesChart(this.resultadoPregunta);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  showRespuestasTexto() {
    this.loading = true;
    this.respuestaService.getRespuestasTexto(this.informe.encuestaId, this.resultadoPregunta.preguntaId,this.informe.nivel1,this.informe.nivel2,this.informe.nivel3,this.informe.nivel4)
      .subscribe(respuestas => {
        this.openRespuestasTextoDialog(respuestas);
        this.loading = false;
      });
  }

  openRespuestasTextoDialog(respuestas: Respuesta[]) {
    const dialogRef = this.dialog.open(RespuestasTextoDialog, {
      data: { respuestas, pregunta: this.resultadoPregunta.texto },
      width: '60%',
    });
  }

  drawSeleccionesChart(rp: ResultadoPregunta) {
    rp.resultadoOpciones.forEach(ro => {
      this.seleccionChartData.selecciones.push(ro.recuento);
      this.seleccionChartData.opciones.push(ro.texto);
    });
  }
}