import { Categoria } from './../../models/categoria';
import { Lider } from './../../models/lider';
import { Usuario } from './../../models/usuario';
import { Participante } from './../../models/participantes';
import { Nivel } from './../../models/nivel';
import { ReceptoresResult,LideresResult } from './../../services/estructura.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { merge } from 'rxjs/observable/merge';
import { async } from '@angular/core/testing';
import { CuestionarioService, CuestionariosResult } from './../../services/cuestionario.service';
import { InformeService } from './../../services/informe.service';
import { ResultadoCategoria } from './../../models/informe';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Encuesta } from '../../models/encuesta';
import { Informe, InformeInfo } from '../../models/informe';
import { EncuestaService } from '../../services/encuesta.service';
import { AuthService } from '../../services/auth.service';
import { EstructuraService } from '../../services/estructura.service';
import { Observable } from 'rxjs/Observable';
import { Persona } from '../../models/persona';
import {TranslateService} from '@ngx-translate/core';
import { MatSnackBar } from "@angular/material";

@Component({
  selector: 'app-informe-component',
  templateUrl: 'informe.component.html',
  styleUrls: ['informe.component.css'],
})

export class InformeComponent implements OnInit {

  model: Encuesta = new Encuesta();
  informe: Informe;
  loading: boolean = false;
  participantes: ReceptoresResult;
  cuestionarios: CuestionariosResult;
  participacion: Participante[] = [];
  personas: Persona[]= [];
  lideres: Lider[];
  informeresultado: string;

  preguntasChartData: any = {
    type: 'pie',
    categorias: [],
    preguntas: [],
    options: {
      legend: {
        position: 'right',
      },
    },
  };

  categoriasChartData: any = {
    type: 'horizontalBar',
    categorias: [],
    datasets: [],
    legend: false,
    colors: [],

    options: {
      scaleShowVerticalLines: false,
      legend: {
        position: 'right',
      },
      title: {
        display: true,
        text: 'Aceptación (%)',
      },
      scales: {
        xAxes: [{
          ticks: {
            min: 0,
            max: 100,
          },
        }],
      },
      animation: {
        onComplete: function () {
          var chartInstance = this.chart,
          ctx = chartInstance.ctx;
          ctx.textAlign = 'center';
          ctx.font="20px Arial";
          ctx.textBaseline = 'top';
          this.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
              var data = dataset.data[index]+"%";
              ctx.fillText(data, (dataset.data[index]>80)?bar._model.x-300:bar._model.x+35, bar._model.y-12);
            });
          });
        }
      },
    },
  };

  constructor(
    private encuestaService: EncuestaService,
    private estructuraService: EstructuraService,
    private cuestionarioService: CuestionarioService,
    private informeService: InformeService,
    private authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
  ) {
    route.params.subscribe((p) => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  ngOnInit() {
    this.setIdioma();

    this.translate.get('informes.activacion').subscribe(translation => {
      this.categoriasChartData.options.title.text = translation;      
    });     

    this.translate.get('informes.resultado').subscribe(translation => {
      this.informeresultado = translation;      
    });   

    this.loading = true;
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  public ngAfterViewInit() {
    this.encuestaService.getEncuesta(this.model.id, true).subscribe((encuesta) => {
      encuesta.global = this.model.global;
      this.model = encuesta;
      var categoriaPreguntas = this.model.preguntas.map(p=> p.categoriaId);
      this.model.categorias = this.model.categorias.filter(x=> categoriaPreguntas.indexOf(x.id)!=-1)
      const sources: Array<Observable<any>> = [
        this.estructuraService.getReceptores(this.model.estructuraId),
        this.cuestionarioService.getCuestionarios(this.model.id),
        this.estructuraService.getLideres(this.model.estructuraId),
      ];
      forkJoin(sources).subscribe((data) => {
        this.participantes = data[0];
        this.cuestionarios = data[1];
        this.lideres = data[2].items;
        this.personas = this.participantes.items.concat(this.lideres);
        this.loading = false;
      });
    });
  }

  drawCategoriasChart(informe: Informe) {
    if (informe != undefined)
    {
      if (informe.resultadoCategorias != undefined)
      {
        const rcs: ResultadoCategoria[] = informe.resultadoCategorias;
        const categorias: string[] = rcs.filter(rc => rc.valor != 0).map((rc) => rc.nombreCategoria);
        const valores: number[] = rcs.filter(rc => rc.valor != 0).map((rc) => parseFloat((rc.valor * 100).toFixed(2)));
        const backgroundColor = rcs.map((it) => '#00a68b');
        this.categoriasChartData.colors.push({ backgroundColor });
        this.categoriasChartData.categorias = categorias;
        this.categoriasChartData.datasets = [{
          label: this.informeresultado,
          data: valores,
        }];
      }
    }
  }

  informeSelected(informeInfo: InformeInfo) {
    
    this.informeService.getInforme(informeInfo.encuestaId, informeInfo.global, informeInfo.nivel1, informeInfo.nivel2, informeInfo.nivel3, informeInfo.nivel4)
      .subscribe((informe2) => {
        informe2.nivel1 = informeInfo.nivel1;
        informe2.nivel2 = informeInfo.nivel2;
        informe2.nivel3 = informeInfo.nivel3;
        informe2.nivel4 = informeInfo.nivel4;
        
        this.informe = informe2;

        this.drawCategoriasChart(this.informe);
        var nivelSelected: Nivel = new Nivel();
        nivelSelected.nivel1 = this.informe.nivel1;
        nivelSelected.nivel2 = this.informe.nivel2;
        nivelSelected.nivel3 = this.informe.nivel3;
        nivelSelected.nivel4 = this.informe.nivel4;
        this.participacionSelected(nivelSelected);        
      },
        (err) => 
        {
          var texto = "";
          this.translate.get('informes.confidencialidad').subscribe(translation => {
            texto = translation;        
          });    

          this.snackBar.open(texto, 'Error', { duration: 5000, panelClass: ['red-snackbar'] });
        } 
      );

  }

  participacionSelected(nivel: Nivel) {
    this.loadParticipacion(nivel);
  }

  public loadParticipacion(nivel: Nivel) {
    this.participacion = [];
    var participacionAux: Participante[] = [];
    if (nivel.nivel1 === undefined) {
      
      var tartas: string[];
      var texto = "";
      participacionAux = [];

      this.translate.get('informes.totalencuesta').subscribe(translation => {
        texto = translation;        
      });   

      var totalencuesta = texto;
      participacionAux.push(this.crearParticipante(totalencuesta, this.cuestionarios.totalItems, this.personas.length));
      tartas = this.personas.map((i) => i.nivel1).filter((val, idx, self) => self.indexOf(val) === idx);
      tartas.forEach(element => {
        if (element !=null && element != "") {
          participacionAux.push(this.crearParticipante(element, this.cuestionarios.items.filter(p => p.nivel1 == element).length, this.personas.filter(p => p.nivel1 == element).length));
        }
      });
    } else if (nivel.nivel2 === null) {
      var tartas: string[];
      participacionAux = [];
      participacionAux.push(this.crearParticipante(nivel.nivel1, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1).length, this.personas.filter(p => p.nivel1 === nivel.nivel1).length));
      tartas = this.personas.filter(i => i.nivel1 == nivel.nivel1).map((i) => i.nivel2).filter((val, idx, self) => self.indexOf(val) === idx);
      if (tartas[0] != null) {
        tartas.forEach(element => {
          if (element !=null && element != "") {
            participacionAux.push(this.crearParticipante(element, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === element).length, this.personas.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === element).length));
          }
        });
      }
    } else if (nivel.nivel3 === null) {
      var tartas: string[];
      participacionAux = [];
      participacionAux.push(this.crearParticipante(nivel.nivel2, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2).length, this.personas.filter(p => p.nivel1=== nivel.nivel1 && p.nivel2 === nivel.nivel2).length));
      tartas = this.personas.filter(i => i.nivel1 == nivel.nivel1 && i.nivel2 == nivel.nivel2).map((i) => i.nivel3).filter((val, idx, self) => self.indexOf(val) === idx);
      if (tartas[0] != null) {
        tartas.forEach(element => {
          if (element !=null && element != "") {
            participacionAux.push(this.crearParticipante(element, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === element).length, this.personas.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === element).length));
          }
        });

      }
    } else if (nivel.nivel4 === null) {
      var tartas: string[];
      participacionAux = [];
      participacionAux.push(this.crearParticipante(nivel.nivel3, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === nivel.nivel3).length, this.personas.filter(p => p.nivel1=== nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === nivel.nivel3).length));
      tartas = this.personas.filter(i => i.nivel1 == nivel.nivel1 && i.nivel2 === nivel.nivel2 && i.nivel3 === nivel.nivel3).map((i) => i.nivel4).filter((val, idx, self) => self.indexOf(val) === idx);
      if (tartas[0] != null) {
        tartas.forEach(element => {
          if (element !=null && element != "") {
            participacionAux.push(this.crearParticipante(element, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === nivel.nivel3 && p.nivel4 === element).length, this.personas.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === nivel.nivel3 && p.nivel4 === element).length));
          }
        });
      }
    } else {
      participacionAux = [];
      participacionAux.push(this.crearParticipante(nivel.nivel4, this.cuestionarios.items.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3=== nivel.nivel3 && p.nivel4 === nivel.nivel4).length, this.personas.filter(p => p.nivel1 === nivel.nivel1 && p.nivel2 === nivel.nivel2 && p.nivel3 === nivel.nivel3 && p.nivel4 === nivel.nivel4).length));
    }
    this.participacion = participacionAux;
  }



  public crearParticipante(nombre: string, cuestionariosRealizados: number, participantes: number): Participante {
    var participante1 = new Participante();
    participante1.nombreTarta = nombre;
    participante1.cuestionariosRealizados = cuestionariosRealizados;
    participante1.participantes = participantes - cuestionariosRealizados;
    if (participante1.participantes < 0) participante1.participantes = 0;
    var porcentaje = 0;
    if (participantes != 0) {
      porcentaje = cuestionariosRealizados / participantes;
    }
    participante1.porcentaje = porcentaje==0 ? 0 : parseFloat((porcentaje * 100).toFixed(2));
    return participante1;
  }
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }
}
