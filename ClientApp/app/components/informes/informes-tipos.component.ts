import { Cuestionario } from './../../models/cuestionario';
import { InformeService } from "./../../services/informe.service";
import { GenericDialog } from "./../app/dialogs/generic.dialog";
import { Component, ViewChild, OnInit } from "@angular/core";
import { MatTableDataSource, MatSort, MatPaginator, MatPaginatorIntl, MatDialog, MatSnackBar } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";

import { merge } from "rxjs/observable/merge";
import "rxjs/add/operator/map";
import { startWith } from "rxjs/operators/startWith";
import { switchMap } from "rxjs/operators/switchMap";
import { Router, ActivatedRoute } from "@angular/router";

import { EncuestaService, EncuestasResult } from "./../../services/encuesta.service";
import { Encuesta, EstadoEncuesta } from "../../models/encuesta";
import { Observable } from "rxjs/Observable";
import { CuestionariosResult, CuestionarioService } from "../../services/cuestionario.service";
import {saveAs} from "file-saver";

import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import { forEach } from '@angular/router/src/utils/collection';
import { TipoEncuestaService } from '../../services/tipoencuesta.service';
import { TipoEncuesta } from '../../models/tipoencuesta';

@Component({
  selector: "app-informes-tipos",
  templateUrl: "informes-tipos.component.html",
  styleUrls: ["./informes-tipos.component.css"],
})

export class InformesTiposComponent implements OnInit {

  public displayedColumns = ["select","titulo", "estado", "descripcion", "administrador", "geografia"];
  public dataSource = new MatTableDataSource<Encuesta>([]);
  public totalItems: number = 0;
  public loading: boolean = false;
  public cerradas: boolean = false;
  informefichero: string = "";
  public selectedTipo : string ;
  public tipos: string[] = [] ;
  selection = new SelectionModel<Encuesta>(true, []);
  model: Encuesta [] ;
  

  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;

  constructor(
    private encuestaService: EncuestaService,
    private tiposEncuestaService : TipoEncuestaService,
    private informeService: InformeService,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private  cuestionarioService: CuestionarioService,
    private authService: AuthService,
    private translate: TranslateService
  ) {
  }

  public ngOnInit() {
    this.setIdioma();    

    this.translate.get('informeslista.informefichero').subscribe(translation => {
      this.informefichero = translation
    });

    this.setIdiomaPaginador();
    this.populateList();
    this.loading = true;
  }

  public ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.tiposEncuestaService.getTiposEncuestaAll()
      .subscribe(tiposB => {
        var tipos = [] ;
        tiposB.forEach(function (value) {
          tipos.push(value.nombre)
        });
        this.tipos = tipos;
        this.selectedTipo = tipos[0];
        this.applyFilter(this.selectedTipo);
      });
    
  }

  public populateList() {
    this.loading = true;
    let result$: Observable<EncuestasResult>;
    result$ = this.encuestaService.getEncuestasParaInforme();
    result$.subscribe((data) => {
    data.items.map(e => 
      { if(e.titulo.startsWith("GLOBAL")) {
          e.global=true;
        };

        this.traducirestado(e.estado);
      })
    this.dataSource.data = data.items;
    this.totalItems = this.dataSource.filteredData.length;
    this.loading = false;
    });
  }

  traducirestado(estado: EstadoEncuesta)
  {

    // Creada : 0, Pendiente : 1, Preactiva : 2, Activa : 3, Cerrada : 4, Historico : 5
    switch(estado.id) { 
      case 0: { 
        this.translate.get('estadoencuesta.creada').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 1: { 
        this.translate.get('estadoencuesta.pendiente').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 2: { 
        this.translate.get('estadoencuesta.preactiva').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 3: { 
        this.translate.get('estadoencuesta.activa').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      }  
      case 4: { 
        this.translate.get('estadoencuesta.cerrada').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      } 
      case 5: { 
        this.translate.get('estadoencuesta.historico').subscribe(translation => {
          estado.descripcion = translation
        });
        break; 
      }                 
      default: { 
         //statements; 
         break; 
      } 
   }     
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    this.dataSource.filterPredicate = 
      (data: Encuesta, filter: string) => data.tipoEncuesta.nombre.indexOf(filter) != -1;
    this.dataSource.filter = filterValue;
    this.selection.clear();
  }



  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
  loadInforme(){
    const encuestasSelecionadas = this.selection.selected;
    var idsEncuestasSeleccionadas = new Array();
    var nombresEncuestas = new Array();
    encuestasSelecionadas.forEach(function (value) {
      idsEncuestasSeleccionadas.push(value.id)
      nombresEncuestas.push(value.titulo);
    });
    var queryparams = { encuestas: idsEncuestasSeleccionadas};
    this.router.navigate([`/informestiposgrafico`],{queryParams: queryparams});

  }
  protected masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.filteredData.forEach((row) => this.selection.select(row));
  }
  protected isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;
    return numSelected === numRows;
  }
}
