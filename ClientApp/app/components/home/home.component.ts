import { Usuario } from './../../models/usuario';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit {

    usuario: Usuario | undefined;
    
    constructor(private authService: AuthService,
        private translate: TranslateService) {

        }

    ngOnInit() {
        this.setIdioma();
        this.setUsuario();
    }

    ngDoCheck() {
        this.setIdioma();
        this.setUsuario();
    }

    setUsuario() {
        const user = this.authService.getLoggedUser();
        this.usuario = user ? user : undefined;
    }

    setIdioma()
    {
        var idiomaLocale = this.translate.getBrowserLang();

        var lidiomaLocale = this.authService.getCurrentLocale();
        if (lidiomaLocale != "")
        {              
          idiomaLocale = lidiomaLocale;      
        } 

        this.translate.use(idiomaLocale);
    }
}
