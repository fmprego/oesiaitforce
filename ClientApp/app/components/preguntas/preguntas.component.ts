import { Observable } from 'rxjs/Observable';
import { CategoriaService } from './../../services/categoria.service';
import { Component, OnInit } from '@angular/core';

import { Categoria } from './../../models/categoria';

import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-preguntas',
  templateUrl: 'preguntas.component.html',
  styleUrls: ['preguntas.component.css']
})

export class PreguntasComponent implements OnInit {

  categorias: Categoria[];

  constructor(private categoriaService: CategoriaService,  
    private authService: AuthService,
    private translate: TranslateService) { }

  ngOnInit() {
    this.setIdioma(); 
    this.populateCategorias();
  }

  populateCategorias() {
    this.categoriaService.getCategorias()
      .subscribe(res => {
        this.categorias = res.items;
      });
  }

  select(categoria: Categoria) {
   // console.log(categoria);
   
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);      
  }
}