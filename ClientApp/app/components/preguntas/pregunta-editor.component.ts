import { CategoriaService, CategoriasResult } from './../../services/categoria.service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from "rxjs/observable/forkJoin";

import { PreguntaService } from './../../services/preguntas.service';
import { Pregunta, TipoPregunta } from './../../models/pregunta';
import { Opcion } from '../../models/opcion';
import { MatTableDataSource, MatSort, MatPaginator, MatPaginatorIntl, MatDialog, MatSnackBar } from "@angular/material";
import { UnsavedChangesDialog } from '../common/unsaved-changes-dialog.component';
import { GrupoOpciones } from '../../models/grupo-opciones';
import { GrupoOpcionesService } from '../../services/grupo-opciones.service';
import { OpcionesEditorComponent } from './opciones-editor.component';
import { Encuesta, EstadoEncuesta } from '../../models/encuesta';

import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

import { ModificarEncuestaActivaDialog } from "../encuestas/dialogs/modificar-encuesta-activa.dialog";

@Component({
  selector: 'app-pregunta-editor',
  templateUrl: 'pregunta-editor.component.html',
  styleUrls: ['pregunta-editor.component.css']
})

export class PreguntaEditorComponent implements OnInit {

  model: Pregunta = new Pregunta();  
  encuestaId: number = 0;
  categorias: CategoriasResult;
  tipoPreguntas: TipoPregunta[];
  loading: boolean = false;  
  isSeleccion: boolean = false;
  hasChanges: boolean = false;
  readOnlyMode: boolean = false;

  public displayedColumns = ["titulo", "estado", "descripcion", "actions"];
  public dataSourceEncuestas = new MatTableDataSource<Encuesta>([]);
  public totalItems: number = 0;

  @ViewChild(OpcionesEditorComponent) child: OpcionesEditorComponent;
  @ViewChild(MatSort) public sort: MatSort;
  @ViewChild(MatPaginator) public paginator: MatPaginator;  

  constructor(
    private preguntaService: PreguntaService,
    private categoriaService: CategoriaService,
    private grupoOpcionesService: GrupoOpcionesService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,  
    private authService: AuthService,
    private translate: TranslateService,
  ) {
    route.params.subscribe(p => {
      if (p.encuestaId) this.encuestaId = p.encuestaId;
      this.model.id = p.preguntaId ? p.preguntaId : 0;
      if (p.categoriaId) this.model.categoriaId = p.categoriaId;
      if( p.action != undefined)
      {
        this.readOnlyMode = (p.action.toString() === 'v');
      }
    }); 
  }

  ngOnInit() {
    this.setIdioma();     

    this.loading = true;
    
    this.dataSourceEncuestas.sort = this.sort;
    this.dataSourceEncuestas.paginator = this.paginator;

    let sources: Observable<any>[] = [
      this.categoriaService.getCategorias(),
      this.preguntaService.getTipos(),
    ];
    if (this.model.id) {
      sources.push(this.preguntaService.getPregunta(this.model.id));
    }
    if (this.model.id) {
      sources.push(this.preguntaService.getEncuestasByPregunta(this.model.id));
    }    
    forkJoin(sources).subscribe(data => {
      this.categorias = data[0];

      data[1].map(e => 
        {          
          this.traducirtipopregunta(e);
        });

      this.tipoPreguntas = data[1];      

      if (data[2]) this.model = data[2];
      if (data[3]) 
      {
        data[3].map(e => 
          {          
            this.traducirestado(e.estado);
          });
        this.dataSourceEncuestas.data =  data[3]; 
        this.totalItems = data[3].length;            
      }
      const catId: string = this.model.categoriaId.toString();
      const nombreCategoria = this.categorias.items
        .filter(c => c.id === parseInt(catId))
        .map(c => c.nombre);
      this.model.nombreCategoria = nombreCategoria[0] || '';

      this.loading = false;

      if (this.totalItems !=0)
      {
        this.setIdiomaPaginador();
      }
    })
  }

  back() {
    if ((this.hasChanges) && (!this.readOnlyMode)) {
      this.openUnsavedChangesDialog();
    } else {
      if (this.encuestaId != 0)
      {
        const url = `/encuesta/preguntas/${this.encuestaId}`;
        this.router.navigate([url]);
      }
      else
      {
        const url = this.model.categoriaId ? `/categoria/${this.model.categoriaId}` : '/categorias';
        this.router.navigate([url]);        
      }
    }
  }

  openUnsavedChangesDialog() {
    const dialogRef = this.dialog.open(UnsavedChangesDialog, {
      width: '60%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const url = this.model.categoriaId ? `/categoria/${this.model.categoriaId}` : '/categorias';
        this.router.navigate([url]);
      }
    });    
  }

  submit() {
    var result$: Observable<Pregunta>;
    if (this.model.id) result$ = this.preguntaService.update(this.model);
    else result$ = this.preguntaService.create(this.model);

    result$.subscribe(
      res => {
        var texto1 = "";
        var texto2 = "";

        this.translate.get('revisiones.cambiosguardados').subscribe(translation => {
          texto1 = translation;        
        }); 

        this.translate.get('revisiones.preguntacreada').subscribe(translation => {
          texto2 = translation;        
        }); 

        const msg = (this.model.id) ? texto1 : texto2;
        this.showMsg(msg);
        this.hasChanges = false;
        this.back();
      },
      err => {
        var texto = "";

        this.translate.get('revisiones.compruebadatos').subscribe(translation => {
          texto = translation;        
        }); 

        this.showMsg(texto);
      }
    );
  }

  onAdded(opcion: Opcion) {
    this.hasChanges = true;
    if (!this.model.opciones) {
      this.model.opciones = new Array<Opcion>();
    }
    this.model.opciones = [
      ...this.model.opciones, opcion
    ];
  }

  onDeleted(opcion: Opcion) {
    const pos = this.model.opciones
      .findIndex(op => 
        op.peso === opcion.peso
        && op.texto === opcion.texto);
    this.model.opciones = [
      ...this.model.opciones.slice(0, pos),
      ...this.model.opciones.slice(pos + 1),
    ];
    this.hasChanges = true;
  }

  onDeletedAll() {
    this.model.opciones = new Array<Opcion>();
  }

  onAddedAll(opciones: Opcion[]) {
    this.model.opciones = opciones;
  }

  onSaveAsGrupo(nombre: string) {
    let grupo = new GrupoOpciones();
    grupo.nombre = nombre;
    grupo.opciones = this.model.opciones;
    this.grupoOpcionesService.saveGrupo(grupo)
      .subscribe(
        grupo => {
          var texto = "";

          this.translate.get('revisiones.grupoañadido').subscribe(translation => {
            texto = translation;        
          }); 

          this.showMsg(texto);
          this.child.populateGrupos();
        },
        err => this.showMsg(err),
      )
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('revisiones.ok').subscribe(translation => {
      texto = translation;        
    }); 

    this.snackBar.open(text, texto, { duration: 2000 });
  }

  public edit(encuesta: Encuesta) {
    if(encuesta.estado.id===3){
        const dialogRef = this.dialog.open(ModificarEncuestaActivaDialog,{
            width: "60%",
        });
        dialogRef.afterClosed().subscribe((result) =>{
            if(result)
            {
            this.router.navigate([`/encuesta/resumen/${encuesta.id}`]);
            }
        });
    } else {
        this.router.navigate([`/encuesta/resumen/${encuesta.id}`]);
    }
  }  

  traducirtipopregunta(tipo: TipoPregunta)
  {
    //Texto, Seleccion, Escala, Comentario
    switch(tipo.id) { 
      case 0: { 
        this.translate.get('tipopregunta.texto').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 1: { 
        this.translate.get('tipopregunta.seleccion').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 2: { 
        this.translate.get('tipopregunta.escala').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 3: { 
        this.translate.get('tipopregunta.comentario').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      }                  
      default: { 
         //statements; 
         break; 
      } 
   }   
  }

  traducirestado(estado: EstadoEncuesta)
    {
  
      // Creada : 0, Pendiente : 1, Preactiva : 2, Activa : 3, Cerrada : 4, Historico : 5
      switch(estado.id) { 
        case 0: { 
          this.translate.get('estadoencuesta.creada').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 1: { 
          this.translate.get('estadoencuesta.pendiente').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 2: { 
          this.translate.get('estadoencuesta.preactiva').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 3: { 
          this.translate.get('estadoencuesta.activa').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        }  
        case 4: { 
          this.translate.get('estadoencuesta.cerrada').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 5: { 
          this.translate.get('estadoencuesta.historico').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        }                 
        default: { 
           //statements; 
           break; 
        } 
     }     
    }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
}