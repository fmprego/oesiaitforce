import { MatSnackBar } from '@angular/material';
import { GrupoOpcionesService } from './../../services/grupo-opciones.service';
import { Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { Pregunta } from '../../models/pregunta';
import { Opcion } from '../../models/opcion';
import { GrupoOpciones } from '../../models/grupo-opciones';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-opciones-editor',
    templateUrl: 'opciones-editor.component.html',
    styleUrls: ['opciones-editor.component.css']
})

export class OpcionesEditorComponent implements OnInit {

    @Input() pregunta: Pregunta;
    @Input() readonly: boolean;
    @Output() onAdded = new EventEmitter<Opcion>();
    @Output() onDelete = new EventEmitter<Opcion>();
    @Output() onAddAll = new EventEmitter<Opcion[]>();
    @Output() onDeleteAll = new EventEmitter<Opcion>();
    @Output() onSaveAsGrupo = new EventEmitter<string>();
    @Output() onMsg = new EventEmitter<string>();
    addMode: boolean = false;
    readonlyMode: boolean = false;
    addGrupoOpcionesMode: boolean = false;
    saveGrupoMode: boolean = false;
    editMode: boolean = false;
    newOpcion: Opcion = new Opcion();
    editOpcion: Opcion = new Opcion();
    loading: boolean = false;
    grupoOpcionesId: number;
    gruposOpciones: GrupoOpciones[];
    newGrupoNombre: string;
    imagen: string;
    progress: number;
    progressBar: boolean = false;
    noimagen: boolean = false;
      
    private _placeHolderSafe: SafeUrl;

    constructor(
        private grupoOpcionesService: GrupoOpcionesService,
        private sanitizer: DomSanitizer,
        private snackBar: MatSnackBar,
        private authService: AuthService,
        private translate: TranslateService,
    ) { }

    public get placeholder() {
        return this._placeHolderSafe;
    }

    public get NoImagen() {
        let bresp = true;

        if (this.addMode) {
            bresp = (this.newOpcion.imagenCab == undefined);
        }
        else {
            bresp = (this.editOpcion.imagenCab == undefined);
        }

        return bresp;
    }

    public get ReadOnly(): boolean {
        return this.readonly;
    }
    //public set ReadOnly(value: boolean) {
    //    this.readonly = value;
    //}
    
    ngOnInit() {
        this.setIdioma(); 

        this.loading = true;
        this.progressBar = false;
        this.populateGrupos();
    }

    populateGrupos() {
        this.grupoOpcionesService.getAll()
            .subscribe(data => {
                this.gruposOpciones = data;
                this.loading = false;
            });
    }

    toggleAddMode() {
        this.editMode = false;
        this.readonlyMode = false;
        this.addGrupoOpcionesMode = false;
        this.saveGrupoMode = false;
        this.newOpcion = new Opcion();
        this.addMode = !this.addMode;
    }

    deactivateAddMode() {
        this.addMode = false;
        this._placeHolderSafe = null;
    }

    addOpcion() {
        const opcion = Object.assign({}, this.newOpcion);
        this.onAdded.emit(opcion);
        this.deactivateAddMode();
    }

    delete(opcion: Opcion) {
        this.onDelete.emit(opcion);
    }

    toggleEditMode(opcion: Opcion) {

        this.addMode = false;
        this.saveGrupoMode = false;
        this.addGrupoOpcionesMode = false;

        if (!this.ReadOnly) {
            if (!this.editMode) this.editMode = true;

            else if (this.editMode && this.editOpcion === opcion) {
                this.editMode = false;
            }
        }
        else {
            if (!this.readonlyMode) this.readonlyMode = true;
            else if (this.readonlyMode && this.editOpcion === opcion) {
                this.readonlyMode = false;
            }
        }
           
        this.editOpcion = opcion;
        this.urlimagen();
    }
       

    deactivateEditMode() {
        this.editMode = false;
        this.readonlyMode = false;
    }

    toggleAddGrupoOpcionesMode() {
        this.editMode = false;
        this.readonlyMode = false;
        this.addMode = false;
        this.saveGrupoMode = false;
        this.addGrupoOpcionesMode = !this.addGrupoOpcionesMode;
    }

    toggleSaveGrupoMode() {
        this.editMode = false;
        this.readonlyMode = false;
        this.addMode = false;
        this.addGrupoOpcionesMode = false;
        this.saveGrupoMode = !this.saveGrupoMode;
    }

    addGrupoOpciones() {
        const grupo: GrupoOpciones = this.gruposOpciones.find(g =>
            g.id === this.grupoOpcionesId) || new GrupoOpciones();
        this.onDeleteAll.emit();
        this.onAddAll.emit(grupo.opciones);
    }

    saveGrupo() {
        this.onSaveAsGrupo.emit(this.newGrupoNombre);
    }

    deleteGrupoOpciones() {
        this.loading = true;
        this.grupoOpcionesService.deleteGrupo(this.grupoOpcionesId)
            .subscribe(
                data => {
                    this.onMsg.emit(`Grupo ${data.nombre} eliminado.`);
                    this.populateGrupos();
                    this.toggleAddGrupoOpcionesMode();
                },
                err => {
                    this.onMsg.emit(err);
                    this.loading = false;
                }
            );
    }

    upload(files) {

        if (files.length === 0)
            return;

        var reader = new FileReader();
        var self = this;
        var currentFile = files[0];
        this.progress = 0;

        this.progressBar = true;

        reader.onload = function () {
            var dataURL = reader.result;
        }
        reader.onerror = function () {
            console.log("Error Lectura");
        }

        reader.onprogress = function (currentFile) {
            if (currentFile.lengthComputable) {

              self.progress = ((currentFile.loaded / currentFile.total) * 100);
            }
        }

        reader.onloadend = function () {

            self.progress = 100;

            if (reader.result.length > 0) {

                if (self.addMode) {
                    self.newOpcion.imagen = files[0].name;
                    self.newOpcion.imagenCab = reader.result.substring(0, reader.result.lastIndexOf("base64") + 7);
                    self.newOpcion.imagenContain = reader.result.substring(reader.result.lastIndexOf("base64") + 7);
                }
                else {
                    self.editOpcion.imagen = files[0].name;
                    self.editOpcion.imagenCab = reader.result.substring(0, reader.result.lastIndexOf("base64") + 7);
                    self.editOpcion.imagenContain = reader.result.substring(reader.result.lastIndexOf("base64") + 7);
                }
                self.urlimagen();
            }

            setTimeout(() => {    
                self.progressBar = false;
            }, 2000);
        }

        reader.readAsDataURL(files[0]);
    }

    showError(text: string) {
        var texto = "";

        this.translate.get('revisiones.error').subscribe(translation => {
          texto = translation;        
        }); 
        this.snackBar.open(text,texto, { duration: 5000, panelClass: ['red-snackbar'] });
      }

    urlimagen() {

        this._placeHolderSafe = "";

        if (this.addMode) {
            if (this.newOpcion.imagenContain != undefined) {
                this._placeHolderSafe = this.sanitizer.bypassSecurityTrustUrl(this.newOpcion.imagenCab + this.newOpcion.imagenContain);
            }
        }
        else {

            if (this.editOpcion.imagenContain != undefined) {
                this._placeHolderSafe = this.sanitizer.bypassSecurityTrustUrl(this.editOpcion.imagenCab + this.editOpcion.imagenContain);
            }
        }
    }

    setIdioma()
    {
        var idiomaLocale = this.translate.getBrowserLang();
  
        var lidiomaLocale = this.authService.getCurrentLocale();
        if (lidiomaLocale != "")
        {              
          idiomaLocale = lidiomaLocale;      
        } 
  
        this.translate.use(idiomaLocale);      
    }
}