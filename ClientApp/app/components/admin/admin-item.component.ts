import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Admin } from '../../models/admin';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admin-item',
  templateUrl: 'admin-item.component.html',
  styleUrls: ['admin-item.component.css']
})

export class AdminItemComponent{

  @Input() admin: Admin;
  @Output() onDelete = new EventEmitter<Admin>();
  @Output() onEdit = new EventEmitter<Admin>();

  constructor(
    private authService: AuthService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setIdioma(); 
  }

  delete() {
    this.onDelete.emit(this.admin);
  }

  edit() {
    this.onEdit.emit(this.admin);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}