import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'admin-delete-dialog',
  templateUrl: 'admin-delete.dialog.html',
})
export class AdminDeleteDialog {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dialogRef: MatDialogRef<AdminDeleteDialog>
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}