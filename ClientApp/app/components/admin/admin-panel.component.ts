import { createServerRenderer } from 'aspnet-prerendering';
import { GeografiaService } from './../../services/geografia.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { AdminService } from './../../services/admin.service';
import { Admin } from '../../models/admin';
import { Router } from '@angular/router';
import { Geografia } from '../../models/geografia';
import { AdminDeleteDialog } from './dialog/admin-delete.dialog';
import { MatDialog } from '@angular/material';
import { GenericErrorDialog } from '../common/dialog/generic-error.dialog';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admin-panel',
  templateUrl: 'admin-panel.component.html',
  styleUrls: ['admin-panel.component.css']
})

export class AdminPanelComponent implements OnInit {

  admins: Admin[];
  geografias: Geografia[];
  selectedGeografiaId: number;
  loading: boolean = false;

  constructor(
    private adminService: AdminService,
    private geografiaService: GeografiaService,
    private snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setIdioma(); 

    this.geografiaService.getGeografiasSimple()
      .subscribe(geografias => this.geografias = geografias);
    this.populateAdmins();
  }

  onGeografiaChange() {
    this.populateAdmins(this.selectedGeografiaId);
  }

  populateAdmins(geografiaId?: number) {
    this.loading = true;
    this.adminService.getAdmins(geografiaId)
      .subscribe(data => {
        this.admins = data
        this.loading = false;
      });
  }

  deleteAdmin(admin: Admin) {
    this.adminService.deleteAdmin(admin.id)
      .subscribe(
        res => {
          var texto = "";
          var titulo = "";

          this.translate.get('administrador.administradoreliminado').subscribe(translation => {
            texto = translation
          });

          this.translate.get('administrador.eliminado').subscribe(translation => {
            titulo = translation
          });

          this.snackBar.open(texto,titulo,{duration:5000});
        this.populateAdmins();
      },
      err => this.openErrorDialog(err)
    );
  }

  openErrorDialog(err: any) {

    var texto = "";

    this.translate.get('administrador.reviselog').subscribe(translation => {
      texto = translation
    });

    const json = !!err ? err.json() : undefined;
    const errText = !!json ? json.Message : undefined;
    this.dialog.open(GenericErrorDialog, { 
      data: {
        error: errText || texto,
      },
      width: '60%'
    });
    this.loading = false;
  }  

  openDeleteDialog() {
    const dialogRef = this.dialog.open(AdminDeleteDialog, { width: '60%' });
  }

  editAdmin(admin: Admin) {
    this.router.navigate([`/admin-form/${admin.id}`]);
  }

  newAdmin() {
    this.router.navigate(['/admin-form']);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}