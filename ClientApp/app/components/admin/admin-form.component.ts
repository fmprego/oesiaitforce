import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthHttp } from 'angular2-jwt';
import { MatSnackBar } from '@angular/material';

import { Admin } from '../../models/admin';
import { Geografia } from '../../models/geografia';
import { AdminService } from './../../services/admin.service';
import { GeografiaService } from '../../services/geografia.service';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admin-form',
  templateUrl: 'admin-form.component.html',
  styleUrls: ['admin-form.component.css']
})

export class AdminFormComponent implements OnInit {

  model: Admin = new Admin();
  geografias: Geografia[];
  selectedGeografiaId: number;

  constructor(
    private adminService: AdminService,
    private geografiaService: GeografiaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  ngOnInit() {

    this.setIdioma(); 

    this.populateGeografias();
    if (this.model.id) {
      this.adminService.getAdmin(this.model.id)
        .subscribe(data => this.setAdminData(data));
    }
  }

  setAdminData(admin: Admin) {
    this.model.username = admin.username;
    this.model.nombre = admin.nombre;
    this.model.apellidos = admin.apellidos;
    this.model.email = admin.email;
    this.model.geografiaId = admin.geografiaId;
  }

  populateGeografias() {
    this.geografiaService.getGeografiasSimple()
      .subscribe(data => this.geografias = data);
  }

  submit() {
    var result$: Observable<Admin>;
    if (this.model.id) result$ = this.adminService.updateAdmin(this.model);
    else result$ = this.adminService.createAdmin(this.model);

    result$.subscribe(
      res => {

        var texto1 = "";
        var texto2 = "";

        this.translate.get('administrador.cambiosguardados').subscribe(translation => {
          texto1 = translation
        });

        this.translate.get('administrador.administradorcreado').subscribe(translation => {
          texto2 = translation
        });

        const msg = (this.model.id) ? texto1 : texto2;
        this.showMsg(msg);
        this.router.navigate(['/admin-panel']);
      },
      err => {
        var texto = "";

        this.translate.get('administrador.compruebadatos').subscribe(translation => {
          texto = translation
        });

        this.showError(texto);
      }
    );
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('administrador.errordatos').subscribe(translation => {
      texto = translation
    });

    this.snackBar.open(text, texto, {duration:5000});
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('administrador.hecho').subscribe(translation => {
      texto = translation
    });

    this.snackBar.open(text, texto, {duration:5000});
  }

  cancel() {
    this.router.navigate(['/admin-panel']);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}