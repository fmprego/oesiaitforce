import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { AuthService } from './../../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: "app-aprobacion",
  styleUrls: ["aprobacion.dialog.css"],
  templateUrl: "aprobacion.dialog.html",
})

export class AprobacionDialog {

  constructor(
    public dialogRef: MatDialogRef<AprobacionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setIdioma(); 
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);      
  }
}
