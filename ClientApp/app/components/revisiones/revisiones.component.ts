import { AprobacionDialog } from './dialogs/aprobacion.dialog';
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatPaginatorIntl, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { merge } from 'rxjs/observable/merge';
import 'rxjs/add/operator/map';
import { startWith } from 'rxjs/operators/startWith';
import { switchMap } from 'rxjs/operators/switchMap';
import { Router } from '@angular/router';

import { EncuestaService } from './../../services/encuesta.service';
import { Encuesta } from '../../models/encuesta';
import { MatSnackBar } from '@angular/material';

import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-revisiones',
  templateUrl: 'revisiones.component.html',
  styleUrls: ['revisiones.component.css'],
})

export class RevisionesComponent implements OnInit {

  displayedColumns = ['titulo', 'estado', 'descripcion', 'administrador', 'geografia', 'actions'];
  dataSource = new MatTableDataSource<Encuesta>([]);
  totalItems: number = 0;
  loading: boolean = false;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;  
    
  constructor(
    private encuestaService: EncuestaService,
    private router: Router,
    public dialog: MatDialog,    
    private snackBar: MatSnackBar,  
    private authService: AuthService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setIdioma(); 
    this.setIdiomaPaginador();

    this.loading = true;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.populateList();
  }

  populateList() {
    this.encuestaService.getPendientes()
      .subscribe(data => {
        this.dataSource.data = data.items;
        this.totalItems = data.totalItems;
        this.loading = false;
      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  review(encuesta: Encuesta) {
    this.router.navigate([`/encuesta/resumen/${encuesta.id}`]);
  }

  approve(encuesta: Encuesta) {

    var texto = "";

    this.translate.get('revisiones.erroraprobar').subscribe(translation => {
      texto = translation;        
    }); 

    const dialogRef = this.dialog.open(AprobacionDialog, {
      width: '60%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.encuestaService.aprobarEncuesta(encuesta)
          .subscribe(
            () => this.populateList(),
            err =>this.showError(texto)
          );
      }
    });
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('revisiones.error').subscribe(translation => {
      texto = translation;        
    }); 

    this.snackBar.open(text, texto, { duration: 5000, panelClass: ['red-snackbar'] });
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
}