import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { TipoEncuestaService } from '../../services/tipoencuesta.service';
import { TipoEncuesta } from '../../models/tipoencuesta';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tipoencuesta-list',
  templateUrl: 'tipoencuesta-list.component.html',
  styleUrls: ['tipoencuesta-list.component.css']
})

export class TipoEncuestaListComponent implements OnInit {
  
  tiposencuesta: TipoEncuesta[];
  loading: boolean = false;
  
  constructor(
    private tipoEncuestaService: TipoEncuestaService,
    private snackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.setIdioma(); 

    this.populateTipoEncuesta();
  }

  populateTipoEncuesta() {
    this.loading = true;
    this.tipoEncuestaService.getTiposEncuestaAll()
      .subscribe(tipos => {
        this.tiposencuesta = tipos;
        this.loading = false;
      });
  }

  newTipoEncuesta() {
    this.router.navigate(['/tipoencuesta-form']);
  }

  deleteTipoEncuesta(tipo: TipoEncuesta) {
    this.tipoEncuestaService.deleteTipoEncuesta(tipo.id)
      .subscribe(res => {

        var texto = "";
        var titulo = "";

        this.translate.get('tipoencuesta.tipoencuestaeliminada').subscribe(translation => {
          texto = translation
        });

        this.translate.get('tipoencuesta.eliminada').subscribe(translation => {
          titulo = translation
        });

        this.snackBar.open(texto,titulo, {duration:5000});
        this.populateTipoEncuesta();
      })
  }

  editTipoEncuesta(tipo: TipoEncuesta) {
    this.router.navigate([`/tipoencuesta-form/${tipo.id}`]);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}