import { TipoEncuestaDeleteDialogForbidden } from './dialogs/tipoencuesta-delete-dialog-forbidden';
import { AdminService } from '../../services/admin.service';
import { Component, Input, Output, EventEmitter, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { TipoEncuesta } from '../../models/tipoencuesta';
import { TipoEncuestaDeleteDialog } from './dialogs/tipoencuesta-delete-dialog';

import { AuthService } from '../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tipoencuesta-item',
  templateUrl: 'tipoencuesta-item.component.html',
  styleUrls: ['tipoencuesta-item.component.css']
})

export class TipoEncuestaItemComponent implements OnInit {
  
  @Input() tipoencuesta: TipoEncuesta;
  @Output() onDelete = new EventEmitter<TipoEncuesta>();
  @Output() onEdit = new EventEmitter<TipoEncuesta>();
  loading: boolean = false;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private translate: TranslateService) {}

  ngOnInit() {
    this.setIdioma(); 

    this.loading = false;
  }

  openDeleteDialog(): void {
    if (this.tipoencuesta.encuestacount) {
      let dialogRef = this.dialog.open(TipoEncuestaDeleteDialogForbidden, {width: '60%'});
    } else {
      let dialogRef = this.dialog.open(TipoEncuestaDeleteDialog, {width: '60%'});
      dialogRef.afterClosed().subscribe(result => {
        if (result) this.delete();
      });
    }
  }

  delete() {
    this.onDelete.emit(this.tipoencuesta);
  }

  edit() {
    this.onEdit.emit(this.tipoencuesta);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}