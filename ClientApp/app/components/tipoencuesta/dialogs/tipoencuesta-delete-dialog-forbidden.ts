import { Component, Inject } from "@angular/core";
import { MatDialogRef } from "@angular/material";

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'tipoencuesta-delete-dialog-forbidden',
  templateUrl: 'tipoencuesta-delete-dialog-forbidden.html',
})
export class TipoEncuestaDeleteDialogForbidden {

  constructor(public dialogRef: MatDialogRef<TipoEncuestaDeleteDialogForbidden>,
    private authService: AuthService,
    private translate: TranslateService) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.setIdioma(); 
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}