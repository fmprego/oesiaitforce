import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { TipoEncuesta } from './../../models/tipoencuesta';
import { TipoEncuestaService } from '../../services/tipoencuesta.service';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tipoencuesta-form',
  templateUrl: 'tipoencuesta-form.component.html',
  styleUrls: ['tipoencuesta-form.component.css']
})

export class TipoEncuestaFormComponent implements OnInit {

  model: TipoEncuesta = new TipoEncuesta();

  constructor(
    private tipoEncuestaService: TipoEncuestaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService    
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });    
  }

  ngOnInit() {
    this.setIdioma(); 

    if (this.model.id) {
      this.tipoEncuestaService.getTipoEncuesta(this.model.id)
        .subscribe(data => this.setTipoEncuestaData(data));
    }    
  }

  setTipoEncuestaData(tipo: TipoEncuesta) {
    this.model.nombre = tipo.nombre;
  }

  submit() {
    var result$: Observable<TipoEncuesta>;
    if (this.model.id) result$ = this.tipoEncuestaService.updateTipoEncuesta(this.model);
    else result$ = this.tipoEncuestaService.createTipoEncuesta(this.model);

    result$.subscribe(
      res => {
        var texto1 = "";
        var texto2 = "";

        this.translate.get('tipoencuesta.cambiosguardados').subscribe(translation => {
          texto1 = translation
        });

        this.translate.get('tipoencuesta.tipoencuestacreado').subscribe(translation => {
          texto2 = translation
        });

        const msg = (this.model.id) ? texto1: texto2;
        this.showMsg(msg);
        this.router.navigate(['/tiposencuesta']);
      },
      err => {
        var texto = "";

        this.translate.get('tipoencuesta.compruebadatos').subscribe(translation => {
          texto = translation
        });

        this.showError(texto)
      }
    );
  }

  cancel() {
    this.router.navigate(['/tiposencuesta']);
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('tipoencuesta.errordatos').subscribe(translation => {
      texto = translation
    });

    this.snackBar.open(text,texto, {duration:5000,panelClass:['red-snackbar']});
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('tipoencuesta.hecho').subscribe(translation => {
      texto = translation
    });

    this.snackBar.open(text,texto, {duration:5000});
  }  

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

}