import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { GeografiaService } from '../../services/geografia.service';
import { Geografia } from '../../models/geografia';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-geografias-list',
  templateUrl: 'geografias-list.component.html',
  styleUrls: ['geografias-list.component.css']
})

export class GeografiasListComponent implements OnInit {
  
  geografias: Geografia[];
  loading: boolean = false;
  
  constructor(
    private geografiaService: GeografiaService,
    private snackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.setIdioma(); 

    this.populateGeografias();
  }

  populateGeografias() {
    this.loading = true;
    this.geografiaService.getGeografiasSimple()
      .subscribe(geografias => {
        this.geografias = geografias;
        this.loading = false;
      });
  }

  newGeografia() {
    this.router.navigate(['/geografia-form']);
  }

  deleteGeografia(geografia: Geografia) {
    this.geografiaService.deleteGeografia(geografia.id)
      .subscribe(res => {

        var texto = "";
        var titulo = "";

        this.translate.get('geografia.geografiaeliminada').subscribe(translation => {
          texto = translation
        });

        this.translate.get('geografia.eliminada').subscribe(translation => {
          titulo = translation
        });

        this.snackBar.open(texto,titulo, {duration:5000});
        this.populateGeografias();
      })
  }

  editGeografia(geografia: Geografia) {
    this.router.navigate([`/geografia-form/${geografia.id}`]);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}