import { Component, Inject } from "@angular/core";
import { MatDialogRef } from "@angular/material";

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'geografia-delete-dialog',
  templateUrl: 'geografia-delete-dialog.html',
})
export class GeografiaDeleteDialog {

  constructor(public dialogRef: MatDialogRef<GeografiaDeleteDialog>,
    private authService: AuthService,
    private translate: TranslateService  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.setIdioma(); 
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

}