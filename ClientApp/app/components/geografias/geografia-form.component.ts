import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Geografia } from './../../models/geografia';
import { GeografiaService } from '../../services/geografia.service';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-geografia-form',
  templateUrl: 'geografia-form.component.html',
  styleUrls: ['geografia-form.component.css']
})

export class GeografiaFormComponent implements OnInit {

  model: Geografia = new Geografia();

  constructor(
    private geografiaService: GeografiaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService    
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });    
  }

  ngOnInit() {
    this.setIdioma(); 

    if (this.model.id) {
      this.geografiaService.getGeografia(this.model.id)
        .subscribe(data => this.setGeografiaData(data));
    }    
  }

  setGeografiaData(geo: Geografia) {
    this.model.nombre = geo.nombre;
  }

  submit() {
    var result$: Observable<Geografia>;
    if (this.model.id) result$ = this.geografiaService.updateGeografia(this.model);
    else result$ = this.geografiaService.createGeografia(this.model);

    result$.subscribe(
      res => {
        var texto1 = "";
        var texto2 = "";

        this.translate.get('geografia.cambiosguardados').subscribe(translation => {
          texto1 = translation
        });

        this.translate.get('geografia.geografiacreada').subscribe(translation => {
          texto2 = translation
        });

        const msg = (this.model.id) ? texto1: texto2;
        this.showMsg(msg);
        this.router.navigate(['/geografias']);
      },
      err => {
        var texto = "";

        this.translate.get('geografia.compruebadatos').subscribe(translation => {
          texto = translation
        });

        this.showError(texto)
      }
    );
  }

  cancel() {
    this.router.navigate(['/geografias']);
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('geografia.errordatos').subscribe(translation => {
      texto = translation
    });

    this.snackBar.open(text,texto, {duration:5000,panelClass:['red-snackbar']});
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('geografia.hecho').subscribe(translation => {
      texto = translation
    });

    this.snackBar.open(text,texto, {duration:5000});
  }  

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

}