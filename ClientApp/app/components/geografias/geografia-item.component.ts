import { GeografiaDeleteDialogForbidden } from './dialogs/geografia-delete-dialog-forbidden';
import { AdminService } from './../../services/admin.service';
import { Component, Input, Output, EventEmitter, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { Geografia } from './../../models/geografia';
import { GeografiaDeleteDialog } from './dialogs/geografia-delete-dialog';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-geografia-item',
  templateUrl: 'geografia-item.component.html',
  styleUrls: ['geografia-item.component.css']
})

export class GeografiaItemComponent implements OnInit {
  
  @Input() geografia: Geografia;
  @Output() onDelete = new EventEmitter<Geografia>();
  @Output() onEdit = new EventEmitter<Geografia>();
  loading: boolean = false;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private translate: TranslateService) {}

  ngOnInit() {
    this.setIdioma(); 

    this.loading = false;
  }

  openDeleteDialog(): void {
    if (this.geografia.adminCount) {
      let dialogRef = this.dialog.open(GeografiaDeleteDialogForbidden, {width: '60%'});
    } else {
      let dialogRef = this.dialog.open(GeografiaDeleteDialog, {width: '60%'});
      dialogRef.afterClosed().subscribe(result => {
        if (result) this.delete();
      });
    }
  }

  delete() {
    this.onDelete.emit(this.geografia);
  }

  edit() {
    this.onEdit.emit(this.geografia);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}