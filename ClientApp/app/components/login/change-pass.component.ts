import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { PassChange } from './../../models/passchange';
import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-change-pass',
  templateUrl: 'change-pass.component.html',
  styleUrls: ['change-pass.component.css']
})

export class ChangePassComponent {

  model = new PassChange('', '', '', '', '');

  constructor(
    private authService: AuthService,
    private translate: TranslateService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.setIdioma();
}

  cancel() {
    this.router.navigate(['/home']);
  }

  submit() {
    var texto = "";

    if (this.model.oldPass.length < 8
        || this.model.newPass.length < 8
        || this.model.newPass2.length < 8) {

      this.translate.get('login.pwd8').subscribe(translation => {
        texto = translation;        
      });  

      return this.showError(texto)
    }
    if (this.model.newPass !== this.model.newPass2) {
      this.translate.get('login.pwdnocoincide').subscribe(translation => {
        texto = translation;        
      });  
      return this.showError(texto);
    }
    this.model.email = this.authService.getCurrentEmail();
    
    this.authService.changePassword(this.model)
      .subscribe(
        res => {
          this.translate.get('login.pwdcambiado').subscribe(translation => {
            texto = translation;        
          });            
          this.showMsg(texto);
          this.router.navigate(['/home']);
        },
        err => {
          if (err._body) {
            let errJSON = JSON.parse(err._body);
            if (errJSON.errors && errJSON.errors.length > 0) {
              return this.showError(errJSON.errors[0].description);
            }
          }

          this.translate.get('login.errorpwdcambio').subscribe(translation => {
            texto = translation;        
          });           
          return this.showError(texto);
        }
      );
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('login.pwdcambiada').subscribe(translation => {
      texto = translation;        
    });     
    this.snackBar.open(text,texto, {duration:5000});
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('login.erroridentificacion').subscribe(translation => {
      texto = translation;        
    });        
    this.snackBar.open(text,texto, {duration:5000,panelClass:['red-snackbar']});
  }
  
  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
  }
}