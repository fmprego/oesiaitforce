import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { PassChange } from './../../models/passchange';
import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { RecoveryPasswordService } from '../../services/recovery-password.service';


@Component({
  selector: 'app-recovery-pass',
  templateUrl: 'recovery-pass.component.html',
  styleUrls: ['recovery-pass.component.css']
})


export class RecoveryPassComponent implements OnInit {

  pwdData = [];
  model = new PassChange('', '', '', '', '');
  strValidationCode : string;
  email : string;
  idiomaLocale: string = "";

  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private router: Router,
    private snackBar: MatSnackBar,
    private recoveryPasswordService: RecoveryPasswordService

  ) {
  }

  ngOnInit(){
    this.setIdioma();
    this.activatedRoute.params.subscribe(routeParams => {

        this.strValidationCode = routeParams['validationcode'];
        this.email = routeParams['email'];
        this.model.validationCode =  this.strValidationCode;
        var decodedMail = decodeURIComponent(atob(this.email).replace(' ',''));
        this.model.email = decodedMail;
    });
  } 

  cancel() {
    this.router.navigate(['/home']);
  }

  submit() {
    var texto = "";

    if (this.model.newPass.length < 8
        || this.model.newPass2.length < 8) {

      this.translate.get('login.pwd8').subscribe(translation => {
        texto = translation;        
      });  

      return this.showError(texto)
    }
    if (this.model.newPass !== this.model.newPass2) {
      this.translate.get('login.pwdnocoincide').subscribe(translation => {
        texto = translation;        
      });  
      return this.showError(texto);
    }
    this.recoveryPasswordService.setNewPassword(this.model)
      .subscribe(
        res => {
          this.translate.get('login.pwdcambiado').subscribe(translation => {
            texto = translation;        
          });            
          this.showMsg(texto);
          this.router.navigate(['/login']);
        },
        err => {
          this.router.navigate(['/login']);
        }
      );
      
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('login.pwdcambiada').subscribe(translation => {
      texto = translation;        
    });     
    this.snackBar.open(text,texto, {duration:5000});
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('login.erroridentificacion').subscribe(translation => {
      texto = translation;        
    });        
    this.snackBar.open(text,texto, {duration:5000,panelClass:['red-snackbar']});
  }
  
  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 
      this.translate.use(idiomaLocale);
  }
}