import { Router } from '@angular/router';
import { Component } from '@angular/core';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { RecoveryPasswordService } from '../../services/recovery-password.service';
import { Login } from '../../models/login';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-mail-recovery-pass',
  templateUrl: 'mail-recovery-pass.component.html',
  styleUrls: ['mail-recovery-pass.component.css']
})

export class MailRecoveryPassComponent {

  model = new Login('', '', '');
  idiomaLocale: string = "";
  private readonly urlRecoverPass: string = '/api/pwdrecovery';

  constructor(
    private authService: AuthService,
    private recoveryPasswordService: RecoveryPasswordService,
    private translate: TranslateService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.setIdioma();
  }

  cancel() {
    this.router.navigate(['/home']);
  }

  getCurrentAbsoluteSiteUrl(): string {
    if (window
      && "location" in window
      && "protocol" in window.location
      && "host" in window.location) {
      return window.location.protocol + "//" + window.location.host;
    }
    return null;
  }

  submit() {
   this.model.url = this.getCurrentAbsoluteSiteUrl();
   var texto = "";
   var result$ = this.recoveryPasswordService.mailRecoverPass(this.model);
   
   result$.subscribe(
       token => {          
         this.authService.setSession(token, this.idiomaLocale);
         this.setIdioma();
         this.translate.get('login.correcto').subscribe(translation => {
           texto = translation;        
         });  
         this.showMsg(texto);
         this.router.navigate(['/home']);
       },
       err => {
         this.translate.get('login.incorrecto').subscribe(translation => {
           texto = translation;        
         });  
         this.showError(texto);
       }
    );
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('login.bienvenido').subscribe(translation => {
      texto = translation;        
    });     
    this.snackBar.open(text,texto, {duration:5000});
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('login.erroridentificacion').subscribe(translation => {
      texto = translation;        
    });        
    this.snackBar.open(text,texto, {duration:5000,panelClass:['red-snackbar']});
  }
  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 
      this.translate.use(idiomaLocale);
  }
}