import { Component } from '@angular/core';
import { Login } from '../../models/login';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { IdiomaEncuesta } from "../../models/encuesta";
import { EncuestaService } from "../../services/encuesta.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  idiomas: IdiomaEncuesta[];
  idiomaLocale: string = "";
  model = new Login('', '', '');

  constructor(
    private authService: AuthService,
    private encuestaService: EncuestaService,
    private translate: TranslateService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) { }

  setIdioma()
  {
      this.idiomaLocale = this.translate.getBrowserLang();

      var idiomaLocale = this.authService.getCurrentLocale();
      if (idiomaLocale != "")
      {              
        this.idiomaLocale = idiomaLocale;      
      } 

      this.translate.use(this.idiomaLocale);
  }

  public ngOnInit() {        
    this.setIdioma();

    this.encuestaService.getIdiomas()
    .subscribe(idi => {      
      this.idiomas = idi
    });   
  }

  submit() {
    var texto = "";

    if (this.model.password.length < 8) {      
      this.translate.get('login.pwd8').subscribe(translation => {
        texto = translation;        
      });     
       
      return this.showError(texto);

    } else {
      var result$ = this.authService.login(this.model);
      result$.subscribe(
        token => {          
          this.authService.setSession(token, this.idiomaLocale);

          this.setIdioma();

          this.translate.get('login.correcto').subscribe(translation => {
            texto = translation;        
          });  

          this.showMsg(texto);
          this.router.navigate(['/home']);
        },
        err => {
          this.translate.get('login.incorrecto').subscribe(translation => {
            texto = translation;        
          });  
          this.showError(texto);
        }
      );
    }
  }

  sendPass(){
    if(this.model.email.length == 0){
      var texto = "";
      var titulo = "";

      this.translate.get('login.escribetumail').subscribe(translation => {
        texto = translation;        
      }); 
      
      this.translate.get('login.escribetumailtitulo').subscribe(translation => {
        titulo = translation;        
      });       

      return this.snackBar.open(texto, titulo, {duration:10000});
    }
  }

  showMsg(text: string) {
    var texto = "";

    this.translate.get('login.bienvenido').subscribe(translation => {
      texto = translation;        
    });     
    this.snackBar.open(text,texto, {duration:5000});
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('login.erroridentificacion').subscribe(translation => {
      texto = translation;        
    });        
    this.snackBar.open(text,texto, {duration:5000,panelClass:['red-snackbar']});
  }

  recoverPassView(){
    this.router.navigate(['/mail-recovery-pass']);
  }
}