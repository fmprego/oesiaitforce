import { InformeService } from "./../../services/informe.service";
import { GenericDialog } from "./../app/dialogs/generic.dialog";
import { Component, ViewChild, OnInit } from "@angular/core";
import { MatTableDataSource, MatSort, MatPaginator, MatPaginatorIntl, MatDialog, MatSnackBar } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";

import { merge } from "rxjs/observable/merge";
import "rxjs/add/operator/map";
import { startWith } from "rxjs/operators/startWith";
import { switchMap } from "rxjs/operators/switchMap";
import { Router, ActivatedRoute } from "@angular/router";

import { EncuestaService, EncuestasResult } from "./../../services/encuesta.service";
import { Encuesta, EstadoEncuesta, IdiomaEncuesta } from "../../models/encuesta";
import { ActivacionDialog } from "./dialogs/activacion.dialog";
import { LinkEncuestaDialog } from "./dialogs/link-encuesta.dialog";
import { EncuestaDeleteDialog } from "./dialogs/encuesta-delete.dialog";
import { Observable } from "rxjs/Observable";
import { SendMailDialog } from "./dialogs/send-mail.dialog";
import { ModificarEncuestaActivaDialog } from "./dialogs/modificar-encuesta-activa.dialog";
import { ContextMenuComponent } from "ngx-contextmenu";

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: "app-encuestas-list",
    templateUrl: "encuestas-list.component.html",
    styleUrls: ["./encuestas-list.component.css"],
})

export class EncuestasListComponent implements OnInit {

    public displayedColumns = ["titulo", "estado", "descripcion", "administrador", "geografia", "idioma", "actions"];
    public dataSource = new MatTableDataSource<Encuesta>([]);
    public totalItems: number = 0;
    public loading: boolean = false;
    public cerradas: boolean = false;
    idiomas: IdiomaEncuesta[];

    @ViewChild(MatSort) public sort: MatSort;
    @ViewChild(MatPaginator) public paginator: MatPaginator;
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;

    constructor(
        private encuestaService: EncuestaService,
        private informeService: InformeService,
        private router: Router,
        private snackBar: MatSnackBar,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private authService: AuthService,
        private translate: TranslateService,
    ) {
        this.cerradas = route.snapshot.url
            .some((it) => it.path === "informes");
    }

    public ngOnInit() {
        this.setIdioma(); 
        this.setIdiomaPaginador();

        this.encuestaService.getIdiomas()
        .subscribe(idi => {      
          this.idiomas = idi
        });  
        
        this.loading = true;
    }

    public ngAfterViewInit() {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.populateList();
    }

    public populateList() {
        this.loading = true;
        let result$: Observable<EncuestasResult>;
        if (this.cerradas) {
            result$ = this.encuestaService.getEncuestasCerradas();
        } else {
            result$ = this.encuestaService.getEncuestas();
        }
        result$.subscribe((data) => {
            data.items.map(e => 
                {          
                  this.traducirestado(e.estado);
                  if (this.idiomas != undefined)
                  {
                    this.idiomas.forEach(i => { if (i.locale == e.idioma.locale) 
                        {
                        e.idioma.nombre = i.nombre;             
                        }
                    });    
                  }
                });

            this.dataSource.data = data.items;
            this.totalItems = data.totalItems;
            this.loading = false;
        });
    }

    public applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }

    public newEncuesta() {
        this.router.navigate(["/encuesta"]);
    }

    public edit(encuesta: Encuesta) {
        if(encuesta.estado.id===3){
            const dialogRef = this.dialog.open(ModificarEncuestaActivaDialog,{
                width: "60%",
            });
            dialogRef.afterClosed().subscribe((result) =>{
                if(result)
                {
                this.router.navigate([`/encuesta/resumen/${encuesta.id}`]);
                }
            });
        } else {
            this.router.navigate([`/encuesta/resumen/${encuesta.id}`]);
        }
    }

    public activate(encuesta: Encuesta) {
        const dialogRef = this.dialog.open(ActivacionDialog, {
            width: "60%",
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                const sendMailsDialog = this.dialog.open(SendMailDialog, {
                    width: "60%",
                });
                sendMailsDialog.afterClosed().subscribe((result) => {
                  
                    if(result){
                        encuesta.sendMails = true;
                        var encrypt = "encuesta" + encuesta.id;
                        encuesta.url = this.getCurrentAbsoluteSiteUrl() + '/encuestas/cuestionario/'+btoa(encrypt);
                        
                    }else{
                        encuesta.sendMails = false;
                    }
                    this.encuestaService.activarEncuesta(encuesta)
                            .subscribe(
                                () => this.populateList(),
                                (err) => this.showError(err),
                        );
                    
                });
            }
        });
    }

    public getlink(encuesta: Encuesta) {
        this.dialog.open(LinkEncuestaDialog, {
            data: { id: encuesta.id },
            width: "60%",
        });
    }

    public gotolink(encuesta: Encuesta) {

        var encrypt = "encuesta" + encuesta.id;
        this.router.navigate([`/encuestas/cuestionario/${btoa(encrypt)}`]);
    }

    public copylink(encuesta: Encuesta) {

        var encrypt = "encuesta" + encuesta.id;

        let selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = this.getCurrentAbsoluteSiteUrl() + `/encuestas/cuestionario/${btoa(encrypt)}`;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);

        var texto = "";
        var titulo = "";

        this.translate.get('encuesta.clipboard').subscribe(translation => {
          texto = translation
        });

        this.translate.get('encuesta.ok').subscribe(translation => {
            titulo = translation
        });        

        this.snackBar.open(
            texto,
            titulo,
            { duration: 2000 },
        );

    }

    public getCurrentAbsoluteSiteUrl(): string {
        if (window
            && "location" in window
            && "protocol" in window.location
            && "host" in window.location) {
          return window.location.protocol + "//" + window.location.host ;
        }
        return null;
      }

    public gotoinforme(encuesta: Encuesta) {
        this.informeService.generarInformes(encuesta.id)
            .subscribe(
                () => this.router.navigate([`/informes/${encuesta.id}`]),
                (err) => this.showError(err),
        );
    }

    public showError(err: Error) {
        // TODO console.error(err);
        this.loading = false;

        var texto = "";
        var titulo = "";

        this.translate.get('encuesta.errorOops').subscribe(translation => {
          texto = translation
        });

        this.translate.get('encuesta.ok').subscribe(translation => {
            titulo = translation
        });  

        this.snackBar.open(
            texto,
            titulo,
            { duration: 2000 },
        );
    }

    public openDeleteDialog(encuesta: Encuesta) {
        const dialogRef = this.dialog.open(EncuestaDeleteDialog, {
            data: {
                titulo: encuesta.titulo,
            },
            width: "60%",
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) { this.remove(encuesta); }
        });
    }

    public remove(encuesta: Encuesta) {
        this.encuestaService.delete(encuesta)
            .subscribe(() => {
                var texto = "";
                var titulo = "";
        
                this.translate.get('encuesta.encuestaeliminada').subscribe(translation => {
                  texto = translation
                });
        
                this.translate.get('encuesta.eliminado').subscribe(translation => {
                    titulo = translation
                }); 

                this.snackBar.open(texto, titulo, { duration: 5000 });
                this.populateList();
            }, (err) => {
                var texto = "";
                var titulo = "";
        
                this.translate.get('encuesta.noeliminarencuesta').subscribe(translation => {
                  texto = translation
                });
        
                this.translate.get('encuesta.atencion').subscribe(translation => {
                    titulo = translation
                }); 

                this.snackBar.open(texto, titulo, { duration: 5000, panelClass: ['red-snackbar'] });
            });
    }

    public openCloseDialog(encuesta: Encuesta) {
        var texto = "";

        this.translate.get('encuesta.preguntacerrar').subscribe(translation => {
          texto = translation
        });

        const dialogRef = this.dialog.open(GenericDialog, {
            data: {
                titulo: encuesta.titulo,
                texto: texto,
            },
            width: "60%",
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) { this.close(encuesta); }
        });
    }

    public close(encuesta: Encuesta) {
        this.encuestaService.cerrarEncuesta(encuesta)
            .subscribe(
                () => this.populateList(),
                (err) => {
                    var texto = "";
                    var titulo = "";

                    this.translate.get('encuesta.nocerrar').subscribe(translation => {
                      texto = translation
                    });

                    this.translate.get('encuesta.atencion').subscribe(translation => {
                        titulo = translation
                    });                     
                    
                    this.snackBar.open(texto, titulo, { duration: 5000, panelClass: ['red-snackbar'] });
                },
        );
    }

    public toHistorical(encuesta: Encuesta) {
        this.encuestaService.pasoHistoricoEncuesta(encuesta)
            .subscribe(
                () => {
                    var texto = "";

                    this.translate.get('encuesta.historico').subscribe(translation => {
                      texto = translation
                    });

                    this.showMsg(texto);
                    this.populateList();
                },
                (err) => {
                    var texto = "";

                    this.translate.get('encuesta.nohistorico').subscribe(translation => {
                      texto = translation
                    });

                    this.showErrorMsg(texto);
                },
        )
    }


    public copyEncuesta(encuesta: Encuesta) {
        this.encuestaService.copiarEncuesta(encuesta)
            .subscribe(
                () => {
                    var texto = "";

                    this.translate.get('encuesta.copiada').subscribe(translation => {
                        texto = translation
                    });   

                    this.showMsg(texto);
                    this.populateList();
                },
                (err) => {
                    var texto = "";

                    this.translate.get('encuesta.errorcopiar').subscribe(translation => {
                        texto = translation
                    });   

                    this.showErrorMsg(texto);
                },
        )
    }

    public showMsg(text: string) {
        var titulo = "";

        this.translate.get('encuesta.ok').subscribe(translation => {
            titulo = translation
        });           
        this.snackBar.open(text, titulo, { duration: 5000 });
    }

    public showErrorMsg(text: string) {
        var titulo = "";

        this.translate.get('encuesta.error').subscribe(translation => {
            titulo = translation
        });           
        this.snackBar.open(text, titulo, { duration: 5000 });
    }

    traducirestado(estado: EstadoEncuesta)
    {
  
      // Creada : 0, Pendiente : 1, Preactiva : 2, Activa : 3, Cerrada : 4, Historico : 5
      switch(estado.id) { 
        case 0: { 
          this.translate.get('estadoencuesta.creada').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 1: { 
          this.translate.get('estadoencuesta.pendiente').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 2: { 
          this.translate.get('estadoencuesta.preactiva').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 3: { 
          this.translate.get('estadoencuesta.activa').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        }  
        case 4: { 
          this.translate.get('estadoencuesta.cerrada').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        } 
        case 5: { 
          this.translate.get('estadoencuesta.historico').subscribe(translation => {
            estado.descripcion = translation
          });
          break; 
        }                 
        default: { 
           //statements; 
           break; 
        } 
     }     
    }

    setIdioma()
    {
        var idiomaLocale = this.translate.getBrowserLang();
  
        var lidiomaLocale = this.authService.getCurrentLocale();
        if (lidiomaLocale != "")
        {              
          idiomaLocale = lidiomaLocale;      
        } 
  
        this.translate.use(idiomaLocale);
        
    }

    setIdiomaPaginador()
    {
        this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
          this.paginator._intl.itemsPerPageLabel = translation
        });
        this.translate.get('paginador.firstPageLabel').subscribe(translation => {
          this.paginator._intl.firstPageLabel = translation
        });
        this.translate.get('paginador.lastPageLabel').subscribe(translation => {
          this.paginator._intl.lastPageLabel = translation
        });
        this.translate.get('paginador.nextPageLabel').subscribe(translation => {
          this.paginator._intl.nextPageLabel = translation
        });
        this.translate.get('paginador.previousPageLabel').subscribe(translation => {
          this.paginator._intl.previousPageLabel = translation
        });                        
    }

}
