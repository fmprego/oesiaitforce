import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatPaginatorIntl } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { forkJoin } from "rxjs/observable/forkJoin";
import { MatSnackBar } from '@angular/material';

import { Categoria } from '../../../models/categoria';
import { ActivatedRoute, Router } from '@angular/router';
import { Encuesta } from '../../../models/encuesta';
import { EncuestaService } from './../../../services/encuesta.service';
import { Observable } from 'rxjs/Observable';
import { CategoriaService } from '../../../services/categoria.service';

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-select-categorias',
  templateUrl: 'select-categorias.component.html',
  styleUrls: ['select-categorias.component.css'],
})

export class SelectCategoriasComponent implements OnInit {

  model: Encuesta = new Encuesta();
  displayedColumns = ['select', 'nombre', 'preguntasCount'];
  dataSource = new MatTableDataSource<Categoria>([]);
  selection = new SelectionModel<Categoria>(true, []);
  totalItems: number = 0;
  loading: boolean = false;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private encuestaService: EncuestaService,
    private categoriaService: CategoriaService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private translate: TranslateService,
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });
  };

  ngOnInit() {
    this.setIdioma(); 

    this.loading = true;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    let sources: Observable<any>[] = [
      this.categoriaService.getCategorias(),
    ];
    if (this.model.id) {
      sources.push(this.encuestaService.getEncuesta(this.model.id));
    }
    forkJoin(sources).subscribe(data => {
      this.dataSource.data = data[0].items;
      this.totalItems = data[0].totalItems;
      this.model = data[1];
      this.loading = false;
      this.applySelection(this.dataSource.data);
    });
  }

  applySelection(categorias: Categoria[]) {
    const ids = this.model.categorias.map(it => it.id);
    const selectedCategorias = categorias.filter(it => ids.indexOf(it.id) > -1);
    selectedCategorias.forEach(it => this.selection.select(it));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  save() {
    this.loading = true;
    this.encuestaService.updateCategorias(this.model, this.selection.selected)
      .subscribe(
        data => {
          this.loading = false;
          this.router.navigate([`/encuesta/resumen/${this.model.id}`]);
        },
        err => {
          this.loading = false;

          var texto = "";

          this.translate.get('encuesta.incluircategorias').subscribe(translation => {
            texto = translation
          });      

          this.showError(texto);
        }
      );
  }

  back() {
    this.router.navigate([`/encuesta/resumen/${this.model.id}`]);
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('encuesta.error').subscribe(translation => {
      texto = translation
    });  

    this.snackBar.open(text,texto,{duration:5000,panelClass:['red-snackbar']})
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}