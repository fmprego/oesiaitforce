import { forkJoin } from "rxjs/observable/forkJoin";
import { Component, OnInit } from "@angular/core";
import { Encuesta, IdiomaEncuesta } from "../../../models/encuesta";
import { EncuestaService } from "../../../services/encuesta.service";
import { TipoEncuesta } from "../../../models/tipoencuesta";
import { TipoEncuestaService } from "../../../services/tipoencuesta.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { EstructuraService } from "../../../services/estructura.service";
import { MatDialog, MatSnackBar } from "@angular/material";
import { GenericErrorDialog } from "../../common/dialog/generic-error.dialog";
import { AprobacionDialog } from "../../revisiones/dialogs/aprobacion.dialog";
import { ActivacionDialog } from "../dialogs/activacion.dialog";
import { SendMailDialog } from "../dialogs/send-mail.dialog";

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { modelGroupProvider } from "@angular/forms/src/directives/ng_model_group";
import { AnimationGroupPlayer } from "@angular/animations/src/players/animation_group_player";
import { EncuestaPregunta } from "../../../models/encuesta-pregunta";

@Component({
  selector: "app-resumen-encuesta",
  templateUrl: "resumen-encuesta.component.html",
  styleUrls: ["resumen-encuesta.component.css"],
})

export class ResumenEncuestaComponent implements OnInit {

  public model: Encuesta = new Encuesta();  
  public loading: boolean = false;
  public editing: boolean = false;
  public lideres: number;
  public receptores: number;
  public editMode: boolean = false;
  idiomas: IdiomaEncuesta[];
  tiposencuesta: TipoEncuesta[];
  validaciones: string[] = [];
  valido: boolean = true;

  constructor(
    private encuestaService: EncuestaService,
    private estructuraService: EstructuraService,
    private tipoencuestaService: TipoEncuestaService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService,    
  ) {
    route.params.subscribe((p) => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  public ngOnInit() {

    this.setIdioma(); 

    this.loading = true;

    this.encuestaService.getIdiomas()
    .subscribe(idi => {      
      this.idiomas = idi
    });  

    this.tipoencuestaService.getTiposEncuesta()
    .subscribe(tipos => {      
      this.tiposencuesta = tipos
    });  

    this.encuestaService.getEncuesta(this.model.id, true)
      .subscribe((encuesta) => {   
                
        this.idiomas.forEach(i => { if (i.locale == encuesta.idioma.locale) 
          {
            encuesta.idioma.nombre = i.nombre;             
          }
        });           
        
       // 0: texto 1: //Opcion 2: //Escala 3: //Comentarios (No es obligatorio)       
        encuesta.categorias.forEach(categoria => {

        var peso = 0;
        var existenPesos = false;
        encuesta.preguntas.forEach(pregunta => {
          if (pregunta.categoriaId == categoria.id)
            {
              if (pregunta.tipoPregunta.id != 0 && pregunta.tipoPregunta.id != 3)
              {
                existenPesos =true;
                peso += pregunta.peso;
              }
            }
          })        

          /* POR AHORA NO LO COMPROBAMOS
        if (existenPesos && peso != 1.0 ) 
        {
          this.validaciones.push(categoria.nombre);
          this.valido = false;
        }*/

        });

        const sources: Array<Observable<any>> = [
          this.estructuraService.getTotalLideres(encuesta.estructuraId),
          this.estructuraService.getTotalReceptores(encuesta.estructuraId),
        ];
        forkJoin(sources).subscribe(
          (data) => {
            this.lideres = data[0];
            this.receptores = data[1];
            this.loading = false;
          },
          (err) => {
            this.showError(err);
            this.lideres = 0;
            this.receptores = 0;
            this.loading = false;
          },
        );

        this.model = encuesta; 

        
      });       
  }

  public populateList() {
    this.loading = true;
    this.encuestaService.getEncuesta(this.model.id, true)
      .subscribe((encuesta) => {

        this.model = encuesta;
        const sources: Array<Observable<any>> = [
          this.estructuraService.getLideres(encuesta.estructuraId),
          this.estructuraService.getReceptores(encuesta.estructuraId),
        ];
        forkJoin(sources).subscribe(
          (data) => {
            this.lideres = data[0].totalItems;
            this.receptores = data[1].totalItems;
            this.loading = false;
          },
          (err) => {
            this.showError(err);
            this.lideres = 0;
            this.receptores = 0;
            this.loading = false;
          },
        );
      });
  }
  public goToCategorias() {
    this.router.navigate([`/encuesta/categorias/${this.model.id}`]);
  }

  public goToPreguntas() {
    this.router.navigate([`/encuesta/preguntas/${this.model.id}`]);
  }

  public goToEstructura() {
    this.router.navigate([`/encuesta/estructura/${this.model.id}`]);
  }

  public goToFechas() {
    this.router.navigate([`/encuesta/fechas/${this.model.id}`]);
  }

  public encolar() {
    this.loading = true;
    this.encuestaService.encolarEncuesta(this.model)
      .subscribe(
        (res) => {
          this.loading = false;
          this.router.navigate([`/encuestas`]);
        },
        (err) => {

          var texto = "";

          this.translate.get('encuesta.revisarlog').subscribe(translation => {
            texto = translation
          });  

          const json = !!err ? err.json() : undefined;
          const errText = !!json ? json.Message : undefined;
          this.dialog.open(GenericErrorDialog, {
            data: {
              error: errText || texto,
            },
            width: "60%",
          });
          this.loading = false;
        },
    );
  }

  public toggleEditMode() {
    this.editMode = !this.editMode;
  }

  public edit() {    
    this.onCambioIdioma();
    this.onCambioTipoEncuesta();
    this.editing = true;

    this.encuestaService.updateEncuesta(this.model)
      .subscribe(
        () => {
          var texto = "";

          this.translate.get('encuesta.encuestamodificada').subscribe(translation => {
            texto = translation
          }); 

          this.showMsg(texto);
          this.editing = false;
          this.toggleEditMode();
        },
        (err) => {
          var texto = "";

          this.translate.get('encuesta.encuestaerrormodificando').subscribe(translation => {
            texto = translation
          }); 

          this.showMsg(texto);
          this.editing = false;
          this.toggleEditMode();
        },
    );
  }

  public onCambioIdioma()
  {
    try
    {
      this.model.idioma = this.idiomas.find(x=>x.id == this.model.idiomaId);          
    }
    catch (Exception)
    {
      var texto = "";

      this.translate.get('encuesta.errorasignandoidioma').subscribe(translation => {
        texto = translation
      }); 

      alert(texto);
    }
  }

  public onCambioTipoEncuesta()
  {
    try
    {    
      this.model.tipoEncuesta = this.tiposencuesta.find(x=>x.id == this.model.tipoEncuestaId);                
    }
    catch (Exception)
    {
      var texto = "";

      this.translate.get('encuesta.errorasignandotipoencuesta').subscribe(translation => {
        texto = translation
      }); 

      alert(texto);
    }
  }

  public showMsg(text: string) {
    var texto = "";

    this.translate.get('encuesta.ok').subscribe(translation => {
      texto = translation
    }); 

    this.snackBar.open(text, texto, { duration: 2000 });
  }

  public back() {
    this.router.navigate(["/encuestas"]);
  }

  public aprobar() {
    const dialogRef = this.dialog.open(AprobacionDialog, {
      width: "60%",
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.encuestaService.aprobarEncuesta(this.model)
          .subscribe(
            () => this.back(),
            (err) => 
            {
              var texto = "";

              this.translate.get('encuesta.erroraprobando').subscribe(translation => {
                texto = translation
              }); 

              this.showMsg(texto)
            },
        );
      }
    });
  }

  public lanzar() {
    const dialogRef = this.dialog.open(ActivacionDialog, {
      width: "60%",
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const sendMailsDialog = this.dialog.open(SendMailDialog, {
          width: "60%",
        });
        sendMailsDialog.afterClosed().subscribe((result) => {

          if (result) {
            this.model.sendMails = true;
            var encrypt = "encuesta" + this.model.id;
            this.model.url = this.getCurrentAbsoluteSiteUrl() + '/encuestas/cuestionario/' + btoa(encrypt);

          } else {
            this.model.sendMails = false;
          }
          this.encuestaService.activarEncuesta(this.model)
            .subscribe(
              () => this.populateList(),
              (err) => this.showError(err),
          );

        });
      }
    });
  }

  public getCurrentAbsoluteSiteUrl(): string {
    if (window
      && "location" in window
      && "protocol" in window.location
      && "host" in window.location) {
      return window.location.protocol + "//" + window.location.host;
    }
    return null;
  }
  public showError(err: Error) {
    // TODO console.error(err);
    var texto = "";

    this.translate.get('encuesta.errorOops').subscribe(translation => {
      texto = translation
    }); 

    var titulo = "";

    this.translate.get('encuesta.ok').subscribe(translation => {
      titulo = translation
    });     

    this.loading = false;
    this.snackBar.open(
      texto,
      titulo,
      { duration: 2000 },
    );
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}
