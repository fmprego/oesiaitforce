import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-load-estructura-no-file',
  template: `
    <h2 mat-dialog-title>No se ha seleccionado ningún archivo.</h2>
    <mat-dialog-content>
      Por favor, selecciona un archivo para cargar la estructura de la encuesta.
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button mat-dialog-close>Aceptar</button>
    </mat-dialog-actions>
  `,
})

export class LoadEstructuraNoFileDialog {

  constructor(public dialogRef: MatDialogRef<LoadEstructuraNoFileDialog>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}