import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { AuthService } from './../../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-alta-lideres",
  templateUrl: "alta-lideres.dialog.html",
  styleUrls: ["alta-lideres.dialog.css"],
})

export class AltaLideresDialog {

  constructor(
    public dialogRef: MatDialogRef<AltaLideresDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private translate: TranslateService,  
  ) { }

  ngOnInit() {
    this.setIdioma();
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}
