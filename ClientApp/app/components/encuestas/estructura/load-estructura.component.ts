import { AltaLideresDialog } from './dialogs/alta-lideres.dialog';
import { LoadEstructuraNoFileDialog } from './dialogs/load-estructura-no-file.dialog';
import { KeysPipe } from './../../../pipes/keys.pipe';
import { EstructuraTableType } from './../../../models/enum/estructura-table-type.enum';
import { EstructuraService } from './../../../services/estructura.service';
import { Component, OnInit, ViewChild, Inject, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { EncuestaService } from './../../../services/encuesta.service';
import { Encuesta } from './../../../models/encuesta';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { PreguntaService } from './../../../services/preguntas.service';
import { Pregunta } from '../../../models/pregunta';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Persona } from '../../../models/persona';
import { GenericErrorDialog } from '../../common/dialog/generic-error.dialog';
import { Lider } from '../../../models/lider';
import { LiderService } from '../../../services/lider.service';

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-load-estructura',
  templateUrl: 'load-estructura.component.html',
  styleUrls: ['load-estructura.component.css']
})

export class LoadEstructuraComponent implements OnInit {

  model: Encuesta = new Encuesta();
  displayedColumns = ['usuarioId', 'nombre', 'nivel1', 'nivel2', 'nivel3', 'nivel4',  'centro', 'email'];
  dataSource = new MatTableDataSource<Persona>([]);
  totalItems: number = 0;  
  file: any;
  loading: boolean = false;
  tableTypes = EstructuraTableType;
  tableType: string = '1';

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;  

  constructor(
    private encuestaService: EncuestaService,
    private estructuraService: EstructuraService,
    private liderService: LiderService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private translate: TranslateService,      
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });    
  }

  ngOnInit() {
    this.setIdioma();
    this.setIdiomaPaginador();

    this.loading = true;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.encuestaService.getEncuesta(this.model.id)
      .subscribe(encuesta => {
        this.model = encuesta;
        this.populateLideres();
      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  handleFileInput(files: FileList) {
    let aux = files.item(0);
    if(aux) {this.file = aux;}
  }

  uploadFile() {
    if (this.file === undefined) return this.openNoFileDialog();
    this.loading = true;
    this.estructuraService.postFile(this.file, this.model.id)
      .subscribe(
        (data) => {          
          this.model.estructuraId = data.encuesta.estructuraId;
          this.feedTable();
        },
        err => {          
          this.openErrorDialog(err)
        }
      );
  }

  openNoFileDialog() {
    this.dialog.open(LoadEstructuraNoFileDialog, { width: '60%' });
  }

  openErrorDialog(err: any) {

    var texto = "";

    this.translate.get('encuesta.revisarlog').subscribe(translation => {
      texto = translation
    });  

    const json = !!err ? err.json() : undefined;
    const errText = !!json ? json.Message : undefined;
    this.dialog.open(GenericErrorDialog, { 
      data: {
        error: errText || texto,
      },
      width: '60%'
    });
    this.loading = false;
  }

  feedTable() {
    return this.tableType === '1' ? 
      this.populateLideres() : this.populateReceptores();
  }

  populateLideres() {
    if (!!this.model.estructuraId) {
      this.loading = true;
      this.estructuraService.getLideres(this.model.estructuraId)
        .subscribe(lideres => {
          this.dataSource.data = lideres.items;
          this.totalItems = lideres.totalItems;
          this.loading = false;
        });
    } else {
      this.loading = false;
    }
  }

  populateReceptores() {
    if (!!this.model.estructuraId) {
      this.loading = true;
      this.estructuraService.getReceptores(this.model.estructuraId)
      .subscribe(receptores => {
        this.dataSource.data = receptores.items;
          this.totalItems = receptores.totalItems;
          this.loading = false;
        });
    } else {
      this.loading = false;
    }
  }

  save() {    
    if (!!this.model.estructuraId) {
      this.loading = true;
      this.estructuraService.getLideres(this.model.estructuraId)
        .subscribe(lideres => {
          const lideresSinUsuario = lideres.items.filter(it => it.usuarioId == null);
          this.loading = false;
          if (lideresSinUsuario.length > 0) {
            this.openAltaLideresDialog(lideresSinUsuario);
          } else {
            this.router.navigate([`/encuesta/resumen/${this.model.id}`])
          }
        });
    } else {
      var texto = "";

      this.translate.get('encuesta.nodatos').subscribe(translation => {
        texto = translation
      });  

      this.dialog.open(GenericErrorDialog, { 
        data: {
          error: texto,
        },
        width: '60%'
      });
    }
  }

  openAltaLideresDialog(lideres: Lider[]) {
    const dialogRef = this.dialog.open(AltaLideresDialog, {
      data: { lideres },
      width: '60%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        var titulo = "";

        this.translate.get('encuesta.error').subscribe(translation => {
          titulo = translation
        }); 

        var texto = "";

        this.translate.get('encuesta.erroraltalideres').subscribe(translation => {
          texto = translation
        });  

        this.loading = true;
        this.liderService.signupAll(this.model.estructuraId)
          .subscribe(
            () => this.router.navigate([`/encuesta/resumen/${this.model.id}`]),
            err => {
              this.snackBar.open(texto, titulo, {duration:5000});
              this.loading = false;
            }
          );
      }
    });
  }

  back() {
    this.router.navigate([`/encuesta/resumen/${this.model.id}`]);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
}