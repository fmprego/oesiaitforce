import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-modificar-encuesta-activa',
  templateUrl: 'modificar-encuesta-activa.dialog.html',
  styleUrls: ['modificar-encuesta-activa.dialog.css'],
})

export class ModificarEncuestaActivaDialog {

  constructor(
    public dialogRef: MatDialogRef<ModificarEncuestaActivaDialog>,
    private authService: AuthService,
    private translate: TranslateService,    
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.setIdioma(); 
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}