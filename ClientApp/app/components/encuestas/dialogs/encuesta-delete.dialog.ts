import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "encuesta-delete-dialog",
  templateUrl: "encuesta-delete-dialog.html",
})
export class EncuestaDeleteDialog {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private translate: TranslateService,
    public dialogRef: MatDialogRef<EncuestaDeleteDialog>,
  ) { }

  ngOnInit() {
    this.setIdioma(); 
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}
