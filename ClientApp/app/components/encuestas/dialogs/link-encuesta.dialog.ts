import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-link-encuesta',
  templateUrl: 'link-encuesta.dialog.html',
  styleUrls: ['link-encuesta.dialog.css'],
})

export class LinkEncuestaDialog {

  public id;

  constructor(
    public dialogRef: MatDialogRef<LinkEncuestaDialog>,
    private authService: AuthService,
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.setIdioma(); 
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}