import { MatSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { Encuesta } from '../../../models/encuesta';
import { EncuestaService } from '../../../services/encuesta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material';

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-fechas-encuesta',
  templateUrl: 'fechas-encuesta.component.html',
  styleUrls: ['fechas-encuesta.component.css']
})

export class FechasEncuestaComponent implements OnInit {

  model: Encuesta = new Encuesta();
  loading: boolean = false;
  minFechaInicio: Date = new Date();
  minFechaCierre: Date = new Date();
  maxFechaInicio: Date = new Date(2100, 0, 1);
  maxFechaCierre: Date = new Date(2100, 0, 1);

  constructor(
    private encuestaService: EncuestaService,
    private route: ActivatedRoute,
    private router: Router,  
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService,  
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  ngOnInit() {
    this.setIdioma();
    this.loading = true;
    this.encuestaService.getEncuesta(this.model.id)
    .subscribe(encuesta => {
      this.model = encuesta;
      this.loading = false;
    });    
  }

  fechaInicioInput(event: MatDatepickerInputEvent<Date>) {
    if (event.value) this.minFechaCierre = event.value;
  }

  submit() {
    this.encuestaService.encolarEncuesta(this.model)
      .subscribe(
        res => {
          this.router.navigate([`/encuesta/resumen/${res.id}`]);
        },
       err => {
        var texto = "";

        this.translate.get('encuesta.errorencolar').subscribe(translation => {
          texto = translation
        }); 

        var titulo = "";

        this.translate.get('encuesta.error').subscribe(translation => {
          titulo = translation
        }); 

         this.snackBar.open(texto, titulo, {duration:5000})
       }
      );
  }
  save() {
  this.encuestaService.updateEncuesta(this.model)
    .subscribe(
      res => {
        this.router.navigate([`/encuesta/resumen/${res.id}`]);
      },
      err => this.showError(err)
    );
  }

  back() {
    this.router.navigate([`/encuesta/estructura/${this.model.id}`]);
  }
  
  showError(text: string) {
    var texto = "";

    this.translate.get('encuesta.error').subscribe(translation => {
      texto = translation
    });  

    this.snackBar.open(text, texto, { duration: 5000, panelClass: ['red-snackbar'] });
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}