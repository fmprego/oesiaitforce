import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { EncuestaService } from "./../../../services/encuesta.service";
import { Encuesta } from "./../../../models/encuesta";
import { MatSort, MatSortable, MatPaginator, MatTableDataSource, MatSnackBar} from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";
import { PreguntaService } from "./../../../services/preguntas.service";
import { Pregunta, TipoPregunta } from "../../../models/pregunta";
import { Observable } from "rxjs/Observable";
import { EncuestaPregunta } from "../../../models/encuesta-pregunta";

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-select-preguntas",
  templateUrl: "select-preguntas.component.html",
  styleUrls: ["select-preguntas.component.css"],
})

export class SelectPreguntasComponent implements OnInit {

  model: Encuesta = new Encuesta();
  displayedColumns = ["select", "id", "texto", "categoria", "creator", "tipo", "peso", "actions"];
  dataSource = new MatTableDataSource<Pregunta>([]);
  selection = new SelectionModel<Pregunta>(true, []);
  totalItems: number = 0;
  loading: boolean = false;

  @ViewChild(MatSort) protected sort: MatSort;
  @ViewChild(MatPaginator) protected paginator: MatPaginator;

  constructor(
    private encuestaService: EncuestaService,
    private preguntaService: PreguntaService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private translate: TranslateService,      
  ) {
    route.params.subscribe((p) => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  public ngOnInit() {
    this.setIdioma();
    this.setIdiomaPaginador();
    this.loading = true;
  }

  protected ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.encuestaService.getEncuesta(this.model.id, true)
      .subscribe((encuesta) => {
        this.model = encuesta;
        this.preguntaService.getByCategorias(encuesta.categorias.map((it) => it.id))
          .subscribe((preguntas) => {            

            preguntas.items.sort((t1:Pregunta, t2:Pregunta) => {                
              if (this.model.preguntas.find(x => x.id == t1.id) && !this.model.preguntas.find(x => x.id == t2.id)) {
                  return -1;
              }    
              
              if (!this.model.preguntas.find(x => x.id == t1.id) && this.model.preguntas.find(x => x.id == t2.id)) {
                return 1;
              }               

              if (t1.id > t2.id)
              {
                return -1;
              }
          
              if (t1.id < t2.id)
              {
                return 1;
              }

              return 0;
            });

            preguntas.items.map(e => 
              {          
                this.traducirtipopregunta(e.tipoPregunta);
              });

            this.dataSource.data = preguntas.items;
            this.totalItems = preguntas.totalItems;
            this.loading = false;
            this.applySelection(this.dataSource.data);
            this.initPesos();
          });
      });
  }

  protected applySelection(preguntas: Pregunta[]) {
    const ids = this.model.preguntas.map((it) => it.id);
    const selectedPreguntas = preguntas.filter((it) => ids.indexOf(it.id) > -1);
    selectedPreguntas.forEach((it) => this.selection.select(it));
  }

  protected initPesos() {
    this.encuestaService.getEncuestaPreguntas(this.model.id)
      .subscribe(
        (eps) => {
          this.dataSource.data.forEach((p) => {
            const ep = eps.filter((ep2) => ep2.preguntaId === p.id)[0];
            if (!!ep) {
              const peso = ep.peso || 0.5;
              Object.assign(p, { peso });
            } else {
              Object.assign(p, { peso: 0.5 });
            }
          });
        },
         err => this.showError(err)
      );
  }

  showError(text: string) {
    var texto = "";

    this.translate.get('encuesta.error').subscribe(translation => {
      texto = translation
    }); 

    this.snackBar.open(text, texto, { duration: 5000, panelClass: ['red-snackbar'] });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  protected isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  protected masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach((row) => this.selection.select(row));
  }

    protected save() {
        const preguntas = this.selection.selected;

        var texto = "";

        this.translate.get('encuesta.noañadirpreguntas').subscribe(translation => {
          texto = translation
        });   
        
        var titulo = "";

        this.translate.get('encuesta.atencion').subscribe(translation => {
          titulo = translation
        });          

        const encuestaPreguntas = preguntas.map((p) => new EncuestaPregunta(this.model.id, p.id, p.peso));
        this.encuestaService.updateEncuestaPreguntas(this.model.id, encuestaPreguntas)
            .subscribe(
                () => this.router.navigate([`/encuesta/resumen/${this.model.id}`]),
                (err) => this.snackBar.open(texto, titulo,{duration:10000}),
        );
    }

  protected back() {
    this.router.navigate([`/encuesta/resumen/${this.model.id}`]);
  }

  viewPregunta(pregunta: Pregunta) {
    this.router.navigate([`/encuesta/${this.model.id}/categoria/${pregunta.categoriaId}/pregunta-editor/${pregunta.id}/v`]);
  }

  traducirtipopregunta(tipo: TipoPregunta)
  {
    //Texto, Seleccion, Escala, Comentario
    switch(tipo.id) { 
      case 0: { 
        this.translate.get('tipopregunta.texto').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 1: { 
        this.translate.get('tipopregunta.seleccion').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 2: { 
        this.translate.get('tipopregunta.escala').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 3: { 
        this.translate.get('tipopregunta.comentario').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      }                  
      default: { 
         //statements; 
         break; 
      } 
   }   
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
}
