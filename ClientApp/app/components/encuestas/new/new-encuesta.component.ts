import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Encuesta, IdiomaEncuesta } from "../../../models/encuesta";
import { EncuestaService } from "../../../services/encuesta.service";
import { MatSnackBar } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { TipoEncuesta } from "../../../models/tipoencuesta";
import { TipoEncuestaService } from "../../../services/tipoencuesta.service";

import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-new-encuesta",
  templateUrl: "new-encuesta.component.html",
  styleUrls: ["new-encuesta.component.css"],
})

export class NewEncuestaComponent implements OnInit {

  public model: Encuesta = new Encuesta();
  public loading: boolean = false; 
  idiomas: IdiomaEncuesta[];
  tiposencuesta: TipoEncuesta[];

  constructor(
    private encuestaService: EncuestaService,
    private tipoencuestaService: TipoEncuestaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService,  
  ) {
    route.params.subscribe((p) => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  public ngOnInit() {    
    this.setIdioma();

    this.encuestaService.getIdiomas()
    .subscribe(idi => {      
      this.idiomas = idi
    });   

    this.tipoencuestaService.getTiposEncuesta()
    .subscribe(tipos => {      
      this.tiposencuesta = tipos
    }); 

    if (this.model.id) {
      this.loading = true;    
      this.encuestaService.getEncuesta(this.model.id)
        .subscribe((data) => {
          this.setEncuestaData(data)           
        });                     
    }

  }

  public setEncuestaData(encuesta: Encuesta) {
    this.model = encuesta;
    this.loading = false;
  }

  public create() {
    let result$: Observable<Encuesta>;
    if (this.model.id) {
      result$ = this.encuestaService.updateEncuesta(this.model);
    } else {
      result$ = this.encuestaService.createEncuesta(this.model);
    }

    var texto = "";

    this.translate.get('encuesta.cambiosguardados').subscribe(translation => {
      texto = translation
    });   
    
    var titulo = "";

    this.translate.get('encuesta.encuestacreada').subscribe(translation => {
      titulo = translation
    }); 

    result$.subscribe(
      (res) => {
        const msg = (res.id) ? texto : titulo;
        this.showMsg(msg);
        this.router.navigate([`/encuesta/resumen/${res.id}`]);
      },
      (err) => 
      {
        var texto = "";

        this.translate.get('encuesta.compruebadatos').subscribe(translation => {
          texto = translation
        });   
        this.showError(texto);
      },
    );
  }

  public cancel() {
    this.router.navigate(["/encuestas"]);
  }

  public showError(text: string) {
    var texto = "";

    this.translate.get('encuesta.errordatos').subscribe(translation => {
      texto = translation
    });   

    this.snackBar.open(text,texto,{duration:5000,panelClass:['red-snackbar']});
  }

  public showMsg(text: string) {
    var texto = "";

    this.translate.get('encuesta.hecho').subscribe(translation => {
      texto = translation
    });  

    this.snackBar.open(text,texto,{duration:5000});
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

}
