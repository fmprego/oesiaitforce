import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-generic-error',
  templateUrl: 'generic-error.dialog.html',
  styleUrls: ['generic-error.dialog.css'],
})

export class GenericErrorDialog {

  constructor(
    public dialogRef: MatDialogRef<GenericErrorDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}