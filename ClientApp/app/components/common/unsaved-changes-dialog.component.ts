import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-unsaved-changes-dialog",
  templateUrl: "unsaved-changes-dialog.component.html",
})
export class UnsavedChangesDialog {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UnsavedChangesDialog>,
  ) { }

  public onNoClick(): void {
    this.dialogRef.close();
  }
}
