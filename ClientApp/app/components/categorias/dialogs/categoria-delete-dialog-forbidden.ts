import { Component, Inject } from "@angular/core";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: 'categoria-delete-dialog-forbidden',
  templateUrl: 'categoria-delete-dialog-forbidden.html',
})
export class CategoriaDeleteDialogForbidden {

  constructor(public dialogRef: MatDialogRef<CategoriaDeleteDialogForbidden>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}