import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'pregunta-delete-dialog',
  templateUrl: 'pregunta-delete-dialog.html',
})
export class PreguntaDeleteDialog {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dialogRef: MatDialogRef<PreguntaDeleteDialog>
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}