import { CategoriaDeleteDialogForbidden } from './dialogs/categoria-delete-dialog-forbidden';
import { Observable } from 'rxjs/Observable';
import { CategoriaService } from './../../services/categoria.service';
import { Component, OnInit } from '@angular/core';

import { Categoria } from './../../models/categoria';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core'; 

@Component({
  selector: 'app-categorias',
  templateUrl: 'categorias-list.component.html',
  styleUrls: ['categorias-list.component.css']
})

export class CategoriasListComponent implements OnInit {

  categorias: Categoria[];
  loading: boolean = false;

  constructor(
    private categoriaService: CategoriaService,
    private router: Router,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setIdioma();    

    this.populateCategorias();
  }

  populateCategorias() {
    this.loading = true;
    this.categoriaService.getCategorias()
      .subscribe(res => {
        this.categorias = res.items;
        this.loading = false;
      });
  }

  select(categoria: Categoria) {
    this.router.navigate([`/categoria/${categoria.id}`]);
  }

  newCategoria() {
    this.router.navigate(['/categoria-form']);
  }

  edit(categoria: Categoria) {
    this.router.navigate([`/categoria-form/${categoria.id}`]);
  }

  openDeleteDialog(categoria: Categoria): void {
    if (categoria.preguntasCount) {
      let dialogRef = this.dialog.open(CategoriaDeleteDialogForbidden, {width: '60%'});
    } else {
      this.delete(categoria);
    }
  }

  delete(categoria: Categoria) {
    this.categoriaService.delete(categoria.id)
      .subscribe(
        res => {
          this.populateCategorias();

          var texto = "";
          var titulo = "";

          this.translate.get('categorias.categoriaeliminada').subscribe(translation => {
            texto = translation
          });

          this.translate.get('categorias.hecho').subscribe(translation => {
            titulo = translation
          });

          this.snackBar.open(texto,titulo,{duration:5000});
        },
        err => {

          var texto = "";
          var titulo = "";

          this.translate.get('categorias.erroreliminando').subscribe(translation => {
            texto = translation
          });

          this.translate.get('categorias.alerta').subscribe(translation => {
            titulo = translation
          });

          this.snackBar.open(texto,titulo,{duration:5000,panelClass:['red-snackbar']});
        }
      )
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}