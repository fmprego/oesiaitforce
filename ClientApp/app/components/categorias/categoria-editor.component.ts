import { PreguntaService } from './../../services/preguntas.service';
import { PreguntaDeleteDialog } from './dialogs/pregunta-delete-dialog';
import { CategoriaService } from './../../services/categoria.service';
import { Categoria } from './../../models/categoria';
import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { Pregunta, TipoPregunta } from './../../models/pregunta';

import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-categoria-editor',
  templateUrl: 'categoria-editor.component.html',
  styleUrls: ['categoria-editor.component.css']
})

export class CategoriaEditorComponent implements OnInit {

  @Output() onDelete = new EventEmitter<Pregunta>();

  displayedColumns = ['id', 'tipo', 'creador', 'texto', 'actions'];
  dataSource = new MatTableDataSource<Pregunta>([]);
  totalItems: number = 0;

  model: Categoria = new Categoria();
  loading: boolean = false;
  selectedPregunta: Pregunta;

  constructor(
    private categoriaService: CategoriaService,
    private preguntaService: PreguntaService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private translate: TranslateService,
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;      
    }); 
    this.route.snapshot.queryParams; 
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.setIdioma(); 

    this.setIdiomaPaginador();

    if (this.model.id) {
      this.populateCategoria();
    }
  }

  populateCategoria() {
    this.loading = true;
    this.categoriaService.getCategoria(this.model.id)
      .subscribe(categoria => {
        this.model = categoria

        this.model.preguntas.map(e => 
          {          
            this.traducirtipopregunta(e.tipoPregunta);
          });

        this.dataSource.data = this.model.preguntas;
        this.totalItems = this.model.preguntasCount;
        this.loading = false;
      });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  newPregunta() {
    this.router.navigate([`/categoria/${this.model.id}/pregunta-editor`]);
  }

  edit(pregunta: Pregunta) {
    this.router.navigate([`/categoria/${this.model.id}/pregunta-editor/${pregunta.id}`]);
  }

  openDeleteDialog(pregunta: Pregunta) {
    this.selectedPregunta = pregunta;
    const dialogRef = this.dialog.open(PreguntaDeleteDialog, {
      data: {
        pregunta: pregunta.texto,
      },
      width: '60%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) this.delete(pregunta);
    });
  }

  delete(pregunta: Pregunta) {

    var texto = "";
      
    this.translate.get('categorias.preguntaeliminada').subscribe(translation => {
      texto = translation;
    });

    var titulo = "";
      
    this.translate.get('categorias.hecho').subscribe(translation => {
      titulo = translation;
    });

    this.preguntaService.delete(pregunta)
      .subscribe(
        res => {
          this.populateCategoria();
          this.snackBar.open(texto,titulo,{duration:5000});
        },
        err => {
          this.snackBar.open(texto,titulo,{duration:5000,panelClass:['red-snackbar']});
        }
      )
  }

  back() {
    this.router.navigate(['/categorias']);
  }

  viewPregunta(pregunta: Pregunta) {
    this.router.navigate([`/categoria/${this.model.id}/pregunta-editor/${pregunta.id}/v`]);
  }

  copyPregunta(pregunta: Pregunta){
    this.preguntaService.copyPregunta(pregunta)
        .subscribe(
            () => {
                var texto = "";
      
                this.translate.get('categorias.preguntacopiada').subscribe(translation => {
                  texto = translation;
                });

                this.showMsg(texto);
                this.populateCategoria(); // TODO: No estos seguro de que sea el metodo.
            },
        (err) => {
            var texto = "";
    
            this.translate.get('categorias.errorcopiandopregunta').subscribe(translation => {
              texto = translation;
            }); 

            this.showErrorMsg(texto + pregunta.descripcion + '.');
            },
    )
  }  

  showMsg(text: string) {
    var texto = "";
  
    this.translate.get('categorias.ok').subscribe(translation => {
      texto = translation;
    }); 

    this.snackBar.open(text, texto, { duration: 5000 });
  }

  showErrorMsg(text: string) {

    var texto = "";
  
    this.translate.get('categorias.error').subscribe(translation => {
      texto = translation;
    });  

    this.snackBar.open(text, texto, { duration: 5000 });
  }

  traducirtipopregunta(tipo: TipoPregunta)
  {
    //Texto, Seleccion, Escala, Comentario
    switch(tipo.id) { 
      case 0: { 
        this.translate.get('tipopregunta.texto').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 1: { 
        this.translate.get('tipopregunta.seleccion').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 2: { 
        this.translate.get('tipopregunta.escala').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      } 
      case 3: { 
        this.translate.get('tipopregunta.comentario').subscribe(translation => {
          tipo.descripcion = translation
        });
        break; 
      }                  
      default: { 
         //statements; 
         break; 
      } 
   }   
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }

  setIdiomaPaginador()
  {
      this.translate.get('paginador.itemsPerPageLabel').subscribe(translation => {
        this.paginator._intl.itemsPerPageLabel = translation
      });
      this.translate.get('paginador.firstPageLabel').subscribe(translation => {
        this.paginator._intl.firstPageLabel = translation
      });
      this.translate.get('paginador.lastPageLabel').subscribe(translation => {
        this.paginator._intl.lastPageLabel = translation
      });
      this.translate.get('paginador.nextPageLabel').subscribe(translation => {
        this.paginator._intl.nextPageLabel = translation
      });
      this.translate.get('paginador.previousPageLabel').subscribe(translation => {
        this.paginator._intl.previousPageLabel = translation
      });                        
  }
}