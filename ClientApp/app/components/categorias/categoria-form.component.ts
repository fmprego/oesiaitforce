import { Categoria } from './../../models/categoria';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { CategoriaService } from '../../services/categoria.service';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-categoria-form',
  templateUrl: 'categoria-form.component.html',
  styleUrls: ['categoria-form.component.css']
})

export class CategoriaFormComponent implements OnInit {

  model: Categoria = new Categoria();
  loading: boolean = false;

  constructor(
    private categoriaService: CategoriaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private translate: TranslateService,
  ) {
    route.params.subscribe(p => {
      this.model.id = p.id ? p.id : 0;
    });
  }

  ngOnInit() {
    this.setIdioma(); 

    if (this.model.id) {
      this.categoriaService.getCategoria(this.model.id)
        .subscribe(
          categoria => {
            this.model = categoria;
            this.loading = false;
          },
          err => {
            var texto = "";
  
            this.translate.get('categorias.errorrecuperandodatos').subscribe(translation => {
              texto = translation;
            });  

            this.showError(texto);
          }
        );
    }    
  }

  submit() {
    var result$: Observable<Categoria>;
    if (this.model.id) result$ = this.categoriaService.update(this.model);
    else result$ = this.categoriaService.create(this.model);

    result$.subscribe(
      res => {

        var texto1 = "";
        var texto2 = "";
  
        this.translate.get('categorias.cambiosguardados').subscribe(translation => {
          texto1 = translation;
        });  

        this.translate.get('categorias.categoriacreada').subscribe(translation => {
          texto2 = translation;
        });          

        const msg = (this.model.id) ? texto1 : texto2;
        this.showMsg(msg);
        this.router.navigate(['/categorias']);
      },
      err => 
      {
        var texto = "";
  
        this.translate.get('categorias.compruebadatos').subscribe(translation => {
          texto = translation;
        });  

        this.showError(texto);        
      }
    );
  }

  showMsg(text: string) {
    var texto = "";
  
    this.translate.get('categorias.hecho').subscribe(translation => {
      texto = translation;
    });  

    this.snackBar.open(text,texto,{duration:5000});
  }  

  showError(text: string) {
    var texto = "";
  
    this.translate.get('categorias.error').subscribe(translation => {
      texto = translation;
    });  

    this.snackBar.open(text,texto,{duration:5000,panelClass:['red-snackbar']});
  }

  cancel() {
    this.router.navigate(['/categorias']);
  }

  setIdioma()
  {
      var idiomaLocale = this.translate.getBrowserLang();

      var lidiomaLocale = this.authService.getCurrentLocale();
      if (lidiomaLocale != "")
      {              
        idiomaLocale = lidiomaLocale;      
      } 

      this.translate.use(idiomaLocale);
      
  }
}