import { AuthService } from './../../services/auth.service';
import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    constructor(private authService: AuthService, private translate: TranslateService) {
        translate.setDefaultLang('es');

        var idiomaLocale = this.authService.getCurrentLocale();
        if (idiomaLocale != "")
        {
          this.translate.setDefaultLang(idiomaLocale);
        }

    }
}
