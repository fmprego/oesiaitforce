import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'generic-dialog',
  templateUrl: 'generic.dialog.html',
})
export class GenericDialog {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dialogRef: MatDialogRef<GenericDialog>
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}