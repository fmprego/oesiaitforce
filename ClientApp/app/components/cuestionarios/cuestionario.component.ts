import { forkJoin } from 'rxjs/observable/forkJoin';
import { Encuesta } from './../../models/encuesta';
import { EstructuraService } from './../../services/estructura.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EncuestaService } from '../../services/encuesta.service';
import { Cuestionario } from '../../models/cuestionario';
import { PreguntaService } from '../../services/preguntas.service';
import { Pregunta } from '../../models/pregunta';
import { Respuesta } from '../../models/respuesta';
import { CuestionarioService } from '../../services/cuestionario.service';
import { Usuario } from './../../models/usuario';
import { AuthService } from './../../services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    selector: 'app-cuestionario',
    templateUrl: 'cuestionario.component.html',
    styleUrls: ['cuestionario.component.css'],
})

//class OrdenPregunta{ idPregunta: string; Orden: string };

export class CuestionarioComponent implements OnInit {

    model: Cuestionario = new Cuestionario(0, new Array<Respuesta>(), '');
    encuesta: Encuesta = new Encuesta();
    usuario: Usuario | undefined;
    preguntas: Pregunta[];
    begining: boolean = false;
    loading: boolean = false;
    success: boolean = false;
    fail: boolean = false;
    nivel1: string[];
    nivel2: string[];
    nivel3: string[];
    nivel4: string[];
    scrollY: number = 0;

    preguntaActual: number = 0;
    maxPreguntaOrden: number = 2; // dos preguntas fijas

    vPreguntaMostrada: number[] = new Array();

    constructor(
        private authService: AuthService,
        private encuestaService: EncuestaService,
        private preguntaService: PreguntaService,
        private estructuraService: EstructuraService,
        private cuestionarioService: CuestionarioService,
        private translate: TranslateService,
        private route: ActivatedRoute,
    ) {
        route.params.subscribe(p => {
            var cadena = atob(p.id ? p.id : "encuesta0")

            var ids = cadena.replace("encuesta","");
            var id = +ids;
    
            this.encuesta.id = id ? id : 0;
        });
    }

    ngOnInit() {
        this.initCuestionario();
    }

    initCuestionario() {
        this.loading = true;
        this.begining = true;
        this.setUsuario();
        
        this.encuestaService.getEncuesta(this.encuesta.id, true)
            .subscribe(
                encuesta => {
                    this.encuesta = encuesta;
                    if (encuesta.idioma != null)
                    {
                        this.useLanguage(encuesta.idioma.locale);
                    }
                    
                    forkJoin([
                        this.estructuraService.getNivel1(encuesta.estructuraId),
                        this.preguntaService.getByEncuestaId(encuesta.id),
                    ]).subscribe(
                        (data: any) => {
                            this.nivel1 = data[0];
                            this.preguntas = data[1].items;
                            this.preguntas = this.initPreguntas();
                            this.loading = false;
                            this.begining = false;
                        },
                        err => this.showError(err)
                    )
                },
                err => this.showError(err)
        );
    }

    useLanguage(language: string) {
        this.translate.use(language);
    }

    setUsuario() {
        const user = this.authService.getLoggedUser();
        this.usuario = user ? user : undefined;
    }

    showError(err: Error) {
        console.error(err);
        this.loading = false;
        this.begining = false;
    }

    public TieneRespuesta(): boolean {
        
        let bresp: boolean = false;

        //si la pregunta es la final, entendemos que si tiene respuesta
        if (this.preguntaActual === 999) {
            return true;
        }
        
        let pregunta: Pregunta = this.preguntas.find(p => p.id === this.preguntaActual);
        if (pregunta == undefined) {
            return bresp;
        }

        switch (pregunta.tipoPregunta.id) {

            case 0: //texto
                bresp = (pregunta.respuesta.texto != undefined && pregunta.respuesta.texto !== "");
                break;
            case 1: //Opcion
                 bresp = (pregunta.respuesta.opcionId != undefined);
                break;
            case 2: //Escala
                bresp = (pregunta.respuesta.escala != undefined);
                break;
            case 3: //Comentarios (No es obligatorio)
                bresp = true;
                break;
        }

        return bresp;
    }

    initPreguntas(): Pregunta[] {

        //Ordenar pregunta por tipo 
        this.shuffleArray(this.preguntas);
        var preguntasListAux = this.preguntas.filter(p=>p.tipoPregunta.id==0 || p.tipoPregunta.id==1 || p.tipoPregunta.id==2);

        preguntasListAux = preguntasListAux.concat(this.preguntas.filter(p=>p.tipoPregunta.id==3));
        this.preguntas = preguntasListAux;
       
        // establecer Orden y añadir ordinal al titulo.
        let contadorOrden: number = 1;
        for (let p of this.preguntas) {
            if (p.texto.length == 0) {
                this.translate.get('cuestionario.preguntasintitulo').subscribe(translation => {
                    p.texto = translation;
                  });
            }
            contadorOrden++;
            p.texto = contadorOrden.toString() + '.- ' + p.texto;
            p.orden = contadorOrden;
        }
        this.maxPreguntaOrden = contadorOrden; // almacenar numero maximo de preguntas, para verificar limites.
        this.addToVectorPreguntaMostrada(1); // Añade la primera pregunta al vector ya que se a llegado automaticamente a ella.

        return this.preguntas.map(it => {
            return {
                ...it,
                respuesta: new Respuesta(it.tipoPregunta, it.id),
            };
        });
    }

    onNivel1Change() {
        this.model.nivel2 = null;
        this.model.nivel3 = null;
        this.model.nivel4 = null;
        this.feedNivel2(this.model.nivel1);
    }

    onNivel2Change() {
        this.model.nivel3 = null;
        this.model.nivel4 = null;
        this.feedNivel3(this.model.nivel1, this.model.nivel2);
    }

    onNivel3Change() {
        this.model.nivel4 = null;
        this.feedNivel4(this.model.nivel1, this.model.nivel2, this.model.nivel3);
    }

    feedNivel2(nivel1: string) {
        this.loading = true;
        this.estructuraService.getNivel(this.encuesta.estructuraId, nivel1)
            .subscribe(
                niveles => {
                    this.nivel2 = niveles.map(it => it.nivel2)
                        .filter(it => it != null)
                        .filter((val, idx, self) => self.indexOf(val) === idx);
                    this.loading = false;
                },
                err => this.showError(err)
            )
    }

    feedNivel3(nivel1: string, nivel2: string | null) {
        this.loading = true;
        this.estructuraService.getNivel(this.encuesta.estructuraId, nivel1, nivel2)
            .subscribe(
                niveles => {
                    this.nivel3 = niveles.map(it => it.nivel3)
                        .filter(it => it != null)
                        .filter((val, idx, self) => self.indexOf(val) === idx);
                    this.loading = false;
                },
                err => this.showError(err)
            )
    }

    feedNivel4(nivel1: string, nivel2: string | null, nivel3: string | null) {
        this.loading = true;
        this.estructuraService.getNivel(this.encuesta.estructuraId, nivel1, nivel2, nivel3)
            .subscribe(
                niveles => {
                    this.nivel4 = niveles.map(it => it.nivel4)
                        .filter(it => it != null)
                        .filter((val, idx, self) => self.indexOf(val) === idx);
                    this.loading = false;
                },
                err => this.showError(err)
            )
    }

    enviarRespuestas() {
        this.loading = true;
        let respuestas: Respuesta[] = this.preguntas.map(it => it.respuesta);
        respuestas.forEach(it => { if (!!it.escala) it.escala = parseFloat(it.escala.toFixed(1)) })
        const cuestionario: Cuestionario = new Cuestionario(
            this.encuesta.id,
            respuestas,
            this.model.nivel1,
            this.model.nivel2 || undefined,
            this.model.nivel3 || undefined,
            this.model.nivel4 || undefined,
        );
        this.cuestionarioService.postCuestionario(cuestionario)
            .subscribe(
                () => {
                    this.success = true;
                    this.loading = false;
                },
                err => {
                    this.fail = true
                    this.loading = false;
                },
        );
    }

    showPregunta(orden: number) {
        // Controlar limites.
        this.preguntaActual = this.getIdPreguntaByOrden(orden);
    }

    irSiguiente(orden: number) {

        let idPregunta: number = this.getIdPreguntaByOrden(orden);
        // Si es la ultima
        if (idPregunta == 999) {
            this.preguntaActual = 999;
            return;
        }
        // Si es la primera
        if (idPregunta == 0) {
            this.preguntaActual = this.getIdPreguntaByOrden(2);
            this.addToVectorPreguntaMostrada(2);  // añadir al vector de indice lateral
            return;
        }

        if (orden == this.maxPreguntaOrden) {
            // Es el ultimo elemento solo puede ir al final            
            this.preguntaActual = 999;
            this.addToVectorPreguntaMostrada(this.maxPreguntaOrden + 1);
        }
        else {
            orden++;
            this.preguntaActual = this.getIdPreguntaByOrden(orden);
            this.addToVectorPreguntaMostrada(orden);
        }
    }

    irAnterior(orden: number) {

        let idPregunta: number = this.getIdPreguntaByOrden(orden);

        if (idPregunta == 999) {
            this.preguntaActual = this.getIdPreguntaByOrden(this.maxPreguntaOrden);
            return;
        }

        if (orden == 2) {
            // Es la primero, se muestra la pregunta fija inicial.
            this.preguntaActual = 0;
        }
        else {
            orden--;
            this.preguntaActual = this.getIdPreguntaByOrden(orden);
        }
    }

    change(event) {
        if (event.isUserInput) {
            let prOrd: number = event.source.value;
            this.preguntaActual = this.getIdPreguntaByOrden(prOrd);
        }
    }

    private getIdPreguntaByOrden(orden: number): number {
        let id: number = 0;
        if (orden == this.maxPreguntaOrden + 1) {
            id = 999;
        } else if (orden > 1) {
            id = this.preguntas.find(p => p.orden === orden).id;
        }
        return id;
    }

    private addToVectorPreguntaMostrada(orden: number) {
        if (this.vPreguntaMostrada.find(p => p == orden) === undefined) {
            this.vPreguntaMostrada.push(orden);
        }
    }

    private isLevelsNotCompleted(): boolean {

        let rc: boolean = true;

        rc = (this.model.nivel1 !== "");
        
        return !rc;
    }
    private shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]]; // eslint-disable-line no-param-reassign
        }
    }

}
