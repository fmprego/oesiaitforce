using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
  [Route("/api/categorias")]
  public class CategoriasController : Controller
  {
    private readonly ICategoriaRepository repository;
    private readonly IPreguntaRepository preguntaRepository;
    private readonly IUnitOfWork unitOfWork;
    private readonly IMapper mapper;

    public CategoriasController(
        IMapper mapper,
        ICategoriaRepository repository,
        IPreguntaRepository preguntaRepository,
        IUnitOfWork unitOfWork)
    {
      this.mapper = mapper;
      this.repository = repository;
      this.preguntaRepository = preguntaRepository;
      this.unitOfWork = unitOfWork;
    }

    [HttpGet]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<QueryResultResource<SimpleCategoriaResource>> GetCategorias()
    {
      var queryResult = await repository.GetCategorias();
      return mapper.Map<QueryResult<Categoria>, QueryResultResource<SimpleCategoriaResource>>(queryResult);
    }

    [HttpGet("{id}")]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> GetCategoria(int id, [FromQuery] bool related)
    {
      var cat = await repository.GetCategoria(id, related);
      if (cat == null) return NotFound();
      return Ok(mapper.Map<Categoria, CategoriaResource>(cat));
    }

    [HttpPost]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> CreateCategoria([FromBody] CategoriaResource categoriaResource)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      var cat = mapper.Map<CategoriaResource, Categoria>(categoriaResource);
      var result = await repository.CreateCategoria(cat);
      return Ok(mapper.Map<Categoria, CategoriaResource>(result));
    }

    [HttpDelete("{id}")]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> DeleteCategoria(int id)
    {
      var cat = await repository.GetCategoria(id);
      if (cat == null) return NotFound();
      var preguntas = await preguntaRepository.FindByCategoria(cat.Id);
      if (preguntas.Count() > 0) {
        return BadRequest("No es posible eliminar una categoría que tiene preguntas.");
      } 
      else
      {
        repository.DeleteCategoria(cat);
        await unitOfWork.CompleteAsync();
        return Ok(id);
      }
    }

    [HttpPut("{id}")]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> UpdateCategoria(int id, [FromBody] CategoriaResource categoriaResource)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      var changed = mapper.Map<CategoriaResource, Categoria>(categoriaResource);
      var updated = await repository.UpdateCategoria(id, changed);
      return Ok(mapper.Map<Categoria, CategoriaResource>(updated));
    }
  }
}