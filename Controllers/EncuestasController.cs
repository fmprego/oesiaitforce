using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Auth;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using ee.Models.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
    [Route("/api/encuestas")]
    public class EncuestasController : Controller
    {
        private readonly IAuthorizationService authorizationService;
        private readonly IMapper mapper;
        private readonly IEncuestaService service;
        private readonly UserManager<Usuario> userManager;

        private readonly IEstructuraService estructuraService;

        public EncuestasController(
            IAuthorizationService authorizationService,
            IMapper mapper,
            IEncuestaService service,
            UserManager<Usuario> userManager, IEstructuraService estructuraService)
        {
            this.mapper = mapper;
            this.service = service;
            this.userManager = userManager;
            this.authorizationService = authorizationService;
            this.estructuraService = estructuraService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEncuesta(int id, [FromQuery] bool includeRelated)
        {
            var encuesta = await service.GetOne(id, includeRelated);
            if (encuesta != null)
            {
                var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Access);
                if (authResult.Succeeded) 
                {
                    var resultado = mapper.Map<Encuesta, EncuestaResource>(encuesta);      
                    return Ok(resultado);
                }
                return Forbid();
            }
            return NotFound();
        }

        [HttpGet]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<QueryResultResource<EncuestaResource>> GetEncuestas()
        {
            QueryResult<Encuesta> queryResult;

            queryResult = await service.GetAll(User);

            return mapper.Map<QueryResult<Encuesta>, QueryResultResource<EncuestaResource>>(queryResult);
        }

        [HttpGet("idiomas/{locale}")]
        public async Task<IEnumerable<IdiomaResource>> GetIdiomas(string locale)
        {
            var queryResult = await service.GetIdiomas(locale);

            return mapper.Map<IEnumerable<Idioma>, IEnumerable<IdiomaResource>>(queryResult);
        }             

        [HttpGet("paraInforme")]
        [Authorize(Policy = "RequireLider")]
        public async Task<QueryResultResource<EncuestaResource>> GetEncuestasParaInforme()
        {
            QueryResult<Encuesta> queryResult;
            Usuario usuario = await userManager.GetUserAsync(User);
            queryResult = await service.getEncuestasParaInformes(User, usuario);
            IEnumerable<Encuesta> items = queryResult.Items;
            IList<Encuesta> itemsAdd = new List<Encuesta>();
            Boolean add = false;
            foreach (var item in items)
            {
                if (item.Estado.Equals(EstadoEncuesta.Activa) || item.Estado.Equals(EstadoEncuesta.Cerrada) || item.Estado.Equals(EstadoEncuesta.Historico))
                {
                    add = false;
                    if (item.OrigenId == 0)
                    {
                        foreach (var item2 in items)
                        {
                            if (item2.OrigenId == item.Id)
                            {
                                add = true;
                            }
                        }
                        if (add == true)
                        {
                            Encuesta item3 = new Encuesta(item.Id, "GLOBAL " + item.Titulo, item.Descripcion, item.Usuario, item.UsuarioId, item.OrigenId, item.GeografiaId, item.Estructura, 
                                item.Estado, item.IdiomaId, item.Idioma, item.TipoEncuestaId, item.TipoEncuesta);
                            item3.FechaCreacion = item.FechaCreacion;
                            item3.FechaCierre = item.FechaCierre;
                            item3.FechaInicio = item.FechaInicio;
                            item3.Preguntas = item.Preguntas;
                            item3.Categorias = item.Categorias;
                            itemsAdd.Add(item3);
                        }
                    }

                    if (!add)
                    {
                        itemsAdd.Add(item);
                    }
                }
            }
            queryResult.Items = itemsAdd.AsEnumerable();
            queryResult.TotalItems = queryResult.Items.Count();
            return mapper.Map<QueryResult<Encuesta>, QueryResultResource<EncuestaResource>>(queryResult);
        }


        [HttpGet("cerradas")]
        [Authorize(Policy = "RequireLider")]
        public async Task<QueryResultResource<EncuestaResource>> GetEncuestasCerradas()
        {
            var queryResult = await service.GetCerradas(User);
            return mapper.Map<QueryResult<Encuesta>, QueryResultResource<EncuestaResource>>(queryResult);
        }

        [HttpGet("pendientes")]
        [Authorize(Policy = "RequireMaster")]
        public async Task<QueryResultResource<EncuestaResource>> GetPending()
        {
            var queryResult = await service.GetPending();
            return mapper.Map<QueryResult<Encuesta>, QueryResultResource<EncuestaResource>>(queryResult);
        }

        [HttpPost]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> CreateEncuesta([FromBody] SaveEncuestaResource saveEncuestaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var encuesta = mapper.Map<SaveEncuestaResource, Encuesta>(saveEncuestaResource);
            encuesta.FechaCreacion = DateTimeOffset.Now;
            Usuario user = await userManager.GetUserAsync(User);
            encuesta.GeografiaId = user.GeografiaId;
            var result = await service.Create(encuesta, user.Id);
            return Ok(mapper.Map<Encuesta, EncuestaResource>(result));
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> DeleteEncuesta(int id)
        {
            var encuesta = await service.GetOne(id);
            if (encuesta == null) return NotFound();
            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Delete);
            if (!authResult.Succeeded) return Forbid();

            // TODO validar estado de la encuesta antes de eliminar (capa de servicio? lógica en repository?).
            await service.Delete(encuesta);
            return Ok(id);
        }

        [HttpPut("{id}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> UpdateEncuesta(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var encuesta = await service.GetOne(id);
            if (encuesta == null) return NotFound();
            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Save);
            if (!authResult.Succeeded) return Forbid();
            var changed = mapper.Map<EncuestaResource, Encuesta>(encuestaResource);

            if (encuestaResource.Estado.Id == (int)EstadoEncuesta.Historico
                || encuestaResource.Estado.Id == (int)EstadoEncuesta.Cerrada)
            {
                //Estamos entrando en la modificacion segun estado
                changed.Estado = (EstadoEncuesta)encuestaResource.Estado.Id;
            }

            var updated = await service.Update(id, changed);

            return Ok(mapper.Map<Encuesta, EncuestaResource>(updated));
        }

        [HttpPost("{id}/encolar")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> EncolarEncuesta(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var encuesta = await service.GetOne(id);
            if (encuesta == null) return NotFound();
            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Save);
            if (!authResult.Succeeded) return Forbid();

            var changed = mapper.Map<EncuestaResource, Encuesta>(encuestaResource);
            var updated = await service.Update(id, changed);
            updated = await service.Encolar(id);

            return Ok(mapper.Map<Encuesta, EncuestaResource>(updated));
        }

        [HttpPost("{id}/aprobar")]
        [Authorize(Policy = "RequireMaster")]
        public async Task<IActionResult> AprobarEncuesta(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var encuesta = await service.GetOne(id);
            if (encuesta == null) return NotFound();
            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Save);
            if (!authResult.Succeeded) return Forbid();

            var updated = await service.Aprobar(id);

            return Ok(mapper.Map<Encuesta, EncuestaResource>(updated));
        }

        [HttpPost("{id}/activar")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> ActivarEncuesta(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var encuesta = await service.GetOne(id);
            if (encuesta == null) return NotFound();
            encuesta.FechaInicio = DateTimeOffset.Now;
            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Save);
            if (!authResult.Succeeded) return Forbid();

            var updated = await service.Activar(id,encuestaResource.SendMails,encuestaResource.Url);

            return Ok(mapper.Map<Encuesta, EncuestaResource>(updated));
        }

        [HttpPost("{id}/cerrar")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> CerrarEncuesta(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var encuesta = await service.GetOne(id);
            if (encuesta == null) return NotFound();
            encuesta.FechaCierre = DateTimeOffset.Now;
            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Save);
            if (!authResult.Succeeded) return Forbid();

            var updated = await service.Cerrar(id);

            return Ok(mapper.Map<Encuesta, EncuestaResource>(updated));
        }

        [HttpPost("{id}/pasohistorico")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> PasoHistorico(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var encuesta = await service.GetOne(id);
            if (encuesta == null)
                return NotFound();

            encuesta.Estado = Models.Enum.EstadoEncuesta.Historico;
            encuesta.FechaCierre = DateTimeOffset.Now;

            var authResult = await authorizationService.AuthorizeAsync(User, encuesta, Operations.Save);
            if (!authResult.Succeeded)
                return Forbid();

            var updated = await service.Update(id, encuesta);

            return Ok(mapper.Map<Encuesta, EncuestaResource>(updated));
        }

        [HttpPost("{id}/copiaEncuesta")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> CopiaEncuesta(int id, [FromBody] EncuestaResource encuestaResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var encuesta = await service.GetOne(id, false);
            if (encuesta == null)
                return NotFound();

            // Obtenemos las preguntas de la encuesta
            var encuestaPreguntas = await service.GetEncuestaPreguntas(id);
            var resultEncuestasPreguntas = mapper.Map<IEnumerable<EncuestaPregunta>, IEnumerable<EncuestaPreguntaResource>>(encuestaPreguntas);

            //Obtenemos las categorias de la encuesta
            var encuestaCategorias = await service.GetEncuestaCategorias(id);
            var resultEncuestasCategorias = mapper.Map<IEnumerable<EncuestaCategoria>, IEnumerable<EncuestaCategoriaResource>>(encuestaCategorias);

            // Creamos nueva encuesta

            var saveEncuestaResource = new SaveEncuestaResource();
            saveEncuestaResource.Descripcion = encuesta.Descripcion;
            saveEncuestaResource.Titulo = "COPIA "+ encuesta.Titulo;
            saveEncuestaResource.IdiomaId = encuesta.IdiomaId;
            saveEncuestaResource.TipoEncuestaId = encuesta.TipoEncuestaId;

            Encuesta encuestaMapped = mapper.Map<SaveEncuestaResource, Encuesta>(saveEncuestaResource);
            Usuario user = await userManager.GetUserAsync(User);
            encuestaMapped.GeografiaId = user.GeografiaId;
            encuestaMapped.Estado = Models.Enum.EstadoEncuesta.Creada;
            encuestaMapped.FechaCreacion = DateTimeOffset.Now;
            encuestaMapped.OrigenId = encuesta.Id;
            encuestaMapped.FechaInicio = encuesta.FechaInicio;
            encuestaMapped.Usuario = encuesta.Usuario;
            encuestaMapped.UsuarioId = encuesta.UsuarioId;
            encuestaMapped.IdiomaId = encuesta.IdiomaId;
            encuestaMapped.TipoEncuestaId = encuesta.TipoEncuestaId;

            var EncuestaCreada = await service.Create(encuestaMapped, user.Id);

            // Iteramos y guaradamos las pregunta-encuestas
            foreach (var item in resultEncuestasPreguntas)
            {
                item.EncuestaId = EncuestaCreada.Id;
            }

            var encuestaPreguntasToUpdate = mapper.Map<IEnumerable<EncuestaPreguntaResource>, IEnumerable<EncuestaPregunta>>(resultEncuestasPreguntas);
            await service.UpdateEncuestaPreguntas(EncuestaCreada.Id, encuestaPreguntasToUpdate, true);

            // Iteramos y guaradamos las categorias-encuestas
            foreach (var item in resultEncuestasCategorias)
            {
                item.EncuestaId = EncuestaCreada.Id;
            }

            var encuestaCategoriasToUpdate = mapper.Map<IEnumerable<EncuestaCategoriaResource>, IEnumerable<EncuestaCategoria>>(resultEncuestasCategorias);
            await service.UpdateEncuestaCategorias(EncuestaCreada.Id, encuestaCategoriasToUpdate, true);

            var authResult = await authorizationService.AuthorizeAsync(User, EncuestaCreada, Operations.Save);
            if (!authResult.Succeeded)
                return Forbid();

            return Ok(mapper.Map<Encuesta, EncuestaResource>(EncuestaCreada));
        }


        [HttpPost("{id}/addcategorias")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> UpdateCategorias(int id, [FromBody] IEnumerable<CategoriaResource> categoriasResources)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var categorias = mapper.Map<IEnumerable<CategoriaResource>, IEnumerable<Categoria>>(categoriasResources);
            var encuesta = await service.UpdateCategorias(id, categorias);
            return Ok(mapper.Map<Encuesta, EncuestaResource>(encuesta));
        }

        [HttpGet("{id}/ep")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> GetEncuestaPreguntas(int id)
        {
            var encuestaPreguntas = await service.GetEncuestaPreguntas(id);
            var result = mapper.Map<IEnumerable<EncuestaPregunta>, IEnumerable<EncuestaPreguntaResource>>(encuestaPreguntas);
            return Ok(result);
        }

        [HttpPost("{id}/ep")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> UpdateEncuestaPreguntas(int id, [FromBody] IEnumerable<EncuestaPreguntaResource> epResources)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var encuestaPreguntas = mapper.Map<IEnumerable<EncuestaPreguntaResource>, IEnumerable<EncuestaPregunta>>(epResources);
            await service.UpdateEncuestaPreguntas(id, encuestaPreguntas);
            return Ok(true);
        } // todo peso en cliente debe tomarse desde backend

    }
}