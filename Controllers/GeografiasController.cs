using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using ee.Models.Query;
using ee.Persistence;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ee.Controllers
{
  [Route("/api/geografias")]
  public class GeografiasController : Controller
  {
    private readonly IMapper mapper;
    private readonly IGeografiaRepository repository;
    private readonly IUnitOfWork unitOfWork;
    private readonly IUsuarioService usuarioService;
    public GeografiasController(
      IGeografiaRepository repository,
      IMapper mapper,
      IUnitOfWork unitOfWork,
      IUsuarioService usuarioService)
    {
      this.usuarioService = usuarioService;
      this.unitOfWork = unitOfWork;
      this.repository = repository;
      this.mapper = mapper;
    }

    [HttpGet]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IEnumerable<GeografiaResource>> GetGeografias()
    {
      var filter = new GeografiaQuery(); // todo
      var queryResult = await repository.GetGeografias(filter);
      return mapper.Map<IEnumerable<Geografia>, IEnumerable<GeografiaResource>>(queryResult);
    }

    [HttpGet("{id}")]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> GetGeografia(int id)
    {
      var geo = await repository.GetGeografia(id);
      if (geo == null) return NotFound();
      var geoResource = mapper.Map<Geografia, GeografiaResource>(geo);
      return Ok(geoResource);
    }

    [HttpPost]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> CreateGeografia([FromBody] GeografiaResource geografiaResource)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      var geo = mapper.Map<GeografiaResource, Geografia>(geografiaResource);
      var result = await repository.CreateGeografia(geo);
      return Ok(mapper.Map<Geografia, GeografiaResource>(result));
    }

    [HttpDelete("{id}")]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> DeleteGeografia(int id)
    {
      var geo = await repository.GetGeografia(id);
      if (geo == null) return NotFound();
      var admins = await this.usuarioService.GetAdmins(geo.Id);
      if (admins.Count() > 0) {
        return BadRequest("No es posible eliminar una geografía que tiene administradores.");
      } 
      else
      {
        repository.DeleteGeografia(geo);
        await unitOfWork.CompleteAsync();
        return Ok(id);
      }
    }

    [HttpPut("{id}")]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> UpdateGeografia(int id, [FromBody] GeografiaResource geografiaResource)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      var changed = mapper.Map<GeografiaResource, Geografia>(geografiaResource);
      var updated = await repository.UpdateGeografia(id, changed);
      return Ok(mapper.Map<Geografia, GeografiaResource>(updated));
    }

  }
}