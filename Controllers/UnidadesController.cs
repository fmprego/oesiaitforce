using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Controllers.Resources.Query;
using ee.Core;
using ee.Models;
using ee.Models.Query;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
  [Route("/api/unidades")]
  public class UnidadesController
  {
    private readonly IUnidadRepository repository;
    private readonly IMapper mapper;
    public UnidadesController(IUnidadRepository repository, IMapper mapper)
    {
      this.mapper = mapper;
      this.repository = repository;
    }

    [HttpGet]
    [Authorize(Policy = "RequireMaster")]
    public async Task<QueryResultResource<UnidadResource>> GetUnidades(UnidadQueryResource filterResource)
    {
        var filter = mapper.Map<UnidadQueryResource, UnidadQuery>(filterResource);
        var queryResult = await repository.GetUnidades(filter);
        return mapper.Map<QueryResult<Unidad>, QueryResultResource<UnidadResource>>(queryResult);
    }
  }
}