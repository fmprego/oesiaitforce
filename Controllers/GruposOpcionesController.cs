using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Core.Exceptions;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
  [Route("/api/gruposopciones")]
  public class GruposOpcionesController : Controller
  {
    private readonly IMapper mapper;
    private readonly IGrupoOpcionesRepository repository;
    private readonly IOpcionRepository opcionRepository;

    public GruposOpcionesController(
        IMapper mapper,
        IGrupoOpcionesRepository repository,
        IOpcionRepository opcionRepository
    )
    {
      this.repository = repository;
      this.opcionRepository = opcionRepository;
      this.mapper = mapper;
    }

    [HttpGet]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> GetAll()
    {
        var result = await repository.GetAll();
        return Ok(mapper.Map<IEnumerable<GrupoOpciones>, IEnumerable<GrupoOpcionesResource>>(result));
    }

    [HttpPost]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> SaveGrupo([FromBody] GrupoOpcionesResource grupoResource)
    {
      var grupo = mapper.Map<GrupoOpcionesResource, GrupoOpciones>(grupoResource);
      grupo = await this.repository.Save(grupo);
      return Ok(mapper.Map<GrupoOpciones, GrupoOpcionesResource>(grupo));
    }

    [HttpDelete("{id}")]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> Delete(int id)
    {
      var grupo = await this.repository.GetOne(id);
      if (grupo == null) throw new GrupoOpcionesNotFoundException();
      await this.opcionRepository.DeleteGrupo(grupo);
      await this.repository.Delete(grupo);
      return Ok(mapper.Map<GrupoOpciones, GrupoOpcionesResource>(grupo));
    }
  }
}