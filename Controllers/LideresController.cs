using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Core.Exceptions;
using ee.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ee.Controllers
{
    [Route("/api/lideres")]
    public class LideresController : Controller
    {
        private readonly IUsuarioService usuarioService;
        private readonly IEstructuraService estructuraService;
        private readonly IGeografiaRepository geografiaRepository;
        private readonly UserManager<Usuario> userManager;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        //private readonly ILogger serviceLogger;

        public LideresController(
            IUsuarioService usuarioService,
            IEstructuraService estructuraService,
            IGeografiaRepository geografiaRepository,
            UserManager<Usuario> userManager,
            IMapper mapper,
            IConfiguration configuration)//,
            //ILogger serviceLogger)
        {
            this.usuarioService = usuarioService;
            this.estructuraService = estructuraService;
            this.geografiaRepository = geografiaRepository;
            this.userManager = userManager;
            this.mapper = mapper;
            this.configuration = configuration;
           // this.serviceLogger = serviceLogger;
        }

        [HttpGet("{estructuraId}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IEnumerable<UsuarioResource>> GetLideres(int estructuraId)
        {
            var result = new List<Usuario>();
            var lideres = await estructuraService.GetLideres(estructuraId);
            foreach (Lider lider in lideres.Items)
            {
                if (lider.UsuarioId != null)
                {
                    var usuario = await usuarioService.GetLider(lider.UsuarioId);
                    result.Add(usuario);
                }
            }
            return mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioResource>>(result);
        }

        [HttpPost]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> CreateLideres([FromBody] IEnumerable<SaveLiderResource> resources)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var lideres = mapper.Map<IEnumerable<SaveLiderResource>, IEnumerable<Usuario>>(resources);
            var currentAdminId = userManager.GetUserId(User);
            var currentAdmin = await usuarioService.GetAdmin(currentAdminId);
            if (currentAdmin == null)
            {
                //this.serviceLogger.LogError("Usuario no encontrado con id {1}", currentAdminId);
                throw new UsuarioNotFoundException();
            }
            lideres = await usuarioService.CreateLideres(lideres, configuration["DefaultPassword"], currentAdmin.GeografiaId);
            return Ok(mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioResource>>(lideres));
        }

        [HttpPost("{estructuraId}/signupall")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<ActionResult> SignupAll(int estructuraId)
        {
            var currentAdminId = userManager.GetUserId(User);
            var currentAdmin = await usuarioService.GetAdmin(currentAdminId);
            if (currentAdmin == null)
            {
               // this.serviceLogger.LogError("Usuario no encontrado con id {1}", currentAdminId);
                throw new UsuarioNotFoundException();
            }
            var lideresSinUsuario = await estructuraService.GetLideresSinUsuario(estructuraId);
            var usuarios = mapper.Map<IEnumerable<Lider>, IEnumerable<Usuario>>(lideresSinUsuario);
            var lideres = await usuarioService.CreateLideres(usuarios, configuration["DefaultPassword"], currentAdmin.GeografiaId);
            return Ok(mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioResource>>(lideres));
        }

    }
}