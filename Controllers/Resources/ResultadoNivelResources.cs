using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ee.Controllers.Resources
{
    public class ResultadoNivelResources
    {
         public ICollection<float> valores {get; set; }
         public HashSet<string> categorias {get; set; }
         public string nombreEncuesta {get; set;}

    }
}