namespace ee.Controllers.Resources
{
    public class EncuestaPreguntaResource
    {
        public int EncuestaId { get; set; }
        public int PreguntaId { get; set; }
        public float Peso { get; set; }
    }
}