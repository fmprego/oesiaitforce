namespace ee.Controllers.Resources
{
    public class SaveOpcionResource
    {
        public string Texto { get; set; }
        public float Peso { get; set; }
        public string Imagen { get; set; }
        public string ImagenCab { get; set; }
        public byte[] ImagenContain { get; set; }
    }
}