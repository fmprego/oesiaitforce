using System.Collections.Generic;
using ee.Models;

namespace ee.Controllers.Resources
{
    public class CategoriaResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int PreguntasCount { get; set; }
        public ICollection<PreguntaResource> Preguntas { get; set; }
    }
}