using ee.Models;

namespace ee.Controllers.Resources
{
    public class EncuestaCategoriaResource
    {
        public int EncuestaId { get; set; }
        public Encuesta Encuesta { get; set; }
        public int CategoriaId { get; set; }
        public Categoria Categoria { get; set; }
    }
}