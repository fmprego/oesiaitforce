using System;

namespace ee.Controllers.Resources
{
    public class UsuarioResource
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Geografia { get; set; }
        public int GeografiaId { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime LastLogin { get; set; }
    }
}