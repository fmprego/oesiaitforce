namespace ee.Controllers.Resources
{
    public class LoginResource
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
    }
}