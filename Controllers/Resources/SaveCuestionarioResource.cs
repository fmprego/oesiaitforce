using System;
using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class SaveCuestionarioResource
    {
        public int EncuestaId { get; set; }
        public DateTimeOffset Hora { get; set; }
        public string Nivel1 { get; set; }
        public string Nivel2 { get; set; }
        public string Nivel3 { get; set; }
        public string Nivel4 { get; set; }
        public IEnumerable<SaveRespuestaResource> Respuestas { get; set; }
    }
}