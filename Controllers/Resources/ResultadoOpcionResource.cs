namespace ee.Controllers.Resources
{
    public class ResultadoOpcionResource
    {
        public int Id { get; set; }
        public int OpcionId { get; set; }
        public string Texto { get; set; }
        public int Recuento { get; set; }
        public float Porcentaje { get; set; }
    }
}