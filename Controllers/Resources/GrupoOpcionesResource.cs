using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class GrupoOpcionesResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public ICollection<OpcionResource> Opciones { get; set; }
    }
}