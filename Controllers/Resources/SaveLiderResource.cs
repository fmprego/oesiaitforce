using System.ComponentModel.DataAnnotations;

namespace ee.Controllers.Resources
{
    public class SaveLiderResource
    {
        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Apellidos { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }    
    }
}