namespace ee.Controllers.Resources
{
    public class EstadoEncuestaResource
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}