namespace ee.Controllers.Resources
{
    public class OpcionResource
    {
        public int Id { get; set; }
        public string Texto { get; set; }
        public float Peso { get; set; }
        public string Imagen { get; set; }
        public string ImagenCab { get; set; }
        public byte[] ImagenContain { get; set; }
    }
}