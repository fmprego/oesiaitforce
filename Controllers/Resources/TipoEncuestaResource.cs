using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ee.Controllers.Resources
{
    public class TipoEncuestaResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int EncuestaCount { get; set; }

        public TipoEncuestaResource() {}
    }
}