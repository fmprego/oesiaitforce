using System;
using System.Collections.Generic;
using ee.Models;

namespace ee.Controllers.Resources
{
    public class QueryResultResource<T>
    {
        public int TotalItems { get; set; }
        public IEnumerable<T> Items { get; set; }

        public static implicit operator QueryResultResource<T>(QueryResult<Encuesta> v)
        {
            throw new NotImplementedException();
        }
    }
}