using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ee.Controllers.Resources
{
    public class GeografiaResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int AdminCount { get; set; }

        public GeografiaResource() {}
    }
}