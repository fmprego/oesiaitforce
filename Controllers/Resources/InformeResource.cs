using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class InformeResource
    {
        public int Id { get; set; }  
        public int EncuestaId { get; set; }

        // public EncuestaResource Encuesta { get; set; }

        public string Nivel1 { get; set; }

        public string Nivel2 { get; set; }

        public string Nivel3 { get; set; }
        
        public string Nivel4 { get; set; }

        public int Respuestas { get; set; }

        public ICollection<ResultadoCategoriaResource> ResultadoCategorias { get; set; }

        public ICollection<ResultadoPreguntaResource> ResultadoPreguntas { get; set; }

        public ICollection<ResultadoNivelResources> ResultadoNiveles {get; set; }
    }
}