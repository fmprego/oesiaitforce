namespace ee.Controllers.Resources
{
    public class PersonaResource
    {
        public int Id { get; set; } 
        public string Numero { get; set; }
        public string Nivel1 { get; set; }
        public string Nivel2 { get; set; }
        public string Nivel3 { get; set; }
        public string Nivel4 { get; set; }
        public string Nombre { get; set; }
        public bool EsLider { get; set; }
        public int NumCentro { get; set; }
        public string NomCentro { get; set; }
        public string Email { get; set; }
        public int EstructuraId { get; set; }
    }
}