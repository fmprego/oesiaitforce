namespace ee.Controllers.Resources
{
    public class SaveAdminResource
    {
        public string UserName { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public int GeografiaId { get; set; }

    }
}