namespace ee.Controllers.Resources
{
    public class UpdatePasswordResource
    {
        public string Email { get; set; }
        public string OldPass { get; set; }
        public string NewPass { get; set; }
        public string NewPass2 { get; set; }
        public string ValidationCode { get; set; }
  }
}