namespace ee.Controllers.Resources
{
    public class IdiomaResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Locale { get; set; }
    }
}