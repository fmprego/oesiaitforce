namespace ee.Controllers.Resources
{
    public class RespuestaResource
    {
        public int Id { get; set; }
        public int? PreguntaId { get; set; }
        public int CuestionarioId { get; set; }
        public int TipoPregunta { get; set; }
        public float? Escala { get; set; }
        public string Texto { get; set; }
        public int? OpcionId { get; set; }
    }
}