namespace ee.Controllers.Resources
{
    public class SaveEncuestaResource
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int IdiomaId {get; set;}

        public int TipoEncuestaId {get; set;}
        

    }
}