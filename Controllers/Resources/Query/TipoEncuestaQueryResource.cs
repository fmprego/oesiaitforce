namespace ee.Controllers.Resources.Query
{
    public class TipoEncuestaQueryResource
    {
        public string Nombre { get; set; }
        int Page { get; set; }
        byte PageSize { get; set; }
    }
}