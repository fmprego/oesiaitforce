namespace ee.Controllers.Resources.Query
{
    public class UnidadQueryResource
    {
        public string Nombre { get; set; }
        public int? GeografiaId { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
    }
}