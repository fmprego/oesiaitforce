using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class SavePreguntaResource
    {
        public int CategoriaId { get; set; }
        public string Texto { get; set; }

        public string Descripcion{ get;set;}
        public int TipoPregunta { get; set; }
        public int? Escala { get;set; }
        public IEnumerable<SaveOpcionResource> Opciones { get; set; }
    }
}