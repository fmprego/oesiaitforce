using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class ResultadoPreguntaResource
    {
        public int Id { get; set; }  
        public int PreguntaId { get; set; }
        public string Texto { get; set; }
        public string TipoPregunta { get; set; }
        public int CategoriaId { get; set; }
        public float Valor { get; set; } 
        public float Peso { get; set; }
        public ICollection<ResultadoOpcionResource> ResultadoOpciones { get; set; }
    }
}