using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class PreguntaResource
    {
        public int Id { get; set; }
        public string Texto { get; set; }
        public string Descripcion { get;set; }
        public int CategoriaId { get; set; }
        public string Creator { get; set; }
        public string NombreCategoria { get; set; }
        public TipoPreguntaResource TipoPregunta { get; set; }
        public ICollection<OpcionResource> Opciones { get; set; }
        public bool Editable { get; set; }
        public float Peso { get; set; }
        public int? Escala { get;set; }
    }
}