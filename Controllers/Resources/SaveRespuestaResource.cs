namespace ee.Controllers.Resources
{
    public class SaveRespuestaResource
    {
        public int PreguntaId { get; set; }
        public TipoPreguntaResource TipoPregunta { get; set; }
        public float Escala { get; set; }
        public string Texto { get; set; }
        public int? OpcionId { get; set; }
    }
}