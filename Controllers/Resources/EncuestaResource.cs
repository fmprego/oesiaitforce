using System;
using System.Collections.Generic;
using ee.Models.Enum;

namespace ee.Controllers.Resources
{
    public class EncuestaResource
    {
        public int Id { get; set; }
        public int OrigenId { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int EstructuraId { get; set; }
        public int GeografiaId { get; set; }
        public int IdiomaId { get; set; }
        public int? TipoEncuestaId { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaCierre { get; set; }
        public EstadoEncuestaResource Estado { get; set; }
        public IdiomaResource Idioma { get; set; }
        public TipoEncuestaResource TipoEncuesta { get; set; }
        public ICollection<CategoriaResource> Categorias { get; set; }
        public ICollection<PreguntaResource> Preguntas { get; set; }
        public ICollection<InformeResource> Informes { get; set; }
        public UsuarioResource Usuario { get; set; }
        public Boolean SendMails { get;set; }
        public string Url { get;set;}
    }
}