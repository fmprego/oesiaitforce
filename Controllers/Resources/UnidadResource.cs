namespace ee.Controllers.Resources
{
    public class UnidadResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Geografia { get; set; }
    }
}