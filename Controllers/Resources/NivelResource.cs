namespace ee.Controllers.Resources
{
    public class NivelResource
    {
        public string Nivel1 { get; set; }
        public string Nivel2 { get; set; }
        public string Nivel3 { get; set; }
        public string Nivel4 { get; set; }
    }
}