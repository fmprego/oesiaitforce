using System;
using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class CuestionarioResource
    {
        public int Id { get; set; } 

        public int EncuestaId { get; set; }

        public EncuestaResource Encuesta { get; set; }

        public string Nivel1 { get; set; }

        public string Nivel2 { get; set; }

        public string Nivel3 { get; set; }
        
        public string Nivel4 { get; set; }

        public DateTimeOffset Hora { get; set; }

        public ICollection<RespuestaResource> Respuestas { get; set; }
    }
}