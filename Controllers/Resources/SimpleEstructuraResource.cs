namespace ee.Controllers.Resources
{
    public class SimpleEstructuraResource
    {
        public int Id { get; set; }
        public EncuestaResource Encuesta { get; set; }
        public int PersonasCount { get; set; }
    }
}