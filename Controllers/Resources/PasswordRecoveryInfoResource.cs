using System;

namespace ee.Controllers.Resources{

 public class PasswordRecoveryInfoResource
    {
        public int Id { get; set; }
        public string idAspNetUser { get; set; }
        public DateTime requestDate  { get; set; }
        public int requestExpirationTime { get; set; }
        public string requestCode { get; set; }
        public bool codeUsed { get; set; }
    }
}