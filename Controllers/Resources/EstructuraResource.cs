using System.Collections.Generic;

namespace ee.Controllers.Resources
{
    public class EstructuraResource
    {
        public int Id { get; set; }
        public EncuestaResource Encuesta { get; set; }
        public ICollection<PersonaResource> Personas { get; set; }
    }
}