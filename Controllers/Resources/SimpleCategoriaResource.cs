namespace ee.Controllers.Resources
{
    public class SimpleCategoriaResource
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int PreguntasCount { get; set; }
    }
}