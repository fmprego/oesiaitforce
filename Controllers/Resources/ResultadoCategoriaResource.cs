namespace ee.Controllers.Resources
{
    public class ResultadoCategoriaResource
    {
        public int Id { get; set; }  
        public int CategoriaId { get; set; }
        public string NombreCategoria { get; set; }
        public float Valor { get; set; }        
    }
}