using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Core.Exceptions;
using ee.dbo.Core.Exceptions;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ee.Controllers
{
    [Route("/api/admins")]
    public class UsuariosController : Controller
    {
        private readonly IUsuarioService usuarioService;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        private readonly ILogger serviceLogger;

        public UsuariosController(IUsuarioService usuarioService, IMapper mapper, IConfiguration configuration, ILogger<UsuariosController> serviceLogger)
        {
            this.configuration = configuration;
            this.mapper = mapper;
            this.usuarioService = usuarioService;
            this.serviceLogger = serviceLogger;
        }

        [HttpGet]
        [Authorize(Policy = "RequireMaster")]
        public async Task<IEnumerable<UsuarioResource>> GetAdmins([FromQuery] int? geo)
        {
            var result = await usuarioService.GetAdmins(geo);
            return mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioResource>>(result);
        }

        [HttpGet("{id}")]
        [Authorize(Policy = "RequireMaster")]
        public async Task<IActionResult> GetAdmin(string id)
        {
            try
            {
                var result = await usuarioService.GetAdmin(id);
                var usuarioResource = mapper.Map<Usuario, UsuarioResource>(result);
                return Ok(usuarioResource);
            }
            catch (UsuarioNotFoundException e)
            {
                serviceLogger.LogError(e.Message + "Id {1}",id);
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Authorize(Policy = "RequireMaster")]
        public async Task<IActionResult> CreateAdmin([FromBody] SaveAdminResource saveAdminResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var usuario = mapper.Map<SaveAdminResource, Usuario>(saveAdminResource);
                var admin = await usuarioService.CreateAdmin(usuario, configuration["DefaultPassword"]);
                var result = mapper.Map<Usuario, UsuarioResource>(admin);
                return Ok(result);
            }
            catch (Exception e)
            {
                if (e is UsuarioAlreadyExistsException || e is GeografiaNotFoundException || e is ArgumentException)
                {
                    serviceLogger.LogError(e.Message);
                    return BadRequest(e.Message);
                }
                throw;
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = "RequireMaster")]
        public async Task<IActionResult> DeleteAdmin(string id)
        {
            try
            {
                await usuarioService.DeleteAdmin(id);
                return Ok();
            }
            catch (UsuarioNotFoundException e)
            {
                serviceLogger.LogError(e.Message + "Id {1}", id);
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Policy = "RequireMaster")]
        public async Task<IActionResult> UpdateAdmin(string id, [FromBody] SaveAdminResource saveAdminResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var changed = mapper.Map<SaveAdminResource, Usuario>(saveAdminResource);
            try
            {
                var updatedAdmin = await usuarioService.UpdateAdmin(id, changed);
                var result = mapper.Map<Usuario, UsuarioResource>(updatedAdmin);
                return Ok(result);
            }
            catch (Exception e)
            {
                serviceLogger.LogError(e.Message + "Id {1}", id);
                if (e is UsuarioNotFoundException || e is GeografiaNotFoundException)
                {
                    return BadRequest(e.Message);
                }
                throw;
            }
        }

    }
}