using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
  [Route("/api/respuestas")]
  public class RespuestasController : Controller
  {
    private readonly IMapper mapper;
    private readonly IRespuestaService service;
    public RespuestasController(IMapper mapper, IRespuestaService service)
    {
      this.service = service;
      this.mapper = mapper;
    }

    [HttpGet]
    [Authorize(Policy = "RequireLider")]
    public async Task<IEnumerable<RespuestaResource>> GetRespuestasTexto([FromQuery] int encuesta, [FromQuery] int pregunta,[FromQuery] string nivel1, [FromQuery] string nivel2, [FromQuery] string nivel3, [FromQuery] string nivel4)
    {


        Nivel nivel = new Nivel();
        nivel.Nivel1 = nivel1;
        nivel.Nivel2 = nivel2;
        nivel.Nivel3 = nivel3;
        nivel.Nivel4 = nivel4;      
        var respuestas = await service.GetRespuestasTexto(encuesta, pregunta,nivel);
        return mapper.Map<IEnumerable<Respuesta>, IEnumerable<RespuestaResource>>(respuestas);
    }
  }
}