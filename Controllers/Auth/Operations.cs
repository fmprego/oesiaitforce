using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace ee.Controllers.Auth
{
    public static class Operations
    {
        public static OperationAuthorizationRequirement Access = new OperationAuthorizationRequirement { Name = "Access" };
        public static OperationAuthorizationRequirement Save = new OperationAuthorizationRequirement { Name = "Save" };
        public static OperationAuthorizationRequirement Delete = new OperationAuthorizationRequirement { Name = "Delete" };
    }
}