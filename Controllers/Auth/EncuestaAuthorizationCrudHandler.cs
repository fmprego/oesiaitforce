using System.Security.Claims;
using System.Threading.Tasks;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;

namespace ee.Controllers.Auth
{
  public class EncuestaAuthorizationCrudHandler : AuthorizationHandler<OperationAuthorizationRequirement, Encuesta>
  {
    private readonly UserManager<Usuario> userManager;
    public EncuestaAuthorizationCrudHandler(UserManager<Usuario> userManager)
    {
        this.userManager = userManager;
    }
    protected override Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        OperationAuthorizationRequirement requirement,
        Encuesta encuesta)
    {
      // se autorizan todas las operaciones al rol master.
      if (context.User.HasClaim("Role", "Master"))
      {
        context.Succeed(requirement);
      }
      // se autoriza acceso a todos los roles.
      else if (requirement.Name == Operations.Access.Name)
      {
        context.Succeed(requirement);
      }
      // se autoriza el resto de operaciones sólo al usuario propietario de la encuesta.
      else if (userManager.GetUserId(context.User) == encuesta.UsuarioId)
      {
        context.Succeed(requirement);
      }
      return Task.CompletedTask;
    }
  }
}