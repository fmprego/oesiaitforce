using System;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ee.Controllers
{
    [Route("/api/estructuras")]
    public class EstructurasController : Controller
    {
        private readonly IAuthorizationService authorizationService;
        private readonly IMapper mapper;
        private readonly ILogger<EstructurasController> logger;
        private readonly IEncuestaService encuestaService;
        private readonly UserManager<Usuario> userManager;
        private readonly IEstructuraService service;

        public EstructurasController(
                IAuthorizationService authorizationService,
                IMapper mapper,
                ILogger<EstructurasController> logger,
                IEncuestaService encuestaService,
                UserManager<Usuario> userManager,
                IEstructuraService service)
            {
            this.authorizationService = authorizationService;
            this.mapper = mapper;
            this.logger = logger;
            this.encuestaService = encuestaService;
            this.userManager = userManager;
            this.service = service;
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetEstructura(int id)
        {
            var estructura = await service.GetOne(id);
            if (estructura == null) return NotFound();
            return Ok(mapper.Map<Estructura, EstructuraResource>(estructura));
        }

        [HttpGet("{id}/nivel1")]
        public async Task<IActionResult> GetNivel1(int id)
        {
            var estructura = await service.GetOne(id);
            if (estructura == null) return NotFound();
            var nivel1 = await service.GetNivel1(id);
            return Ok(nivel1);
        }

        [HttpGet("{id}/nivel")]
        public async Task<IActionResult> GetNivel(int id, [FromQuery] string nivel1, [FromQuery] string nivel2, [FromQuery] string nivel3)
        {
            var estructura = await service.GetOne(id);
            if (estructura == null) return NotFound();
            var niveles = await service.GetNivel(id, nivel1, nivel2, nivel3);
            return Ok(niveles);
        }

        [HttpGet("{id}/lideres")]
        [Authorize]
        public async Task<IActionResult> GetLideres(int id)
        {
            if (id != 0)
            {
                var estructura = await service.GetOne(id);
                if (estructura == null) return NotFound();
            }
            var result = await service.GetLideres(id);
            if (result != null){
                var mapResult = mapper.Map<QueryResult<Lider>, QueryResultResource<LiderResource>>(result);
                return Ok(mapResult);
            } 
            return NotFound();
            
        }

        [HttpGet("{id}/totallideres")]
        [Authorize]
        public async Task<IActionResult> GetTotalLideres(int id)
        {
            if (id != 0)
            {
                var estructura = await service.GetOne(id);
                if (estructura == null) return NotFound();
            }
            var result = await service.GetTotalLideres(id);
            return Ok(result);
        }

        [HttpGet("{id}/receptores")]
        [Authorize]
        public async Task<IActionResult> GetReceptores(int id)
        {
            if (id != 0)
            {
                var estructura = await service.GetOne(id);
                if (estructura == null) return NotFound();
            }
            var result = await service.GetReceptores(id);
            return Ok(mapper.Map<QueryResult<Receptor>, QueryResultResource<ReceptorResource>>(result));
        }

        [HttpGet("{id}/totalreceptores")]
        [Authorize]
        public async Task<IActionResult> GetTotalReceptores(int id)
        {
            if (id != 0)
            {
                var estructura = await service.GetOne(id);
                if (estructura == null) return NotFound();
            }
            var result = await service.GetTotalReceptores(id);
            return Ok(result);
        }

        [HttpPost("{id}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<ActionResult> UploadEstructura(IFormFile file, int id)
        {
            long size = file.Length;
            var encuesta = await encuestaService.GetOne(id);
            var estructura = await service.UpdateEstructura(encuesta, file);
            return Ok(mapper.Map<Estructura, SimpleEstructuraResource>(estructura));
        }
        
    }
}