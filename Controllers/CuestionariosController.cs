using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
    [Route("/api/cuestionarios")]
    public class CuestionariosController : Controller
    {
        private readonly IMapper mapper;
        private readonly ICuestionarioService service;
        private readonly IAuthorizationService authorizationService;
        private readonly UserManager<Usuario> userManager;



        public CuestionariosController(IAuthorizationService authorizationService, ICuestionarioService service, UserManager<Usuario> userManager, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
            this.authorizationService = authorizationService;
            this.userManager = userManager;

        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] SaveCuestionarioResource resource)
        {
            var cuestionario = mapper.Map<SaveCuestionarioResource, Cuestionario>(resource);
            var result = await service.Save(cuestionario);
            return Ok(mapper.Map<Cuestionario, CuestionarioResource>(cuestionario));
        }

        [HttpGet("{encuestaId}/getCSV")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IList<List<string>>> getCuestionariosParaCSV(int encuestaId)
        {
            return await service.GetCuestionariosParaCSV(encuestaId, false);
        }


        [HttpGet("{encuestaId}")]
        [Authorize(Policy = "RequireLider")]
        public async Task<QueryResultResource<CuestionarioResource>> getCuestionarios(int encuestaId)
        {

            var queryResult = await service.GetByEncuestaAndNivel(encuestaId);
            return mapper.Map<QueryResult<Cuestionario>, QueryResultResource<CuestionarioResource>>(queryResult);
        }
    }
}