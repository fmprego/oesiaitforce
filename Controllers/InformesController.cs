using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ee.Controllers
{
  [Route("/api/informes")]
  public class InformesController : Controller
  {
    private readonly IInformeService informeService;
    private readonly IConfiguration configuration;
    private readonly IMapper mapper;
    private readonly ILogger serviceLogger;


    public InformesController(IConfiguration configuration,
        IInformeService informeService,
        IMapper mapper, ILogger<InformesController> serviceLogger)
    {
      this.configuration = configuration;
      this.mapper = mapper;
      this.informeService = informeService;
      this.serviceLogger = serviceLogger;
    }

    [HttpGet("{encuestaId}/{global}/nivel")]
    [Authorize(Policy = "RequireLider")]
    public async Task<IActionResult> GetInforme(int encuestaId,bool global,[FromQuery] string nivel1, [FromQuery] string nivel2, [FromQuery] string nivel3, [FromQuery] string nivel4)
    {
        Nivel nivel = new Nivel();
        nivel.Nivel1 = nivel1;
        nivel.Nivel2 = nivel2;
        nivel.Nivel3 = nivel3;
        nivel.Nivel4 = nivel4;
      try{
        var minCuestionarioInforme = Int32.Parse(configuration["MinCuestionarioInforme"]);
        var informe = await this.informeService.GetInforme(encuestaId,global,nivel,minCuestionarioInforme);
        return Ok(mapper.Map<Informe, InformeResource>(informe));
       } catch (Exception e){
               serviceLogger.LogError(e.Message);
               return BadRequest(e.Message);
        }
    }

    [HttpGet("{encuestaId}/{global}/comparativa/nivel")]
    [Authorize(Policy = "RequireLider")]
    public async Task<IActionResult> GetInformeComparativa(int encuestaId,bool global,[FromQuery] string nivel1, [FromQuery] string nivel2, [FromQuery] string nivel3, [FromQuery] string nivel4)
    {
        Nivel nivel = new Nivel();
        nivel.Nivel1 = nivel1;
        nivel.Nivel2 = nivel2;
        nivel.Nivel3 = nivel3;
        nivel.Nivel4 = nivel4;
      try{
        var minCuestionarioInforme = Int32.Parse(configuration["MinCuestionarioInforme"]);
        var informe = await this.informeService.GetInformeComparativa(encuestaId,global,nivel,minCuestionarioInforme);
        return Ok(mapper.Map<Informe, InformeResource>(informe));
       } catch (Exception e){
               serviceLogger.LogError(e.Message);
               return BadRequest(e.Message);
        }
    }

    [HttpPost("{encuestaId}")]
    [Authorize(Policy = "RequireLider")]
    public async Task<IEnumerable<InformeResource>> GenerarInforme(int encuestaId)
    {
        var informes = await this.informeService.GenerarInformes(User, encuestaId);
        return mapper.Map<IEnumerable<Informe>, IEnumerable<InformeResource>>(informes);
    }

    [HttpGet("tipos")]
    [Authorize(Policy = "RequireLider")]
    public async Task<IActionResult> GenerarInformeTipos([FromQuery] string ids)
    {
        var idsEncuestas = ids.Trim().Split(",");
        var idsEncuestasInt = new List<int>();
        foreach(string id in idsEncuestas){
          idsEncuestasInt.Add(Int32.Parse(id));
        }
        var informe = await this.informeService.GetInformeTipos(idsEncuestasInt);
         return Ok(mapper.Map<Informe, InformeResource>(informe));
    }
  }
}
