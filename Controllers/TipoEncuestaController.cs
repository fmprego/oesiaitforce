using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using ee.Models.Query;
using ee.Persistence;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ee.Controllers
{
  [Route("/api/tipoencuesta")]
  public class TipoEncuestaController : Controller
  {
    private readonly IMapper mapper;
    private readonly ITipoEncuestaRepository repository;
    private readonly IEncuestaService encuestaService;
    private readonly IUnitOfWork unitOfWork;    
    public TipoEncuestaController(
      ITipoEncuestaRepository repository,      
      IMapper mapper,
      IUnitOfWork unitOfWork,
      IEncuestaService encuestaService)
    {
      this.unitOfWork = unitOfWork;
      this.repository = repository;
      this.mapper = mapper;
      this.encuestaService = encuestaService;
    }

    [HttpGet]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IEnumerable<TipoEncuestaResource>> GetTiposEncuesta()
    {
      var filter = new TipoEncuestaQuery(); // todo
      var queryResult = await repository.GetTiposEncuesta(filter);
      return mapper.Map<IEnumerable<TipoEncuesta>, IEnumerable<TipoEncuestaResource>>(queryResult);
    }

    [HttpGet("all")]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IEnumerable<TipoEncuestaResource>> GetTiposEncuestaAll()
    {
      var filter = new TipoEncuestaQuery(); // todo
      var queryResult = await repository.GetTiposEncuestaAll(filter);
      return mapper.Map<IEnumerable<TipoEncuesta>, IEnumerable<TipoEncuestaResource>>(queryResult);
    }

    [HttpGet("{id}")]
    [Authorize(Policy = "RequireAdmin")]
    public async Task<IActionResult> GetTipoEncuesta(int id)
    {
      var tipo = await repository.GetTipoEncuesta(id);
      if (tipo == null) return NotFound();
      var tipoResource = mapper.Map<TipoEncuesta, TipoEncuestaResource>(tipo);
      return Ok(tipoResource);
    }

    [HttpPost]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> CreateTipoEncuesta([FromBody] TipoEncuestaResource tipoResource)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      var tipo = mapper.Map<TipoEncuestaResource, TipoEncuesta>(tipoResource);
      var result = await repository.CreateTipoEncuesta(tipo);
      return Ok(mapper.Map<TipoEncuesta, TipoEncuestaResource>(result));
    }

    [HttpDelete("{id}")]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> DeleteTipoEncuesta(int id)
    {
      var tipo = await repository.GetTipoEncuesta(id);
      if (tipo == null) return NotFound();
      var num = await this.encuestaService.GetNumeroEncuestasPorTipo(tipo.Id);
      if (num > 0) {
        return BadRequest("No es posible eliminar un tipo que tiene encuestas.");
      } 
      else
      {
        repository.DeleteTipoEncuesta(tipo);
        await unitOfWork.CompleteAsync();
        return Ok(id);
      }
    }

    [HttpPut("{id}")]
    [Authorize(Policy = "RequireMaster")]
    public async Task<IActionResult> UpdateTipoEncuesta(int id, [FromBody] TipoEncuestaResource tipoResource)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      var changed = mapper.Map<TipoEncuestaResource, TipoEncuesta>(tipoResource);
      var updated = await repository.UpdateTipoEncuesta(id, changed);
      return Ok(mapper.Map<TipoEncuesta, TipoEncuestaResource>(updated));
    }

  }
}