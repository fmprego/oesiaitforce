using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using ee.Models;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using ee.Controllers.Resources;
using Microsoft.AspNetCore.Authorization;
using ee.Core.Exceptions;
using Microsoft.Extensions.Logging;
using ee.Core;
using ee.Utils;

namespace ee.Controllers
{
    [Route("api/auth")]
    public class AuthController : Controller
    {

        private readonly UserManager<Usuario> _userManager;
        private readonly SignInManager<Usuario> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly ILogger _serviceLogger;
        private readonly IPasswordRecoveryInfoService service;

        public AuthController(
            UserManager<Usuario> userManager,
            SignInManager<Usuario> signInManager,
            IConfiguration configuration,
            ILogger<AuthController> serviceLogger,
            IPasswordRecoveryInfoService service
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _serviceLogger = serviceLogger;
             this.service = service;
        }
 
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginResource loginResource)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(loginResource.Email);
                if (user != null)
                {
                    var result = await _signInManager.CheckPasswordSignInAsync(user, loginResource.Password, lockoutOnFailure: false);

                    if (!result.Succeeded)
                    {
                        return Unauthorized();
                    }

                    var userClaims = await _userManager.GetClaimsAsync(user);
                    userClaims.Add(new Claim(JwtRegisteredClaimNames.Sub, loginResource.Email));
                    userClaims.Add(new Claim(JwtRegisteredClaimNames.UniqueName, user.Nombre));
                    userClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

                    var token = new JwtSecurityToken
                    (
                        issuer: _configuration["Token:Issuer"],
                        audience: _configuration["Token:Audience"],
                        claims: userClaims,
                        expires: DateTime.UtcNow.AddDays(60),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Token:Key"])),
                                SecurityAlgorithms.HmacSha256)
                    );

                    user.LastLogin = DateTime.Now;
                    await _userManager.UpdateAsync(user);
                    _serviceLogger.LogInformation("Login correcto. User:{1}", loginResource.Email);
                    return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
                }
            }

            return BadRequest();
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] SigninResource signinResource)
        {
            if (ModelState.IsValid)
            {
                var user = new Usuario { UserName = signinResource.Email, Email = signinResource.Email };
                var result = await _userManager.CreateAsync(user, signinResource.Password);
                if (result.Succeeded)
                {
                    _serviceLogger.LogInformation("Registro correcto. User:{1}", signinResource.Email);
                    return Ok(result);
                }
            }
            else
            {
                _serviceLogger.LogInformation("Register incorrecto. User:{1}", signinResource.Email);
            }
            return BadRequest();
        }

        [HttpPut("pass")]
        [Authorize(Policy = "RequireLider")]
        public async Task<IActionResult> UpdatePassword([FromBody] UpdatePasswordResource updatePasswordResource)
        {
            if (ModelState.IsValid)
            {
                
                var user = await _userManager.FindByEmailAsync(updatePasswordResource.Email);
                var userId = _userManager.GetUserId(User);

                if (user == null)
                {
                    _serviceLogger.LogWarning("No existe el usuario. User:{1}", updatePasswordResource.Email);
                    return BadRequest("El usuario no existe.");
                }

                if (userId == null || user.Id != userId)
                    return Unauthorized();

                if (user.UserName == null)
                {
                    user.UserName = user.Email;
                    await _userManager.UpdateAsync(user);
                }

                var result = await _userManager.ChangePasswordAsync(user, updatePasswordResource.OldPass, updatePasswordResource.NewPass);

                if (result.Succeeded)
                {
                    _serviceLogger.LogInformation("Cambio correcto de la contrase�a. User:{1}", updatePasswordResource.Email);
                    return Ok(result);
                }
                else
                {
                    _serviceLogger.LogWarning("No se ha podido cambiar la contrase�a. User:{1}", updatePasswordResource.Email);
                    return BadRequest(result);
                }
            }
            return BadRequest();
        }

    }
}