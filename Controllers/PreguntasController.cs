using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using ee.Models.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ee.Controllers
{
    [Route("/api/preguntas")]
    public class PreguntasController : Controller
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IPreguntaService service;
        private readonly UserManager<Usuario> userManager;

        public PreguntasController(
          IMapper mapper,
          IUnitOfWork unitOfWork,
          IPreguntaService service,
          UserManager<Usuario> userManager
        )
        {
            this.unitOfWork = unitOfWork;
            this.service = service;
            this.userManager = userManager;
            this.mapper = mapper;
        }

        [HttpGet]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<QueryResultResource<PreguntaResource>> GetPreguntas([FromQuery] int[] categorias)
        {
            var queryResult = await service.GetAll(categorias);
            return mapper.Map<QueryResult<Pregunta>, QueryResultResource<PreguntaResource>>(queryResult);
        }

        [HttpGet("tipos")]
        [Authorize(Policy = "RequireAdmin")]
        public IActionResult GetTiposPregunta()
        {
            var tipos = Enum.GetValues(typeof(TipoPregunta)).Cast<TipoPregunta>();
            return Ok(mapper.Map<IEnumerable<TipoPregunta>, IEnumerable<TipoPreguntaResource>>(tipos));
        }

        [HttpGet("{id}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> GetPregunta(int id)
        {
            // todo auth
            var pregunta = await service.GetOne(id);
            if (pregunta == null) return NotFound();
            return Ok(mapper.Map<Pregunta, PreguntaResource>(pregunta));
        }

        [HttpPost]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> CreatePregunta([FromBody] SavePreguntaResource savePreguntaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var pregunta = mapper.Map<SavePreguntaResource, Pregunta>(savePreguntaResource);
            var result = await service.Create(pregunta, userManager.GetUserId(User));
            return Ok(mapper.Map<Pregunta, PreguntaResource>(result));
        }

        [HttpPut("{id}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> Update(int id, [FromBody] SavePreguntaResource savePreguntaResource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var pregunta = await service.GetOne(id);
            if (pregunta == null) return NotFound();
            // TODO
            // var authResult = await authorizationService.AuthorizeAsync(User, pregunta, Operations.Save);
            // if (!authResult.Succeeded) return Forbid();
            var changed = mapper.Map<SavePreguntaResource, Pregunta>(savePreguntaResource);
            var updated = await service.Update(id, changed, userManager.GetUserId(User));

            return Ok(mapper.Map<Pregunta, PreguntaResource>(updated));
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            var pregunta = await service.GetOne(id);
            if (pregunta == null) return NotFound();
            // TODO
            // var authResult = await authorizationService.AuthorizeAsync(User, pregunta, Operations.Delete);
            // if (!authResult.Succeeded) return Forbid();
            await service.Delete(pregunta.Id);
            return Ok(id);
        }

        [HttpGet("byencuesta/{encuestaId}")]
        public async Task<QueryResultResource<PreguntaResource>> GetByEncuesta(int encuestaId)
        {
            var queryResult = await service.GetByEncuesta(encuestaId);
            return mapper.Map<QueryResult<Pregunta>, QueryResultResource<PreguntaResource>>(queryResult);
        }

        [HttpPost("{id}/copiaPregunta")]
        [Authorize(Policy = "RequireAdmin")]
        public async Task<IActionResult> copiaPregunta(int id, [FromBody] PreguntaResource preguntaResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var pregunta = await service.GetOne(id);
            if (pregunta == null)
                return NotFound();

            // Crear la nueva pregunta
            var savePreguntaResource = new SavePreguntaResource();
            savePreguntaResource.Descripcion = pregunta.Descripcion;
            savePreguntaResource.Texto = "COPIA " + pregunta.Texto;
            savePreguntaResource.CategoriaId = pregunta.CategoriaId;
            savePreguntaResource.Escala = pregunta.Escala;
            savePreguntaResource.TipoPregunta = (int)pregunta.TipoPregunta;

            var preguntaMapped = mapper.Map<SavePreguntaResource, Pregunta>(savePreguntaResource);

            preguntaMapped.Editable = true;
            preguntaMapped.Usuario = pregunta.Usuario;
            preguntaMapped.UsuarioId = pregunta.UsuarioId;
            
            // iterar sobre sus opciones establecer nuevos id's
            if (pregunta.Opciones.Count > 0)
            {
                foreach (var item in pregunta.Opciones)
                {
                    Opcion nueva = new Opcion()
                    {
                        Texto = item.Texto,
                        Peso = item.Peso,
                        Pregunta = item.Pregunta,
                        GrupoOpcionesId = item.GrupoOpcionesId,
                        ImagenContain = item.ImagenContain,
                        ImagenCab = item.ImagenCab,
                        Imagen = item.Imagen
                    };
                    preguntaMapped.Opciones.Add(nueva);
                }
            }

            var preguntaCreada = await service.Create(preguntaMapped, userManager.GetUserId(User));

            //TODO: Chequear permisos, en todo el controlador esta comentado.
            //var authResult = await authorizationService.AuthorizeAsync(User, preguntaCreada, Operations.Save);
            //if (!authResult.Succeeded)
            //    return Forbid();

            return Ok(mapper.Map<Pregunta, PreguntaResource>(preguntaCreada));
        }

        [HttpGet("getencuestasbypregunta/{preguntaId}")]
        public async Task<IEnumerable<EncuestaResource>> getEncuestasByPregunta(int preguntaId)
        {
            var queryResult = await service.getEncuestasByPregunta(preguntaId);
            return mapper.Map<IEnumerable<Encuesta>, IEnumerable<EncuestaResource>>(queryResult);
        }

    }
}