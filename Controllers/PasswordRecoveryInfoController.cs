using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core;
using ee.Models;
using ee.Models.Enum;
using ee.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ee.Models.Configuration;

namespace ee.Controllers
{
    [Route("/api/pwdrecovery")]
    public class PasswordRecoveryInfoController : Controller
    {        
        private readonly static string REST_ENDPOINT_UPDATEPASSWORD = "/recovery-pass";
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IPasswordRecoveryInfoService service;
        private readonly IEmailService emailService;

        public PasswordRecoveryInfoController(
          IMapper mapper,
          IUnitOfWork unitOfWork,
          IPasswordRecoveryInfoService service,
          IEmailService emailService
        )
        {
            this.unitOfWork = unitOfWork;
            this.service = service;
            this.mapper = mapper;
            this.emailService = emailService;
        }

    //[HttpPost("add")]
    public void Add(PasswordRecoveryInfo passwordRecoveryInfo){
        service.Add(passwordRecoveryInfo);
    }
    
    //[HttpGet("delete/{id}")]
    public void Delete(int id){
        service.Delete(id);
    }
    
    
    //[HttpPost("update/{id}")]
    public void UpdateRecoveryInfoById(int id, [FromBody] PasswordRecoveryInfo newInfo){
        newInfo.requestDate = DateTime.Now;
        service.UpdateRecoveryInfoById(id, newInfo);
    }
    
    //[HttpGet("get/{id}")]
    public PasswordRecoveryInfo getPwdRecoveryInfoById(int id){
        return service.getPasswordInfoById(id);
    }

    [HttpGet("test")]
    public string getTest(){
        return "//Abanca Express Engagement";
    }

    // Metodo que envia el enlace para renovar la contrasenha con un enlace incrustado en un mail
    [HttpPost("mailRecoverPass")]
    public async Task<IActionResult> mailRecoverPass([FromBody] LoginResource loginResource)
    {
        string from = "";
        string to = loginResource.Email;
        var pwdChangingCode = "";
        try {
            pwdChangingCode = await service.getPwdRequest(to);
        } catch(InvalidOperationException){
            return Ok("No encontrado");
        }
        
        if (pwdChangingCode.Equals("") || pwdChangingCode == null){
            return Ok("");
        }

        Encoding utf8 = Encoding.UTF8;
        string mailEncoded = Convert.ToBase64String(utf8.GetBytes(loginResource.Email));  
        string urlAndCode = loginResource.Url + REST_ENDPOINT_UPDATEPASSWORD + "/"+pwdChangingCode + "/"+mailEncoded;
        mailComposer(loginResource.Email, from, to, urlAndCode, pwdChangingCode);
        return Ok(urlAndCode);
    }

    // Metodo para setear nueva contrasenha desde formulario en caso de haber olvidado la contrasenha original
     [HttpPut("{setnewpassword}")]
     public async Task<IActionResult> setNewPassword([FromBody] UpdatePasswordResource updatePasswordResource) {

        var usuario = await service.getAspNetUserMailByEmail(updatePasswordResource.Email);
        if (usuario == null)
              return Ok("User's email is not valid");

        string idAspNetUser = usuario.Id;
        var pri = await service.getPwdRecoveryByIdAspNetUserAndCode(idAspNetUser, updatePasswordResource.ValidationCode);
    
        if (pri==null)
             return Ok("Recovery password is not authorized");
        if (pri.codeUsed.Equals(true)) {
              return Ok("Recovery Code is out of date");
        }
        if (pri.requestCode.Trim() != updatePasswordResource.ValidationCode.ToString().Trim()) {
             return Ok("Recovery code is not valid, retry password recovery"); 
        }
        DateTime oldDate =  pri.requestDate;
        DateTime newDate = oldDate.AddHours(pri.requestExpirationTime);
        if (newDate.Date<DateTime.Now) {
             return Ok("User's recovery password request is out of date"); 
        }
        pri.codeUsed = true;
        service.UpdateRecoveryInfoById(pri.Id, pri);
        service.UpdateAspNetUser(pri.idAspNetUser, updatePasswordResource.NewPass);

        return Ok("Password changed"); 
    }
     
    //[HttpGet("getusermail/{email}")]
    public async Task<Usuario> getUserMailById(string email){
        
        return await service.getAspNetUserMailByEmail(email);
        }

    //[HttpGet("getaspuser/{idaspnetuser}")]
    public string getPwdRecoveryByIdAspNetUser(string idaspnetuser){

        var res = service.getPwdRecoveryByIdAspNetUser(idaspnetuser);
        return res.Id.ToString();
    }

    void mailComposer(string userMail, string from, string to, string urlAndCode, string pwdChangingCode){

        EmailMessage emailMessage = new EmailMessage();
        
        List<EmailAddress> toAddresses = new List<EmailAddress>();
        EmailAddress emailAddress = new EmailAddress();
        emailAddress.Address = to; //change to usermail
        emailAddress.Name = to;
        toAddresses.Add(emailAddress);        
        emailMessage.ToAddresses = toAddresses;
        
        emailMessage.Subject = "Express Engagement Password Recovery";
        emailMessage.Content = string.Format("<p>Follow the next link for password recovery <a href=\"{0}\">{1}</a></p>", urlAndCode, urlAndCode);
        
        emailService.Send(emailMessage);
        Console.WriteLine("Sending password recovery email for " + userMail);
    }

    }  
}


