using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using ee.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using ee.Core;
using ee.Controllers.Auth;
using Microsoft.AspNetCore.Authorization;
using ee.Models.Enum;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using ee.Models.Error;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using ee.Models.Configuration;
using Microsoft.Extensions.Logging;

namespace ee
{
    public class Startup
    {

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            _Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment _Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();

            services.AddDbContext<EEDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("Default"), b => b.UseRowNumberForPaging())); // sqlserver2008 only.

            services.AddTransient<IAuthorizationHandler, EncuestaAuthorizationCrudHandler>();
        
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IGeografiaRepository, GeografiaRepository>();
            services.AddScoped<IUnidadRepository, UnidadRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<IEncuestaRepository, EncuestaRepository>();
            services.AddScoped<IEncuestaService, EncuestaService>();
            services.AddScoped<ICategoriaRepository, CategoriaRepository>();
            services.AddScoped<IPreguntaRepository, PreguntaRepository>();
            services.AddScoped<IPreguntaService, PreguntaService>();
            services.AddScoped<IOpcionRepository, OpcionRepository>();
            services.AddScoped<IGrupoOpcionesRepository, GrupoOpcionesRepository>();
            services.AddScoped<IEstructuraService, EstructuraService>();
            services.AddScoped<IEstructuraRepository, EstructuraRepository>();
            services.AddScoped<IPersonaRepository, PersonaRepository>();
            services.AddScoped<ICuestionarioService, CuestionarioService>();
            services.AddScoped<ICuestionarioRepository, CuestionarioRepository>();
            services.AddScoped<IInformeService, InformeService>();
            services.AddScoped<IInformeRepository, InformeRepository>();
            services.AddScoped<IResultadoPreguntaService, ResultadoPreguntaService>();
            services.AddScoped<IResultadoCategoriaService, ResultadoCategoriaService>();
            services.AddScoped<IResultadoOpcionService, ResultadoOpcionService>();
            services.AddScoped<IRespuestaService, RespuestaService>();
            services.AddScoped<IRespuestaRepository, RespuestaRepository>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<ITipoEncuestaRepository, TipoEncuestaRepository>();
            services.AddScoped<ILogger, Log4NetLogger>();
            services.AddScoped<IPasswordRecoveryInfoService, PasswordRecoveryInfoService>();
            services.AddScoped<IPasswordRecoveryInfoRepository, PasswordRecoveryInfoRepository>();

            services.AddIdentity<Usuario, IdentityRole>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
                .AddEntityFrameworkStores<EEDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(option =>
                {
                    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    option.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(jwtBearerOptions =>
                {
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                    {
					    ValidateActor = true,
					    ValidateAudience = true,
					    ValidateLifetime = true,
					    ValidateIssuerSigningKey = true,
					    ValidIssuer = Configuration["Token:Issuer"],
					    ValidAudience = Configuration["Token:Audience"],
					    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Token:Key"]))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireMaster", policy => policy.RequireClaim("Role", "Master"));
                options.AddPolicy("RequireAdmin", policy => policy.RequireClaim("Role", "Admin"));
                options.AddPolicy("RequireLider", policy => policy.RequireClaim("Role", "Lider"));
            });

            services.AddMvc()
                .AddJsonOptions(opt => 
                {
                   opt.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                });
            services.AddMvc();
	        services.AddSingleton<IEmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
        {
            app.Use(async (HttpContext, next) =>
            {
                HttpContext.Response.Headers["Cache-Control"] = "no-cache";
                await next();
            });

            if (env.IsDevelopment())
            {
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";

                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    if (error != null)
                    {
                        var ex = error.Error;

                        await context.Response.WriteAsync(new ErrorDto()
                        {
                            Code = 400,
                            Message = ex.Message
                        }.ToString(), Encoding.UTF8);
                    }
                });
            });

            app.UseStaticFiles();

            app.UseAuthentication();

            loggerFactory.AddLog4Net();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });

            //this.InitDatabase(serviceProvider).Wait();

        }

        private async Task InitDatabase(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();
            await context.Database.EnsureCreatedAsync();
            await this.CreateGeografias(serviceProvider);
            await this.CreateMasterUser(serviceProvider);
            await this.CreateAdminUser(serviceProvider);
            await this.CreateLiderUser(serviceProvider);
            await this.CreateCategorias(serviceProvider);
            await this.CreateEncuestasPreguntas(serviceProvider);
            await this.CreateEncuestasCategorias(serviceProvider);
            await this.CreateCuestionarios(serviceProvider);
            await this.CreateEstructura(serviceProvider);
            await this.CreateIdiomas(serviceProvider);
        }

        private async Task CreateGeografias(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();
            if (!context.Geografias.Any())
            {
                await context.Geografias.AddAsync(new Geografia("Europa"));
                await context.Geografias.AddAsync(new Geografia("América"));
                await context.Geografias.AddAsync(new Geografia("Asia"));
                context.SaveChanges();
            }
        }

        private async Task CreateMasterUser(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<Usuario>>();
            var context = serviceProvider.GetService<EEDbContext>();
            var geo = await context.Geografias.Where(g => g.Nombre.Equals("Europa")).FirstOrDefaultAsync();

            var master = new Usuario
            {
                UserName = Configuration["Master:UserName"],
                Email = Configuration["Master:UserEmail"],
                Nombre = "NomMaster",
                Apellidos = "ApMaster1 ApMaster2",
                GeografiaId = geo.Id,
                LastUpdate = DateTime.Now,
            };
            var user = await userManager.FindByEmailAsync(Configuration["Master:UserEmail"]);

            if (user == null)
            {
                var createdMaster = await userManager.CreateAsync(master, Configuration["DefaultPassword"]);
                if (createdMaster.Succeeded)
                {
                    await userManager.AddClaimAsync(master, new Claim("Role", "Master"));
                    await userManager.AddClaimAsync(master, new Claim("Role", "Admin"));
                    await userManager.AddClaimAsync(master, new Claim("Role", "Lider"));
                    await userManager.AddClaimAsync(master, new Claim(ClaimTypes.NameIdentifier, master.Id));
                }
            }
        }

        private async Task CreateAdminUser(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<Usuario>>();
            var context = serviceProvider.GetService<EEDbContext>();
            var geo = await context.Geografias.Where(g => g.Nombre.Equals("Europa")).FirstOrDefaultAsync();

            var admin = new Usuario
            {
                UserName = Configuration["Admin:UserName"],
                Email = Configuration["Admin:UserEmail"],
                Nombre = "NomAdmin",
                Apellidos = "ApAdmin1 ApAdmin2",
                GeografiaId = geo.Id,
                LastUpdate = DateTime.Now,
            };
            var user = await userManager.FindByEmailAsync(Configuration["Admin:UserEmail"]);

            if (user == null)
            {
                var createdAdmin = await userManager.CreateAsync(admin, Configuration["DefaultPassword"]);
                if (createdAdmin.Succeeded)
                {
                    await userManager.AddClaimAsync(admin, new Claim("Role", "Admin"));
                    await userManager.AddClaimAsync(admin, new Claim("Role", "Lider"));
                    await userManager.AddClaimAsync(admin, new Claim(ClaimTypes.NameIdentifier, admin.Id));
                }
            }
        }

        private async Task CreateLiderUser(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<Usuario>>();
            var context = serviceProvider.GetService<EEDbContext>();
            var geo = await context.Geografias.Where(g => g.Nombre.Equals("Europa")).FirstOrDefaultAsync();

            var lider = new Usuario
            {
                UserName = Configuration["Lider:UserName"],
                Email = Configuration["Lider:UserEmail"],
                Nombre = "NomLider",
                Apellidos = "ApLider1 ApLider2",
                GeografiaId = geo.Id,
                LastUpdate = DateTime.Now,
            };
            var user = await userManager.FindByEmailAsync(Configuration["Lider:UserEmail"]);

            if (user == null)
            {
                var createdLider = await userManager.CreateAsync(lider, Configuration["DefaultPassword"]);
                if (createdLider.Succeeded)
                {
                    await userManager.AddClaimAsync(lider, new Claim("Role", "Lider"));
                    await userManager.AddClaimAsync(lider, new Claim(ClaimTypes.NameIdentifier, lider.Id));
                }
            }
        }        

        private async Task CreateCategorias(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();
            if (!context.Categorias.Any())
            {
                await context.Categorias.AddAsync(new Categoria("Identidad corporativa"));
                await context.Categorias.AddAsync(new Categoria("Alineación con la estrategia"));
                await context.Categorias.AddAsync(new Categoria("Estabilidad"));
                await context.Categorias.AddAsync(new Categoria("Comunicación"));
                await context.Categorias.AddAsync(new Categoria("Engagement"));
                context.SaveChanges();
            }
        }

        private async Task CreateEncuestasPreguntas(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<Usuario>>();

            var user = await userManager.FindByEmailAsync(Configuration["Admin:UserEmail"]);

            var cat1 = context.Categorias.SingleOrDefault(c => c.Nombre == "Identidad corporativa");
            var cat2 = context.Categorias.SingleOrDefault(c => c.Nombre == "Estabilidad");

            if (!context.GruposOpciones.Any())
            {
                var grupoFrecuencia = new GrupoOpciones {
                    Nombre = "Frecuencia",
                    Opciones = new[]
                    {
                        new Opcion {
                            Texto = "Siempre",
                            Peso = 1f,
                        },
                        new Opcion {
                            Texto = "Frecuentemente",
                            Peso = 0.75f,
                        },
                        new Opcion {
                            Texto = "A veces",
                            Peso = 0.5f,
                        },
                        new Opcion {
                            Texto = "Rara vez",
                            Peso = 0.25f,
                        },
                        new Opcion {
                            Texto = "Nunca",
                            Peso = 0f,
                        },
                    }
                };
                var grupoSatisfaccion = new GrupoOpciones {
                    Nombre = "Grado de satisfacción",
                    Opciones = new[]
                    {
                        new Opcion {
                            Texto = "Totalmente satisfecho/a",
                            Peso = 1f,
                        },
                        new Opcion {
                            Texto = "Muy satisfecho/a",
                            Peso = 0.75f,
                        },
                        new Opcion {
                            Texto = "Razonablemente satisfecho/a",
                            Peso = 0.5f,
                        },
                        new Opcion {
                            Texto = "Poco satisfecho/a",
                            Peso = 0.25f,
                        },
                        new Opcion {
                            Texto = "Nada satisfecho/a",
                            Peso = 0f,
                        },
                    }
                };
                await context.GruposOpciones.AddAsync(grupoFrecuencia);
                await context.GruposOpciones.AddAsync(grupoSatisfaccion);
                context.SaveChanges();
            }

            if (!context.Preguntas.Any()
                && !context.Encuestas.Any()
                && cat1 != null && cat2 != null)
            {
                var encuestas = new[]
                {
                    new Encuesta {
                        Titulo = "Test 1",
                        Descripcion = "Primera encuesta de pruebas",
                        Usuario = user
                    },
                    new Encuesta {
                        Titulo = "Test 2",
                        Descripcion = "Segunda encuesta de pruebas",
                        Usuario = user
                    },
                    new Encuesta {
                        Titulo = "Test 3",
                        Descripcion = "Tercera encuesta de pruebas",
                        Usuario = user,
                        Estado = EstadoEncuesta.Cerrada,
                    },
                };
                var opciones = new[]
                {
                    new Opcion {
                        Texto = "Siempre",
                        Peso = 1f,
                    },
                    new Opcion {
                        Texto = "Frecuentemente",
                        Peso = 0.75f,
                    },
                    new Opcion {
                        Texto = "A veces",
                        Peso = 0.5f,
                    },
                    new Opcion {
                        Texto = "Rara vez",
                        Peso = 0.25f,
                    },
                    new Opcion {
                        Texto = "Nunca",
                        Peso = 0f,
                    },
                };
                var preguntas = new[]
                {
                    new Pregunta {
                        Texto = "¿Te sientes identificado con la marca?",
                        Categoria = cat1,
                        Usuario = user,
                        TipoPregunta = TipoPregunta.Escala
                    },
                    new Pregunta {
                        Texto = "Te parece que la empresa hace una buena labor.",
                        Categoria = cat1,
                        Usuario = user,
                        TipoPregunta = TipoPregunta.Texto
                    },
                    new Pregunta {
                        Texto = "Me siento respaldado por mis jefes.",
                        Categoria = cat2,
                        Usuario = user,
                        TipoPregunta = TipoPregunta.Escala
                    },
                    new Pregunta {
                        Texto = "Pienso estar muchos años en la empresa.",
                        Categoria = cat2,
                        Usuario = user,
                        TipoPregunta = TipoPregunta.Escala
                    },
                    new Pregunta {
                        Texto = "No creo que vaya a ser despedido.",
                        Categoria = cat2,
                        Usuario = user,
                        TipoPregunta = TipoPregunta.Seleccion,
                        Opciones = new Collection<Opcion> {
                            opciones[0], opciones[1], opciones[2], opciones[3], opciones[4]
                        }
                    },
                    new Pregunta {
                        Texto = "Estoy orgullosos de pertenecer a esta empresa.",
                        Categoria = cat1,
                        Usuario = user,
                        TipoPregunta = TipoPregunta.Seleccion,
                        Opciones = new Collection<Opcion> {
                            opciones[0], opciones[1], opciones[2], opciones[3], opciones[4]
                        }
                    },
                };

                context.AddRange(
                    new EncuestaPregunta { Encuesta = encuestas[0], Pregunta = preguntas[0], Peso = 0.45F },
                    new EncuestaPregunta { Encuesta = encuestas[0], Pregunta = preguntas[1], Peso = 0.35F },
                    new EncuestaPregunta { Encuesta = encuestas[0], Pregunta = preguntas[5], Peso = 0.75F },
                    new EncuestaPregunta { Encuesta = encuestas[1], Pregunta = preguntas[2], Peso = 0.5F },
                    new EncuestaPregunta { Encuesta = encuestas[1], Pregunta = preguntas[3], Peso = 0.75F },
                    new EncuestaPregunta { Encuesta = encuestas[1], Pregunta = preguntas[4], Peso = 0.5F },
                    new EncuestaPregunta { Encuesta = encuestas[1], Pregunta = preguntas[5], Peso = 0.5F },
                    new EncuestaPregunta { Encuesta = encuestas[2], Pregunta = preguntas[0], Peso = 0.7F },
                    new EncuestaPregunta { Encuesta = encuestas[2], Pregunta = preguntas[1], Peso = 0.9F },
                    new EncuestaPregunta { Encuesta = encuestas[2], Pregunta = preguntas[2], Peso = 0.96F },
                    new EncuestaPregunta { Encuesta = encuestas[2], Pregunta = preguntas[3], Peso = 0.3F },
                    new EncuestaPregunta { Encuesta = encuestas[2], Pregunta = preguntas[4], Peso = 0.33F },
                    new EncuestaPregunta { Encuesta = encuestas[2], Pregunta = preguntas[5], Peso = 0.55F }
                );

                context.AddRange(
                    new EncuestaCategoria { Encuesta = encuestas[2], Categoria = cat1 },
                    new EncuestaCategoria { Encuesta = encuestas[2], Categoria = cat2 }
                );

                context.SaveChanges();
            }
        }

        private async Task CreateEncuestasCategorias(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();

            var cat1 = context.Categorias.SingleOrDefault(c => c.Nombre == "Identidad corporativa");
            var cat2 = context.Categorias.SingleOrDefault(c => c.Nombre == "Estabilidad");
            var encuesta = context.Encuestas.FirstOrDefault();

            if (!context.EncuestasCategorias.Any() && cat1 != null && cat2 != null && encuesta != null)
            {
                context.AddRange(
                    new EncuestaCategoria{ Encuesta = encuesta, Categoria = cat1 },
                    new EncuestaCategoria{ Encuesta = encuesta, Categoria = cat2 }
                );
                await context.SaveChangesAsync();
            }
        }

        private async Task CreateEstructura(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();

            if (!context.Personas.Any())
            {
                var encuesta = await context.Encuestas.SingleOrDefaultAsync(e => e.Titulo.Equals("Test 3"));
                var estructura = new Estructura {
                    Encuesta = encuesta
                };
                var personas = this.CreateRandomPersonas(estructura);
                context.Personas.AddRange(personas);
                await context.SaveChangesAsync();
            }
        }

        private IEnumerable<Persona> CreateRandomPersonas(Estructura estructura)
        {
            var personas = new Collection<Persona>();
            Random rnd = new Random();
            for (int i = 0; i < 9; i++)
            {
                var n = rnd.Next(99999);
                personas.Add(new Persona {
                    Numero = "0",
                    Nivel1 = "ABANCA SEGUROS",
                    Nivel2 = "AUDITORIA SEGUROS",
                    Nombre = "Test",
                    EsLider = false,
                    NumCentro = 0,
                    Email = $"test{n}@abanca.com",
                    Estructura = estructura,
                });
                n = rnd.Next(99999);
                personas.Add(new Persona {
                    Numero = "0",
                    Nivel1 = "ABANCA SEGUROS",
                    Nivel2 = "DIRECCION COMERCIAL SEGUROS",
                    Nombre = "Test",
                    EsLider = false,
                    NumCentro = 0,
                    Email = $"test{n}@abanca.com",
                    Estructura = estructura,
                });
                n = rnd.Next(99999);
                personas.Add(new Persona {
                    Numero = "0",
                    Nivel1 = "ABANCA SEGUROS",
                    Nivel2 = "EXPERIENCIA CLIENTES",
                    Nombre = "Test",
                    EsLider = false,
                    NumCentro = 0,
                    Email = $"test{n}@abanca.com",
                    Estructura = estructura,
                });
                n = rnd.Next(99999);
                personas.Add(new Persona {
                    Numero = "0",
                    Nivel1 = "CAPITAL HUMANO",
                    Nivel2 = "CAPACITACION Y GESTION DEL CAMBIO",
                    Nombre = "Test",
                    EsLider = false,
                    NumCentro = 0,
                    Email = $"test{n}@abanca.com",
                    Estructura = estructura,
                });
            }
            return personas;
        }

        private async Task CreateCuestionarios(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();

            if (!context.Cuestionarios.Any())
            {
                var encuestas = context.Encuestas.ToArray();

                var cuestionarios = new Collection<Cuestionario>();
                for (int i = 0; i < 24; i++)
                {
                    cuestionarios.Add(new Cuestionario {
                        EncuestaId = encuestas[2].Id,
                        Encuesta = encuestas[2],
                        Nivel1 = "ABANCA SEGUROS",
                        Nivel2 = "AUDITORIA SEGUROS"
                    });
                    cuestionarios.Add(new Cuestionario {
                        EncuestaId = encuestas[2].Id,
                        Encuesta = encuestas[2],
                        Nivel1 = "ABANCA SEGUROS",
                        Nivel2 = "DIRECCION COMERCIAL SEGUROS"
                    });
                    cuestionarios.Add(new Cuestionario {
                        EncuestaId = encuestas[2].Id,
                        Encuesta = encuestas[2],
                        Nivel1 = "ABANCA SEGUROS",
                        Nivel2 = "EXPERIENCIA CLIENTES"
                    });
                    cuestionarios.Add(new Cuestionario {
                        EncuestaId = encuestas[2].Id,
                        Encuesta = encuestas[2],
                        Nivel1 = "CAPITAL HUMANO",
                        Nivel2 = "CAPACITACION Y GESTION DEL CAMBIO"
                    });
                }
                context.AddRange(this.CreateRandomRespuestas(serviceProvider, cuestionarios));
            }
            await context.SaveChangesAsync();
        }

        private ICollection<Respuesta> CreateRandomRespuestas(IServiceProvider serviceProvider, IEnumerable<Cuestionario> cuestionarios)
        {
            var context = serviceProvider.GetService<EEDbContext>();
            var preguntas = context.Preguntas.ToArray();
            var opciones = context.Opciones.ToArray();
            var respuestas = new Collection<Respuesta>();

            foreach (Cuestionario c in cuestionarios)
            {
                Random rnd = new Random();
                for (int j = 0; j < preguntas.Length; j++)
                {
                    var pregunta = preguntas[j];

                    var k = rnd.Next(opciones.Length - 1);
                    var opcion = opciones[k];
                    var escala = (float) rnd.NextDouble();
                    var texto = "Test";

                    if (pregunta.TipoPregunta == TipoPregunta.Texto)
                    {
                        respuestas.Add(new Respuesta {
                            Pregunta = pregunta,
                            Cuestionario = c,
                            TipoPregunta = pregunta.TipoPregunta,
                            Texto = texto,
                        });
                    }
                    else if (pregunta.TipoPregunta == TipoPregunta.Seleccion)
                    {
                        respuestas.Add(new Respuesta {
                            Pregunta = pregunta,
                            Cuestionario = c,
                            TipoPregunta = pregunta.TipoPregunta,
                            Opcion = opcion,
                        });
                    }
                    else if (pregunta.TipoPregunta == TipoPregunta.Escala)
                    {
                        respuestas.Add(new Respuesta {
                            Pregunta = pregunta,
                            Cuestionario = c,
                            TipoPregunta = pregunta.TipoPregunta,
                            Escala = escala,
                        });
                    }
                    
                }
            }
            return respuestas;
        }

        private async Task CreateIdiomas(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<EEDbContext>();
            if (!context.Idiomas.Any())
            {
                await context.Idiomas.AddAsync(new Idioma("Castellano","es"));
                await context.Idiomas.AddAsync(new Idioma("Inglés","en"));
                await context.Idiomas.AddAsync(new Idioma("Portugués","pt"));
                context.SaveChanges();
            }
        }


    }
}
