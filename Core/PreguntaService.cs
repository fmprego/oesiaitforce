using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using System.Linq;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core.Exceptions;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
}
public class PreguntaService : IPreguntaService
{
    private readonly IPreguntaRepository repository;
    private readonly IOpcionRepository opcionRepository;
    private readonly IUnitOfWork unitOfWork;
    private readonly IMapper mapper;
    private readonly ILogger serviceLogger;

    public PreguntaService(
        IPreguntaRepository repository,
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IOpcionRepository opcionRepository,
        ILogger<PreguntaService> serviceLogger)
    {
        this.unitOfWork = unitOfWork;
        this.mapper = mapper;
        this.opcionRepository = opcionRepository;
        this.repository = repository;
        this.serviceLogger = serviceLogger;
    }

    public async Task<QueryResult<Pregunta>> GetAll(int[] categorias)
    {
        return await repository.GetAll(categorias);
    }

    public async Task<QueryResult<Pregunta>> GetByEncuesta(int encuestaId) =>
      await repository.GetByEncuesta(encuestaId);

    public async Task<Pregunta> GetOne(int id) =>
      await repository.GetOne(id);

    public async Task<Pregunta> Create(Pregunta pregunta, string userId) =>
      await repository.Create(pregunta, userId);

    public async Task Delete(int id)
    {
        var pregunta = await this.GetOne(id);
        if (!pregunta.Editable)
        {
            serviceLogger.LogWarning("Se ha intentado modificar una pregunta no-editable.id {1}",id.ToString());
            throw new OperacionEnEstadoInvalidoException("Se ha intentado modificar una pregunta no-editable.");
        }
        repository.Delete(pregunta);
        await unitOfWork.CompleteAsync();
    }

    public async Task<Pregunta> Update(int id, Pregunta changed, string userId)
    {
        var pregunta = await GetOne(id);
        if (!pregunta.Editable)
        {
            serviceLogger.LogWarning("Se ha intentado modificar una pregunta no-editable.id {1}", id.ToString());
            throw new OperacionEnEstadoInvalidoException("Se ha intentado modificar una pregunta no-editable.");
        }
        pregunta = await repository.Update(id, changed, userId);
        if (changed.Opciones.Count() > 0)
        {
            var currentOpciones = await opcionRepository.FindByPregunta(pregunta.Id);
            foreach (var currentOpcion in currentOpciones)
            {
                opcionRepository.Delete(currentOpcion);
            }
            await unitOfWork.CompleteAsync();
            foreach (var opcion in changed.Opciones)
            {
                if(opcion.ImagenContain.Length == 0)
                {
                    opcion.ImagenContain = null;
                }

                await opcionRepository.Create(opcion, pregunta.Id);
            }
        }
        return await GetOne(id);
    }
    public async Task<IEnumerable<Encuesta>> getEncuestasByPregunta(int id) =>
      await repository.getEncuestasByPregunta(id);
}