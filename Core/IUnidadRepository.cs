using System.Threading.Tasks;
using ee.Models;
using ee.Models.Query;

namespace ee.Core
{
    public interface IUnidadRepository
    {
         Task<Unidad> GetUnidad(int id);
         Task<QueryResult<Unidad>> GetUnidades(UnidadQuery query);
    }
}