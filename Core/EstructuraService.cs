using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CsvHelper;
using CsvHelper.Configuration;
using ee.Models;
using ee.Models.Csv;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace ee.Core
{
    public class EstructuraService : IEstructuraService
    {
        private readonly IEstructuraRepository repository;
        private readonly IPersonaRepository personaRepository;
        private readonly IMapper mapper;
        private readonly ILogger serviceLogger;
        private readonly IUnitOfWork unitOfWork;
        private readonly IHostingEnvironment _hostingEnvironment;

        public EstructuraService(
                IHostingEnvironment hostingEnvironment,
                IEstructuraRepository repository,
                IPersonaRepository personaRepository,
                IMapper mapper,
                ILogger<EstructuraService> serviceLogger,
                IUnitOfWork unitOfWork)
        {
            this.personaRepository = personaRepository;
            this._hostingEnvironment  = hostingEnvironment;
            this.mapper = mapper;
            this.serviceLogger = serviceLogger;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        public async Task<Estructura> GetOne(int id) =>
            await repository.GetOne(id);

        public async Task<Estructura> UpdateEstructura(Encuesta encuesta, IFormFile file)
        {
            var estructura = await repository.GetByEncuesta(encuesta);
            if (estructura == null)
            {
                estructura = new Estructura
                {
                    EncuestaId = encuesta.Id,
                    Encuesta = encuesta,
                    Personas = new Collection<Persona>()
                };
                estructura = await repository.Create(estructura);
            }
            var personas = await Task.Run(() => this.ProcessFile(estructura, file));
            personaRepository.DeleteByEstructura(estructura.Id);
            await unitOfWork.CompleteAsync();
            await this.saveInChunks(personas);
            return await this.GetOne(estructura.Id);
        }

        public async Task<QueryResult<Lider>> GetLideres(int id) =>
            await repository.GetLideres(id);
        public async Task<int> GetTotalLideres(int id) =>
            await repository.GetTotalLideres(id);
        public async Task<QueryResult<Receptor>> GetReceptores(int id) =>
            await repository.GetReceptores(id);
        public async Task<int> GetTotalReceptores(int id) =>
            await repository.GetTotalReceptores(id);
        public async Task<IEnumerable<string>> GetNivel1(int id) =>
            await personaRepository.GetNivel1(id);

        public async Task<IEnumerable<Nivel>> GetNivel(int id, string nivel1, string nivel2, string nivel3) =>
            await personaRepository.GetNivel(id, nivel1, nivel2, nivel3);

        private IEnumerable<PersonaRecord> GetRecordsCsv(IFormFile file)
        {
            var stream = new StreamReader(file.OpenReadStream(), System.Text.Encoding.GetEncoding("iso-8859-1"), true);
            var csv = new CsvReader(stream);
            csv.Configuration.Delimiter = ";";
            csv.Configuration.TrimOptions = TrimOptions.Trim;
            csv.Configuration.RegisterClassMap<PersonaMapping>();
            csv.Configuration.HasHeaderRecord = true;
            csv.Configuration.HeaderValidated = null;
            IEnumerable<PersonaRecord> records = csv.GetRecords<PersonaRecord>();

            return records;
        }
        private IEnumerable<PersonaRecord> GetRecordsExcel(IFormFile file)
        {
            List<PersonaRecord> records = new List<PersonaRecord>();
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;

                string folderName = "Upload";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }

                string fullPath = Path.Combine(newPath, file.FileName);

                if (File.Exists(fullPath))
                {
                    File.Delete(fullPath);
                }

                using (var stream = new FileStream(fullPath, FileMode.CreateNew))
                {
                    file.CopyTo(stream);
                    stream.Position= 0;

                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }

                    IRow headerRow = sheet.GetRow(0); //Primera linea
                    int cellCount = headerRow.LastCellNum;

                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);

                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                        PersonaRecord record = new PersonaRecord();

                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            var value = "";
                            if (row.GetCell(j) != null)
                            {
                                value = row.GetCell(j).ToString().Trim();
                            }

                            switch(j)
                            {
                                case 0:
                                    record.Numero = value;
                                    break;
                                case 1:
                                    record.Nivel1 = value;
                                    break;  
                                case 2:
                                    record.Nivel2 = value;
                                    break;                                                                
                                case 3:
                                    record.Nivel3 = value;
                                    break;  
                                case 4:
                                    record.Nivel4 = value;
                                    break;                                  
                                case 5:
                                    record.Nombre = value;
                                    break;          
                                case 6:
                                    record.EsLider = value;
                                    break;                                                                  
                                case 7:
                                    record.NumCentro = value;
                                    break;                                       
                                case 8:
                                    record.NomCentro = value;
                                    break;                                          
                                case 9:
                                    record.Email = value;
                                    break;                                                
                            }
                        }

                        records.Add(record);

                    }
                }

            }

            return records;
        }

        private Task<List<Persona>> ProcessFile(Estructura estructura, IFormFile file)
        {       
            IEnumerable<PersonaRecord> records;

            string sFileExtension = Path.GetExtension(file.FileName).ToLower();
            if (sFileExtension == ".csv")
            {
                records = GetRecordsCsv(file);
            }
            else
            {
                records = GetRecordsExcel(file);
            }

            List<Persona> personas = new List<Persona>();
            try
            {
                foreach (PersonaRecord pr in records)
                {
                    var persona = mapper.Map<PersonaRecord, Persona>(pr);
                    persona.EstructuraId = estructura.Id;
                    if (persona.EsLider)
                    {
                        var lider = mapper.Map<Persona, Lider>(persona);
                        personas.Add(lider);
                    }
                    else
                    {
                        var receptor = mapper.Map<Persona, Receptor>(persona);
                        personas.Add(receptor);
                    }
                }
            }
            catch (AutoMapperMappingException e)
            {
                serviceLogger.LogError(e.Message);
            }
            serviceLogger.LogInformation("Imporacion CSV correcta");
            return Task.FromResult(personas);
        }

        private async Task saveInChunks(IEnumerable<Persona> personas)
        {
            int i = 0;
            int parts = 1;

            if (personas.Count() >= 1000)
                parts = personas.Count() / 1000;

            var chunks = from item in personas
                         group item by i++ % parts into part
                         select part.AsEnumerable();

            foreach (IEnumerable<Persona> chunk in chunks)
            {
                personaRepository.Add(chunk);
                await unitOfWork.CompleteAsync();
            }
        }

        public async Task<IEnumerable<Lider>> GetLideresSinUsuario(int id) =>
            await repository.GetLideresSinUsuario(id);

        public async Task<Estructura> GetByEncuesta(Encuesta encuesta) =>
            await repository.GetByEncuesta(encuesta);

        public async Task<Lider> GetLider(Estructura estructura, string liderEmail)
        {
            var persona = await repository.GetLider(estructura, liderEmail);
            return mapper.Map<Persona, Lider>(persona);
        }
    }
}