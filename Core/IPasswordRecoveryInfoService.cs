using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IPasswordRecoveryInfoService
    {
        void Add(PasswordRecoveryInfo passwordRecoveryInfo);
        void Delete(int id);
        void UpdateRecoveryInfoById(int id, PasswordRecoveryInfo newInfo);
        PasswordRecoveryInfo getPasswordInfoById(int id);
        Task<string> getPwdRequest(string email);
        Task<Usuario> getAspNetUserMailByEmail(string email);
        PasswordRecoveryInfo getPwdRecoveryByIdAspNetUser(string IdAspNetUser);
        void UpdateAspNetUser(string id, string newPassword);
        Task<PasswordRecoveryInfo> getPwdRecoveryByIdAspNetUserAndCode(string IdAspNetUser, string requestCode);
  }
} 