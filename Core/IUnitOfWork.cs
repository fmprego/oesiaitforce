using System.Threading.Tasks;

namespace ee.Core
{

  public interface IUnitOfWork
  {
    Task CompleteAsync();
  }
}