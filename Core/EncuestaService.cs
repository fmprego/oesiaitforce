using ee.Core.Exceptions;
using ee.Models;
using ee.Models.Enum;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ee.Core
{
    public class EncuestaService : IEncuestaService
    {
        private readonly IEncuestaRepository repository;
        private readonly IPreguntaRepository preguntaRepository;
        private readonly ICategoriaRepository categoriaRepository;
        private readonly IEstructuraRepository estructuraRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailService emailService;
        private readonly ILogger serviceLogger;

        public EncuestaService(
            IEncuestaRepository repository,
            IPreguntaRepository preguntaRepository,
            ICategoriaRepository categoriaRepository,
            IEstructuraRepository estructuraRepository,
            IUnitOfWork unitOfWork,
            IEmailService emailService,
            ILogger<EncuestaService> serviceLogger)
        {
            this.repository = repository;
            this.preguntaRepository = preguntaRepository;
            this.categoriaRepository = categoriaRepository;
            this.estructuraRepository = estructuraRepository;
            this.unitOfWork = unitOfWork;
            this.emailService = emailService;
            this.serviceLogger = serviceLogger;

        }
        public async Task<Encuesta> Create(Encuesta encuesta, string userId) =>
            await repository.Create(encuesta, userId);

        public async Task Delete(Encuesta encuesta)
        {
            //Borra la encuesta y todas las tablas relacionadas
            repository.Delete(encuesta);
            
            //Hace editables las preguntas estaban en la encuesta
            await preguntaRepository.SetEditableByEncuesta(true,encuesta.Id);

            checkEstadoDelete(encuesta);
            await unitOfWork.CompleteAsync();
        }

        public async Task<IEnumerable<Idioma>> GetIdiomas(string locale) =>
            await repository.GetIdiomas(locale);
        public async Task<QueryResult<Encuesta>> GetAll(ClaimsPrincipal user) =>
            await repository.GetAll(user);

        public async Task<QueryResult<Encuesta>> GetCerradas(ClaimsPrincipal user) =>
            await repository.GetCerradas(user);

        public async Task<QueryResult<Encuesta>> GetPending() =>
            await repository.GetPending();

        public async Task<Encuesta> GetOne(int id, bool includeRelated = false) =>
            await repository.GetOne(id, includeRelated);

        public async Task<Encuesta> Update(int id, Encuesta changed)
        {
            var encuesta = await this.GetOne(id, true);
            checkEstado(encuesta);
            return await repository.Update(id, changed);
        }

        public async Task<Encuesta> Encolar(int id)
        {
            var encuesta = await this.GetOne(id, true);
            checkEstado(encuesta);
            await checkEncuesta(encuesta);
            encuesta.Estado = EstadoEncuesta.Pendiente;
            return await repository.Update(id, encuesta);
        }

        public async Task<Encuesta> Aprobar(int id)
        {
            var encuesta = await this.GetOne(id, true);
            checkEstado(encuesta);
            // a partir de este punto las preguntas asociadas a la encuesta dejarán de ser editables
            await preguntaRepository.SetEditableByEncuesta(editable: false, encuestaId: id);
            encuesta.Estado = EstadoEncuesta.Preactiva;
            return await repository.Update(id, encuesta);
        }

        public async Task<Encuesta> Activar(int id, bool sendMails,string url)
        {
            var encuesta = await this.GetOne(id, true);
            if (encuesta.Estado != EstadoEncuesta.Preactiva)
            {
                serviceLogger.LogError("Se ha intentado activar una encuesta que no está en estado Preactiva, id {1}", id.ToString());
                throw new OperacionEnEstadoInvalidoException("Se ha intentado activar una encuesta que no está en estado Preactiva");
            }
            encuesta.Estado = EstadoEncuesta.Activa;
            //todo lógica de activación (enviar mails, inicializar resultados (?))

            if (sendMails)
            {
                EmailMessage msg = await MailBuilder(id,url);
                emailService.Send(msg);
            }
            return await repository.Update(id, encuesta);
        }

        public async Task<Encuesta> UpdateCategorias(int id, IEnumerable<Categoria> categorias)
        {
            var encuesta = await this.GetOne(id, true);
            checkEstado(encuesta);
            encuesta = await repository.UpdateCategorias(encuesta, categorias);
            return encuesta;
        }

        public async Task<Encuesta> UpdatePreguntas(int id, IEnumerable<Pregunta> preguntas)
        {
            var encuesta = await this.GetOne(id, true);
            checkEstado(encuesta);
            encuesta = await repository.UpdatePreguntas(encuesta, preguntas);
            return encuesta;
        }

        public async Task<IEnumerable<EncuestaPregunta>> GetEncuestaPreguntas(int id)
        {
            var encuesta = await this.GetOne(id, true);
            return await repository.GetEncuestaPreguntas(encuesta);
        }

        public async Task UpdateEncuestaPreguntas(int id, IEnumerable<EncuestaPregunta> encuestaPreguntas)
        {
            await UpdateEncuestaPreguntas(id, encuestaPreguntas, false);
        }


        public async Task UpdateEncuestaPreguntas(int id, IEnumerable<EncuestaPregunta> encuestaPreguntas, bool isCopy)
        {
            var encuesta = await this.GetOne(id, true);
            if (!isCopy)
            {
                checkEstado(encuesta);
            }
            await repository.UpdateEncuestaPreguntas(encuesta, encuestaPreguntas);
        }

        public async Task<IEnumerable<EncuestaCategoria>> GetEncuestaCategorias(int id)
        {
            var encuesta = await this.GetOne(id, true);
            return await repository.GetEncuestaCategorias(encuesta);
        }

        public async Task UpdateEncuestaCategorias(int id, IEnumerable<EncuestaCategoria> encuestaCategorias)
        {
            await UpdateEncuestaCategorias(id, encuestaCategorias, false);
        }

        public async Task UpdateEncuestaCategorias(int id, IEnumerable<EncuestaCategoria> encuestaCategorias, bool isCopy)
        {
            var encuesta = await this.GetOne(id, true);
            if (!isCopy)
            {
                checkEstadoCerradaHistorica(encuesta);
            }
            await repository.UpdateEncuestaCategorias(encuesta, encuestaCategorias);
        }


        private void checkEstadoCerradaHistorica(Encuesta encuesta)
        {
            if (encuesta.Estado != EstadoEncuesta.Cerrada
            && encuesta.Estado != EstadoEncuesta.Historico)
                throw new OperacionEnEstadoInvalidoException("Se ha intentado modificar una encuesta que está en un estado diferente a cerrada o histórica.");
        }

        private void checkEstado(Encuesta encuesta)
        {
            if (encuesta.Estado != EstadoEncuesta.Creada
            && encuesta.Estado != EstadoEncuesta.Pendiente
            && encuesta.Estado != EstadoEncuesta.Historico
            && encuesta.Estado != EstadoEncuesta.Cerrada)
                throw new OperacionEnEstadoInvalidoException("Se ha intentado modificar una encuesta que está en un estado diferente a creada,pendiente o cerrada.");
        }

        private void checkEstadoDelete(Encuesta encuesta)
        {
            if (encuesta.Estado == EstadoEncuesta.Activa
            || encuesta.Estado == EstadoEncuesta.Cerrada)
                throw new OperacionEnEstadoInvalidoException("Se ha intentado eliminar una encuesta activa o cerrada.");
        }

        private async Task checkEncuesta(Encuesta encuesta)
        {
            if (encuesta.Estructura == null)
            {
                serviceLogger.LogWarning("Se ha intentado encolar una encuesta sin estructura. Id {1}", encuesta.Id.ToString());
                throw new OperacionEnEstadoInvalidoException("Se ha intentado encolar una encuesta sin estructura.");
            }

            var estructura = await this.estructuraRepository.GetOne(encuesta.Estructura.Id);
            if (estructura == null)
            {
                serviceLogger.LogWarning("Se ha intentado encolar una encuesta sin estructura. Id {1}", encuesta.Id.ToString());
                throw new OperacionEnEstadoInvalidoException("Se ha intentado encolar una encuesta sin estructura.");
            }


            var lideres = await this.estructuraRepository.GetLideres(estructura.Id);
            var receptores = await this.estructuraRepository.GetReceptores(estructura.Id);
            if (lideres.TotalItems == 0 || receptores.TotalItems == 0)
            {
                serviceLogger.LogWarning("Se ha intentado encolar una encuesta sin líderes/receptores. Id {1}", encuesta.Id.ToString());
                throw new OperacionEnEstadoInvalidoException("Se ha intentado encolar una encuesta sin líderes/receptores.");
            }

            if (encuesta.Preguntas.Count == 0 || encuesta.Categorias.Count == 0)
            {
                serviceLogger.LogWarning("Se ha intentado encolar una encuesta sin categorías/preguntas. Id {1}", encuesta.Id.ToString());
                throw new OperacionEnEstadoInvalidoException("Se ha intentado encolar una encuesta sin categorías/preguntas.");
            }
        }

        public async Task<Encuesta> Cerrar(int id)
        {
            var encuesta = await this.GetOne(id, true);
            if (encuesta.Estado != EstadoEncuesta.Activa)
                throw new OperacionEnEstadoInvalidoException("Se ha intentado cerrar una encuesta que no está activa");
            encuesta.Estado = EstadoEncuesta.Cerrada;
            // todo lógica de cierre (enviar mails, generar resultados (?))
            return await repository.Update(id, encuesta);
        }

        public async Task<QueryResult<Encuesta>> getEncuestasParaInformes(ClaimsPrincipal usuario, Usuario user) =>
         await repository.getEncuestasParaInformes(usuario, user);

        /// <summary>
        /// Mails the builder.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private async Task<EmailMessage> MailBuilder(int id,string url)
        {
            EmailMessage emailMessage = new EmailMessage();
            try
            {


                var encuesta = await this.GetOne(id, true);

                var receptores = await estructuraRepository.GetReceptores(encuesta.Estructura.Id);
                var lideres = await estructuraRepository.GetLideres(encuesta.Estructura.Id);

                foreach (var item in receptores.Items)
                {
                    EmailAddress emailTo = new EmailAddress();
                    emailTo.Name = item.Nombre;
                    emailTo.Address = item.Email;
                    emailMessage.ToAddresses.Add(emailTo);
                }  
                
                foreach (var item in lideres.Items)
                {
                    EmailAddress emailTo = new EmailAddress();
                    emailTo.Name = item.Nombre;
                    emailTo.Address = item.Email;
                    emailMessage.ToAddresses.Add(emailTo);
                }   

                emailMessage.Subject = string.Format("Encuesta {0}", encuesta.Titulo);

                string temp = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(),Path.Combine("Core",Path.Combine("MailTemplate","Plantilla.html"))));
                
                temp = temp.Replace("{Descripcion}", encuesta.Descripcion);
                temp = temp.Replace("{Boton}", "<a href =\""+url + "\" class=\"btn-primary\">Acceda a la encuesta</a>");

                emailMessage.Content = temp;
            }
            catch (Exception ex)
            {
                serviceLogger.LogError("Error en la generacion del mail. {1}",ex.Message);
            }

            return emailMessage;
        }

        public async Task<int> GetNumeroEncuestasPorTipo(int tipoEncuestaId) =>
            await repository.GetNumeroEncuestasPorTipo(tipoEncuestaId);
        }
}