using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ee.Models;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
    public class ResultadoOpcionService : IResultadoOpcionService
    {
        private readonly ILogger serviceLogger;

        public ResultadoOpcionService(ILogger<ResultadoOpcionService> serviceLogger) { this.serviceLogger = serviceLogger; }

        public IEnumerable<ResultadoOpcion> GeneraResultadoOpciones(ResultadoPregunta resultadoPregunta, IEnumerable<Respuesta> respuestas)
        {
            var result = new Collection<ResultadoOpcion>();
            try
            {
                var opciones = resultadoPregunta.Pregunta.Opciones;
                if (opciones.Count != 0)
                {
                    var dict = respuestas.GroupBy(r => r.OpcionId).ToDictionary(it => it.Key, it => it.Count());
                    foreach (var opcion in opciones)
                    {
                        var value =  0;
                        if(dict.ContainsKey(opcion.Id)){
                            value = dict.Single(o => opcion.Id == o.Key).Value;
                        }
                        
                        result.Add(new ResultadoOpcion
                        {
                            ResultadoPregunta = resultadoPregunta,
                            Opcion = opcion,
                            Recuento = value,
                            Porcentaje = ((float)value / respuestas.Count()) * 100F
                        });
                    }
                }
            }
            catch (System.Exception ex)
            {
                serviceLogger.LogError(ex.Message);
                throw;
            }
            return result;
        }
    }
}