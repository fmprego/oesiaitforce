using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ee.Models;
using Microsoft.AspNetCore.Http;

namespace ee.Core
{
    public interface IEncuestaService
    {
        Task<QueryResult<Encuesta>> GetAll(ClaimsPrincipal user);
        Task<IEnumerable<Idioma>> GetIdiomas(string locale);        
        Task<QueryResult<Encuesta>> GetCerradas(ClaimsPrincipal user);
        Task<QueryResult<Encuesta>> GetPending();
        Task<Encuesta> GetOne(int id, bool includeRelated = false);
        Task<Encuesta> Create(Encuesta encuesta, string userId);
        Task<Encuesta> Update(int id, Encuesta changed);
        Task<Encuesta> Encolar(int id);
        Task<Encuesta> Aprobar(int id);
        Task<Encuesta> Activar(int id,bool sendMails,string url);
        Task<Encuesta> Cerrar(int id);
        Task Delete(Encuesta encuesta);
        Task<Encuesta> UpdateCategorias(int id, IEnumerable<Categoria> categorias);
        Task<Encuesta> UpdatePreguntas(int id, IEnumerable<Pregunta> preguntas);
        Task<IEnumerable<EncuestaPregunta>> GetEncuestaPreguntas(int encuestaId);
        Task UpdateEncuestaPreguntas(int id, IEnumerable<EncuestaPregunta> encuestaPreguntas);
        Task UpdateEncuestaPreguntas(int id, IEnumerable<EncuestaPregunta> encuestaPreguntas, bool isCopy);
        Task<IEnumerable<EncuestaCategoria>> GetEncuestaCategorias(int encuestaId);
        Task UpdateEncuestaCategorias(int id, IEnumerable<EncuestaCategoria> encuestaCategorias);
        Task UpdateEncuestaCategorias(int id, IEnumerable<EncuestaCategoria> encuestaCategorias, bool isCopy);
        Task<QueryResult<Encuesta>> getEncuestasParaInformes(ClaimsPrincipal usuario,Usuario user);
        Task<int> GetNumeroEncuestasPorTipo(int tipoEncuestaId);
    }
}