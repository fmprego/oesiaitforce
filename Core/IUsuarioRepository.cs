using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IUsuarioRepository
    {
        Task<IEnumerable<Usuario>> GetAdmins(int? geografiaId);
        Task<Usuario> GetAdmin(string id);
        Task<Usuario> CreateAdmin(Usuario admin, string password);
        Task DeleteAdmin(Usuario admin);
        Task<Usuario> UpdateAdmin(Usuario admin, Usuario changed);
        Task<Usuario> GetLider(string id);
        Task<IEnumerable<Usuario>> CreateLideres(IEnumerable<Usuario> lideres, string pass, int geografiaId);
  }
}