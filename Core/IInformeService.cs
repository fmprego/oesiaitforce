using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IInformeService
    {
         Task<Informe> GetOne(int informeId);
         Task<Informe> GetInforme(int encuestaId, bool global,Nivel nivel,int minCuestionarioInforme);
         Task<Informe> GetInformeComparativa(int encuestaId, bool global,Nivel nivel,int minCuestionarioInforme);
         Task<IEnumerable<Informe>> GenerarInformes(ClaimsPrincipal user, int encuestaId);
         Task<Informe> GetInformeTipos(List<int> encuestasIds);
    }
}