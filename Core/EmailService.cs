using System;
using System.Linq;
using ee.Models;
using ee.Models.Configuration;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;

namespace ee.Core
{
    public class EmailService: IEmailService
    {
        private readonly IEmailConfiguration _emailConfiguration;
        private readonly ILogger serviceLogger;

        public EmailService(IEmailConfiguration emailConfiguration,
            ILogger<EmailService> serviceLogger)
        {
            _emailConfiguration = emailConfiguration;
            this.serviceLogger = serviceLogger;
        }
        public void Send(EmailMessage emailMessage)
        {
            var message = new MimeMessage();
             EmailAddress emailFrom = new EmailAddress();
                emailFrom.Name = _emailConfiguration.DefaultName;
                emailFrom.Address = _emailConfiguration.DefaultMail;
                emailMessage.FromAddresses.Add(emailFrom);
            message.Bcc.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.Subject = emailMessage.Subject;

            emailMessage.Content=emailMessage.Content.Replace("{domain}", _emailConfiguration.Domain);
            //We will say we are sending HTML. But there are options for plaintext etc. 
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };

            try
            {
                //Be careful that the SmtpClient class is the one from Mailkit not the framework!
                using (var emailClient = new SmtpClient())
                {
                    //The last parameter here is to use SSL (Which you should!)
                    //emailClient.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, false);
                    SecureSocketOptions opcionStartTls = (SecureSocketOptions)_emailConfiguration.StartTls;

                    emailClient.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, opcionStartTls);

                    //Remove any OAuth functionality as we won't be using it. 
                    //emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    if (_emailConfiguration.User != "")
                    {
                        emailClient.Authenticate(_emailConfiguration.User, _emailConfiguration.Password);
                    }

                    emailClient.Send(message);
                    this.serviceLogger.LogInformation("Se ha enviado correctamente el mail:" + message.ToString());

                    emailClient.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;                //TODO: Pendiente de establecer que logica se quiere llevar para el control del usuario
            }
        }
    }
}