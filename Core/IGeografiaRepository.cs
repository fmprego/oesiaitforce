using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;
using ee.Models.Query;

namespace ee.Core
{
    public interface IGeografiaRepository
    {
         Task<Geografia> GetGeografia(int id);
         Task<IEnumerable<Geografia>> GetGeografias(GeografiaQuery query);
         Task<Geografia> CreateGeografia(Geografia geografia);
         Task<Geografia> UpdateGeografia(int id, Geografia geografia);
         void DeleteGeografia(Geografia geografia);
    }
}