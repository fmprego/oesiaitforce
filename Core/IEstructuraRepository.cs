using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IEstructuraRepository
    {
         Task<Estructura> GetOne(int id);
         Task<Estructura> GetByEncuesta(Encuesta encuesta);
         Task<QueryResult<Lider>> GetLideres(int id);
         Task<int> GetTotalLideres(int id);
         Task<IEnumerable<Lider>> GetLideresSinUsuario(int id);
         Task<QueryResult<Receptor>> GetReceptores(int id);
         Task<int> GetTotalReceptores(int id);
         Task<Estructura> Create(Estructura estructura);
         Task<Persona> GetLider(Estructura estructura, string liderEmail);
    }
}