using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Core.Exceptions;
using ee.Models;
using System.Linq;
using ee.Models.Enum;
using System;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
    public class CuestionarioService : ICuestionarioService
    {
        private readonly ICuestionarioRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEncuestaRepository encuestaRepository;
        private readonly IPreguntaRepository preguntaRepository;
        private readonly ILogger serviceLogger;
        private readonly IEstructuraRepository estructuraRepository;

        public CuestionarioService(
            IUnitOfWork unitOfWork,
            IEncuestaRepository encuestaRepository,
            ICuestionarioRepository repository,
            IEstructuraRepository estructuraRepository,
            IPreguntaRepository preguntaRepository,
            ILogger<CuestionarioService> serviceLogger)
        {
            this.encuestaRepository = encuestaRepository;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.estructuraRepository = estructuraRepository;
            this.preguntaRepository = preguntaRepository;
            this.serviceLogger = serviceLogger;
        }

        public async Task<Cuestionario> Save(Cuestionario cuestionario)
        {
            var encuesta = await this.encuestaRepository.GetOne(cuestionario.EncuestaId);           
            if (encuesta == null)
            {
                throw new EncuestaNotFoundException();
            }
            if (encuesta.Estado != EstadoEncuesta.Activa)
            {
                serviceLogger.LogWarning("No es posible responder a una encuesta que no está en estado activo. Id:{1}", cuestionario.Id);
                throw new OperacionEnEstadoInvalidoException("No es posible responder a una encuesta que no está en estado activo.");
            }

            return await repository.Save(cuestionario);
        
        }

        public async Task<QueryResult<Cuestionario>> GetByEncuestaAndNivel(int encuestaId)
        {
            Nivel nivel = new Nivel();
            Encuesta encuesta = await encuestaRepository.GetOne(encuestaId);
            QueryResult<Cuestionario> queryResult = new QueryResult<Cuestionario>();
            var items = await repository.GetByEncuestaAndNivel2(encuestaId, nivel, false);
            queryResult.Items = items;
            queryResult.TotalItems = items.Count();

            return queryResult;
        }
        public async Task<IEnumerable<Cuestionario>> GetByEncuestaAndNivel2(int encuestaId, Nivel nivel) =>
        await repository.GetByEncuestaAndNivel2(encuestaId, nivel, false);

        public async Task<IList<List<string>>> GetCuestionariosParaCSV(int encuestaId, bool global)
        {
            Nivel nivel = new Nivel();
            
            if(global){
                IEnumerable<Encuesta> encuestasRelacionadas = await encuestaRepository.getEncuestasRelacionadas(encuestaId);
                var preguntas = await preguntaRepository.GetByEncuesta(encuestaId);
                var cuestionarios = await repository.GetByEncuestaAndNivel2(encuestaId, nivel, true);
                IList<List<string>> datosList = generaDatosCSV(preguntas.Items,cuestionarios);
                

                foreach (var item in encuestasRelacionadas)
                {
                     var preguntasRelacionadas = await preguntaRepository.GetByEncuesta(encuestaId);
                     var cuestionariosRelacionados = await repository.GetByEncuestaAndNivel2(encuestaId, nivel, true);
                     datosList.Add(new List<string>());
                     datosList.Concat(generaDatosCSV(preguntasRelacionadas.Items,cuestionariosRelacionados));
                }
            return datosList;
            
            }else {
                var preguntas = await preguntaRepository.GetByEncuesta(encuestaId);
                var cuestionarios = await repository.GetByEncuestaAndNivel2(encuestaId, nivel, true);
                return generaDatosCSV(preguntas.Items,cuestionarios);
            }
        }

        public IList<List<string>> generaDatosCSV(IEnumerable<Pregunta> preguntas, IEnumerable<Cuestionario> cuestionarios){
            IList<Cuestionario> cuestionariosList = cuestionarios.ToList();
             IList<List<string>> datosList = new List<List<string>>();
            if (preguntas.Count() != 0 && cuestionarios.Count() != 0)
            {
                List<string> cabecera = new List<string>();
                var preguntasList = preguntas.OrderBy(p => p.Id).ToList();

                cabecera.Add("FECHA REALIZACIÓN");
                cabecera.Add("NIVEL 1");
                cabecera.Add("NIVEL 2");
                cabecera.Add("NIVEL 3");
                cabecera.Add("NIVEL 4");
                for (int i = 0; i < preguntasList.Count(); i++)
                {
                    cabecera.Add('"'+preguntasList[i].Texto.ToUpper()+'"');
                }
                
               datosList.Add(cabecera);
                for (int i = 0; i < cuestionariosList.Count(); i++)
                {
                    List<string> itemAdd = new List<string>();
                    var item = cuestionariosList[i];
                    itemAdd.Add(item.Hora.ToString());
                    itemAdd.Add(@"""" + item.Nivel1+ @"""");
                    itemAdd.Add(@"""" + item.Nivel2+ @"""");
                    itemAdd.Add(@"""" + item.Nivel3+ @"""");
                    itemAdd.Add(@"""" + item.Nivel4+ @"""");
                    var respuestas = item.Respuestas.OrderBy(r => r.PreguntaId).ToList();
                    if (respuestas.Count().Equals(preguntasList.Count()))
                    {
                        for (int j = 0; j < respuestas.Count(); j++)
                        {
                            if(respuestas[j].TipoPregunta.Equals(TipoPregunta.Texto)){
                                itemAdd.Add(@"""" + respuestas[j].Texto + @"""");
                            }else if (respuestas[j].TipoPregunta.Equals(TipoPregunta.Escala)){
                                itemAdd.Add(@"""" +(respuestas[j].Escala).ToString()+ @"""");
                            }else if (respuestas[j].TipoPregunta.Equals(TipoPregunta.Seleccion)){
                                itemAdd.Add(@""""+ respuestas[j].Texto+ @"""");
                            }else if (respuestas[j].TipoPregunta.Equals(TipoPregunta.Comentario)){
                                itemAdd.Add(@"""" + respuestas[j].Texto + @"""");
                            }
                        }
                    datosList.Add(itemAdd);
                    }
                    else
                    {
                        //TODO ERROR . No coincide el número de respuestas con el de preguntas

                    }
                }
            }
            return datosList;
        }
    }

}