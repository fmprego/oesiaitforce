using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;
using ee.Models.Query;

namespace ee.Core
{
    public interface ITipoEncuestaRepository
    {
         Task<TipoEncuesta> GetTipoEncuesta(int id);
         Task<IEnumerable<TipoEncuesta>> GetTiposEncuestaAll(TipoEncuestaQuery query);
         Task<IEnumerable<TipoEncuesta>> GetTiposEncuesta(TipoEncuestaQuery query);
         Task<TipoEncuesta> CreateTipoEncuesta(TipoEncuesta tipoEncuesta);
         Task<TipoEncuesta> UpdateTipoEncuesta(int id, TipoEncuesta tipoEncuesta);
         void DeleteTipoEncuesta(TipoEncuesta tipoEncuesta);
    }
}