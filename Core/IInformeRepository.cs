using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IInformeRepository
    {
        Task<Informe> GetOne(int informeId);
        Task<Informe> GetByEncuestaAndNivel(int encuestaId, Nivel nivel);
        Task<Informe> InitInforme(int encuestaId, Nivel nivel);
        Task<Informe> Save(Informe informe);
    }
}