using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IEncuestaRepository
    {        
         Task<IEnumerable<Idioma>> GetIdiomas(string locale);
         Task<QueryResult<Encuesta>> GetAll(ClaimsPrincipal user);
         Task<QueryResult<Encuesta>> GetCerradas(ClaimsPrincipal user);
         Task<QueryResult<Encuesta>> GetPending();
         Task<Encuesta> GetOne(int id, bool includeRelated = false);
         Task<Encuesta> Create(Encuesta encuesta, string userId);
         Task<Encuesta> Update(int id, Encuesta encuesta);
         void Delete(Encuesta encuesta);
         Task<Encuesta> UpdateCategorias(Encuesta encuesta, IEnumerable<Categoria> categorias);
         Task<Encuesta> UpdatePreguntas(Encuesta encuesta, IEnumerable<Pregunta> preguntas);
         Task<IEnumerable<EncuestaPregunta>> GetEncuestaPreguntas(Encuesta encuesta);
         Task UpdateEncuestaPreguntas(Encuesta encuesta, IEnumerable<EncuestaPregunta> eps);
        Task<IEnumerable<EncuestaCategoria>> GetEncuestaCategorias(Encuesta encuesta);
        Task UpdateEncuestaCategorias(Encuesta encuesta, IEnumerable<EncuestaCategoria> eps);
        Task<QueryResult<Encuesta>> getEncuestasParaInformes(ClaimsPrincipal usuario,Usuario user);
        Task<IEnumerable<Encuesta>> getEncuestasRelacionadas(int encuestaId);
        Task<int> GetNumeroEncuestasPorTipo(int tipoEncuestaId);
    }
}