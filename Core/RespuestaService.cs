using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ee.Core.Exceptions;
using ee.Models;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
    public class RespuestaService : IRespuestaService
    {
        private readonly IInformeRepository informeRepository;
        private readonly ICuestionarioRepository cuestionarioRepository;
        private readonly IRespuestaRepository repository;
        private readonly ILogger serviceLogger;

        public RespuestaService(
            IRespuestaRepository repository,
            IInformeRepository informeRepository,
            ICuestionarioRepository cuestionarioRepository,
            ILogger<RespuestaService> serviceLogger)
        {
            this.repository = repository;
            this.cuestionarioRepository = cuestionarioRepository;
            this.informeRepository = informeRepository;
            this.serviceLogger = serviceLogger;
        }

        public async Task<IEnumerable<Respuesta>> GetRespuestasTexto(int encuestaId, int preguntaId, Nivel nivel)
        {
            var cuestionarios = await this.cuestionarioRepository.GetByEncuestaAndNivel2(encuestaId, nivel, false);
            var cuestionarioIds = cuestionarios.Select(c => c.Id);
            return await this.repository.GetByPreguntaIdAndCuestionarioIdIn(preguntaId, cuestionarioIds);
        }

        public async Task<IEnumerable<Respuesta>> GetRespuestas(int[] cuestionarioIds)
        {
            return await this.repository.GetRespuestas(cuestionarioIds);
        }
    }
}