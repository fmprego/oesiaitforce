using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IOpcionRepository
    {
        Task<IEnumerable<Opcion>> FindByPregunta(int preguntaId);
        Task<Opcion> Create(Opcion opcion, int preguntaId);
        void Delete(Opcion opcion);
        Task DeleteGrupo(GrupoOpciones grupo);
    }
}