using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface ICuestionarioRepository
    {
        Task<Cuestionario> Save(Cuestionario cuestionario);
        Task<QueryResult<Cuestionario>> GetByEncuestaAndNivel(int encuestaId, Nivel nivel);
        
        Task<IEnumerable<Cuestionario>> GetByEncuestaAndNivel2(int encuestaId, Nivel nivel, bool IncludeOpcion);
    }
}