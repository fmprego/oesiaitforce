using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;
using Microsoft.AspNetCore.Http;

namespace ee.Core
{
    public interface IEstructuraService
    {
        Task<Estructura> GetOne(int id);
        Task<Estructura> GetByEncuesta(Encuesta encuesta);
        Task<QueryResult<Lider>> GetLideres(int id);
        Task<int> GetTotalLideres(int id);
        Task<IEnumerable<Lider>> GetLideresSinUsuario(int id);
        Task<QueryResult<Receptor>> GetReceptores(int id);
        Task<int> GetTotalReceptores(int id);
        Task<Estructura> UpdateEstructura(Encuesta encuesta, IFormFile file);
        Task<IEnumerable<string>> GetNivel1(int id);
        Task<IEnumerable<Nivel>> GetNivel(int id, string nivel1, string nivel2, string nivel3);
        Task<Lider> GetLider(Estructura estructura, string liderEmail);
    }
}