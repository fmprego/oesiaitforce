using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ee.Core.Exceptions;
using ee.Models;
using ee.Models.Enum;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
    public class ResultadoPreguntaService : IResultadoPreguntaService
    {

        private readonly ILogger serviceLogger;

        private readonly IResultadoOpcionService resultadoOpcionService;
        public ResultadoPreguntaService(IResultadoOpcionService resultadoOpcionService, ILogger<ResultadoPreguntaService> serviceLogger)
        {
            this.resultadoOpcionService = resultadoOpcionService;
            this.serviceLogger = serviceLogger;
        }

        public ICollection<ResultadoPregunta> GeneraResultadoPreguntas(
          Encuesta encuesta,
          IEnumerable<Cuestionario> cuestionarios,
          Informe informe)
        {
            var resultadoPreguntas = new Collection<ResultadoPregunta>();
            foreach (EncuestaPregunta p in encuesta.Preguntas)
            {
                var resultadoPregunta = this.GeneraResultadoPregunta(p, cuestionarios, informe);
                resultadoPreguntas.Add(resultadoPregunta);
            }
            return resultadoPreguntas;
        }

        private ResultadoPregunta GeneraResultadoPregunta(
            EncuestaPregunta pregunta,
            IEnumerable<Cuestionario> cuestionarios,
            Informe informe)
        {
            if (pregunta.Pregunta.TipoPregunta == TipoPregunta.Texto)
            {
                return new ResultadoPregunta
                {
                    Informe = informe,
                    PreguntaId = pregunta.PreguntaId,
                    Pregunta = pregunta.Pregunta,
                    Valor = 0F,
                };
            }
            return this.GeneraResultadoPreguntaNumerica(pregunta, cuestionarios, informe);
        }

        private ResultadoPregunta GeneraResultadoPreguntaNumerica(
          EncuestaPregunta pregunta,
          IEnumerable<Cuestionario> cuestionarios,
          Informe informe)
        {

            var respuestas = new Collection<Respuesta>();
            foreach (Cuestionario c in cuestionarios)
            {
                var respuesta = c.Respuestas.SingleOrDefault(r => r.PreguntaId == pregunta.PreguntaId);
                if(respuesta!=null){
                     if (TipoPregunta.Seleccion.Equals(respuesta.Pregunta.TipoPregunta)
                     && respuesta.Opcion == null) // filtro respuestas tipo selección sin respuesta dada.
                    {
                        continue;
                    }
                    if (respuesta != null) respuestas.Add(respuesta);
                }
            }
            if (respuestas.Count != 0)
            {
                var resultadoPregunta = new ResultadoPregunta
                {
                    Informe = informe,
                    PreguntaId = pregunta.PreguntaId,
                    Pregunta = pregunta.Pregunta,
                    Peso = pregunta.Peso
                };
                if (pregunta.Pregunta.TipoPregunta == TipoPregunta.Escala){
                    var dict = respuestas.GroupBy(r => r.Escala).ToDictionary(it => it.Key, it => it.Count());
                    var porcentajes = respuestas.GroupBy(r => r.Escala).ToDictionary(it => it.Key, it => 0.0);
                    var total=0.0F;
                    var totalEstrellas=0.0F;

                    foreach (var opt in dict.ToArray())
                    {  
                        total = ((float)opt.Value / respuestas.Count()) * 100F;  
                        porcentajes[opt.Key] = total;
                        totalEstrellas += (total/100)*(float)opt.Key;
                    }

                    resultadoPregunta.Valor = totalEstrellas/(float)pregunta.Pregunta.Escala;

                }else if (pregunta.Pregunta.TipoPregunta == TipoPregunta.Seleccion){
                    var total=0.0;
                    var resultadoOpciones =  this.resultadoOpcionService.GeneraResultadoOpciones(resultadoPregunta, respuestas).ToArray();
                    
                    //var sumaTotalPesos = resultadoPregunta.Pregunta.Opciones.Sum(element=> element.Peso);
                    var pesoMaximo = resultadoPregunta.Pregunta.Opciones.Max(x => x.Peso);
                    foreach (var item in resultadoOpciones)
                    {
                        total += (item.Porcentaje/100)*item.Opcion.Peso;
                    }
                    resultadoPregunta.ResultadoOpciones = resultadoOpciones;
                    resultadoPregunta.Valor = (float)total/pesoMaximo;
                }else if(pregunta.Pregunta.TipoPregunta == TipoPregunta.Texto){
                    resultadoPregunta.Valor=0;
                }
              
             
                return resultadoPregunta;
            }
            else
            {
                serviceLogger.LogWarning("Respuestas insuficientes");
                throw new RespuestasInsuficientesException(respuestas.Count);
            }
        }
    }
}