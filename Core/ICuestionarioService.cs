using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface ICuestionarioService
    {
         Task<Cuestionario> Save(Cuestionario cuestionario);
         Task<QueryResult<Cuestionario>> GetByEncuestaAndNivel(int encuestaId);
         Task<IEnumerable<Cuestionario>> GetByEncuestaAndNivel2(int encuestaId, Nivel nivel);
         Task<IList<List<string>>> GetCuestionariosParaCSV(int encuestaId, bool global);
    }
}