using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IPreguntaService
    {
         Task<QueryResult<Pregunta>> GetAll(int[] categorias);
         Task<Pregunta> GetOne(int id);
         Task<QueryResult<Pregunta>> GetByEncuesta(int encuestaId);
         Task<Pregunta> Create(Pregunta pregunta, string userId);
         Task<Pregunta> Update(int id, Pregunta changed, string userId);
         Task Delete(int id);
         Task<IEnumerable<Encuesta>> getEncuestasByPregunta(int id);
    }
}