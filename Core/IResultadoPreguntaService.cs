using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IResultadoPreguntaService
    {
        ICollection<ResultadoPregunta> GeneraResultadoPreguntas(Encuesta encuesta, IEnumerable<Cuestionario> cuestionarios, Informe informe);
    }
}