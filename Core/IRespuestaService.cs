using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IRespuestaService
    {
        Task<IEnumerable<Respuesta>> GetRespuestasTexto(int informeId, int preguntaId,Nivel nivel);

        Task<IEnumerable<Respuesta>> GetRespuestas(int[] cuestionariosId);
    }
}