using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IPersonaRepository
    {
        void Add(IEnumerable<Persona> personas);
        void Delete(IEnumerable<Persona> personas);
        void DeleteByEstructura(int estructuraId);
        Task UpdateUsuarioIdByEmail(string email, string id);
        Task<IEnumerable<string>> GetNivel1(int estructuraId);
        Task<IEnumerable<Nivel>> GetNivel(int estructuraId, string nivel1, string nivel2, string nivel3);
  }
}