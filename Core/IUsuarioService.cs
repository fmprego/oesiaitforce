using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
  public interface IUsuarioService
  {
    Task<IEnumerable<Usuario>> GetAdmins(int? geografiaId);
    Task<Usuario> GetAdmin(string id);
    Task<Usuario> CreateAdmin(Usuario usuario, string password);
    Task DeleteAdmin(string id);
    Task<Usuario> UpdateAdmin(string id, Usuario changed);
    // Task<IEnumerable<Usuario>> GetLideres(int? estructuraId);
    Task<Usuario> GetLider(string id);
    Task<IEnumerable<Usuario>> CreateLideres(IEnumerable<Usuario> lideres, string pass, int geografiaId);
    // Task<Usuario> CreateLider(Usuario usuario, string password);
    // Task DeleteLider(string id);
    // Task<Usuario> UpdateLider(string id, Usuario changed);
    bool esLider(ClaimsPrincipal user);
    bool esAdmin(ClaimsPrincipal user);
    bool esMaster(ClaimsPrincipal user);
  }
}