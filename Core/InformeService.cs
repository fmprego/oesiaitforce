using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ee.Core.Exceptions;
using ee.Models;
using Microsoft.AspNetCore.Identity;
using ee.Models.Enum;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
    public class InformeService : IInformeService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IInformeRepository repository;
        private readonly IEstructuraService estructuraService;
        private readonly ICuestionarioService cuestionarioService;
        private readonly IUsuarioService usuarioService;
        private readonly UserManager<Usuario> userManager;
        private readonly IResultadoPreguntaService resultadoPreguntaService;
        private readonly IResultadoCategoriaService resultadoCategoriaService;
        private readonly IEncuestaService encuestaService;
        private readonly IRespuestaService respuestaService;
        private readonly ILogger loggerService;

        public InformeService(
                IUnitOfWork unitOfWork,
                IInformeRepository repository,
                IEstructuraService estructuraService,
                ICuestionarioService cuestionarioService,
                IUsuarioService usuarioService,
                UserManager<Usuario> userManager,
                IResultadoPreguntaService resultadoPreguntaService,
                IResultadoCategoriaService resultadoCategoriaService,
                IEncuestaService encuestaService,
                IRespuestaService respuestaService,
                ILogger<InformeService> loggerService)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.estructuraService = estructuraService;
            this.cuestionarioService = cuestionarioService;
            this.usuarioService = usuarioService;
            this.userManager = userManager;
            this.respuestaService = respuestaService;
            this.resultadoPreguntaService = resultadoPreguntaService;
            this.resultadoCategoriaService = resultadoCategoriaService;
            this.encuestaService = encuestaService;
            this.loggerService = loggerService;
        }

        public async Task<Informe> GetOne(int informeId)
        {
            return await repository.GetOne(informeId);
        }


        public async Task<Informe> GetInforme(int encuestaId, bool global, Nivel nivel,int minCuestionarioInforme)
        {
            var encuesta = await encuestaService.GetOne(encuestaId, true);
            if (encuesta == null) throw new EncuestaNotFoundException();
            IEnumerable<Cuestionario> cuestionarios = await this.cuestionarioService.GetByEncuestaAndNivel2(encuesta.Id, nivel);
            // int[] cuestionarioIds = cuestionarios.Items.Select(c => c.Id).ToArray();
            //IEnumerable<Respuesta> respuestas = await this.respuestaService.GetRespuestas(cuestionarioIds);
            //cuestionarios.Items.Select(c => c.Id = respuesta)

            var informe = new Informe
            {
                EncuestaId = encuestaId,
                Nivel1 = nivel.Nivel1,
                Nivel2 = nivel.Nivel2,
                Nivel3 = nivel.Nivel3,
                Nivel4 = nivel.Nivel4
            };
            
            informe.Respuestas = cuestionarios.Count();
            if(informe.Respuestas>= minCuestionarioInforme){
                informe.ResultadoPreguntas = this.resultadoPreguntaService.GeneraResultadoPreguntas(encuesta, cuestionarios, informe);
                informe.ResultadoCategorias = this.resultadoCategoriaService.GeneraResultadoCategorias(encuesta, informe, false);
            }else {
                
                throw new RespuestasInsuficientesException(informe.Respuestas);
            }
            return informe;
        }

        public async Task<Informe> GetInformeComparativa(int encuestaId, bool global, Nivel nivel,int minCuestionarioInforme)
        {
            var encuesta = await encuestaService.GetOne(encuestaId, true);
            if (encuesta == null) throw new EncuestaNotFoundException();
            Nivel nivelGlobal = new Nivel();
            IEnumerable<Cuestionario> cuestionariosGlobal = await this.cuestionarioService.GetByEncuestaAndNivel2(encuesta.Id, nivelGlobal); 
            IEnumerable<Cuestionario> cuestionariosNivel1 = cuestionariosGlobal.Where(c => c.Nivel1 == nivel.Nivel1).ToList();
            IEnumerable<Cuestionario> cuestionariosNivel2 = cuestionariosGlobal.Where(c => c.Nivel2 == nivel.Nivel2).ToList();
            IEnumerable<Cuestionario> cuestionariosNivel3 = cuestionariosGlobal.Where(c => c.Nivel3 == nivel.Nivel3).ToList();
            IEnumerable<Cuestionario> cuestionariosNivel4 = cuestionariosGlobal.Where(c => c.Nivel4 == nivel.Nivel4).ToList();
           
            var informe = new Informe           
            {
                EncuestaId = encuestaId,
                Nivel1 = nivel.Nivel1,
                Nivel2 = nivel.Nivel2,
                Nivel3 = nivel.Nivel3,
                Nivel4 = nivel.Nivel4
            }; 
            HashSet<string> categorias = new HashSet<string>();            
            foreach(EncuestaCategoria categoria in encuesta.Categorias){
                    categorias.Add(categoria.Categoria.Nombre);                    
                }
            this.GenerarResultados(encuesta, cuestionariosGlobal, informe,minCuestionarioInforme, categorias);
            if(nivel.Nivel1 != null){
                this.GenerarResultados(encuesta, cuestionariosNivel1, informe,minCuestionarioInforme, categorias);
            }
            if(nivel.Nivel2 != null){
                this.GenerarResultados(encuesta, cuestionariosNivel2, informe,minCuestionarioInforme, categorias);

            }
            if(nivel.Nivel3 != null){
                this.GenerarResultados(encuesta, cuestionariosNivel3, informe,minCuestionarioInforme, categorias);

            }
            if(nivel.Nivel4 != null){
                this.GenerarResultados(encuesta, cuestionariosNivel4, informe,minCuestionarioInforme, categorias);

            }
            return informe;
        }

        public async Task<IEnumerable<Informe>> GenerarInformes(ClaimsPrincipal principal, int encuestaId)
        {
            var informes = new Collection<Informe>();
            var userId = userManager.GetUserId(principal);
            var user = await usuarioService.GetAdmin(userId);
            var encuesta = await encuestaService.GetOne(encuestaId, true);
            if (encuesta == null)
            {
                loggerService.LogWarning("Encuesta no encontrada con id {1}", encuestaId.ToString());
                throw new EncuestaNotFoundException();
            }
            var estructura = await estructuraService.GetByEncuesta(encuesta);
            if (usuarioService.esAdmin(principal) || usuarioService.esMaster(principal))
            {
                // generar reportes para todos los niveles.
                var niveles = await estructuraService.GetNivel(estructura.Id, null, null, null);
                foreach (Nivel nivel in niveles)
                {
                    var informe = await repository.GetByEncuestaAndNivel(encuesta.Id, nivel);
                    if (informe == null) // se genera sólo si no existe y hay respuestas para el nivel
                    {
                        var cuestionarios = await cuestionarioService.GetByEncuestaAndNivel2(encuesta.Id, nivel);
                        if (cuestionarios.Count() != 0)
                            informe = await this.GenerarInformeParaNivel(encuesta, nivel, cuestionarios);
                    }
                    if (informe != null) informes.Add(informe);
                }
            }
            else
            {
                // generar reportes para los niveles inferiores al líder. 
                // var lider = await estructuraService.GetLider(estructura, user.Email);
                // todo
                informes.Add(new Informe());
            }
            return informes;
        }

        private async Task<Informe> GenerarInformeParaNivel(Encuesta encuesta, Nivel nivel, IEnumerable<Cuestionario> cuestionarios)
        {
            var informe = new Informe
            {
                EncuestaId = encuesta.Id,
                Nivel1 = nivel.Nivel1,
                Nivel2 = nivel.Nivel2,
                Nivel3 = nivel.Nivel3,
                Nivel4 = nivel.Nivel4
            };
            informe.Respuestas = cuestionarios.Count();
            informe.ResultadoPreguntas = this.resultadoPreguntaService.GeneraResultadoPreguntas(encuesta, cuestionarios, informe);
            informe.ResultadoCategorias = this.resultadoCategoriaService.GeneraResultadoCategorias(encuesta, informe, false);
            return await repository.Save(informe);
        }

        public async Task<Informe> GetInformeTipos(List<int> encuestasIds)
        {
            var encuestas = new List<Encuesta>();
            foreach (int id in encuestasIds){
                var encuesta = await encuestaService.GetOne(id, true);
                if (encuesta == null) throw new EncuestaNotFoundException();
                encuestas.Add(encuesta);
            }
            var cuestionarios = new List<IEnumerable<Cuestionario>>();
            Nivel nivelGlobal = new Nivel();
            HashSet<string> categorias = new HashSet<string>();

            foreach( Encuesta encuesta in encuestas){
               cuestionarios.Add(await this.cuestionarioService.GetByEncuestaAndNivel2(encuesta.Id, nivelGlobal));
               foreach(EncuestaCategoria categoria in encuesta.Categorias){
                    categorias.Add(categoria.Categoria.Nombre);                    
                }
            }
           
            var informe = new Informe           
            {
                EncuestaId = encuestasIds.ElementAt(0),
               
            };    
            
             
            for(int i = 0; i<encuestas.Count();i++){
                if(cuestionarios.ElementAt(i).Count()>= 1){
                    informe.ResultadoPreguntas = this.resultadoPreguntaService.GeneraResultadoPreguntas(encuestas.ElementAt(i), cuestionarios.ElementAt(i), informe);
                    bool borrar = true;
                    foreach (EncuestaCategoria c in encuestas.ElementAt(i).Categorias)
                    {
                        foreach(ResultadoPregunta rp in informe.ResultadoPreguntas){
                            if( rp.Pregunta.CategoriaId == c.CategoriaId && rp.Pregunta.TipoPregunta != TipoPregunta.Texto && rp.Pregunta.TipoPregunta != TipoPregunta.Comentario){
                                borrar = false;
                            }
                        }
                        if (borrar){
                            categorias.Remove(c.Categoria.Nombre);
                        }
                    }  
                }else{
                    if(encuestas.Count()==1){
                        throw new RespuestasInsuficientesException(informe.Respuestas);
                    }
                }             
            }   
            for(int i = 0; i<encuestas.Count();i++){ 
                if(cuestionarios.ElementAt(i).Count()>= 1){
                    this.GenerarResultados(encuestas.ElementAt(i), cuestionarios.ElementAt(i), informe ,1, categorias);             
                }
            }

            return informe;
        }
         public void GenerarResultados (Encuesta encuesta, IEnumerable<Cuestionario> cuestionario, Informe informe, int minCuestionarioInforme, HashSet<string> categorias){
            informe.Respuestas = cuestionario.Count();
            informe.ResultadoPreguntas = this.resultadoPreguntaService.GeneraResultadoPreguntas(encuesta, cuestionario, informe);
             if(informe.Respuestas>= minCuestionarioInforme){
                informe.ResultadoCategorias = this.resultadoCategoriaService.GeneraResultadoCategorias(encuesta, informe, true);
                //ICollection<float> valores = new Collection<float>();
                ICollection<float?> valores = new Collection<float?>();
                ResultadoNivel valoresNivel = new ResultadoNivel();
                
                valoresNivel.categorias= categorias;
                valoresNivel.nombreEncuesta = encuesta.Titulo;

                var listCategorias = categorias.ToList();
                listCategorias.Sort();
                valoresNivel.categorias= listCategorias.ToHashSet();
                informe.ResultadoCategorias = informe.ResultadoCategorias.OrderBy(o => o.Categoria.Nombre).ToList();
                var indices = new List<int>();
                foreach(ResultadoCategoria rc in informe.ResultadoCategorias){
                    indices.Add(listCategorias.IndexOf(rc.Categoria.Nombre));
                }
                for (int i = 0; i < listCategorias.Count();i++){
                    if(indices.Contains(i)){
                        valores.Add(informe.ResultadoCategorias.ElementAt(indices.IndexOf(i)).Valor);                      
                    }else{                     
                        valores.Add(0);
                    }
                }
                valoresNivel.valores = valores;
                
                informe.ResultadoNiveles.Add(valoresNivel);
            }else {                            
                throw new RespuestasInsuficientesException(informe.Respuestas);              
            }
        }
    }
}