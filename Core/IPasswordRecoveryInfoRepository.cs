using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ee.Core;
using ee.Models;
using Microsoft.EntityFrameworkCore;

namespace ee.Persistence
{
  public interface IPasswordRecoveryInfoRepository 
  {    
    void Add(PasswordRecoveryInfo passwordRecoveryInfo);
    void Delete(int id);
    void UpdateRecoveryInfoById(int id, PasswordRecoveryInfo newInfo);
    PasswordRecoveryInfo getPasswordInfoById(int id);
    PasswordRecoveryInfo getPwdRecoveryByIdAspNetUser(string IdAspNetUser);
    Task<Usuario> getAspNetUserByEmail(string email);
    void UpdateAspNetUser(string id, string newPassword);
    Task<PasswordRecoveryInfo> getPwdRecoveryByIdAspNetUserAndCode(string IdAspNetUser, string requestCode);
  }
}