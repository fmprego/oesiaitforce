using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface ICategoriaRepository
    {
        Task<Categoria> GetCategoria(int id, bool includeRelated = true);
        Task<QueryResult<Categoria>> GetCategorias();
         Task<Categoria> CreateCategoria(Categoria categoria);
         Task<Categoria> UpdateCategoria(int id, Categoria categoria);
         void DeleteCategoria(Categoria categoria);        
    }
}