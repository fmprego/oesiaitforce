using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IRespuestaRepository
    {
        Task<IEnumerable<Respuesta>> GetByPreguntaIdAndCuestionarioIdIn(int preguntaId, IEnumerable<int> cuestionarioIds);
        Task<IEnumerable<Respuesta>> GetRespuestas(IEnumerable<int> cuestionarioIds);
    }
}