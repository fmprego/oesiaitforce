using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IGrupoOpcionesRepository
    {
        Task<GrupoOpciones> GetOne(int id);
        Task<IEnumerable<GrupoOpciones>> GetAll();
        Task<GrupoOpciones> Save(GrupoOpciones grupo);
        Task<GrupoOpciones> Delete(GrupoOpciones grupo);
    }
}