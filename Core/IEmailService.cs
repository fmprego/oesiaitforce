using System;
using System.Linq;
using ee.Models;
using ee.Models.Configuration;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace ee.Core
{
    public interface IEmailService
    {
        void Send(EmailMessage emailMessage);
    }
}