using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ee.Core.Exceptions;
using ee.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ee.Core
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository usuarioRepository;
        private readonly IGeografiaRepository geografiaRepository;
        private readonly IPersonaRepository personaRepository;
        private readonly IConfiguration configuration;
        private readonly ILogger serviceLogger;


        public UsuarioService(
      IUsuarioRepository usuarioRepository,
      IGeografiaRepository geografiaRepository,
      IPersonaRepository personaRepository,
      IConfiguration configuration,
      ILogger<UsuarioService> serviceLogger)
        {
            this.usuarioRepository = usuarioRepository;
            this.geografiaRepository = geografiaRepository;
            this.personaRepository = personaRepository;
            this.configuration = configuration;
            this.serviceLogger = serviceLogger;
        }

        public async Task<Usuario> CreateAdmin(Usuario usuario, string password)
        {
            var geografia = await geografiaRepository.GetGeografia(usuario.GeografiaId);
            if (geografia == null)
            {
                serviceLogger.LogWarning("Geografía no encontrada para el usuario: {1} {2}", usuario.Apellidos, usuario.Nombre);
                throw new GeografiaNotFoundException();
            }
            else
            {
                return await usuarioRepository.CreateAdmin(usuario, password);
            }
        }

        public Task<Usuario> GetAdmin(string id)
        {
            return usuarioRepository.GetAdmin(id);
        }

        public Task<IEnumerable<Usuario>> GetAdmins(int? geografiaId)
        {
            return usuarioRepository.GetAdmins(geografiaId);
        }

        public async Task DeleteAdmin(string id)
        {
            var admin = await usuarioRepository.GetAdmin(id);
            await usuarioRepository.DeleteAdmin(admin);
        }

        public async Task<Usuario> UpdateAdmin(string id, Usuario changed)
        {
            var geografia = await geografiaRepository.GetGeografia(changed.GeografiaId);
            if (geografia == null)
            {
                serviceLogger.LogWarning("Geografia no encontrada");
                throw new GeografiaNotFoundException();
            }
            else
            {
                var admin = await usuarioRepository.GetAdmin(id);
                return await usuarioRepository.UpdateAdmin(admin, changed);
            }
        }


        public Task<Usuario> GetLider(string id)
        {
            return usuarioRepository.GetLider(id);
        }

        public async Task<IEnumerable<Usuario>> CreateLideres(IEnumerable<Usuario> lideres, string pass, int geografiaId)
        {
            var lideresCreados = await usuarioRepository.CreateLideres(lideres, pass, geografiaId);
            //var lideresCreados = await usuarioRepository.CreateLideres(lideres, CrearPassword(), geografiaId);

            // Se actualiza la tabla Persona con los usuarioId creados.
            foreach (Usuario lider in lideresCreados)
            {
                await personaRepository.UpdateUsuarioIdByEmail(lider.Email, lider.Id);
            }
            return lideresCreados;
        }

        public bool esLider(ClaimsPrincipal user)
        {
            return user.HasClaim("Role", "Lider");
        }

        public bool esAdmin(ClaimsPrincipal user)
        {
            return user.HasClaim("Role", "Admin");
        }

        public bool esMaster(ClaimsPrincipal user)
        {
            return user.HasClaim("Role", "Master");
        }

        // public async Task<Usuario> CreateLider(Usuario usuario, string password)
        // {
        //   var geografia = await geografiaRepository.GetGeografia(usuario.GeografiaId);
        //   if (geografia == null)
        //     throw new GeografiaNotFoundException();
        //   else
        //     return await usuarioRepository.CreateLider(usuario, password);
        // }

        // public async Task DeleteLider(string id)
        // {
        //   var lider = await usuarioRepository.GetLider(id);
        //   await usuarioRepository.DeleteLider(lider);
        // }

        // public async Task<Usuario> UpdateLider(string id, Usuario changed)
        // {
        //   var geografia = await geografiaRepository.GetGeografia(changed.GeografiaId);
        //   if (geografia == null)
        //     throw new GeografiaNotFoundException();
        //   else
        //   {
        //     var lider = await usuarioRepository.GetLider(id);
        //     return await usuarioRepository.UpdateLider(lider, changed);
        //   }
        // }

//TODO implementar método de manera pública
        public string CrearPassword()
        {
            int longitud = 8;
            string caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.-";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < longitud--)
            {
            res.Append(caracteres[rnd.Next(caracteres.Length)]);
            }
            return res.ToString();
        }
    }

    
}