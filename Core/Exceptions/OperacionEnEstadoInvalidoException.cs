using System;

namespace ee.Core.Exceptions
{
    public class OperacionEnEstadoInvalidoException : Exception
    {
        public OperacionEnEstadoInvalidoException(string message) : base(message) {}
    }
}