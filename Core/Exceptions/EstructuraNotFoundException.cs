namespace ee.Core.Exceptions
{
    public class EstructuraNotFoundException : System.Exception
    {
        public EstructuraNotFoundException() : base("Estructura no encontrada")
        {

        }
    }
}