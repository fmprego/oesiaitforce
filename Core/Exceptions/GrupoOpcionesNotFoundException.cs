using System;

namespace ee.Core.Exceptions
{
    public class GrupoOpcionesNotFoundException : Exception
    {
        public GrupoOpcionesNotFoundException() : base("No se ha encontrado el grupo de opciones")
        {

        }
    }
}