using System;

namespace ee.Core.Exceptions
{
    public class EncuestaNotFoundException: Exception
    {
        public EncuestaNotFoundException() : base("No ha sido posible encontrar la encuesta solicitada.")
        {

        }
    }
}