using System;

namespace ee.Core.Exceptions
{
  public class GeografiaNotFoundException : Exception
  {
    public GeografiaNotFoundException() : base("No se ha encontrado la geografía") {}
    
  }
}