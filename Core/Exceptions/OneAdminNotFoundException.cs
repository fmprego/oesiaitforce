using System;

namespace ee.Core.Exceptions
{
    public class OneAdminNotFoundException: Exception
    {
        public OneAdminNotFoundException() : base("No existe otro Admin.")
        {

        }
    }
}