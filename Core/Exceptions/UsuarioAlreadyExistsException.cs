using System;

namespace ee.dbo.Core.Exceptions
{
    public class UsuarioAlreadyExistsException : Exception
    {
        public UsuarioAlreadyExistsException() : base("El usuario ya existe")
        {

        }
    }
}