using System;

namespace ee.Core.Exceptions
{
    public class CategoriaNotFoundException : Exception
    {
        public CategoriaNotFoundException() : base("No se ha encontrado la categoría solicitada.")
        {

        }
    }
}