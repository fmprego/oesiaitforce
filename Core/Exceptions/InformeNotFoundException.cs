namespace ee.Core.Exceptions
{
    public class InformeNotFoundException : System.Exception
    {
        public InformeNotFoundException() : base("Informe no encontrado")
        {

        }
    }
}