using System;

namespace ee.Core.Exceptions
{
  public class IdiomaNotFoundException : Exception
  {
    public IdiomaNotFoundException() : base("No se ha encontrado el idioma") {}
    
  }
}