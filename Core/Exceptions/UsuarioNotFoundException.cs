using System;

namespace ee.Core.Exceptions
{
    public class UsuarioNotFoundException : Exception
    {
        public UsuarioNotFoundException() : base("Usuario no encontrado")
        {

        }
    }
}