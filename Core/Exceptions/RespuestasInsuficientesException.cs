using System;

namespace ee.Core.Exceptions
{
    public class RespuestasInsuficientesException : Exception
    {
        public RespuestasInsuficientesException(int respuestas) : base("La encuesta no tiene las respuestas suficientes, no se puede generar resultado.Respuestas :" + respuestas)
        {

        }
    }
}