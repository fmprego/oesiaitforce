using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ee.Models;
using ee.Models.Enum;
using Microsoft.Extensions.Logging;
using System;

namespace ee.Core
{
    public class ResultadoCategoriaService : IResultadoCategoriaService
    {
        private readonly ILogger serviceLogger;
        public ResultadoCategoriaService(ILogger<ResultadoCategoriaService> serviceLogger)
        {
            this.serviceLogger = serviceLogger;
        }

        public ICollection<ResultadoCategoria> GeneraResultadoCategorias(Encuesta encuesta, Informe informe, bool nivel)
        {
            var resultadoCategorias= new Collection<ResultadoCategoria>(); 
            try
            {
                foreach (EncuestaCategoria c in encuesta.Categorias)
                {
                    var rpsCategoria = informe.ResultadoPreguntas
                      .Where(rp => rp.Pregunta.CategoriaId == c.CategoriaId)
                      .Where(rp => rp.Pregunta.TipoPregunta != TipoPregunta.Texto)
                      .ToList();
                    if (rpsCategoria.Count != 0)
                    {
                        var resultadoCategoria = this.GeneraResultadoCategoria(c, rpsCategoria, encuesta, informe, nivel);
                        //if(resultadoCategoria.Valor != 0){
                        if (resultadoCategoria.Valor != null)
                        {
                            resultadoCategorias.Add(resultadoCategoria);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                serviceLogger.LogError(ex.Message);
            }

            return resultadoCategorias;
        }


        private ResultadoCategoria GeneraResultadoCategoria(
          EncuestaCategoria categoria,
          IEnumerable<ResultadoPregunta> rpsCategoria,
          Encuesta encuesta,
          Informe informe,
          bool nivel)
        {
            var factor = this.calculaFactorNormal(encuesta, categoria);
            float? valor;

            float resultadoCategoria = 0F;
            foreach (ResultadoPregunta rp in rpsCategoria)
            {
                var encuestaPregunta = encuesta.Preguntas.SingleOrDefault(ep => ep.PreguntaId == rp.PreguntaId);
                var pesoPonderado = encuestaPregunta.Peso * factor;
                if (rp.Valor != null)
                {
                    resultadoCategoria = resultadoCategoria + pesoPonderado * (float)rp.Valor;
                }                
            }
            if (nivel){
                valor = (float)Math.Round(((float)resultadoCategoria * 100), 2);
            }else{
                valor = resultadoCategoria;
            }
            return new ResultadoCategoria
            {
                InformeId = informe.Id,
                CategoriaId = categoria.CategoriaId,
                Valor = valor,
                Categoria = categoria.Categoria,
            };
        }

        // Calcula el factor por que el que multiplicar los pesos de cada una de las preguntas
        // de la categoría. Por ejemplo si los pesos de las preguntas son 0.7, 0.8, 0.3 habría
        // que resolver 0.7x + 0.8x + 0.3x = 1, así obtenemos un factor x que pondera cada respuesta.
        private float calculaFactorNormal(Encuesta encuesta, EncuestaCategoria categoria)
        {
            var preguntas = encuesta.Preguntas.Select(ep => ep.Pregunta);
            var preguntasIds = preguntas
              .Where(p => p.CategoriaId == categoria.CategoriaId)
              .Where(p => p.TipoPregunta != TipoPregunta.Texto)
              .Select(p => p.Id);

            var sum = encuesta.Preguntas.Where(ep => preguntasIds.Contains(ep.PreguntaId)).Sum(ep => ep.Peso);
            return 1 / sum;
        }
    }
}