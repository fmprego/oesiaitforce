using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;
using ee.Persistence;
using AutoMapper;
using ee.Controllers.Resources;
using ee.Core.Exceptions;
using Microsoft.Extensions.Logging;
using ee.Utils;
using System;
using System.Security.Cryptography;

namespace ee.Core
{
    public class PasswordRecoveryInfoService : IPasswordRecoveryInfoService
    {
        
        private readonly static int EXPIRATION_TIME = 24;
        private readonly IPasswordRecoveryInfoRepository repository;
        private readonly IMapper mapper;
        private readonly ILogger serviceLogger;
        private readonly IEmailService emailService;
        
        public PasswordRecoveryInfoService(
            IPasswordRecoveryInfoRepository repository,
            IMapper mapper,
            ILogger<PasswordRecoveryInfoService> serviceLogger,
            IEmailService emailService
            )
            {
                this.mapper = mapper;
                this.repository = repository;
                this.serviceLogger = serviceLogger;
                this.emailService = emailService;
            }
        
        public void Add(PasswordRecoveryInfo passwordRecoveryInfo){
            repository.Add(passwordRecoveryInfo);
            serviceLogger.LogDebug("Adding Password Recovery Info: asp_user {1} id{2}", passwordRecoveryInfo.idAspNetUser, passwordRecoveryInfo.Id);
        }
        public void Delete(int id){
            repository.Delete(id);
            serviceLogger.LogDebug("Deleting Password Recovery Info: id{2}", id);
        }
        public void UpdateRecoveryInfoById(int id, PasswordRecoveryInfo newInfo){
            repository.UpdateRecoveryInfoById(id, newInfo);
            serviceLogger.LogDebug("Updating Password Recovery Info: asp_user {1} id{2}", newInfo.idAspNetUser, newInfo.Id);
        }
        public PasswordRecoveryInfo getPasswordInfoById(int id){
            
            PasswordRecoveryInfo passwordRecoveryInfo =  repository.getPasswordInfoById(id);
            serviceLogger.LogDebug("Getting Password Recovery Info: asp_user {1} id{2}", passwordRecoveryInfo.idAspNetUser, passwordRecoveryInfo.Id);
            return passwordRecoveryInfo;
        }
        public async Task<string> getPwdRequest(string email){

            int codeLenght = 20;
            bool isUpcase = false;
            string ramdonStr = RamdomStrGenerator.RandomString(codeLenght, isUpcase);
            string id;
            
            Usuario user = await repository.getAspNetUserByEmail(email);
            id = user.Id;
            if (user.Id.Equals(0))
                return "";
            serviceLogger.LogDebug("Password Recovery for user {1} [ id = {2} ] is Ok", user.Email, user.Id);
            
            PasswordRecoveryInfo pwdRecInfoUser = repository.getPwdRecoveryByIdAspNetUser(id);
            
            PasswordRecoveryInfo pri = new PasswordRecoveryInfo();

            if (pwdRecInfoUser == null)
            {
                pri.codeUsed = false;
                pri.idAspNetUser = user.Id;
                pri.requestCode =  ramdonStr;
                pri.requestDate = DateTime.Now;
                pri.requestExpirationTime = EXPIRATION_TIME;
                repository.Add(pri);                
            }
            else
            {
                var oldDate =  pwdRecInfoUser.requestDate;
                DateTime newDate = oldDate.AddHours(pwdRecInfoUser.requestExpirationTime);
                if ((pwdRecInfoUser.codeUsed) || (newDate.Date<DateTime.Now)){
                    pri.codeUsed = false;
                    pri.idAspNetUser = user.Id;
                    pri.requestCode =  ramdonStr;
                    pri.requestDate = DateTime.Now;
                    pri.requestExpirationTime = EXPIRATION_TIME;
                    repository.Add(pri);
                } else {
                    ramdonStr = pwdRecInfoUser.requestCode;
                }
            } 
            return ramdonStr;
        }

        public async Task<PasswordRecoveryInfo> getPwdRecoveryByIdAspNetUserAndCode(string IdAspNetUser, string requestCode){
            return await repository.getPwdRecoveryByIdAspNetUserAndCode(IdAspNetUser, requestCode);
        }

        public async Task<Usuario> getAspNetUserMailByEmail(string email){
            return await repository.getAspNetUserByEmail(email);
        }
        public PasswordRecoveryInfo getPwdRecoveryByIdAspNetUser(string IdAspNetUser){
            return repository.getPwdRecoveryByIdAspNetUser(IdAspNetUser);
        }
        public void UpdateAspNetUser(string id, string newPassword){
            repository.UpdateAspNetUser(id, newPassword);
        }
  }
}