using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IResultadoCategoriaService
    {
        ICollection<ResultadoCategoria> GeneraResultadoCategorias(Encuesta encuesta, Informe informe, bool nivel);
    }
}