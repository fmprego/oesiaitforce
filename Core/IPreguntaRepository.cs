using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;

namespace ee.Core
{
    public interface IPreguntaRepository
    {
        Task<QueryResult<Pregunta>> GetAll(int[] categorias);
        Task<QueryResult<Pregunta>> GetByEncuesta(int encuestaId);
        Task<Pregunta> GetOne(int id);
        Task<IEnumerable<Pregunta>> FindByCategoria(int categoriaId);
        Task<Pregunta> Create(Pregunta pregunta, string userId);
        Task<Pregunta> Update(int id, Pregunta pregunta, string userId);
        Task SetEditableByEncuesta(bool editable, int encuestaId);
        void Delete(Pregunta pregunta);        
        Task<IEnumerable<Encuesta>> getEncuestasByPregunta(int id);
    }
}