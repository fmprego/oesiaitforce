using System.Collections.Generic;
using ee.Models;

namespace ee.Core
{
    public interface IResultadoOpcionService
    {
        IEnumerable<ResultadoOpcion> GeneraResultadoOpciones(ResultadoPregunta resultadoPregunta, IEnumerable<Respuesta> respuestas);
    }
}