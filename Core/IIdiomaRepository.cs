using System.Collections.Generic;
using System.Threading.Tasks;
using ee.Models;
using ee.Models.Query;

namespace ee.Core
{
    public interface IIdiomaRepository
    {
         Task<IEnumerable<Idioma>> GetIdiomas(string localeIdioma);
    }
}